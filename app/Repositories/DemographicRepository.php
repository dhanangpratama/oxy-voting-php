<?php

namespace App\Repositories;

use App\Models\Demographic;
use App\Repositories\DemographicQueueRepository;
use Auth, DB;

class DemographicRepository
{
    private $demographicQueue;

    public function __construct() 
    {
        $this->demographicQueue = new DemographicQueueRepository;
    }

    public function paginate($data)
    {
        $members = new Demographic;
        $limit = 20;

        if ( !empty(array_get($data, 'name')) )
        {
            $members = $members->where('name', 'like', '%'.array_get($data, 'name').'%');
        }

        if ( !empty(array_get($data, 'dob')) )
        {
            $members = $members->where('dob', db_format_date(array_get($data, 'dob')));
        }

        if ( !empty(array_get($data, 'pob')) )
        {
            $members = $members->where('pob', 'like', '%'.array_get($data, 'pob').'%');
        }

        if ( !empty(array_get($data, 'nik')) )
        {
            $members = $members->where('nik', array_get($data, 'nik'));
        }

        if ( !empty(array_get($data, 'limit')) )
        {
            $limit = array_get($data, 'limit');
        }

        return $members->with(['religion_data', 'user_data'])->orderBy('name', 'asc')->paginate($limit);
    }
    
    public function create($data)
    {
        $data = array_add($data, 'user_id', Auth::id()); // assign user login ID

        if ( array_has($data, 'dob') && !empty(array_get($data, 'dob')) )
        {
            $data['dob'] = db_format_date(array_get($data, 'dob'));
        }

        DB::beginTransaction();

        try
        {
            $insert = Demographic::create($data);
            $queueUpdate = $this->demographicQueue->create($insert->toArray()); // insert to demographic queue

            DB::commit();

            return ['status' => true, 'data' => $queueUpdate->toArray()];
        }
        catch ( \Exception $e )
        {
            DB::rollback();
            record_error('Error in create new demographic', $e);
            return abort(500, '[btn_back]Gagal menyimpan data ke database.');
        }
    }

    public function findById($id)
    {
        return Demographic::with(['religion_data', 'user_data', 'event_data'])->findOrFail($id)->toArray();
    }

    public function findByNik($nik)
    {
        return Demographic::where('nik', $nik)->with(['religion_data', 'user_data'])->first();
    }

    public function updateById($id, $data)
    {
        $data = array_add($data, 'user_id', Auth::id()); // assign user login ID
        
        if ( array_has($data, 'dob') && !empty(array_get($data, 'dob')) )
        {
            $data['dob'] = db_format_date(array_get($data, 'dob'));
        }

        DB::beginTransaction();

        try
        {
            $update = Demographic::where('demographic_id', $id)->update($data);
            $queueUpdate = $this->demographicQueue->create(Demographic::findOrFail($id)->toArray());

            DB::commit();

            return  ['status' => true, 'data' => $queueUpdate->toArray()];
        }
        catch ( \Exception $e )
        {
            DB::rollback();
            record_error('Error in update demographic data', $e);
            return abort(500, '[btn_back]Gagal mengubah data ke database.');
        }
    }
}