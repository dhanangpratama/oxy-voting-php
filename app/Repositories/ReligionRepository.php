<?php

namespace App\Repositories;

use App\Models\Religion;

class ReligionRepository
{
    public function __construct() {}
    
    public function all()
    {
        $religions = [];
        
        foreach(Religion::all() as $religion)
        {
            $religions[$religion->religion_id] = $religion->label;
        }

        return $religions;
    }
}