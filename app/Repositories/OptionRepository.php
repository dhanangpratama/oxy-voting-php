<?php

namespace App\Repositories;

use App\Models\Option;

class OptionRepository
{
    public function __construct() {}
    
    public function all()
    {
        $options = [];
        
        foreach(Option::all() as $option)
        {
            $options[$option->option_name] = [
                'value' => $option->option_value,
                'type' => $option->option_type
            ];
        }

        return $options;
    }

    public function add($name, $value) 
    {  
        $search = Option::where('option_name', $name)->first();

        if ( ! is_null($search) )
        {
            return false;
        }

        $data_type='text';

        if ( is_array($value) )
        {
            $data_type = 'array';
            $value = serialize($value);
        }

        if( is_object($value))
        {
            $data_type = 'object';
            $value = serialize($value);
        }  

        Option::create([
            'option_name' => $name,
            'option_value' => $value,
            'option_type' => $data_type,
        ]);   

        return true;
    }
    
    public function update($name,$value)
    {
        $data_type='text';

        if ( is_array($value) )
        {
            $data_type = 'array';
            $value = serialize($value);
        }

        if ( is_object($value))
        {
            $data_type = 'object';
            $value = serialize($value);
        }  

        $search = Option::where('option_name', $name)->first();

        if ( is_null($search) )
        {
            $do = Option::create([
                'option_name' => $name,
                'option_value' => $value,
                'option_type' => $data_type,
            ]);  
        }
        else
        {
            $do = Option::where('option_name', $name)->update([
                'option_value' => $value,
                'option_type' => $data_type,
            ]);  
        }

        return true;
    }
    
    public function get($name)
    {
        $options = self::all();

        if ( ! array_key_exists($name, $options) )
        {
            return null;
        }

        if ( $options[$name]['type'] == 'array' || $options[$name]['type'] == 'object' )
        {
            return unserialize($options[$name]['value']);
        }

        return $options[$name]['value'];
    }
    
    public static function delete($name)
    {
        Option::where('option_name', $name)->delete();
    }
}