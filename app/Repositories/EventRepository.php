<?php

namespace App\Repositories;

use App\Models\Event;
use Cache;

class EventRepository
{
    public function __construct() {}
    
    public function all()
    {
        return Cache::remember('events_all', 30, function()
        {
            $events = [];
            
            foreach(Event::all() as $event)
            {
                $events[$event->event_id] = $event->name;
            }
    
            return $events;
        });
    }

    public function create($data)
    {
        try
        {
            return Event::create($data);
        }
        catch ( \Exception $e )
        {
            record('Error in save event data', $e);
            return abort(500, '[btn_back]Gagal menyimpan data.');
        }
    }
}