<?php

namespace App\Repositories;

use App\Models\DemographicQueue;

class DemographicQueueRepository
{
    public function __construct() {}
    
    public function create($data)
    {
        try
        {
            return DemographicQueue::create($data);
        }
        catch ( \Exception $e )
        {
            record_error('Error in create demographic queue', $e);
            return abort(500, '[btn_back]Gagal menyimpan data ke database.');
        }
    }
}