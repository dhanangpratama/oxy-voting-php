<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DemographicSaved
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $currentPath;

    public $demographicId;

    public $subjectId;

    public $demographicQueueId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($currentPath, $demographicId, $subjectId, $demographicQueueId)
    {
        $this->currentPath = $currentPath;
        $this->demographicId = $demographicId;
        $this->subjectId = $subjectId;
        $this->demographicQueueId = $demographicQueueId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
