<?php

namespace App\Listeners;

use App\Events\DemographicSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use File, Auth;

class DemographicActivityLog
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {}

    public function handle(DemographicSaved $event)
    {
        $activity = app()->make('ActivityService');

        if ( session('process.type') == 'card' )
        {
            if ( session('process.mode') == 'new' ) $activity->log('demographic-card-new', 'Memproses data demographic ID ' . $event->subjectId, Auth::id());
            if ( session('process.mode') == 'edit' ) $activity->log('demographic-card-edit', 'Menyimpan perubahan data demographic dengan subject ID ' . $event->subjectId, Auth::id());
        }

        if ( session('process.type') == 'live' )
        {
            if ( session('process.mode') == 'new' ) $activity->log('demographic-live-new', 'Memproses data demographic ID ' . $event->subjectId, Auth::id());
            if ( session('process.mode') == 'edit' ) $activity->log('demographic-live-edit', 'Mengubah data demographic ID ' . $event->subjectId, Auth::id());
        }
    }
}
