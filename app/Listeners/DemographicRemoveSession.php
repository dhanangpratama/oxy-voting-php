<?php

namespace App\Listeners;

use App\Events\DemographicSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DemographicRemoveSession
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DemographicSaved  $event
     * @return void
     */
    public function handle(DemographicSaved $event)
    {
        removeDemographicSession();
    }
}
