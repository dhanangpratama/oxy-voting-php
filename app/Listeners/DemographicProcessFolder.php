<?php

namespace App\Listeners;

use App\Events\DemographicSaved;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use File;

class DemographicProcessFolder
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct() {}

    public function handle(DemographicSaved $event)
    {
        $demographicCompletePath = config('path.data.complete') . DIRECTORY_SEPARATOR . $event->demographicId;

        if ( File::isDirectory($demographicCompletePath) )
        {
            File::deleteDirectory($demographicCompletePath, true);
        }
        else
        {
            File::makeDirectory($demographicCompletePath, 0777);
        }

        $this->copyFiles($event->currentPath, $event->demographicId)->removeFiles($demographicCompletePath)->copyToQueue($demographicCompletePath, $event->demographicQueueId);

        if ( File::isDirectory($event->currentPath) )
        {
            File::deleteDirectory($event->currentPath, true);
        }
    }

    protected function removeFiles($path)
    {
        if ( File::isDirectory($path . DIRECTORY_SEPARATOR . 'fields') )
        {
            File::deleteDirectory($path . DIRECTORY_SEPARATOR . 'fields');
        }

        if ( File::isFile($path . DIRECTORY_SEPARATOR . 'active.log') )
        {
            File::delete($path . DIRECTORY_SEPARATOR . 'active.log');
        }

        if ( File::isFile($path . DIRECTORY_SEPARATOR . '_flag.txt') )
        {
            File::delete($path . DIRECTORY_SEPARATOR . '_flag.txt');
        }

        foreach ( config('ak23.file_data') as $key => $filename )
        {
            if ( File::isFile($path . DIRECTORY_SEPARATOR . $filename) )
            {
                File::delete($path . DIRECTORY_SEPARATOR . $filename);
            }
        }

        return $this;
    }

    public function copyFiles($path, $id)
    {
        try
        {
            File::copyDirectory($path, config('path.data.complete') . DIRECTORY_SEPARATOR . $id);

            return $this;
        }
        catch ( \Exception $e )
        {
            record_error('Error in move image files data demographic', $e);
            return abort(500, '[btn_back]Gagal memproses data gambar. Silahkan klik tombol kembali dan coba lagi. Jika masih terjadi error ini, mohon untuk segera melapor.');
        }
    }

    public function copyToQueue($pathComplete, $queueId)
    {
        $queuePath = config('path.data.complete_queue') . DIRECTORY_SEPARATOR . $queueId;

        try
        {
            if ( File::isDirectory($queuePath) )
            {
                File::deleteDirectory($queuePath, true);
            }
            else
            {
                File::makeDirectory($queuePath, 0777);
            }

            File::copyDirectory($pathComplete, $queuePath);

            return $this;
        }
        catch ( \Exception $e )
        {
            record_error('Error in move image files data demographic complete to queue', $e);
            return abort(500, '[btn_back]Gagal memproses data gambar. Silahkan klik tombol kembali dan coba lagi. Jika masih terjadi error ini, mohon untuk segera melapor.');
        }
    }
}
