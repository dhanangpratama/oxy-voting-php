<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Cache;
use App\Utils\ApiUtils;

class CheckServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ak23:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check server status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try
        {
            $latestRequest = strtotime(file_get_contents(config('path.latestRequestFile')));
            $serverStatusLatest = file_get_contents(config('path.serverStatusFile'));
            $now = strtotime(Carbon::now());
            $minutes = round(abs($now - $latestRequest) / 60);
            $status = array_get(ApiUtils::pingServer(), 'status');

            if ( $minutes < 30 )
            {
                $file = fopen(config('path.serverStatusFile'),"w");

                if ( $status == 'on' )
                {
                    fwrite($file, 'on');
                    echo 'Server status: Online';
                }
                else if ( $status == 'part' )
                {
                    fwrite($file, 'part');
                    echo 'Server status: OS Inafis Offline';
                }
                else
                {
                    fwrite($file, 'off');
                    echo 'Server status: Offline';
                }

                fclose($file);
            }
            else
            {
                echo 'Skip check server because user is idle. Idle time: '.$minutes;
            }
        }
        catch (\Exception $e)
        {
            echo 'There is an error. error message: '.$e->getMessage();
        }
    }
}
