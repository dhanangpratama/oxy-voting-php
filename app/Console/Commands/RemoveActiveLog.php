<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Log;

use File;

class RemoveActiveLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ak23:remove_active_log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to run remove active log files.';

    protected $fileNaming;
    protected $path;
    protected $maximum_folder_to_process = 10;
    protected $tmp_card_path;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        // Storing path in array
        $this->path = [
            'card_source' => config('path.data.card_source'),
            'card' => config('path.data.card'),
            'live' => config('path.data.live'),
            'complete' => config('path.data.complete')
        ];

        // Storing file names in array
        $this->fileNaming = [
            'front' => config('filename.frontName'),
            'back' => config('filename.backName'),
            'front_small' => config('filename.frontSmallName'),
            'back_small' => config('filename.backSmallName'),
            'face_front' => config('filename.face_front'),
            'face_left' => config('filename.face_left'),
            'face_right' => config('filename.face_right'),
            'flat_left_four_fingers' => config('filename.flat_left_four_fingers'),
            'flat_right_four_fingers' => config('filename.flat_right_four_fingers'),
            'flat_thumbs' => config('filename.flat_thumbs')
        ];
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function handle()
    {
        $this->removeLock($this->path['card']);
        $this->removeLock(storage_path('tmp/card/'), 5);

    }


    public function removeLock($target_folder, $limit_remove_lock = 15)
    {
        $target_folder = rtrim(rtrim($target_folder, "/"), "\\");
        $remove_value = ['.', '..', '.gitkeep'];

        $folders = collect(scandir($target_folder));

        $folders = $folders->each(function ($item, $key) use ($folders, $remove_value, $limit_remove_lock, $target_folder) {

             $limit_time = $limit_remove_lock; // in minutes (15 menit)

             $active_log = $target_folder . DIRECTORY_SEPARATOR . $item . DIRECTORY_SEPARATOR . 'active.log';

             $now = new \DateTime(date('Y-m-d H:i:s'));

            if ( is_dir($target_folder . DIRECTORY_SEPARATOR . $item) && ! in_array($item, $remove_value) )
            {
                if ( file_exists($active_log) )
                {
                    $log_time = new \DateTime(date('Y-m-d H:i:s', filemtime($active_log)));
                    $interval = $now->diff($log_time);

                    // $folders[$key] = "{$active_log} ==> " . $interval->format('%i Minutes');

                    // $interval->format('%i') return minutes
                    if ( $interval->format('%i') > $limit_time )
                    {
                        try
                        {
                            File::delete($active_log);
                        }
                        catch (\Exception $e)
                        {
                            Log::error('Error deleting active.log file.', ['path' => $active_log]);
                        }
                    }
                }
                else
                {
                    unset($folders[$key]);
                }
            }
            else
            {
                unset($folders[$key]);
            }
        });

        // dd($folders);
    }
}
