<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\CardService;
use App\Services\LiveService;
use App\Services\IdentificationService;

class ProcessCardScan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ak23:process_card_scan {--crop : Whether the job should be cropping}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Moving images from scan directory to source directory';

    // protected $fileNaming;
    // protected $path;
    // protected $maximum_folder_to_process = 30;
    // protected $tmp_card_path;
    // protected $biometricService;
    // protected $cardTemplateService;
    private $cardService;
    private $liveService;
    private $identificationService;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // $this->maximum_folder_to_process = env('CARDSCAN_MAX_MOVE_FOLDER', 3);

        // $this->biometricService = $biometricService;

        $this->cardService = new CardService;
        $this->liveService = new LiveService;
        $this->identificationService = new IdentificationService;

        // // Storing path in array
        // $this->path = [
        //     'card_source' => config('path.data.card_source'),
        //     'card' => config('path.data.card'),
        //     'live' => config('path.data.live'),
        //     'complete' => config('path.data.complete'),
        //     'tmp' => config('path.data.tmp')
        // ];

        // // Storing file names in array
        // $this->fileNaming = [
        //     'front' => config('filename.frontName'),
        //     'back' => config('filename.backName'),
        //     'front_small' => config('filename.frontSmallName'),
        //     'back_small' => config('filename.backSmallName'),
        //     'face_front' => config('filename.face_front'),
        //     'face_left' => config('filename.face_left'),
        //     'face_right' => config('filename.face_right'),
        //     'flat_left_four_fingers' => config('filename.flat_left_four_fingers'),
        //     'flat_right_four_fingers' => config('filename.flat_right_four_fingers'),
        //     'flat_thumbs' => config('filename.flat_thumbs')
        // ];

        // $this->cardTemplateService = $cardTemplateService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $crop = $this->option('crop');

        $this->cardService->doProcess();
        $this->liveService->doProcess();
        $this->identificationService->compareProcess();
    }
}