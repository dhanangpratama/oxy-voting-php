<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('OptionRepository', \App\Repositories\OptionRepository::class);
        $this->app->bind('ActivityService', \App\Services\ActivityService::class);
        $this->app->bind('DataService', \App\Services\DataService::class);
        $this->app->bind('CardService', \App\Services\CardService::class);
        $this->app->bind('QueueService', \App\Services\QueueService::class);
    }
}
