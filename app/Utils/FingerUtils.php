<?php

namespace App\Utils;
use GuzzleHttp\Client;
use File;

class FingerUtils
{
    public function compare($requested_id, $found_id, $type = null)
    {
        $fingers = flat_fingers_name(); // Use the flat finger
        $finger_name = $this->finger_name();

        $finger_compare = [];
        $complete_name_folder = config('filename.complete_folder');
        $card_name_folder = config('filename.card_folder');
        $live_name_folder = config('filename.live_folder');
        $requested_dir = config('path.data.complete');
        $requsted_url = $complete_url = asset("files/{$complete_name_folder}");

        if ( $type == 'card' )
        {
            $requested_dir = config('path.data.card');
            $requsted_url = asset("files/{$card_name_folder}");
        }

        if ( $type == 'live' )
        {
            $requested_dir = config('path.data.live');
            $requsted_url = asset("files/{$live_name_folder}");
        }

        $path_to_check_requested = $requested_dir . DIRECTORY_SEPARATOR . $requested_id . DIRECTORY_SEPARATOR;
        $path_to_check_founded = config('path.data.complete') . DIRECTORY_SEPARATOR . $found_id . DIRECTORY_SEPARATOR;

        foreach ($fingers as $finger => $imgName) 
        {
            if ( File::exists($path_to_check_requested . $imgName) && File::exists($path_to_check_founded . $imgName) )
            {
                $finger_compare['title'] = $finger_name[$finger];
                $finger_compare['requested_url'] = "{$requsted_url}/{$requested_id}/{$imgName}";
                $finger_compare['founded_url'] = "{$complete_url}/{$found_id}/{$imgName}";
                $finger_compare['requested_dir'] = $path_to_check_requested;
                $finger_compare['founded_dir'] = $path_to_check_founded;
                $finger_compare['requested_file'] = $path_to_check_requested . $imgName;
                $finger_compare['founded_file'] = $path_to_check_founded . $imgName;
                $finger_compare['img_name'] = $imgName;

                break;
            }
        }

        return $finger_compare;
    }

    protected function finger_name()
    {
        $finger = config('ak23.finger_data');
        return [
            'right-thumb' => $finger['right_thumb']['title'],
            'right-index' => $finger['right_index_finger']['title'],
            'right-middle' => $finger['right_middle_finger']['title'],
            'right-ring' => $finger['right_ring_finger']['title'],
            'right-little' => $finger['right_little_finger']['title'],
            'left-thumb' => $finger['left_thumb']['title'],
            'left-index' => $finger['left_index_finger']['title'],
            'left-middle' => $finger['left_middle_finger']['title'],
            'left-ring' => $finger['left_ring_finger']['title'],
            'left-little' => $finger['left_little_finger']['title']
        ];
    }

}