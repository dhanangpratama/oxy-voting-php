<?php

namespace App\Utils;
use GuzzleHttp\Client;
use Log;

class ApiUtils
{
    public static function pingServer()
    {
        
        $client = new Client;

        try
        {
            $response = $client->request('GET', config('url.javaApiServer') . 'sync/ping', [
                'verify' => false,
                'debug' => false,
                'connect_timeout' => 15,
                'headers' => [
                    'content-type' => 'application/json'
                ]
            ]);
        }
        catch ( \Exception $e )
        {
            return ['status' => 'off']; // all off
        }

        $result = json_decode($response->getBody(), TRUE);

        // $result['status'] = 'ERROR';

        if ( array_has($result, 'status') && array_get($result, 'status') == 'OK' )
        {
            return ['status' => 'on'];
        }
        else
        {
            return ['status' => 'part']; // 
        }
        
    }

    public static function getToken($grantType, $clientId, $username, $password)
    {
        $client = new Client;

        try
        {
            $response = $client->request('POST', str_replace('api/', '', config('url.javaApiServer')) . 'oauth/token', [
                'headers' => [
                    'content-type' => 'application/x-www-form-urlencoded'
                ],
                'form_params' => [
                    'grant_type' => $grantType,
                    'client_id' => $clientId
                ],
                'connect_timeout' => 30, // 30 seconds
                'auth' => [
                    $username, 
                    $password
                ],
                'verify' => false,
                'version' => 1.1
            ]);
        }
        catch ( \Exception $e )
        {
            record_error('API - Error in request token', $e);
            return ['status' => 'ERROR', 'message' => $e->getMessage()];
        }

        return ['status' => 'OK', 'contents' => json_decode($response->getBody(), TRUE)];
    }

    public static function generateMinutiae($data)
    {
        try
        {
            $client = new Client;
            
            $response = $client->request('POST', config('url.javaApiClient') . 'finger/generate_minutiae', [
                'json' => $data, 
                'version' => 1.1,
                'headers' => [
                    'content-type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]);

            $res = json_decode($response->getBody(), TRUE);

            if ( $res['status'] === 'ERROR' )
            {
                throw new \Exception($res['message']);
            }

            return $res;
        }
        catch ( \Exception $e )
        {
            // Do nothing here
        }
    }

    public static function extractZip($source, $destination)
    {
        try
        {
            $client = new Client;
            
            $response = $client->request('POST', config('url.javaApiClient') . 'utils/unzip?sourceFilePath='.$source.'&targetPath='.$destination, [
                'version' => 1.1,
                'headers' => [
                    'content-type' => 'application/json',
                ]
            ]);

            $res = json_decode($response->getBody(), TRUE);

            if ( $res['status'] === 'ERROR' )
            {
                throw new \Exception($res['message']);
            }

            return $res;
        }
        catch ( \Exception $e )
        {
            // Do nothing here
        }
    }

    public static function matching($data)
    {
        try
        {
            $client = new Client;
            
            $response = $client->request('POST', config('url.javaApiClient') . 'finger/matching', [
                'json' => $data, 
                'version' => 1.1,
                'headers' => [
                    'content-type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]);

            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - Error in matching system', $e);
        }
    }

    public static function insert($data)
    {
        $client = new Client;

        try
        {
            $response = $client->request('POST', config('url.javaApiClient') . 'finger/insert', [
                'json' => $data, 
                'version' => 1.1,
                'headers' => [
                    'content-type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]);
        }
        catch ( \Exception $e )
        {
            record_error('API - insert template', $e);
            return abort(500, '[btn_back]Gagal menyimpan template pada database.');
        }

        return json_decode($response->getBody(), TRUE);
    }

    public static function patternClassification($data)
    {
        $client = new Client;

        try
        {
            $response = $client->request('POST', config('url.javaApiClient') . 'finger/pattern_classification', [
                'json' => $data,
                'version' => 1.1,
                'headers' => [
                    'content-type' => 'application/json',
                    'Accept' => 'application/json',
                ]
            ]);
        }
        catch ( \Exception $e )
        {
            record_error('API - pattern classification', $e);
            return abort(500, '[btn_back]Gagal melakukan klasifikasi sidik jari.');
        }

        if ( $response->getReasonPhrase() !== 'OK' )
        {
            return false;
        }

        return json_decode($response->getBody(), TRUE);
        
    }

    public static function shareUserIdSession($id)
    {
        $client = new Client;
        
        try
        {
            $response = $client->request('GET', config('url.javaApiClient') . 'session/user?id=' . $id, [
                'version' => 1.1,
                'headers' => [
                    'content-type' => 'application/json',
                    'Accept' => 'application/json'
                ]
            ]);

            $res = json_decode($response->getBody(), TRUE);
            
            if ( $res['status'] != 'OK' )
            {
                return false;
            }
    
            return true;
        }
        catch ( \Exception $e )
        {
            record_error('API - share user id session', $e);
        }
    }

    public static function searchServer($token, $data)
    {
        $client = new Client;

        return $client->request('GET', config('url.javaApiServer') . 'search/demographic?' . http_build_query($data), [
            'version' => 1.1,
            'verify' => false,
            'headers' => [
                'Authorization' => $token,
                'content-type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);
    }

    public static function insertTemplate($data)
    {
        $client = new Client;
        
        $response = $client->request('POST', config('url.javaApiClient') . 'finger/insert_template', [
            'json' => $data, 
            'version' => 1.1,
            'headers' => [
                'content-type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);

        $res = json_decode($response->getBody(), TRUE);

        if ( $res['status'] === 'ERROR' )
        {
            throw new \Exception($res['message']);
        }

        return $res;
    }  

    public static function getHardwareHashId()
    {
        $client = new Client;

        try
        {
            $response = $client->request('GET', config('url.javaApiClient') . 'workstation/hardware_hash_id', [
                'version' => 1.1
            ]);

            if ( $response->getStatusCode() !== 200 )
            {
                throw new \Exception("Gagal terkoneksi ke API");
            }

            return $response->getBody()->getContents();
        }
        catch (\Exception $e)
        {
            return false;
        }
    }

    public static function searchAk23v1($subjectId)
    {
        $client = new Client;

        $response = $client->request('GET', 'http://202.52.13.117:8484/api/search/demographic?subjectId=' . $subjectId, [
            'version' => 1.1,
            'verify' => false,
            'headers' => [
                'content-type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);

        if ( $response->getStatusCode() !== 200 )
        {
            throw new \Exception("Tidak terhubung ke Datacenter");
        }

        return json_decode($response->getBody(), TRUE);
    }

    public static function getIpPublic()
    {
        $client = new Client;
        
        try
        {
            $response = $client->request('GET', config('url.javaApiServer') . 'sync/publicip', [
                'version' => 1.1,
                'verify' => false
            ]);
        }
        catch ( \Exception $e )
        {
            session(['ip_public' => null]);
            return;
        }

        if ( $response->getStatusCode() !== 200 )
        {
            session(['ip_public' => null]);
            return;
        }
        
        session(['ip_public' => $response->getBody()->getContents()]);
        return;
    }

    /**
     * Get reports
     */
    public static function reportServer($token, $startDate, $endDate, $location = null)
    {
        $client = new Client;

        $data = [
            'startDate' => $startDate,
            'endDate' => $endDate
        ];

        if ( ! empty($location) )
        {
            $data['location'] = $location;
        }

        return $client->request('GET', config('url.javaApiServer') . 'report?' . http_build_query($data), [
            'version' => 1.1,
            'verify' => false,
            'headers' => [
                'Authorization' => $token,
                'content-type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);
    }

    /**
     * Get all locations
     */
    public static function locations($token)
    {
        $client = new Client;

        return $client->request('GET', config('url.javaApiServer') . 'locations', [
            'version' => 1.1,
            'verify' => false,
            'headers' => [
                'Authorization' => $token,
                'content-type' => 'application/json',
                'Accept' => 'application/json'
            ]
        ]);
    }

    /**
     * Search Laten
     */
    public static function OSInafisLatenSearch($token, $data)
    {
        return self::handleMultipart($token, 'POST', 'public/identify', $data);
    }

    /**
     * Search Laten Direct
     */
    public static function OSInafisLatenSearchDirect($data)
    {
        $client = new Client;

        $params = [
            'debug' => false,
            'multipart' => $data
        ];

        $username = config('api.osinafis_username');
        $password = config('api.osinafis_password');

        $splitUname = str_split($username);
        sort($splitUname);
        $pwd = sha1($password . sha1(implode('', $splitUname)));
        $params = self::signMultipartRequest($params, $username, $pwd);

        $headers = [
            'headers' => [
                    'Accept' => 'application/json',
                    'Host' => 'inafis.com',
                    'apikey' => '307d9d744d8b4658b09e3ef86eb9676d'
                ]
            ];

        $params = array_merge($params, $headers);

        $response = $client->request('POST', config('url.latenApiServerNew') . 'public/identify', $params);

        if ( $response->getReasonPhrase() !== 'OK' )
        {
            return false;
        }

        return json_decode($response->getBody(), TRUE);
    }

    /**
     * Search Detail
     */
    public static function OSInafisSearch($token, $data, $limit = 10000, $offset = 0)
    {
        return self::handle($token, 'POST', 'public/searchAbisDetails', [
            'json' => json_encode($data),
            'limit' => $limit,
            'offset' => $offset
        ]);
    }

    /**
     * Search Detail Direct
     */
    public static function OSInafisSearchDirect($data, $limit = 20, $offset = 0)
    {
        $params = [
            'json' => json_encode($data),
            'limit' => $limit,
            'offset' => $offset
        ];

        return self::handleDirect('POST', 'public/searchAbisDetails', $params);
    }

    /**
     * Search Detail by External ID
     */
    public static function OSInafisSearchDetailByExternalId($token, $params)
    {
        return self::handle($token, 'POST', 'public/searchDetailByExternalId', $params);
    }

    /**
     * Search Detail by External ID Direct
     */
    public static function searchDetailByExternalIdDirect($params)
    {
        return self::handleDirect('POST', 'public/searchDetailByExternalId', $params);
    }

    /**
     * Get Images by External ID
     */
    public static function OSInafisGetImages($token, $params)
    {
        return self::handle($token, 'POST', 'public/searchImageByExternalId', $params);
    }

    /**
     * Get Images by External ID Direct
     */
    public static function OSInafisGetImagesDirect($params)
    {
        return self::handleDirect('POST', 'public/searchImageByExternalId', $params);
    }

    public static function OSInafisKtpSearchByNIK($token, $nik)
    {
        return self::handle($token, 'POST', 'ektp/searchNik', ['nik' => $nik]);
    }

    public static function OSInafisKtpSearch($token, $params)
    {
        return self::handle($token, 'POST', 'ektp/searchDetails', $params);
    }

    private static function handleDirect($request, $endpoint, $params)
    {
        $client = new Client;

        $username = config('api.osinafis_username');
        $password = config('api.osinafis_password');

        $splitUname = str_split($username);
        sort($splitUname);
        $pwd = sha1($password . sha1(implode('', $splitUname)));
        $params = self::signRequest($params, $username, $pwd);

        $headers = [
            'version' => 1.1,
            'verify' => false,
            'headers' => [
            // 'Accept' => 'application/json',
            // 'Host' => 'inafis_public.com',
            // 'apikey' => '48b393d5a529589ba123f046f6ac0a6c3cff0aa5'
            // 'Authorization' => $token,
            'Accept' => 'application/json',
            'Host' => 'inafis.com',
            'apikey' => '307d9d744d8b4658b09e3ef86eb9676d'
        ]];

        $params = array_merge($params, $headers);

        return $client->request($request, config('url.latenApiServerNew') . $endpoint, $params);
    }

    private static function handleMultipart($token, $request, $endpoint, $params)
    {
        $client = new Client;

        $params = [
            'debug' => false,
            'multipart' => $params
        ];

        $username = config('api.osinafis_username');
        $password = config('api.osinafis_password');

        $splitUname = str_split($username);
        sort($splitUname);
        $pwd = sha1($password . sha1(implode('', $splitUname)));
        $params = self::signMultipartRequest($params, $username, $pwd);

        $headers = [
            'version' => 1.1,
            'verify' => false,
            'headers' => [
                    'Authorization' => $token,
                    'Accept' => 'application/json',
                    'Host' => 'inafis.com',
                    'apikey' => '307d9d744d8b4658b09e3ef86eb9676d'
                ]
            ];

        $params = array_merge($params, $headers);

        return $client->request($request, config('url.javaApiServer') . $endpoint, $params);
    }

    private static function handle($token, $request, $endpoint, $params)
    {
        $client = new Client;

        $username = config('api.osinafis_username');
        $password = config('api.osinafis_password');

        $splitUname = str_split($username);
        sort($splitUname);
        $pwd = sha1($password . sha1(implode('', $splitUname)));
        $params = self::signRequest($params, $username, $pwd);

        $headers = [
            'version' => 1.1,
            'verify' => false,
            'debug' => false,
            'headers' => [
            // 'Accept' => 'application/json',
            // 'Host' => 'inafis_public.com',
            // 'apikey' => '48b393d5a529589ba123f046f6ac0a6c3cff0aa5'
            'Authorization' => $token,
            'Accept' => 'application/json',
            'Host' => 'inafis.com',
            'apikey' => '307d9d744d8b4658b09e3ef86eb9676d'
        ]];

        $params = array_merge($params, $headers);

        return $client->request($request, config('url.javaApiServer') . $endpoint, $params);
    }

    private static function signRequest($sigParams = [], $username = null, $password = null)
    {
        $tempMultipart = false;
        $tempFingerPrints = [];

        if (is_null($username) && is_null($password)) {
            $username = \Session::get('loggedInUname');
            $password = \Session::get('loggedInPwd');
        }

        $timestamp = time();
        $sig = '';            

        $sigParams['timestamp'] = $timestamp;
        $sigParams['username'] = $username;
        $sigParams['password'] = $password;
        $sigParams['hwid'] = 'PF063NAJ';

        $fingerprintCodes = [];
        for ($i = 1; $i <= 10; $i ++) {
            if (isset($sigParams['fingerprint_' . $i])) {
                $name = 'fingerprint_' . $i;
                $value = last(explode('\\', stream_get_meta_data($sigParams[$name])['uri']));

                $tempFingerPrints[$name] = $value;
                unset($sigParams[$name]);
            }
        }
        if (isset($sigParams['multipart'])) {
            $multipart = $sigParams['multipart'];
            $count_multipart = count($multipart);
            for($i = 0; $i < $count_multipart; $i ++) {
                $name = $sigParams['multipart'][$i]['name'];
                $value = last(explode('\\', stream_get_meta_data($sigParams['multipart'][$i]['contents'])['uri']));
                $sigParams[$name] = $value;   
            }   
            // $name = $sigParams['multipart'][0]['name'];
            // $value = last(explode('\\', stream_get_meta_data($sigParams['multipart'][0]['contents'])['uri']));
            // $sigParams[$name] = $value;
            // dd("key = ".$name.", value = ". $value);
            $tempMultipart = $sigParams['multipart'];
            // $sigParams['multipart'] = json_encode($sigParams['multipart']);
            unset($sigParams['multipart']);
        }
        ksort($sigParams);
        foreach ($sigParams as $key => $value) {
            $sig .= $key . sha1($value);
            //echo($key."=>".sha1($value));
        }
        //dd();
        $finalParams = $sigParams;
        $finalParams['sig'] = sha1($sig);
        unset($finalParams['password']);

        //dd($finalParams);

        // if ($tempMultipart) {
        //     $finalParams['multipart'] = $tempMultipart;
        // }
        // dd($finalParams);

        // foreach ($tempFingerPrints as $key => $fingerprint) {
        //     $finalParams[$key] = $fingerprint;
        // }

        return ['form_params' => $finalParams];

    }

    private static function signMultipartRequest($sigParams = [], $username = null, $password = null)
    {
        $tempMultipart = false;
        $tempFingerPrints = [];

        if (is_null($username) && is_null($password)) {
            $username = \Session::get('loggedInUname');
            $password = \Session::get('loggedInPwd');
        }

        $timestamp = time();
        $sig = '';            

        $sigParams['timestamp'] = $timestamp;
        $sigParams['username'] = $username;
        $sigParams['password'] = $password;
        $sigParams['hwid'] = 'PF063NAJ';

        $fingerprintCodes = [];
        for ($i = 1; $i <= 10; $i ++) {
            if (isset($sigParams['fingerprint_' . $i])) {
                $name = 'fingerprint_' . $i;
                $value = last(explode('\\', stream_get_meta_data($sigParams[$name])['uri']));

                $tempFingerPrints[$name] = $value;
                unset($sigParams[$name]);
            }
        }
        if (isset($sigParams['multipart'])) {
            $name = $sigParams['multipart'][0]['name'];
            $value = last(explode('\\', stream_get_meta_data($sigParams['multipart'][0]['contents'])['uri']));
            $sigParams[$name] = $value;
            // dd("key = ".$name.", value = ". $value);
            $tempMultipart = $sigParams['multipart'];
            unset($sigParams['multipart']);
        }
        ksort($sigParams);
        foreach ($sigParams as $key => $value) {
            $sig .= $key . sha1($value);
            // echo $key;
        }
        $finalParams = $sigParams;
        $finalParams['sig'] = sha1($sig);
        unset($finalParams['password']);

        if ($tempMultipart) {
            $finalParams['multipart'] = $tempMultipart;
        }
        $timestamps = [
            'name' => 'timestamp',
            'contents' => $timestamp
        ];
        $usernames = [
            'name' => 'username',
            'contents' => $username
        ];
        $passwords = [
            'name' => 'password',
            'contents' => $password
        ];
        $passwords = [
            'name' => 'sig',
            'contents' => $sig
        ];
        array_push($tempMultipart, $timestamps);
        array_push($tempMultipart, $usernames);
        array_push($tempMultipart, $passwords);
        // dd($tempMultipart);
        // foreach ($tempFingerPrints as $key => $fingerprint) {
        //     $finalParams[$key] = $fingerprint;
        // }

        // return ['form_params' => $finalParams];
        return ['multipart' => $tempMultipart];

    }
}