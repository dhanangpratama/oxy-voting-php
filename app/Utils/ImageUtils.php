<?php

namespace App\Utils;
use GuzzleHttp\Client;
use Auth, Image;
use Carbon\Carbon;

class ImageUtils
{
    public static function imageCompression($sourceFile, $targetFile)
    {
        $client = new Client;
        $form_params = [
            'sourceFile' => $sourceFile,
            'targetFile' => $targetFile,
        ];


        $response = $client->request('POST', config('url.javaApiClient') . 'utils/images/compression', [
            'form_params' => $form_params
        ]);

        if ( $response->getReasonPhrase() !== 'OK' )
        {
            return false;
        }

        $results = json_decode($response->getBody(), TRUE);
        if ( $results['status'] == 'OK')
        {
            return true;
        }

        return false;
    }

    public static function generateOCR($sourceFile)
    {
        $ocr = (new \TesseractOCR($sourceFile))->lang('ind');

        $tesseractExecPath = config('path.tesseractExecPath');

        if(!empty($tesseractExecPath))
        {
            $ocr->executable($tesseractExecPath);
             $ocr->tessdataDir(dirname($tesseractExecPath));
        }

        return $ocr->run();

    }

    public static function saveJpegAsWsq($sourceFile)
    {
        $targetFile = str_replace(".jpg", ".wsq", $sourceFile);
        $targetFile = str_replace(".jpeg", ".wsq", $targetFile);
        return self::saveAsWsq($sourceFile, $targetFile);

    }

    public static function saveAsWsq($sourceFile, $targetFile)
    {
        $client = new Client;
        $data = array('sourceFile' => $sourceFile, 'targetFile' => $targetFile);

        try {
            $response = $client->request('POST', config('url.javaApiClient') . 'utils/images/to_wsq', [
                'form_params' => $data
            ]);

            $res = json_decode($response->getBody(), TRUE);

            if ($res['status'] === 'OK') {
                return true;
            }

        } catch (\Exception $e) {
            //nothing to do here?
        }


        return false;
    }

    public static function saveWsqAsJpg($sourceFile, $targetFile)
    {
        $client = new Client;
        $data = array('sourceFile' => $sourceFile, 'targetFile' => $targetFile);

        try {
            $response = $client->request('POST', config('url.javaApiClient') . 'utils/images/wsqToJpg', [
                'form_params' => $data
            ]);

            $res = json_decode($response->getBody(), TRUE);

            if ($res['status'] === 'OK') {
                return true;
            }

        } catch (\Exception $e) {
            //nothing to do here?
        }


        return false;
    }

    public static function toBase64($image = '', $mime = TRUE)
    {
        try
        {
            // Read image path, convert to base64 encoding
            $imageData = base64_encode(file_get_contents($image));

            if ($mime)
            {
                return 'data: '.img($image)->get_original_info()['exif']['MimeType'].';base64,'.$imageData; // Format the image SRC:  data:{mime};base64,{data};
            }

            return $imageData;
        }
        catch (\Exception $e)
        {
            $imageData = base64_encode(file_get_contents(public_path().'/assets/images/blank-finger.png'));
        }

        
    }

    public static function createWatermarkImage()
    {
        if ( config('ak23.access') == 'desktop' )
        {
            $userText = '';
            
            for ( $i=0; $i<=20; $i++)
            {
                $userText .= Auth::user()->name . ' - ' . Carbon::now()->format('d/m/Y h:i:s a') . ' - ';
            }
    
            $img = Image::canvas(800, 800, '#ffffff');
    
            $margin = 50;
            $init = 250;
            
            for ( $i=1; $i<=30; $i++ )
            {
                $img->text($userText, -220, $init += 50, function($font) {
                    $font->color(array(232, 232, 232, 0.75));
                    $font->file(public_path('fonts\OpenSans-Regular.ttf'));
                    $font->size(14);
                    $font->angle(45);
                });
            }
            
            $img->save(config('path.data.tmp') . DIRECTORY_SEPARATOR . 'watermark.jpg');
        }
    }

}