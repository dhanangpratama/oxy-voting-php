<?php

namespace App\Utils;
use File;
use Log;
use Cache;

class ToolUtils
{
    public $updateUrl;
    protected $updateUrlFile;
    protected $updateChangesLog;
    protected $localChangesLog;
    protected $localUpdateFile;
    protected $localVersion;
    protected $updateVersion;
    protected $localUpdateDir;
    protected $javaClientUpdateFile;
    protected $javaClientLocalFile;
    protected $javaClientUpdateGitProperties;
    protected $javaClientLocalGitProperties;

    public function __construct()
    {
        $this->updateUrl = config('url.updateUrl');
        
        $this->updateUrlFile = $this->updateUrl . 'update.zip';
        $this->updateVersion = $this->updateUrl . 'version.json';
        $this->updateChangesLog = $this->updateUrl . 'changes.txt';

        $this->localUpdateDir = storage_path('tool' . DIRECTORY_SEPARATOR . 'update' . DIRECTORY_SEPARATOR);
        $this->localVersion = $this->localUpdateDir . 'version.json';
        $this->localUpdateFile = $this->localUpdateDir . 'update.zip';
        $this->localChangesLog = $this->localUpdateDir . 'changes.txt';
        $this->javaClientUpdateFile = $this->localUpdateDir . 'ak23api-client.jar';
        $this->javaClientLocalFile = 'C:\Bin\ak23bin\services\ak23api-client.jar';
        $this->javaClientUpdateGitProperties = $this->localUpdateDir . 'git.properties';
        $this->javaClientLocalGitProperties = 'C:\Bin\ak23bin\services\git.properties';
    }

    public function getServerVersion()
    {
        return Cache::remember('server_version', 120, function() 
        {
            /** Remove server changes log cache */
            if ( Cache::has('server_changes_log') ) 
            {
                Cache::forget('server_changes_log');
            }

            if ( checkRemoteFile($this->updateVersion, false) )
            {
                $data = json_decode(file_get_contents($this->updateVersion), true);

                if ( is_array($data) && count($data) > 0 )
                {
                    return $data;
                }
            }
            
            return null;
        });
    }

    public function getLocalVersion()
    {
        if ( File::exists($this->localVersion) )
        {
            $data = json_decode(file_get_contents($this->localVersion), true);

            if ( is_array($data) && count($data) > 0 )
            {
                return $data;
            }
        }

        return null;
    }

    public function getUpdateChangesLog()
    {
        return Cache::remember('server_changes_log', 720, function()
        {
            try
            {
                return file_get_contents($this->updateChangesLog);
            }
            catch(\Exception $e)
            {
                return null;
            }
        });
    }

    public function getLocalChangesLog()
    {
        try
        {
            return file_get_contents($this->localChangesLog);
        }
        catch(\Exception $e)
        {
            return null;
        }
    }

    public function updateUrlFile()
    {
        return $this->updateUrlFile;
    }

    public function localUpdateFile()
    {
        return $this->localUpdateFile;
    }

    public function getUpdateUrl()
    {
        return $this->updateUrl;
    }

    public function updateSoftware()
    {
        $zip = new \ZipArchive;
        $serverVersion = $this->getServerVersion();
        $localVersion = $this->getLocalVersion();
        $javaUpdateStatus = true;
        $successMessage = 'Selamat aplikasi berhasil diperbarui.';

        if ( File::exists($this->localUpdateFile) )
        {
            unlink($this->localUpdateFile);
        }

        try
        {
            if ( checkRemoteFile($this->updateUrlFile, false) )
            {
                getFile($this->updateUrlFile, $this->localUpdateFile);
            }
            else
            {
                return ['status' => false, 'message' => 'Gagal memperbarui aplikasi. File update tidak ditemukan pada server.'];
            }
        }
        catch (\Exception $e)
        {
            Log::error('Error update software.', ['message' => $e->getMessage()]);

            return ['status' => false, 'message' => 'Gagal memperbarui aplikasi. Pesan error: ' . $e->getMessage()];
        }

        try
        {
            if ( $zip->open($this->localUpdateFile) === TRUE ) 
            {
                $zip->extractTo(storage_path('tool' . DIRECTORY_SEPARATOR . 'update'));
                $zip->close();
            } 
            else 
            {
                return ['status' => false, 'message' => 'Gagal memperbarui aplikasi. Gagal melakukan ekstrak file update.'];
            }

            if ( ! empty($serverVersion['javaClientLogUpdate']) && ! empty($localVersion['javaClientLogUpdate']) && $serverVersion['javaClientLogUpdate'] > $localVersion['javaClientLogUpdate'] )
            {
                $javaUpdateStatus = $this->updateJavaClient();
                $successMessage = 'Selamat aplikasi dan java client berhasil diperbarui.';
            }

            if ( ! $javaUpdateStatus )
            {
                return ['status' => false, 'message' => 'Gagal memperbarui java client. Mohon untuk menghubungi bagian support.'];
            }

            if ( is_dir($this->localUpdateDir . 'web') )
            {
                File::copyDirectory($this->localUpdateDir . 'web', base_path());
            }

            // TO DO Remove uneeded files here

            return ['status' => true, 'message' => $successMessage];
        }
        catch (\Exception $e)
        {
            return ['status' => false, 'message' => 'Gagal memperbarui aplikasi. Pesan error: ' . $e->getMessage()];
        }
    }

    public function updateJavaClient() 
    {
        $stopAk23ServiceStatus = $startAk23ServiceStatus = $renameLocalJavaClient = false;

        chdir(base_path());

        try
        {
            if ( strpos($this->checkServices(), 'STOPPED') === false )
            {
                $stopAK23Service = shell_exec('net stop ak23services');

                if ( strpos($stopAK23Service, 'stopped successfully') !== false )
                {
                    $stopAk23ServiceStatus = true;
                }
            }
            else
            {
                $stopAk23ServiceStatus = true;
            }

            if ( $stopAk23ServiceStatus )
            {
                if ( File::exists($this->javaClientUpdateFile) )
                {
                    if ( File::exists($this->javaClientLocalFile) )
                    {
                        rename($this->javaClientLocalFile, $this->javaClientLocalFile . '.old');
                    }

                    File::copy($this->javaClientUpdateFile, $this->javaClientLocalFile);
                }
                else
                {
                    throw new \Exception('File update java client tidak ditemukan.');
                }
            }
            else
            {
                throw new \Exception('Gagal mematikan service AK23');
            }

            if ( strpos($this->checkServices(), 'RUNNING') === false )
            {
                $startAK23Service = shell_exec('net start ak23services');

                if ( strpos($startAK23Service, 'started successfully') !== false )
                {
                    $startAk23ServiceStatus = true;
                }
            }
            else
            {
                $startAk23ServiceStatus = true;
            }

            if ( $startAk23ServiceStatus )
            {
                if ( File::exists($this->javaClientLocalFile . '.old') )
                {
                    unlink($this->javaClientLocalFile . '.old');
                }

                if ( File::exists($this->javaClientLocalGitProperties) )
                {
                    unlink($this->javaClientLocalGitProperties);
                }

                File::copy($this->javaClientUpdateGitProperties, $this->javaClientLocalGitProperties);
                
                return true;
            }
            else
            {
                throw new \Exception('Gagal menjalankan service AK23');
            }
        }
        catch(\Exception $e)
        {
            // TO DO if any error
            return false;
        }
    }

    public function runComposerUpdate()
    {
        chdir(base_path());

        return shell_exec('composer update');
    }

    public function checkServices($serviceName = 'ak23services')
    {
        chdir(base_path());

        $command = shell_exec('sc query "' . $serviceName . '" | find "STATE"');

        return ! empty($command) ? str_replace('STATE:', '', str_replace(' ', '', $command)) : null;
    }

    public function checkEnv()
    {
        $imagick = class_exists('Imagick') ? true : false;
        $curl = function_exists('curl_version') ? true : false;

        return response()->json([
            'php_version' => phpversion(),
            'allow_url_fopen ' => (ini_get('allow_url_fopen')) ? true : false,
            'max_execution_time' => ini_get('max_execution_time'),
            'post_max_size' => ini_get('post_max_size'),
            'memory_limit' => ini_get('memory_limit'),
            'Loaded Configuration File' => php_ini_loaded_file(),  
            'curl' => $curl,
            'imagick' => $imagick,
            'ak23services' => $this->checkServices(),
            'path' => $_SERVER['PATH']
        ]);
    }

    public static function networkStatus()
    {
        $file = config('path.serverStatusFile');

        if ( file_exists($file) )
        {
            return file_get_contents($file);
        }
        
        return 'off';
    }
}