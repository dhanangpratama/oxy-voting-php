<?php

namespace App\Utils;

use DB;
use Schema;

class ReportUtils
{
    public static function generate($startDate, $endDate)
    {
        if ( strtotime($startDate) > strtotime($endDate) )
        {
            return abort(404);
        }

        $tempTableName = 'temp_report';
        $prefix = DB::getTablePrefix();
        $data = [];

        if ( Schema::hasTable($tempTableName) ) 
        {
            Schema::drop($tempTableName);
        }

        if ( ! Schema::hasTable($tempTableName) ) 
        {
            Schema::create($tempTableName, function ($table) 
            {
                $table->smallInteger('t_year')->unsigned();
                $table->smallInteger('t_month')->unsigned();
                $table->string('t_week_range', 255);
                $table->date('t_date');
                $table->integer('t_total')->unsigned()->nullable();
                $table->integer('t_total_criminal')->unsigned()->nullable();
                $table->integer('t_total_non_criminal')->unsigned()->nullable();
                $table->integer('t_total_ak24')->unsigned()->nullable();
                $table->index('t_year');
            });
        }

        $qInsert = "INSERT INTO {$prefix}temp_report (t_year, t_month, t_week_range, t_date) VALUES ";

        $start = new \DateTime($startDate);
        $end   = new \DateTime($endDate);
        
        for($i = $start; $start <= $end; $i->modify('+1 day'))
        {
            $arrayInsert[] = "('".$i->format('Y')."', '".$i->format('m')."', CONCAT(STR_TO_DATE('".$i->format('YW')." Sunday', '%X%V %W'), ' - ', STR_TO_DATE('".$i->format('YW')." Saturday', '%X%V %W')), '".$i->format('Y-m-d')."')";        
        }
        
        $qInsert .= implode(', ', $arrayInsert);

        if ( DB::insert(DB::raw($qInsert)) )
        {
            $qUpdate = "UPDATE {$prefix}temp_report t 
                            SET 
                                t_total = (SELECT COUNT(*) FROM {$prefix}demographics d WHERE t.t_date = DATE(d.created_at)), 
                                t_total_criminal = (SELECT COUNT(*) FROM {$prefix}demographics d WHERE t.t_date = DATE(d.created_at) AND d.status_id = 1), 
                                t_total_non_criminal = (SELECT COUNT(*) FROM {$prefix}demographics d WHERE t.t_date = DATE(d.created_at) AND d.status_id <> 1),
                                t_total_ak24 = (SELECT COUNT(*) FROM {$prefix}ak24_print a WHERE t.t_date = DATE(a.created_at))
                                ";

            if ( DB::update(DB::raw($qUpdate)) )
            {
                $qYearly = "SELECT t_year AS year, SUM(t_total) AS inputFingers, SUM(t_total_criminal) AS criminal, SUM(t_total_non_criminal) AS nonCriminal, SUM(t_total_ak24) AS ak24
                                FROM {$prefix}temp_report 
                                    WHERE t_date BETWEEN '".$startDate."' AND '".$endDate."' 
                                    GROUP BY t_year";

                $qMonthly = "SELECT t_year AS year, t_month AS month, SUM(t_total) AS inputFingers, SUM(t_total_criminal) AS criminal, SUM(t_total_non_criminal) AS nonCriminal, SUM(t_total_ak24) AS ak24
                                FROM {$prefix}temp_report 
                                    WHERE t_date BETWEEN '".$startDate."' AND '".$endDate."' 
                                    GROUP BY t_year, t_month
                                        ORDER BY t_year, t_month";

                $qWeekly = "SELECT t_week_range AS weekRange, SUM(t_total) AS inputFingers, SUM(t_total_criminal) AS criminal, SUM(t_total_non_criminal) AS nonCriminal, SUM(t_total_ak24) AS ak24
                                FROM {$prefix}temp_report 
                                    WHERE t_date BETWEEN '".$startDate."' AND '".$endDate."' 
                                    GROUP BY t_week_range
                                        ORDER BY t_year, t_month, t_week_range";

                $qDaily = "SELECT t_date AS day, t_total AS inputFingers, t_total_criminal AS criminal, t_total_non_criminal AS nonCriminal, t_total_ak24 AS ak24
                                FROM {$prefix}temp_report ORDER BY t_date";

                $years = DB::select(DB::raw($qYearly));
                $months = DB::select(DB::raw($qMonthly));
                $weeks = DB::select(DB::raw($qWeekly));
                $days = DB::select(DB::raw($qDaily));

                // START year data
                $input_fingers_year_data = '[';
                foreach ($years as $key => $value) 
                {
                    $input_fingers_year_data .= '["'.$value->year.'",'.$value->inputFingers.'],';
                    $data['years'][$value->year]['input_fingers_year'] = $value->inputFingers;
                }
                $input_fingers_year_data .= ']';

                $criminal_year_data = '[';
                foreach ($years as $key => $value) 
                {
                    $criminal_year_data .= '["'.$value->year.'",'.$value->criminal.'],';
                    $data['years'][$value->year]['criminal_year'] = $value->criminal;
                }
                $criminal_year_data .= ']';

                $non_criminal_year_data = '[';
                foreach ($years as $key => $value) 
                {
                    $non_criminal_year_data .= '["'.$value->year.'",'.$value->nonCriminal.'],';
                    $data['years'][$value->year]['non_criminal_year'] = $value->nonCriminal;
                }
                $non_criminal_year_data .= ']';

                $ak24_year_data = '[';
                foreach ($years as $key => $value) 
                {
                    $ak24_year_data .= '["'.$value->year.'",'.$value->ak24.'],';
                    $data['years'][$value->year]['ak24_year'] = $value->ak24;
                }
                $ak24_year_data .= ']';
                // END year data

                // START month data
                $input_fingers_month_data = '[';
                foreach ($months as $key => $value) 
                {
                    $label = date('M', mktime(0, 0, 0, $value->month, 10)) . ' ' . $value->year;
                    $input_fingers_month_data .= '["'.$label.'",'.$value->inputFingers.'],';
                    $data['months'][$label]['input_fingers_month'] = $value->inputFingers;
                }
                $input_fingers_month_data .= ']';

                $criminal_month_data = '[';
                foreach ($months as $key => $value) 
                {
                    $label = date('M', mktime(0, 0, 0, $value->month, 10)) . ' ' . $value->year;
                    $criminal_month_data .= '["'.$label.'",'.$value->criminal.'],';
                    $data['months'][$label]['criminal_month'] = $value->criminal;
                }
                $criminal_month_data .= ']';

                $non_criminal_month_data = '[';
                foreach ($months as $key => $value) 
                {
                    $label = date('M', mktime(0, 0, 0, $value->month, 10)) . ' ' . $value->year;
                    $non_criminal_month_data .= '["'.$label.'",'.$value->nonCriminal.'],';
                    $data['months'][$label]['non_criminal_month'] = $value->nonCriminal;
                }
                $non_criminal_month_data .= ']';

                $ak24_month_data = '[';
                foreach ($months as $key => $value) 
                {
                    $label = date('M', mktime(0, 0, 0, $value->month, 10)) . ' ' . $value->year;
                    $ak24_month_data .= '["'.$label.'",'.$value->ak24.'],';
                    $data['months'][$label]['ak24_month'] = $value->ak24;
                }
                $ak24_month_data .= ']';
                // END month data
                
                // START week data
                $input_fingers_week_data = '[';
                foreach ($weeks as $key => $value) 
                {
                    $input_fingers_week_data .= '["'.$value->weekRange.'",'.$value->inputFingers.'],';
                    $data['weeks'][$value->weekRange]['input_fingers_week'] = $value->inputFingers;
                }
                $input_fingers_week_data .= ']';

                $criminal_week_data = '[';
                foreach ($weeks as $key => $value) 
                {
                    $criminal_week_data .= '["'.$value->weekRange.'",'.$value->criminal.'],';
                    $data['weeks'][$value->weekRange]['criminal_week'] = $value->criminal;
                }
                $criminal_week_data .= ']';

                $non_criminal_week_data = '[';
                foreach ($weeks as $key => $value) 
                {
                    $non_criminal_week_data .= '["'.$value->weekRange.'",'.$value->nonCriminal.'],';
                    $data['weeks'][$value->weekRange]['non_criminal_week'] = $value->nonCriminal;
                }
                $non_criminal_week_data .= ']';

                $ak24_week_data = '[';
                foreach ($weeks as $key => $value) 
                {
                    $ak24_week_data .= '["'.$value->weekRange.'",'.$value->ak24.'],';
                    $data['weeks'][$value->weekRange]['ak24_week'] = $value->ak24;
                }
                $ak24_week_data .= ']';
                // END week data
                
                // START Day data
                $input_fingers_day_data = '[';
                foreach ($days as $key => $value) 
                {
                    $label = date('d/m/Y', strtotime($value->day));
                    $input_fingers_day_data .= '["'.$label.'",'.$value->inputFingers.'],';
                    $data['days'][$label]['input_fingers_day'] = $value->inputFingers;
                }
                $input_fingers_day_data .= ']';

                $criminal_day_data = '[';
                foreach ($days as $key => $value) 
                {
                    $label = date('d/m/Y', strtotime($value->day));
                    $criminal_day_data .= '["'.$label.'",'.$value->criminal.'],';
                    $data['days'][$label]['criminal_day'] = $value->criminal;
                }
                $criminal_day_data .= ']';

                $non_criminal_day_data = '[';
                foreach ($days as $key => $value) 
                {
                    $label = date('d/m/Y', strtotime($value->day));
                    $non_criminal_day_data .= '["'.$label.'",'.$value->nonCriminal.'],';
                    $data['days'][$label]['non_criminal_day'] = $value->nonCriminal;
                }
                $non_criminal_day_data .= ']';

                $ak24_day_data = '[';
                foreach ($days as $key => $value) 
                {
                    $label = date('d/m/Y', strtotime($value->day));
                    $ak24_day_data .= '["'.$label.'",'.$value->ak24.'],';
                    $data['days'][$label]['ak24_day'] = $value->ak24;
                }
                $ak24_day_data .= ']';
                // END day data

                $data['input_fingers_year_data'] = $input_fingers_year_data;
                $data['criminal_year_data'] = $criminal_year_data;
                $data['non_criminal_year_data'] = $non_criminal_year_data;
                $data['ak24_year_data'] = $ak24_year_data;

                $data['input_fingers_month_data'] = $input_fingers_month_data;
                $data['criminal_month_data'] = $criminal_month_data;
                $data['non_criminal_month_data'] = $non_criminal_month_data;
                $data['ak24_month_data'] = $ak24_month_data;

                $data['input_fingers_week_data'] = $input_fingers_week_data;
                $data['criminal_week_data'] = $criminal_week_data;
                $data['non_criminal_week_data'] = $non_criminal_week_data;
                $data['ak24_week_data'] = $ak24_week_data;

                $data['input_fingers_day_data'] = $input_fingers_day_data;
                $data['criminal_day_data'] = $criminal_day_data;
                $data['non_criminal_day_data'] = $non_criminal_day_data;
                $data['ak24_day_data'] = $ak24_day_data;

                return $data;
            }
            else
            {
                return false;
            }
        } 
        else
        {
            return false;
        }
    }
}