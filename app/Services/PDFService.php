<?php

namespace App\Services;
use File;
use Image;
use DNS1D;
use TCPDF;
use TCPDF_FONTS;
use setasign\Fpdi;

class PDFService
{
    public static function generateIdCard($data)
    {
        $fontSize = 32;
        $spaceNum = 26;

        $cardBg = config('path.data.tmp') . DIRECTORY_SEPARATOR . 'master.pdf';

        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => false,
            'cellfitalign' => '',
            'border' => false,
            'hpadding' => 'auto',
            'vpadding' => 'auto',
            'fgcolor' => array(0,0,0),
            'bgcolor' => false, //array(255,255,255),
            'text' => false,
            'font' => 'helvetica',
            'fontsize' => 8,
            'stretchtext' => 4
        );
            
        $pdf = new Fpdi\TcpdfFpdi('P', PDF_UNIT, [334, 210], true, 'UTF-8', false);

        $space = strlen(str_replace(' ', '', array_get($data, 'name'))) < $spaceNum ? '<div style="font-size: 22px; color: #FFF;">add</div>' : '';
        $pdf->AddPage();

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins(30, 0, 30);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        $pdf->setSourceFile($cardBg);
        $tplIdx = $pdf->importPage(1);

        // use the imported page and place it at position 10,10 with a width of 100 mm
        $pdf->useTemplate($tplIdx, 0, 0, 210);

        $pdf->SetTextColor(0, 0, 0);
        // TCPDF_FONTS::addTTFfont(public_path('helvetin.ttf'), 'TrueTypeUnicode', '', 96);
        $pdf->SetFont('Helvetica', 'B', $fontSize);
        $pdf->setFontSpacing(-0.25);

    
        $filename = str_slug(array_get($data, 'name'));
        //$barcodeId = mt_rand(111111111111,999999999999);
        // now write some text above the imported page
        // $pdf->SetLineWidth(20);
        $pdf->SetY(110);
        //$pdf->Write(0, $people['fullname'], '', 0, 'C', true, 3, false, false, 0);
        $html = '<table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td align="center">'.$space.strtoupper(array_get($data, 'name')).'</td>
                    </tr>
                </table>';
        $pdf->writeHTML($html, false, false, false, false, '');
        
        if ( file_exists(config('path.data.demographic') . DIRECTORY_SEPARATOR . 'photo.jpg') )
        {         
            $pdf->Image(config('path.data.demographic') . DIRECTORY_SEPARATOR . 'photo.jpg', 15, 152, 99, 99, 'JPG', null, 'N', false, 150, 'C', false, false, 0, false, false, false);
        }

        $pdf->SetY(259);
        $pdf->SetFont('Helvetica', 'B', 32);
        $pdf->Write(0, array_get($data, 'nik'), '', 0, 'C', true, 3, false, false, 0);

        $pdf->SetY(275);
        $pdf->write1DBarcode(str_pad(array_get($data, 'nik'), 8, '0', STR_PAD_LEFT), 'C39', '', '', '', 32, 2.2, $style, 'T');

    $pdf->Output( config('path.data.demographic') . DIRECTORY_SEPARATOR . 'idcard.pdf', 'F');
    }
}