<?php

namespace App\Services;

use File, Image, Log;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Utils\ApiUtils;
use App\Utils\ImageUtils;
use App\Services\Api\JavaServerService;
use App\Services\SipService;
use App\Models\People;

class IdentificationService 
{
    private $javaServerService;
    private $comparePath;
    private $zipService;

    public function __construct()
    {
        $this->javaServerService = new JavaServerService;
        $this->zipService = new ZipService;
        $this->comparePath = config('path.data.tmp_compare');
    }

    public function server($key)
    {
        $data = $dataFound = [];

        $verificationFile = session('process.basePath') . DIRECTORY_SEPARATOR . config('ak23.file_data.verification_data');

        $fingerIndex = config('ak23.os_inafis_finger_key');
        
        foreach (config('ak23.finger_data') as $file) 
        {
            $theFilename = array_get($file, 'roll_name');
            $keyFinger = str_replace('_finger', '', str_replace('.jpg', '', $theFilename));

            $theFile = session('process.basePath') . DIRECTORY_SEPARATOR . $theFilename;

            if ( File::exists($theFile) )
            {
                $data[] = [
                    'name' => 'fingerprint_' . array_get($fingerIndex, "{$keyFinger}.key"),
                    'contents' => fopen($theFile, 'r')
                ];
            }
        }

        // $data[] = [
        //     'name' => 'fingerprint_1',
        //     'contents' => fopen(session('process.basePath') . DIRECTORY_SEPARATOR . 'right_thumb.jpg', 'r')
        // ];

        if ( ! file_exists($verificationFile) )
        {
            $response = $this->javaServerService->OSInafisLatenSearch($data);

            if ( array_get($response, 'status') == 'OK' )
            {
                Log::error('Error in identification demographic page.', ['message' => array_get($response, 'message')]);
            }

            $candidates = array_get($response, 'candidates');

            if ( count($candidates) > 0 )  
            {
                foreach ($candidates as $candidate)
                {
                    if ( strpos(array_get($candidate, 'source'), 'AK') !== false )
                    {
                        $scoreClass = 'bg-red';
                        $score = $candidate['score']/10;
                        
                        if ( ! empty($score) )
                        {
                            if ( $score >= 80 )
                            {
                                $scoreClass = 'bg-green';
                            }
                            else if ( $score >= 50 && $score < 80 )
                            {
                                $scoreClass = 'bg-yellow';
                            }
                        }

                        $dataFound['source'] = 'server';
                        $dataFound['content'][] = [
                            'id' => trim(str_replace('AK23_', '', $candidate['externalId'])),
                            'score' => $score,
                            'fullname' => $candidate['full_name'],
                            'scoreFlag' => $scoreClass
                        ];

                        if ( count(array_get($dataFound, 'content')) > 4 )
                        {
                            break;
                        }
                    }
                }
            }

            File::put($verificationFile, json_encode($dataFound));
        }

        $output = get_data_from_file($verificationFile);
        
        if ( ! empty($output['content']) && ! array_has($output, 'content.'.$key.'.detail') )
        {
            $getDetailPeople = $this->javaServerService->search(['subjectId' => array_get($output, "content.{$key}.id")]);

            if ( ! empty($getDetailPeople['results']['content'][0]) )
            {
                $people =  array_get($getDetailPeople, "results.content.0");
                $demographicFields = demographic_fields();

                foreach($people as $keyArr => $valArr)
                {
                    $people[snake_case($keyArr)] = $valArr;
                }

                $address = '';
                if ( ! empty($people['address']) ) $address .= $people['address'];
                if ( ! empty($people['rt']) ) $address .= ' RT. ' . $people['rt'];
                if ( ! empty($people['rw']) ) $address .= ' RW. ' . $people['rw'];
                if ( ! empty($people['village']) ) $address .= ' ' . $people['village'];
                if ( ! empty($people['regent']) ) $address .= ' ' . $people['regent'];
                if ( ! empty($people['country']) ) $address .= ' ' . $people['country'];
                if ( ! empty($people['postalCode']) ) $address .= ' ' . $people['postalCode'];

                foreach($demographicFields as $demoKey => $demoVal)
                {
                    if ( $demoKey == 'status_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'status.id') ? $people['status']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'race' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'personRace') ? array_get($people, 'personRace') : 0;
                        continue;
                    }

                    if ( $demoKey == 'religion_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'religion.id') ? $people['religion']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'education_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'education.id') ? $people['education']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'skin_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'skin.id') ? $people['skin']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'body_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'body.id') ? $people['body']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'head_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'head.id') ? $people['head']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'hair_color_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'hair_color.id') ? $people['hair_color']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'hair_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'hair.id') ? $people['hair']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'face_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'face.id') ? $people['face']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'forehead_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'forehead.id') ? $people['forehead']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'eye_color_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'eye_color.id') ? $people['eye_color']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'eye_irregularity_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'eye_irregularity.id') ? $people['eye_irregularity']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'nose_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'nose.id') ? $people['nose']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'lip_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'lip.id') ? $people['lip']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'tooth_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'tooth.id') ? $people['tooth']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'chin_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'chin.id') ? $people['chin']['id'] : 0;
                        continue;
                    }

                    if ( $demoKey == 'ear_id' )
                    {
                        $demographicFields[$demoKey] = array_has($people, 'ear.id') ? $people['ear']['id'] : 0;
                        continue;
                    }

                    $demographicFields[$demoKey] = array_has($people, $demoKey) ? $people[$demoKey] : '-'; 
                }

                $output['content'][$key]['status'] = array_has($people, 'status.label') ? $people['status']['label'] : '-';
                $output['content'][$key]['full_address'] = check_empty(trim($address));
                $output['content'][$key]['base_url'] = $people['base_url_assets'];
                $output['content'][$key]['detail'] = $demographicFields;
            }
            
            File::put($verificationFile, json_encode($output));
        }

        return $output;
    }

    public function local($key, $fingers)
    {
        $dataFingerMatching = $resultMatching = $peopleMatch = $output = [];

        $verificationFile = session('process.basePath') . DIRECTORY_SEPARATOR . config('ak23.file_data.verification_data');

        foreach ($fingers as $finger => $atts) 
        {
            if ( file_exists(session('process.basePath') . DIRECTORY_SEPARATOR . $finger . ".jpg") )
            {
                $dataFingerMatching['fingers'][] = [
                    'finger_position' => strtoupper($finger),
                    'path' => session('process.basePath') . DIRECTORY_SEPARATOR . $finger . ".jpg"
                ];
            }
        }

        if ( ! file_exists($verificationFile) )
        {
            $response = ApiUtils::matching($dataFingerMatching);

            if ( $response['status'] != 'OK' )
            {
                throw new \Exception("Gagal melakukan identifikasi");
            }

            $output['source'] = 'local';
            $output['content'] = $response['results'];

            if ( ! empty($output['content'] ) )
            {
                foreach($output['content'] as $val)
                {
                    $resultMatching[$val['id']] = $val['score'];
                }

                $peopleMatching = People::whereIn('subject_id', array_keys($resultMatching))->with('status_data')->get()->toArray();

                foreach ($peopleMatching as $peopleKey => $peopleVal)
                {
                    $peopleMatch[$peopleVal['subject_id']] = $peopleMatching[$peopleKey];
                }

                foreach($output['content'] as $ouKey => $ouVal)
                {
                    if ( array_has($peopleMatch, $ouVal['id'] . '.fullname') )
                    {
                        $output['content'][$ouKey]['fullname'] = $peopleMatch[$ouVal['id']]['fullname'];
                    }
                    else
                    {
                        $output['content'][$ouKey]['fullname'] = 'Tidak diketahui';
                    }

                    if ( ! empty($peopleMatch[$ouVal['id']]['status_id']) && array_has($peopleMatch[$ouVal['id']], 'status_data.label') )
                    {
                        $output['content'][$ouKey]['status'] = $peopleMatch[$ouVal['id']]['status_data']['label'];
                    }
                    else
                    {
                        $output['content'][$ouKey]['status'] = '-';
                    }

                    $output['content'][$ouKey]['score'] = $output['content'][$ouKey]['score']/10;
                    $scoreClass = 'bg-red';

                    if ( ! empty($output['content'][$ouKey]['score']) )
                    {
                        if ( $output['content'][$ouKey]['score'] >= 80 )
                        {
                            $scoreClass = 'bg-green';
                        }
                        else if ( $output['content'][$ouKey]['score'] >= 50 && $output['content'][$ouKey]['score'] < 80 )
                        {
                            $scoreClass = 'bg-yellow';
                        }
                    }

                    $output['content'][$ouKey]['scoreFlag'] = $scoreClass;
                }
            }

            File::put($verificationFile, json_encode($output));
        }
        
        $output = get_data_from_file($verificationFile);

        if ( ! empty($output['content']) && ! array_has($output, 'content.'.$key.'.detail') )
        {
            $getDetailPeople = People::where('subject_id', $output['content'][$key]['id'])->first();

            if ( ! is_null($getDetailPeople) )
            {
                $address = '';
                if ( ! empty($getDetailPeople->address) ) $address .= $getDetailPeople->address;
                if ( ! empty($getDetailPeople->rt) ) $address .= ' RT. ' . $getDetailPeople->rt;
                if ( ! empty($getDetailPeople->rw) ) $address .= ' RW. ' . $getDetailPeople->rw;
                if ( ! empty($getDetailPeople->village) ) $address .= ' ' . $getDetailPeople->village;
                if ( ! empty($getDetailPeople->regent) ) $address .= ' ' . $getDetailPeople->regent;
                if ( ! empty($getDetailPeople->country) ) $address .= ' ' . $getDetailPeople->country;
                if ( ! empty($getDetailPeople->postal_code) ) $address .= ' ' . $getDetailPeople->postal_code;

                $output['content'][$key]['full_address'] = check_empty(trim($address));
                $output['content'][$key]['base_url'] = data_url("complete/{$getDetailPeople->demographic_id}");
                $output['content'][$key]['detail'] = $getDetailPeople->toArray();
            }
            
            File::put($verificationFile, json_encode($output));
        }

        return $output;
    }

    public function compareProcess()
    {
        $file = $this->comparePath . DIRECTORY_SEPARATOR . 'process.json';

        if ( File::exists($file) )
        {
            $data = get_data_from_file($file);
            File::delete($file);
            extract($data);
            $path = $this->comparePath . DIRECTORY_SEPARATOR . $subjectId;
            $downloadSrc = config('url.demographicZip') . "/{$id}.zip";
            $localSave = $path . DIRECTORY_SEPARATOR . 'data.zip';

            if ( !File::isDirectory($path) )
            {
                File::makeDirectory($path, 0777);
            }
            else
            {
                File::deleteDirectory($path, true);
            }

            File::put($path . DIRECTORY_SEPARATOR . 'processing.txt', time());

            if ( $source == 'server' && download_file($downloadSrc, $localSave) !== false )
            {
                $demographic = $this->javaServerService->search(['subjectId' => $subjectId]);
                File::put($path . DIRECTORY_SEPARATOR . '_demographic.json', json_encode($demographic));

                $this->zipService->demographicServerExtractor($localSave, $path);

                try
                {
                    foreach ( config('ak23.finger_index') as $finger )
                    {
                        if ( File::exists($path . DIRECTORY_SEPARATOR . "{$finger}.jpg") )
                        {
                            ApiUtils::generateMinutiae([
                                "file_name_left" => $path . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                                "file_name_right" => $path . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                                "file_name_result_left" => $path . DIRECTORY_SEPARATOR . "_minutiae_{$finger}.jpg",
                                "file_name_result_right" =>  $path . DIRECTORY_SEPARATOR . "_minutiae_{$finger}.jpg"
                            ]);
                        }
                    }
                }
                catch(\Exception $e)
                {
                    record_error('Error in generate minutiae in background process (compare PDF)', $e);
                }
            }

            if ( $source == 'local' )
            {
                try
                {
                    foreach ( config('ak23.finger_index') as $finger )
                    {
                        if ( File::exists(config('path.data.complete') . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . "{$finger}.jpg") )
                        {
                            ApiUtils::generateMinutiae([
                                "file_name_left" => config('path.data.complete') . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                                "file_name_right" => config('path.data.complete') . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                                "file_name_result_left" => $path . DIRECTORY_SEPARATOR . "_minutiae_{$finger}.jpg",
                                "file_name_result_right" =>  $path . DIRECTORY_SEPARATOR . "_minutiae_{$finger}.jpg"
                            ]);
                        }
                    }
                }
                catch(\Exception $e)
                {
                    record_error('Error in generate minutiae in background process (compare PDF)', $e);
                }
            }

            File::delete($path . DIRECTORY_SEPARATOR . 'processing.txt');
        }
    }
}
