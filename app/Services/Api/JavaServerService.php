<?php

namespace App\Services\Api;

use App\Utils\ApiUtils;
use App\Repositories\OptionRepository;
use Carbon\Carbon;
use Cache, File;

class JavaServerService
{
    private $optionRepository;

    private $now;

    private $fileToken;

    public function __construct()
    {
        $this->optionRepository = new OptionRepository;

        $this->now = Carbon::now();

        $this->fileToken = storage_path('token-data.json');
    }

    private function requestToken()
    {
        $serialNumber = config('ak23.access') == 'desktop' ? $this->optionRepository->get('com.oxycreative.inafis.ak23.core.models.WorkstationEntity.serialNumber') : 'ak23web';
        $password = config('ak23.access') == 'desktop' ? $this->optionRepository->get('com.oxycreative.inafis.ak23.core.models.WorkstationEntity.secret') : '4k423w3b!@#';
        
        $token = ApiUtils::getToken('client_credentials', $serialNumber, $serialNumber, $password);

        if ( $token['status'] == 'OK' )
        {
            $dataToken = [
                'value' => $token['contents']['token_type'] . ' ' . $token['contents']['access_token']
            ];

            if ( ! is_null(array_get($token, 'contents.expires_in')) )
            {
                $dataToken['expired'] = strtotime($this->now->addSeconds(array_get($token, 'contents.expires_in') - 60)->toDateTimeString());
            }

            File::put($this->fileToken, json_encode($dataToken));
        }
    }

    public function getToken($forced = false)
    {
        if ( ! File::exists($this->fileToken) || $forced )
        {
            $this->requestToken();
        }
        
        $output = get_data_from_file($this->fileToken);

        if ( !is_null(array_get($output, 'expired')) && array_get($output, 'expired') < strtotime($this->now->toDateTimeString()) )
        {
            $this->requestToken();
        }

        return array_get($output, 'value');
    }

    public function search($data)
    {
        $res = [];

        try
        {
            $response = ApiUtils::searchServer($this->getToken(), $data);

            if ( $response->getStatusCode() !== 200 )
            {
                throw new \Exception("Tidak terhubung ke Datacenter");
            }

            $res = json_decode($response->getBody(), TRUE);

            if ( array_get($res, 'status') === 'OK' )
            {
                foreach (array_get($res, 'results.content') as $key => $value) 
                {
                    if ( isset($value['baseUrlAssets']) )
                    {
                        $res['results']['content'][$key]['baseUrlAssets'] = str_replace("\\", "/", $res['results']['content'][$key]['baseUrlAssets']);
                    }
                }
            }
            else
            {
                throw new \Exception("Response status " . $res['status']);
            }

            return $res;
        }
        catch ( \Exception $e )
        {
            record_error('API - Error in search AK-23', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }

            return abort(500, '[btn_back]Gagal meminta data pada server.');
        }
    }

    public function report($startDate, $endDate, $location = null)
    {
        try
        {
            $response = ApiUtils::reportServer($this->getToken(), $startDate, $endDate, $location);

            if ( $response->getStatusCode() !== 200 )
            {
                throw new \Exception("Tidak terhubung ke Datacenter");
            }

            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - Error in report server AK-23', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }

            return abort(500, '[btn_back]Gagal meminta data pada server.');
        }
    }

    public function locations()
    {
        try
        {
            $response = ApiUtils::locations($this->getToken());

            if ( $response->getStatusCode() !== 200 )
            {
                throw new \Exception("Tidak terhubung ke Datacenter");
            }
    
            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - Error in get locations server', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }

            return abort(500, '[btn_back]Gagal meminta data pada server.');
        }
    }

    public function OSInafisSearch($data, $limit = 10000, $offset = 0)
    {
        try
        {
            $response = ApiUtils::OSInafisSearch($this->getToken(), $data, $limit, $offset);

            if ( $response->getReasonPhrase() !== 'OK' )
            {
                return false;
            }
    
            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - Error in OS Inafis Search', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }
        }
    }

    public function OSInafisSearchDetailByExternalId($params)
    {
        try
        {
            $response = ApiUtils::OSInafisSearchDetailByExternalId($this->getToken(), $params);

            if ( $response->getReasonPhrase() !== 'OK' )
            {
                return false;
            }
    
            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - Error in OS Inafis Search detail by external ID', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }
        }
    }

    public function OSInafisGetImages($params)
    {
        try
        {
            $response = ApiUtils::OSInafisGetImages($this->getToken(), $params);

            if ( $response->getReasonPhrase() !== 'OK' )
            {
                return false;
            }
    
            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - Error in OS Inafis Search images by external ID', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }
        }
    }

    public function OSInafisKtpSearchByNIK($nik)
    {
        try 
        {
            $response = ApiUtils::OSInafisKtpSearchByNIK($this->getToken(), $nik);

            if ( $response->getReasonPhrase() !== 'OK' )
            {
                return false;
            }
    
            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - search NIK', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }

            return abort(500, '[btn_back]Gagal mencari data pada server KTP.');
        }
    }

    public function OSInafisKtpSearch($data)
    {
        try
        {
            $response = ApiUtils::OSInafisKtpSearch($this->getToken(), $data);

            if ( $response->getReasonPhrase() !== 'OK' )
            {
                return false;
            }
    
            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - search enquery KTP', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }

            return abort(500, '[btn_back]Gagal mencari data pada server KTP. Server tidak merespon.');
        }
    }

    public function OSInafisLatenSearch($data)
    {
        try
        {
            $response = ApiUtils::OSInafisLatenSearch($this->getToken(), $data);

            if ( $response->getReasonPhrase() !== 'OK' )
            {
                return false;
            }
    
            return json_decode($response->getBody(), TRUE);
        }
        catch ( \Exception $e )
        {
            record_error('API - search laten', $e);

            if (strpos($e->getMessage(), 'invalid_token') !== false)
            {
                $this->getToken(true); // request new token
                return abort(500, '[btn_back]Token tidak valid. Silahkan ulangi lagi.');
            }
        }
    }
}