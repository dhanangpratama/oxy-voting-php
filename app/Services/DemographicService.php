<?php

namespace App\Services;

use Image, File;
use App\Repositories\DemographicRepository;

class DemographicService
{
    private $demographicRepository;
    
    public function __construct(DemographicRepository $demographicRepository)
    {
        $this->demographicRepository = $demographicRepository;
    }

    public function paginate($data)
    {
        return $this->demographicRepository->paginate($data);
    }

    public function searchNik($nik)
    {
        return $this->demographicRepository->findByNik($nik);
    }

    public function findById($id)
    {
        return $this->demographicRepository->findById($id);
    }

    public function create()
    {
        $data = [];
        
        $data = session('demographic.data');
        $data['event_id'] = session('event_data.id');

        $createdData = $this->demographicRepository->create($data);
        session(['demographic.db_id' => array_get($createdData, 'data.demographic_id')]);

        $dir = config('path.data.complete') . DIRECTORY_SEPARATOR . array_get($createdData, 'data.demographic_id');
        $dirQueue = config('path.data.complete_queue') . DIRECTORY_SEPARATOR . array_get($createdData, 'data.demographic_queue_id');

        $this->copyDir($dir, $dirQueue);
        
        return $createdData;
    }

    public function update()
    {
        $data = [];
        
        $data = session('demographic.data');
        $data['event_id'] = session('event_data.id');
        array_forget($data, 'id');

        $updatedData = $this->demographicRepository->updateById(session('demographic.db_id'), $data);

        $dir = config('path.data.complete') . DIRECTORY_SEPARATOR . session('demographic.db_id');
        $dirQueue = config('path.data.complete_queue') . DIRECTORY_SEPARATOR . array_get($updatedData, 'data.demographic_queue_id');

        $this->copyDir($dir, $dirQueue);
        
        return $updatedData;
    }

    public function copyDir($dir, $dirQueue)
    {
        if ( !File::isDirectory($dir) )
        {
            File::makeDirectory($dir);
        }
        else
        {
            File::deleteDirectory($dir, true);
        }

        if ( !File::isDirectory($dirQueue) )
        {
            File::makeDirectory($dirQueue);
        }
        else
        {
            File::deleteDirectory($dirQueue, true);
        }

        File::copyDirectory(config('path.data.demographic'), $dir);
        File::copyDirectory(config('path.data.demographic'), $dirQueue);
    }
}