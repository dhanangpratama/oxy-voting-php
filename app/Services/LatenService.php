<?php

namespace App\Services;

use File, Image, Auth, Log;
use Illuminate\Http\Request;
use App\Utils\ApiUtils;
use App\Utils\ImageUtils;
use App\Services\ActivityService;
use App\Services\Api\JavaServerService;

class LatenService 
{
    private $activityService;
    private $request;
    private $path;
    private $requestedDir;
    private $javaServerService;

    public function __construct(Request $request, ActivityService $activityService, JavaServerService $javaServerService)
    {
        $this->activityService = $activityService;
        $this->request = $request;
        $this->javaServerService = $javaServerService;
        $this->path = config('path.data.laten');
        $this->requestedDir = $this->path . DIRECTORY_SEPARATOR . 'requested' . DIRECTORY_SEPARATOR;
    }

    public function search($request)
    {
        if ( empty($request['fingerFile']) )
        {
            return ['status' => 'ERROR', 'message' => 'Mohon untuk memilih file terlebih dahulu.'];
        }

        // $fingerType = $request->get('fingerType', 0);
        if ( File::isDirectory($this->path) )
        {
            File::deleteDirectory($this->path, true);
        }

        File::makeDirectory($this->path . DIRECTORY_SEPARATOR . 'requested');

        if ( ! File::isDirectory($this->path . DIRECTORY_SEPARATOR . 'requested') )
        {
            return ['status' => 'ERROR', 'message' => 'Aplikasi gagal melakukan pemrosesan gambar. Mohon untuk mengulang kembali.'];
        }

        if ( ! File::isDirectory($this->requestedDir . 'minutiae') )
        {
            File::makeDirectory($this->requestedDir . 'minutiae');
        }

        if ( ! empty($request['faceFile']) )
        {
            File::put($this->requestedDir . 'file-face.' . $request['faceFileType'], File::get($request['faceFile']));
        }

        if ( ! empty($request['fingerFile']) )
        {
            File::put($this->requestedDir . 'file.' . $request['fingerFileType'], File::get($request['fingerFile']));
 
            if ( $request['fingerFileType'] == 'png' )
            {
                Image::make($this->requestedDir . 'file.' . $request['fingerFileType'])->save($this->requestedDir . 'file.jpg');
            }

            if ( $request['fingerFileType'] == 'wsq' )
            {
                ImageUtils::saveWsqAsJpg($this->requestedDir . 'file.wsq', $this->requestedDir . 'file.jpg');
            }

            ApiUtils::generateMinutiae([
                "file_name_left" => $this->requestedDir . 'file.jpg',
                "file_name_right" => $this->requestedDir . 'file.jpg',
                "file_name_result_left" => $this->requestedDir . 'minutiae' . DIRECTORY_SEPARATOR . 'file.jpg',
                "file_name_result_right" =>  $this->requestedDir . 'minutiae' . DIRECTORY_SEPARATOR . 'file.jpg'
            ]);
        }

        $data = [
            [
                'name'     => 'fingerprint_' . $request['fingerType'],
                'contents' =>  fopen($this->requestedDir . 'file.' . $request['fingerFileType'], 'r')
            ]
        ];

        try
        {
            return $this->doProcess($data, 'upload');
        }
        catch(\Exception $e)
        {
            record_error('Error processing OS Inafis API.', $e, $this->request);

            $description = "Ma'af tidak dapat memproses permintaan. Kemungkinan server offline.";

            if ( strpos($e->getMEssage(), 'Failed to connect') !== false )
            {
                $description = 'Tidak dapat terhubung ke server. Mohon untuk mencoba beberapa saat lagi.';
            }

            if ( strpos($e->getMEssage(), 'timing out') !== false )
            {
                $description = 'Respon server timeout. Mohon untuk mencoba beberapa saat lagi.';
            }

            return ['status' => 'ERROR', 'message' => $description];
        }
    }

    private function createLiveImages($dataImages)
    {
        if ( ! empty($dataImages) )
        {
            foreach ($dataImages as $key => $image)
            {
                $filename = $this->requestedDir . DIRECTORY_SEPARATOR . str_replace('_', '-', str_replace('_finger', '', $key)) . ".jpg";

                if ( ! empty($image) )
                {
                    try
                    {
                        Image::make($image)->save($filename);
                    }
                    catch ( \Exception $e )
                    {
                        record_error('Error in save image', $e, new Request);
                    }
                }
            }
        }
    }

    public function searchLive($dataImages)
    {
        $data = [];

        $this->createLiveImages($dataImages);

        $files = File::glob($this->requestedDir . '*.jpg');

        if ( ! File::isDirectory($this->requestedDir . 'minutiae') )
        {
            File::makeDirectory($this->requestedDir . 'minutiae');
        }

        if ( count($files) > 0 )
        {
            foreach ($files as $file) 
            {
                if ( File::exists($file) )
                {
                    $keyFinger = str_replace('.jpg', '', str_replace('-', '_', basename($file)));

                    $fingerIndex = config('ak23.os_inafis_finger_key');

                    $fileRequestedInfo = pathinfo($this->requestedDir .  basename($file));

                    ApiUtils::generateMinutiae([
                        "file_name_left" => $file,
                        "file_name_right" => $file,
                        "file_name_result_left" => $this->requestedDir . 'minutiae' . DIRECTORY_SEPARATOR . basename($file),
                        "file_name_result_right" => $this->requestedDir . 'minutiae' . DIRECTORY_SEPARATOR . basename($file)
                    ]);

                    $data[] = [
                        'name' => 'fingerprint_' . array_get(config('ak23.os_inafis_finger_key'), "{$keyFinger}.key"),
                        'contents' => fopen($file, 'r')
                    ];
                }
            }

            try
            {
                return $this->doProcess($data, 'live');
            }
            catch(\Exception $e)
            {
                record_error('Error processing OS Inafis API.', $e, $this->request);

                $description = 'Mohon untuk mencoba kembali.';

                if ( strpos($e->getMEssage(), 'Failed to connect') !== false )
                {
                    $description = 'Tidak dapat terhubung ke server. Mohon untuk mencoba beberapa saat lagi.';
                }

                if ( strpos($e->getMEssage(), 'NullPointerException') !== false )
                {
                    $description = 'Tidak dapat terhubung ke server. OS Inafis server tidak merespon.';
                }

                if ( strpos($e->getMEssage(), 'timing out') !== false )
                {
                    $description = 'Respon server timeout. Mohon untuk mencoba beberapa saat lagi.';
                }

                return ['status' => 'ERROR', 'message' => $description];
            }
        }

        return ['status' => 'ERROR', 'message' => 'Berkas jari tidak ditemukan.'];
    }

    protected function doProcess($data, $type = 'upload')
    {
        $candidates = $resultsLog = [];
        $firstData = '';
        $osFinger = config('ak23.os_inafis_finger_key');

        $response = $this->javaServerService->OSInafisLatenSearch($data);

        if ( array_get($response, 'response.code') !== 0 )
        {
            Log::error('Error in return JSON matching.', ['message' => array_get($response, 'response.message')]);
            return ['status' => 'ERROR', 'message' => array_get($response, 'response.message')];
        }

        if ( array_has($response, 'candidates') )
        {
            $candidates = array_get($response, 'candidates');

            if ( count($candidates) > 0 )
            {
                $countCanditates = 1;
                foreach ($candidates as $key => $candidate) 
                {
                    if ($countCanditates <= 5)
                    {
                        if ( ! empty($candidate['externalId']) )
                        {
                            if ( !empty($candidate['score']) )
                            {
                                $candidates[$key]['scoreFlag'] = '';

                                if ( ($candidate['score']/10) >= 80 )
                                {
                                    $candidates[$key]['scoreFlag'] = 'bg-green';
                                }
                                else if ( ($candidate['score']/10) >= 50 && ($candidate['score']/10) < 80 )
                                {
                                    $candidates[$key]['scoreFlag'] = 'bg-yellow';
                                }
                                else
                                {
                                    $candidates[$key]['scoreFlag'] = 'bg-red';
                                }
                            }

                            $resultsLog[] = $candidate['externalId'].' - '.($candidate['score']/10);
                        }

                        $countCanditates++;
                    }
                    else
                    {
                        break;
                    }
                }

                $this->activityService->log('laten-search', 'Melakukan pencarian data. ID yang diperoleh sebagai berikut: '.implode(', ', $resultsLog), Auth::id());

                $candidates['type'] = $type;

                file_put_contents($this->path.DIRECTORY_SEPARATOR.'_candidates.json', json_encode($candidates));

                return ['status' => 'OK'];
            }

            return ['status' => 'NOTFOUND'];
        }

        return ['status' => 'NOTFOUND'];
    }

    public function processDetail($key)
    {
        $latenPath = $this->path.DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR;
        $candidatesDataFile = $this->path.DIRECTORY_SEPARATOR.'_candidates.json';
        $candidatesData = get_data_from_file($candidatesDataFile);
        $foundedFingers = [];

        if ( count($candidatesData) > 1 && ! array_has($candidatesData[$key], 'detail') || empty($candidatesData[$key]['detail']) )
        {
            if ( ! empty($candidatesData[$key]['externalId']) )
            {
                $externalId = $candidatesData[$key]['externalId'];

                try
                {
                    if ( strpos(array_get($candidatesData, "{$key}.source"), 'AK-23') !== false )
                    {
                        if ( strpos($externalId, 'AK23_') !== false )
                        {
                            $externalId = trim(str_replace('AK23_', '', $externalId));
                        }

                        $detailData = $this->javaServerService->search(['subjectId' => $externalId]);
                        $candidatesData[$key]['detail'] = array_has($detailData, 'results.content.0') ? $detailData['results']['content'][0] : null; 
                    }
                }
                catch ( \Exception $e )
                {
                    record_error('Request detail data error', $e, $this->request);
                    return ['status' => 'ERROR', 'message' => 'Gagal mengambil data detail'];
                }

                file_put_contents($candidatesDataFile, json_encode($candidatesData));

                $candidatesData = get_data_from_file($candidatesDataFile);
            }
            else
            {
                return ['status' => 'ERROR', 'message' => 'Data tidak memiliki NIK'];
            }
        }

        /* 
         * =====================================
         * Processing images
         * =====================================
         */

        if ( ! File::isDirectory($this->path . DIRECTORY_SEPARATOR . $key) )
        {
            File::makeDirectory($this->path . DIRECTORY_SEPARATOR . $key);
        }

        if ( ! File::exists($this->path . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . 'done.txt') )
        {
            if ( strpos(array_get($candidatesData, "{$key}.source"), 'AK-23') !== false )
            {
                $this->processImagesAk23($key, $candidatesData);
            }
            else
            {
                $this->processImages($key, $candidatesData);
            }

            File::put($this->path . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . 'done.txt', time());
        }

        return ['status' => 'OK'];
    }

    private function processImagesAk23($key, $candidatesData)
    {
        $srcFace = $candidatesData[$key]['detail']['baseUrlAssets'] . 'face-front.jpg';
        $destFace = $this->path . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . 'photo.jpg';

        if ( checkRemoteFile($srcFace, false) )
        {
            try
            {
                file_put_contents($destFace, ak23_get_contents($srcFace));
            }
            catch(\Exception $e)
            {
                // TO DO catch error logs
            }
        }

        foreach (config('ak23.finger_index') as $fingerName) 
        {
            $src = $candidatesData[$key]['detail']['baseUrlAssets'] . $fingerName . '.jpg';
            $removeFingerString = trim(str_replace('_finger', '', $fingerName));
            $saveFileName = $this->path . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . str_replace('_', '-', $removeFingerString) . '.jpg';
            
            if ( checkRemoteFile($src, false) )
            {
                try
                {
                    file_put_contents($saveFileName, ak23_get_contents($src));

                    ApiUtils::generateMinutiae([
                        "file_name_left" => $saveFileName,
                        "file_name_right" => $saveFileName,
                        "file_name_result_left" => str_replace('.jpg','', $saveFileName) . '-minutiae.jpg',
                        "file_name_result_right" =>  str_replace('.jpg','', $saveFileName) . '-minutiae.jpg'
                    ]);
                }
                catch(\Exception $e)
                {
                    // TO DO catch error logs
                }
            }
        }
    }

    public function processImages($key, $candidatesData)
    {
        $destFace = $this->path . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . 'photo.jpg';

        try
        {
            $data = $this->javaServerService->OSInafisGetImages([
                'external_id' => array_get($candidatesData, "{$key}.externalId"),
                'source' => array_get($candidatesData, "{$key}.source")
            ]);

            if ( !empty(array_get($data, 'images')) && is_array(array_get($data, 'images')) )
            {
                foreach ( array_get($data, 'images') as $keyData => $value )
                {
                    $keyData = strtolower($keyData);
                    $saveFileName = $this->path . DIRECTORY_SEPARATOR . $key . DIRECTORY_SEPARATOR . strtolower(str_replace('_', '-', $keyData) . '.jpg');

                    if ( !empty($value) )
                    {
                        if ( $keyData == 'face' )
                        {
                            Image::make($value)->save($destFace);
                        }
                        else
                        {
                            Image::make($value)->save($saveFileName);

                            ApiUtils::generateMinutiae([
                                "file_name_left" => $saveFileName,
                                "file_name_right" => $saveFileName,
                                "file_name_result_left" => str_replace('.jpg','', $saveFileName) . '-minutiae.jpg',
                                "file_name_result_right" =>  str_replace('.jpg','', $saveFileName) . '-minutiae.jpg'
                            ]);
                        }
                    }
                }
            }
        }
        catch(\Exception $e)
        {
            // TO DO catch error logs
        }
    }
}
