<?php

namespace App\Services;

use File, Image;
use Illuminate\Http\Request;
use App\Utils\ApiUtils;
use App\Utils\ImageUtils;

class BiometricService 
{
    public function patternClassification($path)
    {
        $dataUpload = [];
        $fingerExcludes = ['flat_right_four_fingers.jpg', 'flat_left_four_fingers.jpg', 'flat_thumbs.jpg', 'front.jpg', 'front-small.jpg', 'back.jpg', 'back-small.jpg', 'face-front.jpg', 'face-left.jpg', 'face-right.jpg'];
        $fingerFile = File::glob($path.DIRECTORY_SEPARATOR.'*.{jpg}', GLOB_BRACE);
        $biometricData = get_data_from_file($path . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'));

        if ( ! empty($fingerFile) )
        {
            foreach($fingerFile as $finger)
            {
                // No need flat in card due to flat is pattern manually
                if ( in_array(basename($finger), $fingerExcludes) )
                {
                    continue;
                }

                $dataUpload['fingers'][] = [
                    'finger_position' => strtoupper(rtrim(basename($finger), '.jpg')),
                    'path' => $finger
                ];
            }

            $classification = false;
            $tryNumber = 1;
    
            do 
            {
                if ( $tryNumber < 4 )
                {
                    $henry = ApiUtils::patternClassification($dataUpload);
                    
                    if ( array_get($henry, 'status') == 'OK' && ! empty(array_get($henry, 'results.fingers')) )
                    {
                        $this->parsing($path, array_get($henry, 'results.fingers'), $biometricData);
    
                        $classification = true;
                    }
    
                    $tryNumber++;
                }
                else
                {
                    $classification = true;
                }
            } 
            while ( !$classification );
        }
    }

    private function parsing($path, $fingers, $biometricData)
    {
        if ( ! empty($fingers) )
        {
            foreach ($fingers as $data)
            {
                if ( in_array($data['pattern_classification'], config('ak23.whorl_map')) )
                {
                    $data['pattern_classification'] = 'WHORL';
                }
                else if ( in_array($data['pattern_classification'], config('ak23.ulnar_map')) )
                {
                    $data['pattern_classification'] = 'ULNAR_LOOP';
                }

                $biometricData['fingers'][strtolower($data['finger_position'])] = $data;
            }
        }

        File::put($path . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), json_encode($biometricData));
    }
}
