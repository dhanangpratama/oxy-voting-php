<?php

namespace App\Services;

use Log, File;

class HardwareService
{
    public function check()
    {
        $path = config('path.hardwareStatus');
        $dataStatus = [];

        if ( File::exists($path) && strpos(basename($path), '.txt') !== false && ! empty($data = file($path, FILE_IGNORE_NEW_LINES)) )
        {
            foreach($data as $value)
            {
                $expValue = explode(',', $value);

                if ( strtolower($expValue[1]) == 'fingerprintreader' ) $expValue[1] = 'Fingerprint Scanner';

                $dataStatus[$expValue[1]] = $expValue[3];
            }

            return $dataStatus;
        }
    }

    /**
     * Get disconnected hardware
     *
     * @return array
     */
    public function getDisconnectedHardware()
    {
        $hardware = array();
        $dataStatus = $this->check();

        if ( !is_null($dataStatus) )
        {
            foreach($dataStatus as $hardwareName => $isConnected)
            {
                if($isConnected == "0")
                {
                    $hardware[] = $hardwareName;
                }
            }
        }

        return $hardware;
    }
}