<?php

namespace App\Services;

use File, Image, Auth;
use Illuminate\Http\Request;
use App\Utils\ApiUtils;
use App\Utils\ImageUtils;
use App\Services\BiometricService;

class LiveService 
{
    private $biometricService;

    private $fileData;

    public function __construct()
    {
        $this->biometricService = new BiometricService;

        $this->fileData = config('path.data.tmp') . DIRECTORY_SEPARATOR . '_demographic_data.json';
    }

    public function createImage($dataImages)
    {
        $fingerExcludes = ['flat_right_four_fingers', 'flat_left_four_fingers', 'flat_thumbs'];

        if ( ! empty($dataImages) )
        {
            foreach ($dataImages as $key => $image)
            {
                $filename = session('process.basePath') . DIRECTORY_SEPARATOR . $key . ".jpg";

                if ( ! empty($image) )
                {
                    try
                    {
                        Image::make($image)->save($filename);
                    }
                    catch ( \Exception $e )
                    {
                        record_error('Error in save image', $e, new Request);
                    }
                }
            }
        }

        if ( session('process.mode') != 'edit' )
        {
            flag('enrollment_process_done');
        }

        File::put(session('process.basePath') . DIRECTORY_SEPARATOR . 'pleaseprocesslive.txt', time());

        // return ['status' => 'OK'];
    }

    public function uploadPhoto($dataImages)
    {
        try
        {
            Image::make(stripslashes($dataImages['path']))->save(session('process.basePath') . DIRECTORY_SEPARATOR . $dataImages['position']);
        }
        catch ( \Exception $e )
        {
            record_error('Error in upload live scan photo', $e, new Request);
            return ['status' => 'ERROR', 'message' => 'Gagal mengunggah foto.'];
        }

        return ['status' => 'OK', 'url' => session('process.baseUrl') . '/' . $dataImages['position']];
    }

    public function getDemographicData($getDataTemp = false)
    {
        if ( File::exists($this->fileData) && $getDataTemp )
        {
            $data = File::get($this->fileData);
            File::delete($this->fileData);
        }
        else
        {
            $demographicData = demographic_fields();

            if ( array_has($demographicData, 'sex') )
            {
                $demographicData['sex'] = 'm';
            }

            $data = json_encode($demographicData);
        }
        
        return $data;
    }

    public function doProcess()
    {
        $livePath = config('path.data.live');
        $pleaseProcessFile = $livePath . DIRECTORY_SEPARATOR . 'pleaseprocesslive.txt';
        $minutiaePath = config('path.data.tmp_minutiae');

        if ( File::exists($pleaseProcessFile) )
        {
            File::delete($pleaseProcessFile);

            try
            {
                $this->biometricService->patternClassification($livePath); // Doing pattern classification
            }
            catch ( \Exception $e )
            {
                record_error('Error in generate pattern classification', $e, new Request);
            }

            if ( !File::isDirectory($minutiaePath) )
            {
                File::makeDirectory($minutiaePath, 0777);
            }
            else
            {
                File::deleteDirectory($minutiaePath, true);
            }

            foreach ( config('ak23.finger_index') as $finger )
            {
                if ( File::exists($livePath . DIRECTORY_SEPARATOR . "{$finger}.jpg") )
                {
                    ApiUtils::generateMinutiae([
                        "file_name_left" => $livePath . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                        "file_name_right" => $livePath . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                        "file_name_result_left" => $minutiaePath . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                        "file_name_result_right" =>  $minutiaePath . DIRECTORY_SEPARATOR . "{$finger}.jpg"
                    ]);
                }
            }
        }
    }
}
