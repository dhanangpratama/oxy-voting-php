<?php

namespace App\Services;

use App\Models\ActivityLog;
use App\Repositories\EventRepository;
use Carbon\Carbon;
use Cache, Auth;

class EventService
{
    protected $eventRepository;

    public function __construct(EventRepository $eventRepository)
    {
        $this->eventRepository = $eventRepository;
    }

    public function getAll()
    {
        return $this->eventRepository->all();
    }

    public function autoCompletion()
    {
        $events =  $this->eventRepository->all();

        $html = '[';

        foreach ($events as $key => $event)
        {
            $html .= '"'.$event.'", ';
        }

        return rtrim(trim($html), ',') . ']';
    }

    public function create($name)
    {
        $data = [
            'name' => $name,
            'created_by' => Auth::id()
        ];

        Cache::forget('events_all');

        return $this->eventRepository->create($data);
    }
}