<?php

namespace App\Services;

use App\Models\Skin;
use App\Models\Body;
use App\Models\Head;
use App\Models\HairColor;
use App\Models\Hair;
use App\Models\Face;
use App\Models\Forehead;
use App\Models\EyeColor;
use App\Models\EyeIrregularity;
use App\Models\Nose;
use App\Models\Lip;
use App\Models\Tooth;
use App\Models\Chin;
use App\Models\Ear;
use App\Models\Religion;
use App\Models\Education;
use App\Models\Status;
use App\Models\Role;
use App\Models\Location;
use DB;
use Carbon\Carbon;
use Cache;

class DataService
{
     public function religion()
     {
          return Cache::remember('data_religion', 720, function()
          {
               return Religion::all();
          });
     }

     public function education()
     {
          return Cache::remember('data_education', 720, function()
          {
               return Education::all();
          });
     }

     public function status()
     {
          return Cache::remember('data_status', 720, function()
          {
               return Status::all();
          });
     }

     public function statusArray()
     {
        return Cache::remember('data_status_array', 720, function()
        {
            $status = [];

            foreach ( $this->status()->toArray() as $dataStatus )
            {
                $status[$dataStatus['status_id']] = $dataStatus['label'];
            }

            return $status;
        });
     }

     public function skin()
     {
          return Cache::remember('data_skin', 720, function()
          {
               return Skin::all();
          });
     }

     public function body()
     {
          return Cache::remember('data_body', 720, function()
          {
               return Body::all();
          });
     }

     public function head()
     {
          return Cache::remember('data_head', 720, function()
          {
               return Head::all();
          });
     }

     public function hairColor()
     {
          return Cache::remember('data_hair_color', 720, function()
          {
               return HairColor::all();
          });
     }

     public function hair()
     {
          return Cache::remember('data_hair', 720, function()
          {
               return Hair::all();
          });
     }

     public function face()
     {
          return Cache::remember('data_face', 720, function()
          {
               return Face::all();
          });
     }

     public function forehead()
     {
          return Cache::remember('data_forehead', 720, function()
          {
               return Forehead::all();
          });
     }

     public function eyeColor()
     {
          return Cache::remember('data_eye_color', 720, function()
          {
               return EyeColor::all();
          });
     }

     public function eyeIrregularity()
     {
          return Cache::remember('data_eye_irregularity', 720, function()
          {
               return EyeIrregularity::all();
          });
     }

     public function nose()
     {
          return Cache::remember('data_nose', 720, function()
          {
               return Nose::all();
          });
     }

     public function lip()
     {
          return Cache::remember('data_lip', 720, function()
          {
               return Lip::all();
          });
     }

     public function tooth()
     {
          return Cache::remember('data_tooth', 720, function()
          {
               return Tooth::all();
          });
     }

     public function chin()
     {
          return Cache::remember('data_chin', 720, function()
          {
               return Chin::all();
          });
     }

     public function ear()
     {
          return Cache::remember('data_ear', 720, function()
          {
               return Ear::all();
          });
     }

     public function roles()
     {
        
        return Cache::remember('data_roles', 720, function()
        {
            $roles = [];
            
            foreach(Role::all() as $role)
            {
                $roles[$role->id] = $role->display_name;
            }

            return $roles;
        });
     }

     public function locations()
     {
        
        return Cache::remember('data_locations', 720, function()
        {
            $locations = [];
            
            foreach(Location::all() as $location)
            {
                $locations[$location->location_id] = $location->name;
            }

            return $locations;
        });
     }

     public function activeMessages()
     {
        return Cache::remember('message_broadcast', 15, function()
        { 
            return DB::table('message_broadcast')->whereRaw("'".Carbon::now()->toDateString() . "' BETWEEN DATE(start_date) AND DATE(end_date)")->get();
        });
     }
}