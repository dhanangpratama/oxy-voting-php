<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\ActivityLog;
use DB;

class DashboardService 
{
    public function chartCount()
    {
        $dbCardData = $dbLiveData = [];
        $cardData = $liveData = $label = '[';
        $to = Carbon::now();
        $from = Carbon::now()->subDays(30);

        $queryCard = DB::select('SELECT DATE_FORMAT(created_at, "%d-%m-%Y") AS ak_date, count(created_at) AS total from tbl_demographics WHERE enrollment_type="card" AND DATE(created_at) BETWEEN "'.$from->format('Y-m-d').'" AND "'.$to->format('Y-m-d').'" GROUP BY DATE(created_at)');

        $queryLive = DB::select('SELECT DATE_FORMAT(created_at, "%d-%m-%Y") AS ak_date, count(created_at) AS total from tbl_demographics WHERE enrollment_type="live" AND DATE(created_at) BETWEEN "'.$from->format('Y-m-d').'" AND "'.$to->format('Y-m-d').'" GROUP BY DATE(created_at)');

        foreach( $queryLive as $liveVal )
        {
            $dbLiveData[$liveVal->ak_date] = $liveVal->total;
        }

        foreach( $queryCard as $cardVal )
        {
            $dbCardData[$cardVal->ak_date] = $cardVal->total;
        }

        foreach ($this->generateDateRange($from, $to) as $key => $date)
        {
            if ( array_has($dbLiveData, $date) )
            {
                $liveData .= '['.$key.', '.$dbLiveData[$date].'],';
            }
            else
            {
                $liveData .= '['.$key.', 0],';
            }

            if ( array_has($dbCardData, $date) )
            {
                $cardData .= '['.$key.', '.$dbCardData[$date].'],';
            }
            else
            {
                $cardData .= '['.$key.', 0],';
            }

            $label .= '['.$key.', "'.date('d', strtotime($date)).'"],';
        }

        $liveData = rtrim($liveData, ',');
        $liveData .= ']';

        $cardData = rtrim($cardData, ',');
        $cardData .= ']';

        $label = rtrim($label, ',');
        $label .= ']';

        return ['live' => $liveData, 'card' => $cardData, 'label' => $label];
    }

    public function getActivityLogs($limit = 10)
    {
        $logs = new ActivityLog;

        return $logs->with('user')->limit($limit)->orderby('activity_log_id', 'desc')->get();
    }

    public function generateDateRange(Carbon $start_date, Carbon $end_date)
    {
        $dates = [];
    
        for ( $date = $start_date; $date->lte($end_date); $date->addDay() ) 
        {
            $dates[] = $date->format('d-m-Y');
        }
    
        return $dates;
    }
}
