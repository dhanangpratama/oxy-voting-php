<?php

namespace App\Services;

use App\Models\ActivityLog;

use App\Repositories\OptionRepository;

use Carbon\Carbon;

class ActivityService
{
    public function log($location = null, $description = null, $userId = null)
    {
        $option = new OptionRepository;
        
        ActivityLog::create([
            'location' => $location,
            'description' => $description,
            'user_id' => $userId,
            'workstation_id' => $option->get('com.oxycreative.inafis.ak23.core.models.WorkstationEntity.id'),
            'ip_address' => session('ip_public'),
            'created_at' => Carbon::now()->toDateTimeString()
        ]);
    }
}