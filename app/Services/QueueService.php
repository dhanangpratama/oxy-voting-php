<?php

namespace App\Services;

use App\Models\Queue;

use Carbon\Carbon;
use Log;

class QueueService
{
    public function create($tableName = null, $fieldIdName = null, $fieldIdValue = null, $syncStatus = null)
    {
        try
        {
            return Queue::create([
                'table_name' => $tableName,
                'field_id_name' => $fieldIdName,
                'field_id_value' => $fieldIdValue,
                'sync_status' => $syncStatus,
                'created_date' => Carbon::now()->toDateTimeString()
            ]);
        }
        catch (\Exception $e)
        {
            Log::error('Error insert in table bio sync', $e->getMessage());
            return false;
        }
    }
}