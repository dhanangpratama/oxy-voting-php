<?php

namespace App\Services;

class FileExplorerService
{
	// $dir    = 'E:\\';

	public function generate($dir)
	{
		$files = array();
		
		foreach(scandir($dir) as $f) {
		
			if(!$f || $f[0] == '.') {
				continue; // Ignore hidden files
			}

			if(is_dir($dir . '/' . $f)) {
				echo '<ul><li class="folder">'.$f.'<ul>';
				generate($dir . '/' . $f);
				echo '</ul></li></ul>';
			}
			
			else {

				// It is a file
				
				$arrayExt = explode(".", $f);
				if(count($arrayExt) > 1)
					$ext = $arrayExt[1];
				else
					$ext = '';
					
				if(in_array($ext, array('gif', 'png', 'jpg')))
				{
					echo '<li class="file">'.$f.'</li>';
				}
			}
		}
	}
}