<?php

namespace App\Services;

use File;

class CardTemplateService 
{
    private $jsonCardDefaultTemplate;
    private $cardDefaultTemplate;
    private $path;
    private $cardTemplatePath;

    public function __construct()
    {
        $this->jsonCardDefaultTemplate = '{
            "right_thumb": {
                "rolled_crop_area": {
                    "w": 885,
                    "h": 885,
                    "x": 267,
                    "y": 1375
                },
                "flat_crop_area": {
                    "w": 700,
                    "h": 427,
                    "x": 582,
                    "y": 776
                }
            },
            "right_index_finger": {
                "rolled_crop_area": {
                    "w": 812,
                    "h": 812,
                    "x": 1177,
                    "y": 1375
                },
                "flat_crop_area": {
                    "w": 32,
                    "h": 431,
                    "x": 351,
                    "y": 636
                }
            },
            "right_middle_finger": {
                "rolled_crop_area": {
                    "w": 808,
                    "h": 808,
                    "x": 2017,
                    "y": 1375
                },
                "flat_crop_area": {
                    "w": 341,
                    "h": 137,
                    "x": 385,
                    "y": 601
                }
            },
            "right_ring_finger": {
                "rolled_crop_area": {
                    "w": 819,
                    "h": 819,
                    "x": 2846,
                    "y": 1378
                },
                "flat_crop_area": {
                    "w": 740,
                    "h": 290,
                    "x": 388,
                    "y": 588
                }
            },
            "right_little_finger": {
                "rolled_crop_area": {
                    "w": 842,
                    "h": 842,
                    "x": 3686,
                    "y": 1378
                },
                "flat_crop_area": {
                    "w": 1150,
                    "h": 863,
                    "x": 406,
                    "y": 604
                }
            },
            "left_thumb": {
                "rolled_crop_area": {
                    "w": 892,
                    "h": 892,
                    "x": 259,
                    "y": 2288
                },
                "flat_crop_area": {
                    "w": 78,
                    "h": 387,
                    "x": 602,
                    "y": 0
                }
            },
            "left_index_finger": {
                "rolled_crop_area": {
                    "w": 817,
                    "h": 817,
                    "x": 1177,
                    "y": 2291
                },
                "flat_crop_area": {
                    "w": 1250,
                    "h": 599,
                    "x": 381,
                    "y": 0
                }
            },
            "left_middle_finger": {
                "rolled_crop_area": {
                    "w": 808,
                    "h": 808,
                    "x": 2014,
                    "y": 2291
                },
                "flat_crop_area": {
                    "w": 893,
                    "h": 260,
                    "x": 452,
                    "y": 0
                }
            },
            "left_ring_finger": {
                "rolled_crop_area": {
                    "w": 818,
                    "h": 818,
                    "x": 2843,
                    "y": 2291
                },
                "flat_crop_area": {
                    "w": 461,
                    "h": 329,
                    "x": 495,
                    "y": 0
                }
            },
            "left_little_finger": {
                "rolled_crop_area": {
                    "w": 837,
                    "h": 837,
                    "x": 3687,
                    "y": 2296
                },
                "flat_crop_area": {
                    "w": 0,
                    "h": 812,
                    "x": 408,
                    "y": 0
                }
            }
        }';
        
        $this->cardDefaultTemplate = json_decode($this->jsonCardDefaultTemplate, true);

        $this->path = config('path.data.base');

        $this->cardTemplatePath = $this->path . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'card_template' . DIRECTORY_SEPARATOR;

        if ( config('ak23.access') == 'desktop' && File::isDirectory(config('path.data.base')) )
        {
            $this->checkDefault();
        }
    }

    public function checkDefault()
    {
        $configPath = $this->path . DIRECTORY_SEPARATOR . 'config';
        $cardTemplatePath = $configPath . DIRECTORY_SEPARATOR . 'card_template';
        $cardDefaultFile = $cardTemplatePath . DIRECTORY_SEPARATOR . 'default.json';

        if ( !File::isDirectory($configPath) )
        {
            File::makeDirectory($configPath);
        }

        if ( !File::isDirectory($cardTemplatePath) )
        {
            File::makeDirectory($cardTemplatePath);
        }

        if ( !File::exists($cardDefaultFile) )
        {
            File::put($cardDefaultFile, $this->jsonCardDefaultTemplate);
        }

        return $this;
    }

    public function loadTemplate()
    {
        $loadTemplate = 'default';

        return json_decode(File::get($this->cardTemplatePath . $loadTemplate . '.json'), true);
    }
}