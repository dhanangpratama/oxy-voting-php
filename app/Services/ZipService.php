<?php

namespace App\Services;
use Log, File;
use App\Utils\ApiUtils;

class ZipService
{
    public function demographicServerExtractor($src, $dest)
    {
        if ( File::exists($src) && File::isDirectory($dest) )
        {
            $process = ApiUtils::extractZip($src, $dest);

            if ( array_get($process, 'status') != 'OK' )
            {
                Log::error('Failed extract zip file', ['message' => array_get($process, 'message')]);
                return ['status' => false, 'message' => 'Gagal meng-extract file Zip.'];
            }

            $srcDir = glob($dest . DIRECTORY_SEPARATOR . 'DemographicJob_*' . DIRECTORY_SEPARATOR . 'data', GLOB_ONLYDIR);
            $removeDir = glob($dest . DIRECTORY_SEPARATOR . 'DemographicJob_*', GLOB_ONLYDIR);

            if ( !empty(array_get($srcDir, 0) && File::isDirectory(array_get($srcDir, 0))) )
            {
                File::copyDirectory(array_get($srcDir, 0), $dest);
            }

            if ( !empty(array_get($removeDir, 0) && File::isDirectory(array_get($removeDir, 0))) )
            {
                File::deleteDirectory(array_get($removeDir, 0));
            }

            if ( File::exists($dest . DIRECTORY_SEPARATOR . 'biometric.ics') )
            {
                File::delete($dest . DIRECTORY_SEPARATOR . 'biometric.ics');
            }

            if ( File::exists($src) )
            {
                File::delete($src);
            }

            return ['status' => true];
        }

        Log::error('Failed extract demographic zip file process', ['message' => 'Source file or path destination not found', 'source_file' => $src, 'destination_path' => $dest]);
        return ['status' => false, 'message' => 'File sumber atau folder yang dituju tidak ditemukan.'];
    }
}