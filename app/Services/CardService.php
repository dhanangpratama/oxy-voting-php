<?php

namespace App\Services;

use File;
use Auth;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Utils\ApiUtils;
use App\Utils\ImageUtils;
use App\Services\BiometricService;
use App\Services\CardTemplateService;

class CardService 
{
    protected $fileNaming;
    private $biometricService;
    protected $cardTemplateService;

    public function __construct()
    {
        $this->biometricService = new BiometricService;

        $this->fileNaming = [
            'front' => config('filename.frontName'),
            'back' => config('filename.backName'),
            'front_small' => config('filename.frontSmallName'),
            'back_small' => config('filename.backSmallName'),
            'face_front' => config('filename.face_front'),
            'face_left' => config('filename.face_left'),
            'face_right' => config('filename.face_right'),
            'flat_left_four_fingers' => config('filename.flat_left_four_fingers'),
            'flat_right_four_fingers' => config('filename.flat_right_four_fingers'),
            'flat_thumbs' => config('filename.flat_thumbs')
        ];

        $this->cardTemplateService = new CardTemplateService;
    }

    public function data($filename, $mode = 'new', $ext = 'jpg')
    {
        $front = $filename."/front.{$ext}";
        $back = $filename."/back.{$ext}";

        $path = $mode != 'new' ? config('path.data.edit') : config('path.data.card');
        $folder = $mode != 'new' ? 'edit' : 'card';

        $front_path =$path.DIRECTORY_SEPARATOR.$filename.DIRECTORY_SEPARATOR."front.{$ext}";
        $back_path =$path.DIRECTORY_SEPARATOR.$filename.DIRECTORY_SEPARATOR."back.{$ext}";

        $front_url = data_url($folder.'/'.$front);
        $back_url = data_url($folder.'/'.$back);

        $front_exist = file_exists($front_path);
        $back_exist = file_exists($back_path);

        if ( $front_exist && $back_exist )
        {
            $aa = array(
                'status' => true,
                'front' => array(
                    'name' => $front, 
                    'path' => $front_path, 
                    'url' => $front_url
                ),
                'back' => array(
                    'name' => $back, 
                    'path' => $back_path, 
                    'url' => $back_url
                )
            );

            return $aa;
        }
        else
        {
            $not_found_file = [];
            $message = '<ul>';

            if ( ! $front_exist )
            {
                $not_found_file[] = "depan";
            }

            if ( ! $back_exist )
            {
                $not_found_file[] = "belakang";
            }

            $message_data = implode('|', $not_found_file);
            $message_data = str_replace('|', ' dan ', $message_data);

            $message = "File scan kartu AK-23 bagian {$message_data} tidak dapat ditemukan.";

            return array(
                'status' => false,
                'message' => $message,
                'type' => 'error'
            );
        }
    }

    public function store($dir)
    {
        // $henry = '';

        /**
        * read field value from generated OCR
        */
        $field_json_file = $dir . DIRECTORY_SEPARATOR . config('ak23.file_data.field_ocr');

        if ( file_exists($field_json_file) && ! file_exists($dir . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic')) )
        {
            $field_json = get_data_from_file($field_json_file);
            $demographicData = array_merge($field_json['front'], $field_json['back']);
            File::put($dir . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'), json_encode($demographicData));
        }

        $dataFinger = get_data_from_file($dir . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'));

        if ( ! array_has($dataFinger, 'right_thumb.pattern_classification') )
        {
            $client = new Client;
            $data = [];

            foreach (config('ak23.finger_data') as $finger => $atts)
            {
                if ( file_exists($dir . DIRECTORY_SEPARATOR . "{$finger}.jpg") )
                {
                    $data['fingers'][] = [
                        'finger_position' => strtoupper($finger),
                        'path' => $dir . DIRECTORY_SEPARATOR . "{$finger}.jpg"
                    ];
                }
            }

            $henry = ApiUtils::patternClassification($data);

            if ( $henry['status'] == 'OK')
            {
                foreach ($henry['results'] as $data)
                {
                    if ( in_array($data['pattern_classification'], config('ak23.whorl_map')) )
                    {
                        $data['pattern_classification'] = 'WHORL';
                    }
                    else if ( in_array($data['pattern_classification'], config('ak23.ulnar_map')) )
                    {
                        $data['pattern_classification'] = 'ULNAR_LOOP';
                    }

                    $dataFinger[strtolower($data['finger_position'])] = $data;
                }
            }

            foreach (config('ak23.finger_data') as $finger => $detail)
            {
                $dataFinger[$finger]['filename'] = $detail['roll_name'];
            }

            File::put($dir . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), json_encode($dataFinger));
        }
    }

    public function process($folderName)
    {
        $dir = config('path.data.card').DIRECTORY_SEPARATOR.$folderName;

        try
        {
            // If folder not found, redirect and to dashboard and show message error
            if ( ! is_dir($dir) )
            {
                return redirect()->back()->with('status', ['type' => 'error', 'message' => 'Folder '.$folderName.' tidak ditemukan!']);
            }

            if ( file_exists($dir . DIRECTORY_SEPARATOR .  'active.log') )
            {
                return redirect()->back()->with('status', ['type' => 'error', 'message' => "Ma'af anda tidak bisa mengerjakan data yang baru saja anda pilih karena sudah dikerjakan oleh pengguna lain. Silahkan pilih data lainnya."]);
            }

            $file = $this->data($folderName); 

            if ( ! $file['status'] )
            {
                return redirect()->back()->with('status', ['type' => 'error', 'message' => $file['message']]);
            }

            $this->store($dir);

            // Create session data
            session([
                'process' => [
                    'status' => true,
                    'file' => $folderName,
                    'file_front' => $file['front']['name'],
                    'file_back' => $file['back']['name'],
                    'folder' => $folderName,
                    'basePath' => config('path.data.card').DIRECTORY_SEPARATOR.$folderName,
                    'baseUrl' => data_url('card/'.$folderName),
                    'path_front' => $file['front']['path'],
                    'path_back' => $file['back']['path'],
                    'url_front' => $file['front']['url'],
                    'url_back' => $file['back']['url'],
                    'type' => 'card',
                    'mode' => 'new'
                ]
            ]);

            flag('processedby_'.Auth::id(), true);

            return redirect()->route('demographic_finger_roll');
        }
        catch (\Exception $e)
        {
            record_error('Error in process verification card', $e, new Request);
            return redirect()->route('demographic_card_scan_record')->with('status', ['type' => 'error', 'message' => 'Tidak dapat memproses kartu.']);
        }
    }

    public function doProcess($action = 'all')
    {
        $basePath = config('path.data.card');

        $frontSrc = $basePath . DIRECTORY_SEPARATOR . "front.jpg";
        $backSrc = $basePath . DIRECTORY_SEPARATOR . "back.jpg";
        // $fieldsPath = $basePath . DIRECTORY_SEPARATOR . 'fields';
        // $fields = config('ak23.form_fields');

        // if ( ! File::isDirectory($fieldsPath) )
        // {
        //     File::makeDirectory($fieldsPath, 0777);
        // }

        if ( ! file_exists($frontSrc) || ! file_exists($backSrc) )
        {
            return ['status' => 'ERROR', 'message' => 'File kartu tidak lengkap. Mohon untuk men-scan ulang kartu AK-23.'];
        }

        if ( $action != 'all' )
        {
            // img($frontSrc)->crop(15, 1566, 1000, 2350)->save("{$basePath}/{$this->fileNaming['flat_left_four_fingers']}", 100); // cropping flat left finger
            img($frontSrc)->crop(1865, 1576, 0, 3160)->save("{$basePath}/{$this->fileNaming['flat_left_four_fingers']}"); // cropping flat left finger
            
            // img($frontSrc)->crop(1410, 1566, 2345, 2350)->save("{$basePath}/{$this->fileNaming['flat_right_four_fingers']}", 100); // cropping flat right finger
            img($frontSrc)->crop(1753, 1583, 3011, 3160)->save("{$basePath}/{$this->fileNaming['flat_right_four_fingers']}"); // cropping flat right finger

            // img($frontSrc)->crop(906, 1778, 1498, 2350)->save("{$basePath}/{$this->fileNaming['flat_thumbs']}", 100); // cropping flat thumbs finger
            img($frontSrc)->crop(1454, 1270, 1693, 3464)->save("{$basePath}/{$this->fileNaming['flat_thumbs']}"); // cropping flat thumbs finger

            File::put($basePath . DIRECTORY_SEPARATOR . 'pleaseprocess.txt', time());

            flag('crop_flat_four_fingers_process_done', false, $basePath);
        }
        else
        {
            if ( file_exists($basePath . DIRECTORY_SEPARATOR . 'pleaseprocess.txt') )
            {
                // flagDelete('crop_front_process_done',$basePath);
                File::put($basePath . DIRECTORY_SEPARATOR . 'processing.txt', time());
                File::delete($basePath . DIRECTORY_SEPARATOR . 'pleaseprocess.txt');

                $front_small = img($frontSrc);
                if ( $front_small )
                {
                    $front_small->resize(125, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($basePath . DIRECTORY_SEPARATOR . 'front-small.jpg', 30); // create small image for front image
                }

                // crop finger images one by one
                $img_main = img($frontSrc);
                foreach (config('ak23.finger_data') as $finger => $detail)
                {
                    $img = img($frontSrc);

                    $dataCrop = $this->cardTemplateService->loadTemplate();

                    if ( $img )
                    {
                        $img->crop($dataCrop[$finger]['rolled_crop_area']['w'], $dataCrop[$finger]['rolled_crop_area']['h'], $dataCrop[$finger]['rolled_crop_area']['x'], $dataCrop[$finger]['rolled_crop_area']['y'])->brightness(config('ak23.finger_brightness'))->contrast(config('ak23.finger_contrast'))->save($basePath.DIRECTORY_SEPARATOR.$detail['roll_name']);

                        // ImageUtils::saveJpegAsWsq($basePath.DIRECTORY_SEPARATOR.$detail['roll_name']);
                    }
                }

                // foreach ($fields['front'] as $filename => $coordinate) 
                // {
                //     $fieldImg = img($frontSrc);
        
                //     if ( $fieldImg )
                //     {
                //         $fieldImg->crop($coordinate['x1'], $coordinate['y1'], $coordinate['x2'], $coordinate['y2'])->save($fieldsPath.DIRECTORY_SEPARATOR.$filename);
                //     }
                // }

                // flag('crop_front_process_done', false, $basePath);

                try
                {
                    $this->biometricService->patternClassification($basePath); // Doing pattern classification
                }
                catch ( \Exception $e )
                {
                    record_error('Error in generate pattern classification', $e, new Request);
                }

                // flagDelete('crop_back_process_done', $basePath);

                $back_small = img($backSrc);
                
                if ( $back_small )
                {
                    $back_small->resize(125, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($basePath . DIRECTORY_SEPARATOR . 'back-small.jpg', 30); // create small image for back image
                }

                if ( file_exists($backSrc) )
                {
                    img($backSrc)->crop(1028, 1543, 93, 3117)->save("{$basePath}/{$this->fileNaming['face_front']}"); // cropping photo from back side
                    img($backSrc)->crop(1028, 1543, 1039, 3117)->save("{$basePath}/{$this->fileNaming['face_left']}"); // cropping photo from back side
                    img($backSrc)->crop(1028, 1543, 1986, 3117)->save("{$basePath}/{$this->fileNaming['face_right']}"); // cropping photo from back side
                }

                // foreach ($fields['back'] as $filename => $coordinate) 
                // {
                //     $fieldImg = img($backSrc);
        
                //     if ( $fieldImg )
                //     {
                //         $fieldImg->crop($coordinate['x1'], $coordinate['y1'], $coordinate['x2'], $coordinate['y2'])->save($fieldsPath.DIRECTORY_SEPARATOR.$filename);
                //     }
                // }

                // flag('crop_back_process_done', false, $basePath);

                // if ( ! file_exists($basePath.DIRECTORY_SEPARATOR.'_fields.json') )
                // {
                //     $this->processOcr($basePath);
                // }

                foreach ( config('ak23.finger_index') as $finger )
                {
                    if ( File::exists($basePath . DIRECTORY_SEPARATOR . "{$finger}.jpg") )
                    {
                        ApiUtils::generateMinutiae([
                            "file_name_left" => $basePath . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                            "file_name_right" => $basePath . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                            "file_name_result_left" => config('path.data.tmp_minutiae') . DIRECTORY_SEPARATOR . "{$finger}.jpg",
                            "file_name_result_right" =>  config('path.data.tmp_minutiae') . DIRECTORY_SEPARATOR . "{$finger}.jpg"
                        ]);
                    }
                }

                File::delete($basePath . DIRECTORY_SEPARATOR . 'processing.txt');
                flag('cropping_card_process_done', false, $basePath);
            }
        }

        return ['status' => 'OK'];
    }

    private function processOcr()
    {
        $form_fields = config('ak23.form_fields');
        $ocr_json = array();

        foreach($form_fields as $key => $value)
        {
            $ocr_json[$key] = $this->generateOcr($value, session('process.basePath'));

        }

        /**
         * fixing child field name and value
         */
        $children = array();
        for($i=1; $i <= 6; $i++ )
        {
            if(isset($ocr_json['back']["child_{$i}"]))
            {
                $children[$i-1] = $ocr_json['back']["child_{$i}"];
                unset($ocr_json['back']["child_{$i}"]);
            }

        }
        $ocr_json['back']['children'] = implode(', ', $children);

        File::put(session('process.basePath') . DIRECTORY_SEPARATOR . config('ak23.file_data.field_ocr'), json_encode($ocr_json));
    }

    private function generateOcr($fields, $folder_name)
    {
        $fields_value = array();
        foreach($fields as $file => $coordinate)
        {
            $sourceFile =
                $folder_name . DIRECTORY_SEPARATOR .
                'fields' . DIRECTORY_SEPARATOR .
                $file;

            if(file_exists($sourceFile))
            {
                $field_name = str_replace('.jpg', '', $file);
                $field_name = str_replace('-', '_', $field_name);
                $fields_value[$field_name] = ImageUtils::generateOCR($sourceFile);
            }

        }

        return $fields_value;
    }
}
