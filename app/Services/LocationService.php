<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Services\Api\JavaServerService;

class LocationService 
{
    private $javaServerService;

    public function __construct(JavaServerService $javaServerService)
    {
        $this->javaServerService = $javaServerService;
    }

    public function getAll()
    {
        try
        {
            return $this->javaServerService->locations();
        }
        catch(\Exception $e)
        {
            record_error('Error request server API report', $e);

            return abort(500, 'Gagal meminta data location');
        }
    }
}
