<?php

namespace App\Services;

use Image, File;
use App\Utils\ApiUtils;
use App\Services\BiometricService;

class CameraService
{
    private $biometricService;
    
    public function __construct(BiometricService $biometricService)
    {
        $this->biometricService = $biometricService;
    }

    public function submit($dataImages, $request)
    {
        foreach ($dataImages as $key => $image)
        {
            $explodeData = explode(',', $image);

            $path = session('process.basePath') . DIRECTORY_SEPARATOR . str_replace('_', '-', $key) . '.jpg';

            if ( count($explodeData) > 1 && array_has($explodeData, 1) && ! empty($explodeData[1]) )
            {
                try
                {
                    Image::make($explodeData[1])->save($path);
                }
                catch ( \Exception $e )
                {
                    record_error('Error in save image', $e, $request);
                }
            }
        }

        if ( session('process.mode') != 'edit' )
        {
            flag('camera_process_done');
        }
    }

    public function process($data, $request)
    {
        $image = explode(',', $data['imageData']);

        try
        {
            $crop = (string) Image::make($image[1])->crop(720, 1080)->encode('data-url');
            // $crop = (string) Image::make($image[1])->crop(480, 720)->encode('data-url');

            return [
                'status' => 'OK',
                'image' => $crop
            ];
        }
        catch ( \Exception $e )
        {
            record_error('Error image processing.', $e, $request);
            return [
                'status' => 'ERROR'
            ];
        }
    }

    public function remove($data, $request)
    {
        $noImageUrl = asset('assets/images/ico-front.png');

        if ($data['image'] == 'face-left.jpg') $noImageUrl = asset('assets/images/ico-left-side.png');
        else if ($data['image'] == 'face-right.jpg') $noImageUrl = asset('assets/images/ico-right-side.png');

        if (file_exists(session('process.basePath') . DIRECTORY_SEPARATOR . $data['image']))
        {
            try
            {
                $delete = File::delete(session('process.basePath') . DIRECTORY_SEPARATOR . $data['image']);

                if ( ! $delete )
                {
                    throw new \Exception("Gagal menghapus file foto");
                }
            }
            catch ( \Exception $e)
            {
                record_error('Error delete image file', $e, $request);
                return ['status' => 'error', 'message' => 'Gagal menghapus foto'];
            }
        }

        return ['status' => 'OK', 'path' => $data['image'], 'noImageUrl' => $noImageUrl];
    }
}