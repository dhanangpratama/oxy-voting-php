<?php

namespace App\Services;

use Image, File;

class PhotoService
{
    private $biometricService;
    
    public function __construct()
    {
        //
    }

    public function process($dataImage)
    {
        $image = explode(',', $dataImage);
        
        try
        {
            $crop = (string) Image::make($image[1])->crop(1080, 1080)->encode('data-url');
            Image::make($image[1])->crop(1080, 1080)->save(config('path.data.demographic') . DIRECTORY_SEPARATOR . 'photo.jpg');
            // $crop = (string) Image::make($image[1])->crop(480, 720)->encode('data-url');

            return [
                'status' => 'OK',
                'image' => $crop
            ];
        }
        catch ( \Exception $e )
        {
            record_error('Error image processing.', $e, $request);
            return [
                'status' => 'ERROR'
            ];
        }
    }
}