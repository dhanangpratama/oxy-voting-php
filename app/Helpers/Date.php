<?php

namespace App\Helpers;

class Date 
{
    /**
    * The function is_date() validates the date and returns true or false
    * @param $str sting expected valid date format
    * @return bool returns true if the supplied parameter is a valid date 
    * otherwise false
    */
    public static function isDate( $str ) 
    {
        if ( empty($str) )
        {
            return false;
        }

        try 
        {
            $dt = new DateTime( trim($str) );
        }
        catch( Exception $e ) 
        {
            return false;
        }

        $month = $dt->format('m');
        $day = $dt->format('d');
        $year = $dt->format('Y');

        if( checkdate($month, $day, $year) ) 
        {
            return true;
        }
        else 
        {
            return false;
        }
    }

    public static function createDateRangeArray($strDateFrom,$strDateTo)
    {
        // takes two dates formatted as YYYY-MM-DD and creates an
        // inclusive array of the dates between the from and to dates.

        // could test validity of dates here but I'm already doing
        // that in the main script

        $aryRange=array();

        $iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
        $iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

        if ($iDateTo>=$iDateFrom)
        {
            array_push($aryRange,date('d-m-Y',$iDateFrom)); // first entry
            while ($iDateFrom<$iDateTo)
            {
                $iDateFrom+=86400; // add 24 hours
                array_push($aryRange,date('d-m-Y',$iDateFrom));
            }
        }
        return $aryRange;
    }

    public static function createMonthRangeArray($strDateFrom,$strDateTo)
    {
        $start    = new DateTime($strDateFrom);
        $start->modify('first day of this month');
        $end      = new DateTime($strDateTo);
        $end->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period   = new DatePeriod($start, $interval, $end);

        $months = [];

        foreach ($period as $dt) {
            array_push($months, $dt->format("m-Y"));
        }

        return $months;
    }

    public static function createYearRangeArray($strDateFrom,$strDateTo)
    {
        $report_years = [];

        $strDateFrom = date('Y', strtotime($strDateFrom));
        $strDateTo = date('Y', strtotime($strDateTo));

        if ( $strDateTo > $strDateFrom )
        {
            for ($i = $strDateFrom; $i <= $strDateTo; $i++) { 
                $report_years[] = (int) $i;
            }
        }
        else
        {
            $report_years[] = $strDateFrom;
        }

        return $report_years;
    }

    public static function formatIndonesia($stringDate, $time = false)
    {
        $format = !$time ? 'D, d M Y' : 'D, d M Y H:i:s';
        $stringDate = date($format , strtotime($stringDate));
        /**
        * Replace Day
        */
        if ( strpos($stringDate, 'Mon') !== false )
        {
            $stringDate = str_replace('Mon', 'Senin', $stringDate);
        }
        else if ( strpos($stringDate, 'Tue') !== false )
        {
            $stringDate = str_replace('Tue', 'Selasa', $stringDate);
        }
        else if ( strpos($stringDate, 'Wed') !== false )
        {
            $stringDate = str_replace('Wed', 'Rabu', $stringDate);
        }
        else if ( strpos($stringDate, 'Thu') !== false )
        {
            $stringDate = str_replace('Thu', 'Kamis', $stringDate);
        }
        else if ( strpos($stringDate, 'Fri') !== false )
        {
            $stringDate = str_replace('Fri', 'Jum\'at', $stringDate);
        }
        else if ( strpos($stringDate, 'Sat') !== false )
        {
            $stringDate = str_replace('Sat', 'Sabtu', $stringDate);
        }
        else if ( strpos($stringDate, 'Sun') !== false )
        {
            $stringDate = str_replace('Sun', 'Minggu', $stringDate);
        }
        else
        {
            $stringDate = $stringDate;
        }

        /**
        * Replace month
        */
        if ( strpos($stringDate, 'Jan') !== false )
        {
            $stringDate = str_replace('Jan', 'Januari', $stringDate);
        }
        else if ( strpos($stringDate, 'Feb') !== false )
        {
            $stringDate = str_replace('Feb', 'Februari', $stringDate);
        }
        else if ( strpos($stringDate, 'Mar') !== false )
        {
            $stringDate = str_replace('Mar', 'Maret', $stringDate);
        }
        else if ( strpos($stringDate, 'Apr') !== false )
        {
            $stringDate = str_replace('Apr', 'April', $stringDate);
        }
        else if ( strpos($stringDate, 'May') !== false )
        {
            $stringDate = str_replace('May', 'Mei', $stringDate);
        }
        else if ( strpos($stringDate, 'Jun') !== false )
        {
            $stringDate = str_replace('Jun', 'Juni', $stringDate);
        }
        else if ( strpos($stringDate, 'Jul') !== false )
        {
            $stringDate = str_replace('Jul', 'Juli', $stringDate);
        }
        else if ( strpos($stringDate, 'Aug') !== false )
        {
            $stringDate = str_replace('Aug', 'Agustus', $stringDate);
        }
        else if ( strpos($stringDate, 'Sep') !== false )
        {
            $stringDate = str_replace('Sep', 'September', $stringDate);
        }
        else if ( strpos($stringDate, 'Oct') !== false )
        {
            $stringDate = str_replace('Oct', 'Oktober', $stringDate);
        }
        else if ( strpos($stringDate, 'Nov') !== false )
        {
            $stringDate = str_replace('Nov', 'November', $stringDate);
        }
        else if ( strpos($stringDate, 'Des') !== false )
        {
            $stringDate = str_replace('Des', 'Desember', $stringDate);
        }
        else
        {
            $stringDate = $stringDate;
        }

        return $stringDate;
    }

    public static function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === $timestamp) 
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }

    public static function dmy($date)
    {
        return date('d-m-Y', strtotime($date));
    }
}