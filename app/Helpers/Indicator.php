<?php

namespace App\Helpers;

class Indicator 
{
    public static function card()
    {
        $routes = [
            'demographic_finger_roll',
            'demographic_finger_flat',
            'demographic_identification',
            'demographic_formula',
            'demographic_picture',
            'demographic_front'
        ];

        $data = [
            'Ulir',
            'Rata',
            'Verifikasi',
            'Rumus',
            'Rekam Foto',
            'Rekam Data'
        ];

        return [
            'routes' => $routes,
            'data' => $data
        ];
    }

    public static function live()
    {
        $routes = [
            'demographic_finger_scan_record',
            'demographic_identification',
            'demographic_formula',
            'demographic_camera',
            'demographic_front'
        ];

        $data = [
            'Rekam Jari',
            'Verifikasi',
            'Rumus',
            'Rekam Foto',
            'Rekam Data'
        ];

        return [
            'routes' => $routes,
            'data' => $data
        ];
    }
}