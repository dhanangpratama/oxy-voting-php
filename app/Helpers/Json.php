<?php

namespace App\Helpers;

class Json 
{
    public static function isJson($string)
    {
        return is_string($string) && is_array(json_decode($string, true)) ? true : false;
    }
}