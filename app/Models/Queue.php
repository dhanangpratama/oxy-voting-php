<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Queue extends Model
{
    protected $table = 'queue';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];
    public $timestamps = false;
}
