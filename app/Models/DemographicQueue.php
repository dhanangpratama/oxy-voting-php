<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DemographicQueue extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'demographics_queue';

    protected $primaryKey = 'demographic_queue_id';

    protected $guarded = ['demographic_queue_id'];
}
