<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'religion';
    protected $primaryKey = 'religion_id';
}
