<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $primaryKey = 'option_id';

    protected $fillable = [
        'option_name', 'option_value', 'option_type'
    ];

    public $timestamps = false;
}