<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Demographic extends Model
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'demographics';

    /**
     * Primary key for this table.
     *
     * @var string
     */
    protected $primaryKey = 'demographic_id';

    protected $dates = ['deleted_at'];

    protected $guarded = [];

    public function religion_data()
    {
        return $this->belongsTo('App\Models\Religion', 'religion_id', 'religion_id');
    }

    public function user_data()
    {
    	return $this->belongsTo('App\Models\User', 'user_id', 'user_id');
    }

    public function event_data()
    {
        return $this->belongsTo('App\Models\Event', 'event_id', 'event_id');
    }
}
