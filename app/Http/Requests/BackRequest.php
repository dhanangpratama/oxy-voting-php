<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class BackRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'birth_place' => 'max:255',
            'nationality' => 'max:255',
            'childs' => 'max:255',
            'father_name' => 'max:255',
            'mother_name' => 'max:255',
            'father_address' => 'max:255',
            'mother_address' => 'max:255',
            'wife_husband_name' => 'max:255',
            'wife_husband_address' => 'max:255',
            'provision_code' => 'max:255',
            'tattoo' => 'max:255',
            'scars_and_handicap' => 'max:255'
        ];
    }

    public function messages()
    {
        return [
            'max' => 'Field :attribute melebihi batas karakter maksimum'
        ];
    }
}
