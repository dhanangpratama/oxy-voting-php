<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PictureRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'picture_left_side' => 'mimes:jpeg,bmp,png',
            'picture_front_side' => 'mimes:jpeg,bmp,png',
            'picture_right_side' => 'mimes:jpeg,bmp,png'
        ];
    }

    public function messages()
    {
        return [
            'picture_left_side.mimes' => 'Foto wajah kiri bukan file gambar.',
            'picture_front_side.mimes' => 'Foto wajah depan bukan file gambar.',
            'picture_right_side.mimes' => 'Foto wajah kanan bukan file gambar.'
        ];
    }
}
