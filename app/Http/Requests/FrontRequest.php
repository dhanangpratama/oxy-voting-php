<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FrontRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ( ! array_has(Request::all(), 'back') )
        {
            return [
                'fullname' => 'required|max:255',
                'nickname' => 'max:255',
                'job' => 'max:255',
                'location' => 'max:255',
                'taken_by' => 'max:255',
                'view_by' => 'max:255',
                'sex' => 'required'
            ];
        }
        else
        {
            return [
                'fullname' => 'max:255',
                'nickname' => 'max:255',
                'job' => 'max:255',
                'location' => 'max:255',
                'taken_by' => 'max:255',
                'view_by' => 'max:255'
            ];
        }
    }

    public function messages()
    {
        return [
            'sex.required' => 'Jenis kelamin wajib dipilih',
            'fullname.required' => 'Nama wajib diisi',
            'nickname.required' => 'Nama kecil / Alias wajib diisi'
        ];
    }
}
