<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AddOperatorRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:operator|max:200'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Isian <strong>Email</strong> wajib diisi.',
            'email' => 'Email yang anda masukkan tidak valid.'
        ];
    }
}
