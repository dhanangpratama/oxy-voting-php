<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LatenRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'photo' => 'required|mimetypes:application/octet-stream,image/jpeg,image/png'
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Pilih berkas terlebih dahulu.',
            'photo.mimetypes' => 'Format gambar sidik jari yang diperbolehkan JPG, PNG dan WSQ.'
        ];
    }
}
