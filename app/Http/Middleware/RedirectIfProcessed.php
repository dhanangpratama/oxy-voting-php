<?php

namespace App\Http\Middleware;

use Closure;

class RedirectIfProcessed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( !session()->exists('process') ) // Check session process, if not provide, redirect back
        {
            return redirect()->route('dashboard')->with('status', ['type' => 'info', 'message' => 'Sesi anda melakukan demografi telah hilang. Silahkan melakukan perekaman ulang.']);
        }
        
        return $next($request);
    }
}
