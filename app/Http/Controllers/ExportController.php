<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\People;
use App\Models\Religion;
use App\Models\Education;
use App\Models\Skin;
use App\Models\Body;
use App\Models\Chin;
use App\Models\Ear;
use App\Models\EyeColor;
use App\Models\EyeIrregularity;
use App\Models\Face;
use App\Models\Forehead;
use App\Models\Hair;
use App\Models\HairColor;
use App\Models\Head;
use App\Models\Lip;
use App\Models\Nose;
use App\Models\Tooth;
use App\Models\AK24_Print;
use App\Models\Status;
use App\Http\Requests;
use App\Utils\ApiUtils;
use App\Utils\ImageUtils;
use App\Services\Api\JavaServerService;
use TCPDF, Crypt, Log, DB, File, View, Cache, Image, Auth;
use App\Functions\AKPDF;
use App\Functions\COMPAREPDF;
use App\Services\ZipService;

class ExportController extends MainController
{
    protected $people;

    protected $status_id_array;

    protected $status = '';

    protected $status_allowed = [];

    protected $source;

    private $javaServerService;

    private $zip;

    public function __construct(JavaServerService $javaServerService, ZipService $zip)
    {
        parent::__construct();

        $this->status_id_array = Status::all('status_id')->toArray();

        $this->javaServerService = $javaServerService;

        $this->zip = $zip;

        foreach ($this->status_id_array as $value) 
        {
            $this->status_allowed[] = $value['status_id'];
        }

        $this->people = new People;

        View::share('path', $this->path);
        View::share('fileNaming', $this->fileNaming);
    }

    /**
     * Create PDF file for AK-23
     * @param  integer $id
     * @return PDF
     */
    public function dermalog(Request $request, $afisid = 0)
    {
        if ( empty($afisid) )
        {
            return 'Please provide People ID';
        }

        try 
        {
            $afisid =  Crypt::decrypt($afisid);
        }
        catch (\Exception $e)
        {
            record_error('Error decrypt ID in ExportController -> ak23().', $e, $request);
            return view('errors.custom', ['data' => ['title' => "Ma'af terjadi kesalahan saat mencetak dokumen.", 'description' => 'Mohon untuk mencoba lagi beberapa saat.']]);
        }

        $people = Cache::remember('drm_afisid_' . $afisid, 60, function() use ($afisid)
        {
            return ApiUtils::dermalogAfisid($afisid);
        });

        if ( !empty($people) )
        {
            if ( ! is_dir(config('path.data.dermalog') . DIRECTORY_SEPARATOR . $afisid) )
            {
                File::makeDirectory(config('path.data.dermalog') . DIRECTORY_SEPARATOR . $afisid);
            }

            if ( ! empty($people[0]['photoimage']) && ! file_exists(config('path.data.dermalog') . DIRECTORY_SEPARATOR . $afisid . DIRECTORY_SEPARATOR . 'photo.jpg') )
            {
                Image::make($people[0]['photoimage'])->save(config('path.data.dermalog') . DIRECTORY_SEPARATOR . $afisid . DIRECTORY_SEPARATOR . 'photo.jpg');
            }

            $fingers = Cache::remember('drm_finger_afisid_' . $afisid, 60, function() use ($afisid)
            {
                return ApiUtils::dermalogFinger($afisid);
            });

            $drmPath = config('path.data.dermalog') . DIRECTORY_SEPARATOR . $afisid;

            foreach ($fingers as $finger) 
            {
                $filename = $drmPath . DIRECTORY_SEPARATOR . $finger['fpposition'] . '.wsq';
                $filenameJpg = str_replace('.wsq', '.jpg', $filename);

                if ( ! empty($finger['fpimage']) && ! file_exists($filename) && ! file_exists($filenameJpg) )
                {
                    file_put_contents($filename, base64_decode($finger['fpimage']));
                    ImageUtils::saveWsqAsJpg($filename, $filenameJpg);
                }
            }

            $fingerImagesArr = [];
            foreach (config('ak23.fingermap_key') as $key => $value) 
            {
                $$value = file_exists($drmPath . DIRECTORY_SEPARATOR . $key . '.jpg') ? '<img src="' . asset("files/dermalog/{$afisid}/{$key}.jpg") . '" />' : null;
                $fingerImagesArr[] = "'{$value}'";
            }

            $fingerImages = implode(',', $fingerImagesArr);

            $face_front = file_exists($drmPath . DIRECTORY_SEPARATOR . 'photo.jpg') ? '<img src="' . asset("files/dermalog/{$afisid}/photo.jpg") . '" />' : null;

            $pageOne = view('export.dermalog.page_1', compact('people', 'right_thumb', 'right_index', 'right_middle', 'right_ring', 'right_little', 'left_thumb', 'left_index', 'left_middle', 'left_ring', 'left_little'))->render();
            $pageTwo = view('export.dermalog.page_2', compact('people', 'face_front'))->render();

            try
            {
                // create new PDF document
                $pdf = new TCPDF('P', PDF_UNIT, [200, 200], true, 'UTF-8', false);

                // set document information
                $pdf->SetCreator('Biover Biometric');
                $pdf->SetAuthor('Inafis');
                $pdf->SetTitle($people[0]['fullname'].' - AK-23');
                $pdf->SetSubject($people[0]['fullname'].' - AK-23');
                // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

                // remove default header/footer
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                // set margins
                $pdf->SetMargins(6.5, 5, 6.5);

                // set auto page breaks
                $pdf->SetAutoPageBreak(FALSE, 0);

                // set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                // set font
                $pdf->SetFont('times', 'B', 9);

                // add a page
                $pdf->AddPage();

                // print a block of text using Write()
                $pdf->writeHTML($pageOne, false, false, true, false, '');

                $pdf->AddPage();

                // print a block of text using Write()
                $pdf->writeHTML($pageTwo, false, false, true, false, '');

                if (ob_get_contents()) ob_end_clean();

                // Close and output PDF document
                $pdf->Output(str_slug($people[0]['fullname']).'_ak-23.pdf', 'I');
            }

            catch (\Exception $e)
            {
                record_error('Error Export AK-23 PDF', $e, $request);
                return view('errors.custom', ['data' => ['title' => "Ma'af terjadi kesalahan saat mencetak dokumen.", 'description' => 'Mohon untuk mencoba lagi beberapa saat.']]);
            }
        }
    }

    /**
     * Create PDF file for AK-23
     * @param  integer $id
     * @return PDF
     */
    public function ak23(Request $request, $id = 0)
    {
        if ( empty($id) )
        {
            return 'Please provide People ID';
        }

        try 
        {
            $id =  Crypt::decrypt($id);
        }
        catch (\Exception $e)
        {
            record_error('Error decrypt ID in ExportController -> ak23().', $e, $request);
            return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");
        }

        $datacenter = $request->get('dc', 0);

        ImageUtils::createWatermarkImage();

        if ( $datacenter )
        {
            if ( $this->networkStatus == 'off' )
            {
                return abort(500, '[btn_back]Gagal mencetak dokumen. Tidak dapat terhubung ke server.');
            }

            return $this->ak23Server($request, $id);
        }

        $this->people = $this->people->findOrFail($id);

        $complete_name_folder = config('filename.complete_folder');
        $complete_dir = $this->path['data']['complete'].DIRECTORY_SEPARATOR.$id;

        $pdf_images = 'pdf';
        $pdf_dir = $complete_dir.DIRECTORY_SEPARATOR.$pdf_images;

        $flat_right_thumb =
        $flat_left_thumb =
        $flat_left_index_finger =
        $flat_left_middle_finger =
        $flat_left_ring_finger =
        $flat_left_little_finger =
        $flat_right_index_finger =
        $flat_right_middle_finger =
        $flat_right_ring_finger =
        $flat_right_little_finger =
        $left_thumb =
        $left_index_finger =
        $left_middle_finger =
        $left_ring_finger =
        $left_little_finger =
        $right_thumb =
        $right_index_finger =
        $right_middle_finger =
        $right_ring_finger =
        $right_little_finger =
        $face_front =
        $face_left =
        $face_right = '';

        if ( ! is_dir($pdf_dir) )
        {
            File::makeDirectory($pdf_dir, 0777);
        }

        foreach(config('ak23.finger_data') as $fingerKey => $value)
        {
            $pathRoll = $complete_dir.DIRECTORY_SEPARATOR.$value['roll_name'];
            $pathFlat = $complete_dir.DIRECTORY_SEPARATOR.$value['flat_name'];

            if ( file_exists($pathRoll) )
            {
                $$fingerKey = '<img height="110" src="'.$pathRoll.'">';
            }

            if ( file_exists($pathFlat) )
            {
                $flatName = 'flat_'.$fingerKey;
                $$flatName = '<img height="90" src="'.$pathFlat.'">';
            }
        }

        if ( file_exists($complete_dir."/{$this->fileNaming['face_front']}") )
        {
            img($complete_dir."/{$this->fileNaming['face_front']}")->resize(null, 320, function ($constraint) {
                $constraint->aspectRatio();
            })->save( $this->path['data']['complete'] . "/{$id}/{$pdf_images}/{$this->fileNaming['face_front']}", 100);
            $face_front = '<img src="'.$pdf_dir.'/'.$this->fileNaming['face_front'].'">';
        }

        if ( file_exists($complete_dir."/{$this->fileNaming['face_left']}") )
        {
            img($complete_dir."/{$this->fileNaming['face_left']}")->resize(null, 320, function ($constraint) {
                $constraint->aspectRatio();
            })->save( $this->path['data']['complete'] . "/{$id}/{$pdf_images}/{$this->fileNaming['face_left']}", 100);
            $face_left = '<img src="'.$pdf_dir.'/'.$this->fileNaming['face_left'].'">';
        }

        if ( file_exists($complete_dir."/{$this->fileNaming['face_right']}") )
        {
            img($complete_dir."/{$this->fileNaming['face_right']}")->resize(null, 320, function ($constraint) {
                $constraint->aspectRatio();
            })->save( $this->path['data']['complete'] . "/{$id}/{$pdf_images}/{$this->fileNaming['face_right']}", 100);
            $face_right = '<img src="'.$pdf_dir.'/'.$this->fileNaming['face_right'].'">';
        }

        $father_address = !empty($this->people->father_address) ? '<br>' . $this->people->father_address : '';
        $mother_address = !empty($this->people->mother_address) ? '<br>' . $this->people->mother_address : '';

        $child = explode(',', $this->people->children);
        $child_1 = isset($child[0]) ? $child[0] : '-';
        $child_2 = isset($child[1]) ? $child[1] : '-';
        $child_3 = isset($child[2]) ? $child[2] : '-';
        $child_4 = isset($child[3]) ? $child[3] : '-';
        $child_5 = isset($child[4]) ? $child[4] : '-';
        $child_6 = isset($child[5]) ? $child[5] : '-';

        $religion = (!empty($this->people->religion_id)) ? $this->people->religion_data->label : '-';
        $education = (!empty($this->people->education_id)) ? $this->people->education_data->label : '-';
        $skin = (!empty($this->people->skin_id)) ? $this->people->skin_data->label : '-';
        $body = (!empty($this->people->body_id)) ? $this->people->body_data->label : '-';
        $head = (!empty($this->people->head_id)) ? $this->people->head_data->label : '-';
        $hair_color = (!empty($this->people->hair_color_id)) ? $this->people->hairColor->label : '-';
        $hair = (!empty($this->people->hair_id)) ? $this->people->hair_data->label : '-';
        $face = (!empty($this->people->face_id)) ? $this->people->face_data->label : '-';
        $forehead = (!empty($this->people->forehead_id)) ? $this->people->forehead_data->label : '-';
        $eye_color = (!empty($this->people->eye_color_id)) ? $this->people->eyeColor->label : '-';
        $eye_irregularity = (!empty($this->people->eye_irregularity_id)) ? $this->people->eyeIrregularity->label : '-';
        $nose = (!empty($this->people->nose_id)) ? $this->people->nose_data->label : '-';
        $lip = (!empty($this->people->lip_id)) ? $this->people->lip_data->label : '-';
        $tooth = (!empty($this->people->tooth_id)) ? $this->people->tooth_data->label : '-';
        $chin = (!empty($this->people->chin_id)) ? $this->people->chin_data->label : '-';
        $ear = (!empty($this->people->ear_id)) ? $this->people->ear_data->label : '-';
        $sex = ($this->people->sex == 'm') ? 'Laki-laki' : 'Perempuan';

        $formula = explode('|', $this->people->formula);
        $formula_view = explode('|', $this->people->formula_view);

        $address = '';

        if ( ! empty($this->people->address) ) $address .= $this->people->address;
        if ( ! empty($this->people->rt) ) $address .= ' RT. ' . $this->people->rt;
        if ( ! empty($this->people->rw) ) $address .= ' RW. ' . $this->people->rw;
        if ( ! empty($this->people->village) ) $address .= ' ' . $this->people->village;
        if ( ! empty($this->people->regent) ) $address .= ' ' . $this->people->regent;
        if ( ! empty($this->people->province) ) $address .= ' ' . $this->people->province;
        if ( ! empty($this->people->country) ) $address .= ' ' . $this->people->country;
        if ( ! empty($this->people->postal_code) ) $address .= ' ' . $this->people->postal_code;

        $fullnameFontSize = strlen($this->people->fullname) > 38 ? 'font-size: 7px' : '';

        if ( in_array($this->people->status, $this->status_allowed) )
        {
            $this->status = $this->people->status_data->label;
        }

        $pageOne = view('export.ak23.page_1', compact('formula', 'formula_view', 'sex', 'flat_right_thumb', 'flat_right_index_finger', 'flat_right_middle_finger', 'flat_right_ring_finger', 'flat_right_little_finger', 'flat_left_thumb', 'flat_left_index_finger', 'flat_left_middle_finger', 'flat_left_ring_finger', 'flat_left_little_finger', 'face_left', 'face_right', 'face_front', 'left_thumb', 'left_index_finger', 'left_middle_finger', 'left_ring_finger', 'left_little_finger', 'right_thumb', 'right_index_finger', 'right_middle_finger', 'right_ring_finger', 'right_little_finger', 'pdf_dir', 'pdf_images_url', 'fullnameFontSize'))->with(['people' => $this->people, 'status' => $this->status, 'statusNumber' => $this->people->status_id])->render();

        $pageTwo = view('export.ak23.page_2', compact('child_1', 'child_2', 'child_3', 'child_4', 'child_5', 'child_6', 'father_address', 'mother_address', 'religion', 'education', 'skin', 'body', 'head', 'hair_color', 'hair', 'face', 'forehead', 'eye_color', 'eye_irregularity', 'nose', 'lip', 'tooth', 'chin', 'ear', 'sex', 'face_left', 'face_right', 'face_front', 'pdf_dir', 'pdf_images_url', 'address'))->with(['people' => $this->people])->render();

        // Log activity
        $this->activity->log('print-ak23', 'Pengguna mencetak AK23 data lokal dengan ID ' . $this->people->subject_id, Auth::id());

        try
        {
            // create new PDF document
            $pdf = new AKPDF('P', PDF_UNIT, [200, 200], true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator('Biover Biometric');
            $pdf->SetAuthor('Inafis');
            $pdf->SetTitle($this->people->fullname.' - AK-23');
            $pdf->SetSubject($this->people->fullname.' - AK-23');
            // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

            // remove default header/footer
            $pdf->setPrintHeader(true);
            $pdf->setPrintFooter(false);

            // set default monospaced font
            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $pdf->SetMargins(6.5, 5, 6.5);

            // set auto page breaks
            $pdf->SetAutoPageBreak(FALSE, 0);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set font
            $pdf->SetFont('times', 'B', 9);

            // add a page
            $pdf->AddPage();

            // print a block of text using Write()
            $pdf->writeHTML($pageOne, false, false, true, false, '');

            $pdf->AddPage();

            // print a block of text using Write()
            $pdf->writeHTML($pageTwo, false, false, true, false, '');

            if (ob_get_contents()) ob_end_clean();

            // Close and output PDF document
            $pdf->Output(public_path('pdf/ak-23.pdf'), 'F');

            if (File::isDirectory($pdf_dir))
            {
                /**
                 * Remove PDF directory after pdf load
                 */
                File::deleteDirectory($pdf_dir);
            }

            return redirect(url('pdfjs/web/viewer.html?file=../../pdf/ak-23.pdf'));
        }
        catch (\Exception $e)
        {
            record_error('Error Export AK-23 PDF', $e, $request);
            return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");
        }
    }

    public function ak23Server($request, $id = 0)
    {
        $people_server = $this->javaServerService->search(['id' => $id]);

        if ( $people_server['status'] === 'OK' )
        {
            $people = $people_server['results']['content'][0];

            if ( count($people_server['results']['content']) < 1 )
            {
                record_error('Error API data tidak ditemukan.', $e, $request);
                return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");
            }

            foreach ($people as $key => $value) 
            {
                if ($key == 'subjectId')
                {
                    $people[ucfirst($key)] = $value;
                }
                else
                {
                    $people[snake_case($key)] = $value;
                }
            }

            $people = json_encode($people);
            $people = json_decode($people);

            $imageUrl = $people->baseUrlAssets;
            $zipFile = config('url.demographicZip') . "/{$people->id}.zip";

            $flat_right_thumb =
            $flat_left_thumb =
            $flat_left_index_finger =
            $flat_left_middle_finger =
            $flat_left_ring_finger =
            $flat_left_little_finger =
            $flat_right_index_finger =
            $flat_right_middle_finger =
            $flat_right_ring_finger =
            $flat_right_little_finger =
            $left_thumb =
            $left_index_finger =
            $left_middle_finger =
            $left_ring_finger =
            $left_little_finger =
            $right_thumb =
            $right_index_finger =
            $right_middle_finger =
            $right_ring_finger =
            $right_little_finger =
            $face_front =
            $face_left =
            $face_right = '';

            $path = config('path.data.tmp_ak23_print_images');
            $zipDest = $path . DIRECTORY_SEPARATOR . $people->id . '.zip';

            if ( ! File::isDirectory($path) )
            {
                File::makeDirectory($path);
            }
            else
            {
                File::deleteDirectory($path, true);
            }

            try
            {
                download_file($zipFile, $zipDest);
            }
            catch ( \Exception $e )
            {
                return abort(500, '[btn_close_window]'.$e->getMessage());
            }

            $this->zip->demographicServerExtractor($zipDest, $path);

            foreach(config('ak23.finger_data') as $fingerKey => $value)
            {
                $pathRoll = $path . DIRECTORY_SEPARATOR . $value['roll_name'];
                $pathFlat = $path . DIRECTORY_SEPARATOR . $value['flat_name'];

                if ( file_exists($pathRoll) )
                {
                    $$fingerKey = '<img height="110" src="'.$pathRoll.'">';
                }
    
                if ( file_exists($pathFlat) )
                {
                    $flatName = 'flat_'.$fingerKey;
                    $$flatName = '<img height="90" src="'.$pathFlat.'">';
                }
            }

            if ( file_exists($path . DIRECTORY_SEPARATOR . $this->fileNaming['face_front']) )
            {
                $face_front = '<img src="'.$path . DIRECTORY_SEPARATOR . $this->fileNaming['face_front'].'">';
            }

            if ( file_exists($path . DIRECTORY_SEPARATOR . $this->fileNaming['face_left']) )
            {
                $face_left = '<img src="'.$path . DIRECTORY_SEPARATOR . $this->fileNaming['face_left'].'">';
            }

            if ( file_exists($path . DIRECTORY_SEPARATOR . $this->fileNaming['face_right']) )
            {
                $face_right = '<img src="'.$path . DIRECTORY_SEPARATOR . $this->fileNaming['face_right'].'">';
            }

            $father_address = !empty($people->father_address) ? '<br>' . $people->father_address : '';
            $mother_address = !empty($people->mother_address) ? '<br>' . $people->mother_address : '';

            $child = explode(',', $people->children);
            $child_1 = isset($child[0]) ? $child[0] : '-';
            $child_2 = isset($child[1]) ? $child[1] : '-';
            $child_3 = isset($child[2]) ? $child[2] : '-';
            $child_4 = isset($child[3]) ? $child[3] : '-';
            $child_5 = isset($child[4]) ? $child[4] : '-';
            $child_6 = isset($child[5]) ? $child[5] : '-';

            $religion = (!empty($people->religion)) ? $people->religion->label : '-';
            $education = (!empty($people->education)) ? $people->education->label : '-';
            $skin = (!empty($people->skin)) ? $people->skin->label : '-';
            $body = (!empty($people->body)) ? $people->body->label : '-';
            $head = (!empty($people->head)) ? $people->head->label : '-';
            $hair_color = (!empty($people->hairColor)) ? $people->hairColor->label : '-';
            $hair = (!empty($people->hair)) ? $people->hair->label : '-';
            $face = (!empty($people->face)) ? $people->face->label : '-';
            $forehead = (!empty($people->forehead)) ? $people->forehead->label : '-';
            $eye_color = (!empty($people->eyeColor)) ? $people->eyeColor->label : '-';
            $eye_irregularity = (!empty($people->eyeIrregularity)) ? $people->eyeIrregularity->label : '-';
            $nose = (!empty($people->nose)) ? $people->nose->label : '-';
            $lip = (!empty($people->lip)) ? $people->lip->label : '-';
            $tooth = (!empty($people->tooth)) ? $people->tooth->label : '-';
            $chin = (!empty($people->chin)) ? $people->chin->label : '-';
            $ear = (!empty($people->ear)) ? $people->ear->label : '-';
            $sex = ($people->sex == 'm') ? 'Laki-laki' : 'Perempuan';

            $formula = ! empty($people->formula) ? explode('|', $people->formula) : null;
            $formula_view = ! empty($people->formula_view) ? explode('|', $people->formula_view) : null;

            $this->status = !empty($people->status) ? $people->status->label : '-';
            $statusNumber = !empty($people->status) ? $people->status->id : null;

            $address = '';

            $fullnameFontSize = strlen($people->fullname) > 38 ? 'font-size: 7px' : '';

            $pageOne = view('export.ak23.page_1', compact('formula', 'formula_view', 'sex', 'flat_right_thumb', 'flat_right_index_finger', 'flat_right_middle_finger', 'flat_right_ring_finger', 'flat_right_little_finger', 'flat_left_thumb', 'flat_left_index_finger', 'flat_left_middle_finger', 'flat_left_ring_finger', 'flat_left_little_finger', 'face_left', 'face_right', 'face_front', 'left_thumb', 'left_index_finger', 'left_middle_finger', 'left_ring_finger', 'left_little_finger', 'right_thumb', 'right_index_finger', 'right_middle_finger', 'right_ring_finger', 'right_little_finger', 'people', 'fullnameFontSize', 'statusNumber'))->with(['status' => $this->status])->render();

            $pageTwo = view('export.ak23.page_2', compact('child_1', 'child_2', 'child_3', 'child_4', 'child_5', 'child_6', 'father_address', 'mother_address', 'religion', 'education', 'skin', 'body', 'head', 'hair_color', 'hair', 'face', 'forehead', 'eye_color', 'eye_irregularity', 'nose', 'lip', 'tooth', 'chin', 'ear', 'sex', 'face_left', 'face_right', 'face_front', 'people', 'address'))->render();

            // Log activity
            $this->activity->log('print-ak23', 'Pengguna mencetak AK23 data server dengan ID ' . $people->subjectId, Auth::id());

            try
            {
                // create new PDF document
                $pdf = new AKPDF('P', PDF_UNIT, [200, 200], true, 'UTF-8', false);

                // set document information
                $pdf->SetCreator('Biover Biometric');
                $pdf->SetAuthor('Inafis');
                $pdf->SetTitle($people->fullname.' - AK-23');
                $pdf->SetSubject($people->fullname.' - AK-23');
                // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

                // remove default header/footer
                $pdf->setPrintHeader(true);
                $pdf->setPrintFooter(false);

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                // set margins
                $pdf->SetMargins(6.5, 5, 6.5);

                // set auto page breaks
                $pdf->SetAutoPageBreak(FALSE, 0);

                // set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                // set font
                $pdf->SetFont('times', 'B', 9);

                // add a page
                $pdf->AddPage();

                // print a block of text using Write()
                $pdf->writeHTML($pageOne, false, false, true, false, '');

                // add a page
                $pdf->AddPage();

                // print a block of text using Write()
                $pdf->writeHTML($pageTwo, false, false, true, false, '');

                if (ob_get_contents()) ob_end_clean();

                // Close and output PDF document
                $pdf->Output(public_path('pdf/ak-23.pdf'), 'F');
                
                return redirect(url('pdfjs/web/viewer.html?file=../../pdf/ak-23.pdf'));
            }
            catch (\Exception $e)
            {
                record_error('Error Export AK-23 PDF', $e, $request);
                return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");  
            }
        }
    }

    /**
     * Create PDF file for AK-24
     * @param  string $id
     * @return PDF File
     */
    public function ak24($id = '')
    {
        if ( empty($id) )
        {
            return 'Please provide People ID';
        }

        try 
        {
            $id =  Crypt::decrypt($id);
        }
        catch (\Exception $e)
        {
            Log::error('Error decrypt ID in ExportController -> ak24(). Message: ' . $e->getMessage());
            return abort(404);
        }

        $this->people = $this->people->find($id);

        if ( ! count($this->people) )
        {
            return 'People not found';
        }

        $birth_date = strtotime($this->people->dob);

        $sex = ($this->people->sex == 'm') ? 'Laki-laki' : 'Perempuan';

        ob_clean();

        // create new PDF document
        $pdf = new TCPDF('L', PDF_UNIT, [120, 70], true, 'UTF-8', false);

        // set document information
        $pdf->SetCreator('Biover Biometric');
        $pdf->SetAuthor('Inafis');
        $pdf->SetTitle($this->people->fullname . ' - AK-24');
        $pdf->SetSubject($this->people->fullname . ' - AK-24');
        // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(5, 4, 5);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, 0);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set font
        $pdf->SetFont('times', 'B', 8);

        // add a page
        $pdf->AddPage();

        $tbl = '
            <table border="0" cellspacing="0" cellpadding="2">
                <tr>
                    <td colspan="5" align="right">Bentuk AK-24</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Tersangka</td>
                    <td colspan="2" align="right">'.$sex.'</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">Pegawai / Lain-lain</td>
                    <td colspan="2"></td>
                </tr>
                <tr>
                    <td colspan="3" width="240"></td>
                    <td>Bangsa</td>
                    <td>: '.$this->people->nationality.'</td>
                </tr>
                <tr>
                    <td colspan="3" width="240">Nama : '.ucwords($this->people->fullname).'</td>
                    <td>Suku</td>
                    <td>: '.$this->people->race.'</td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td>Tahun Lahir</td>
                    <td>: '.date('Y', $birth_date).'</td>
                </tr>
                <tr>
                    <td colspan="5" height="2"></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="4" height="30" valign="bottom">Rumus : '.$this->people->formula.'</td>
                </tr>
                <tr>
                    <td colspan="2">Catatan2 yang diperlukan : </td>
                    <td></td>
                    <td colspan="2" align="right">Nama / Tanda Tangan Petugas : </td>
                </tr>
                <tr>
                    <td colspan="5" height="35"></td>
                </tr>
                <tr>
                    <td colspan="2">................................................</td>
                    <td></td>
                    <td colspan="2" align="right">.......................................................</td>
                </tr>
            </table>';

        // print a block of text using Write()
        $pdf->writeHTML($tbl, false, false, true, false, '');

        ob_end_clean();

        // Close and output PDF document
        $pdf->Output(str_slug($this->people->fullname).'_ak-24.pdf', 'I');
    }

    /**
     * Create PDF file for AK-24
     * @param  string $id
     * @return PDF File
     */
    public function ak24_multiple(Request $request)
    {
        $data_print_session = session()->has('print_data') ? session()->get('print_data') : [];
        $data_tracking = [];

        if ( count($data_print_session) > 0 && ! $request->has('data') ) 
        {
            $data_print_session = implode(',', $data_print_session);
            return redirect( url('export/ak24').'?data='.Crypt::encrypt($data_print_session) );
        }

        // session()->forget('print_data');

        try 
        {
            $data_print = explode(',', Crypt::decrypt($request->input('data')));
        }
        catch (\Exception $e)
        {
            record_error('Error decrypt ID in ExportController -> ak24_multiple()', $e, $request);
            return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");
        }

        foreach ($data_print as $value) 
        {
            $data_tracking[] = [
                'subject_id' => $value,
                'ip_address' => $_SERVER['REMOTE_ADDR'],
                'created_at' => date('Y-m-d H:i:s')
            ];
        }
        
        try
        {
            DB::table('ak24_print')->insert($data_tracking);
            // session(['ak24_track_flag' => true]);
        }
        catch (\Exception $e)
        {
            record_error('Error in inserting AK24 tracking data.', $e, $request);
        }

        // Log activity
        $this->activity->log('print-ak24', 'Pengguna mencetak AK24 data lokal dengan ID ' . json_encode($data_print), Auth::id());

        if ( count($data_print) > 0 )
        {
            $data_error = [];            

            $tbl = '<table border="0" cellpadding="0" cellspacing="0" width="806">';
            $count = 1;

            end($data_print);
            $last_key = key($data_print);

            foreach ($data_print as $key => $subjectId) 
            {
                $this->people = $this->people->where('subject_id', $subjectId)->first();

                if ( ! $this->people )
                {
                    array_push($data_error, $subjectId);
                    continue;
                }

                $birth_date = strtotime($this->people->dob);
                $sex = ($this->people->sex == 'm') ? 'Laki-laki' : 'Perempuan';

                $formula = explode('|', $this->people->formula);

                $formula_view = explode('|', $this->people->formula_view);

                if ( in_array($this->people->status, $this->status_allowed) )
                {
                    $this->status = $this->people->status_data->label;
                }
                
                if( $count%2 != 0 )
                {
                    $tbl .= '<tr>';   
                }

                $tbl .= view('export.ak24', compact('formula', 'formula_view', 'sex', 'birth_date'))->with(['people' => $this->people, 'status' => $this->status])->render();

                $count++;

                if( $count%2 == 0 )
                {
                    $tbl .= '<td width="20"></td>';
                }

                if( $count%2 != 0 || $key == $last_key )
                {
                    $tbl .= '</tr>';
                }
            }

            $tbl .= '</table>';

            try
            {
                // create new PDF document
                $pdf = new TCPDF('P', PDF_UNIT, [215.9,279.4], true, 'UTF-8', false);

                // set document information
                $pdf->SetCreator('Biover Biometric');
                $pdf->SetAuthor('Inafis');
                $pdf->SetTitle('Cetak - AK-24');
                $pdf->SetSubject('Cetak - AK-24');
                // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

                // remove default header/footer
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                // set margins
                $pdf->SetMargins(3, 4, 3);

                // set auto page breaks
                $pdf->SetAutoPageBreak(FALSE, 0);

                // set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                // set font
                $pdf->SetFont('times', 'B', 7);

                // add a page
                $pdf->AddPage();

                $pdf->writeHTML($tbl, false, false, true, false, '');
                // print a block of text using Write()

                if (ob_get_contents()) ob_end_clean();

                // Close and output PDF document
                $pdf->Output(public_path('pdf/ak-24.pdf'), 'F');

                return redirect(url('pdfjs/web/viewer.html?file=../../pdf/ak-24.pdf'));
            }
            catch (\Exception $e)
            {
                record_error('Error in AK24 PDF Print Local.', $e, $request);
                return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");
            }
        }
        else
        {
            return abort('404');
        }
    }

    /**
     * Create PDF file for AK-24
     * @param  string $id
     * @return PDF File
     */
    public function ak24MultipleServer(Request $request)
    {
        $data_print_session = session()->has('print_data') ? session()->get('print_data') : [];
        $data_tracking = [];

        if ( count($data_print_session) > 0 && ! $request->has('data') ) 
        {
            $data_print_session = implode(',', $data_print_session);
            return redirect( url('export/ak24-server').'?data='.Crypt::encrypt($data_print_session) );
        }

        // session()->forget('print_data');

        try 
        {
            $data_print = explode(',', decrypt($request->input('data')));
        }
        catch (\Exception $e)
        {
            record_error('Error decrypt ID in ExportController -> ak24MultipleServer()', $e, $request);
            return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");
        }

        if ( count($data_print) > 0 )
        {
            $data_error = [];            

            $tbl = '<table border="0" cellpadding="0" cellspacing="0" width="806">';
            $count = 1;

            end($data_print);
            $last_key = key($data_print);

            foreach ($data_print as $key => $id) 
            {
                $people = $this->javaServerService->search(['id' => $id, 'size' => 1]);

                if ( $people['status'] != 'OK' )
                {
                    array_push($data_error, $id);
                    continue;
                }

                $data_tracking[] = [
                    'subject_id' => $people['results']['content'][0]['subjectId'],
                    'ip_address' => $_SERVER['REMOTE_ADDR'],
                    'created_at' => date('Y-m-d H:i:s')
                ];

                $birth_date = ! empty($people['results']['content'][0]['dob']) ? date('Y', strtotime($people['results']['content'][0]['dob'])) : '-';
                $sex = ($people['results']['content'][0]['sex'] == 'm') ? 'Laki-laki' : 'Perempuan';

                $formula = explode('|', $people['results']['content'][0]['formula']);

                $formula_view = explode('|', $people['results']['content'][0]['formulaView']);

                if ( isset($people['results']['content'][0]['status']) )
                {
                    $this->status = $people['results']['content'][0]['status']['label'];
                }
                
                if( $count%2 != 0 )
                {
                    $tbl .= '<tr>';   
                }

                $tbl .= view('export.ak24_server', compact('formula', 'formula_view', 'sex', 'birth_date'))->with(['people' => $people['results']['content'][0], 'status' => $this->status])->render();

                $count++;

                if( $count%2 == 0 )
                {
                    $tbl .= '<td width="20"></td>';
                }

                if( $count%2 != 0 || $key == $last_key )
                {
                    $tbl .= '</tr>';
                }
            }

            $tbl .= '</table>';

            try
            {
                DB::table('ak24_print')->insert($data_tracking);
                // session(['ak24_track_flag' => true]);
            }
            catch (\Exception $e)
            {
                record_error('Error in inserting AK24 tracking data.', $e, $request);
            }

            // Log activity
            $this->activity->log('print-ak24', 'Pengguna mencetak AK24 data server dengan ID ' . json_encode($data_print), Auth::id());

            try
            {
                // create new PDF document
                // $pdf = new TCPDF('P', PDF_UNIT, [300,240], true, 'UTF-8', false);
                $pdf = new TCPDF('P', PDF_UNIT, [215.9,279.4], true, 'UTF-8', false);

                // set document information
                $pdf->SetCreator('Biover Biometric');
                $pdf->SetAuthor('Inafis');
                $pdf->SetTitle('Cetak - AK-24');
                $pdf->SetSubject('Cetak - AK-24');
                // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

                // remove default header/footer
                $pdf->setPrintHeader(false);
                $pdf->setPrintFooter(false);

                // set default monospaced font
                $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

                // set margins
                $pdf->SetMargins(3, 4, 3);

                // set auto page breaks
                $pdf->SetAutoPageBreak(FALSE, 0);

                // set image scale factor
                $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

                // set font
                $pdf->SetFont('times', 'B', 7);

                // add a page
                $pdf->AddPage();

                $pdf->writeHTML($tbl, false, false, true, false, '');
                // print a block of text using Write()

                if (ob_get_contents()) ob_end_clean();

                // Close and output PDF document
                $pdf->Output(public_path('pdf/ak-24.pdf'), 'F');
                
                return redirect(url('pdfjs/web/viewer.html?file=../../pdf/ak-24.pdf'));
            }
            catch (\Exception $e)
            {
                record_error('Error in AK24 PDF Print server.', $e, $request);
                return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");
            }
        }
        else
        {
            return abort(500, "[btn_close_window]Data tidak ditemukan.");
        }
    }

    public function compare_print(Request $request, $data = null)
    {
        $request_data = $request_path_data = $candidate_path_data = $formula = $formula_view = [];

        $request_folder = '';

        $file = session('process');

        $candidate_image = $request_image = asset('assets/images/no-photo.png'); 

        $completed_folder_name = config('filename.complete_folder');

        $randDir = strtolower(str_random(12));

        try
        {
            $data = decrypt($data);
        }
        catch (\Exception $e)
        {
            record_error('Error in decrypt compare ID.', $e, $request);
            return view('errors.custom', ['data' => ['title' => "Ma'af terjadi kesalahan saat mencetak dokumen.", 'description' => 'Mohon untuk mencoba lagi beberapa saat.']]);
        }

        ImageUtils::createWatermarkImage();

        $data = explode('|', $data);
        
        $requested_id = $data[0];
        $candidate_id = $data[1];
        $source = session('identification_source', 'local');

        if ( session()->has('process') && empty($requested_id) )
        {
            if ( ! file_exists(session('process.basePath') . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic')) )
            {
                return abort(500, '[btn_back]Gagal mencetak dokumen. File data demografik tidak ditemukan');
            }

            $request_data = get_data_from_file(session('process.basePath') . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'));

            $source_name = (session('process.type') != 'card') ? config('filename.live_folder') : config('filename.card_folder');

            if ( array_has(session('process'), 'process.folder') )
            {
                $request_folder = session('process.folder');
            }

            $request_image = File::exists( $file['basePath'] . DIRECTORY_SEPARATOR . $this->fileNaming['face_front'] ) ? "{$file['baseUrl']}/" . $this->fileNaming['face_front'] : asset('assets/images/no-photo.png');

            $request_path_data = [
                    'url' => asset("files/{$source_name}/{$request_folder}/"),
                    'path' => config('path.data.tmp_minutiae')
                ];
        }
        else
        { 
            /**
             * This else section is for edit demographic
             */
            $request_data = $this->people->findorfail($requested_id)->toArray();

            $request_image = File::exists( $this->path['data']['complete'] . DIRECTORY_SEPARATOR . $requested_id . DIRECTORY_SEPARATOR . $this->fileNaming['face_front'] ) ? asset("files/{$completed_folder_name}/{$requested_id}/" . $this->fileNaming['face_front']) : asset('assets/images/no-photo.png');

            $request_path_data = [
                    'url' => asset("files/{$completed_folder_name}/{$requested_id}/"),
                    'path' => $this->path['data']['complete'] . DIRECTORY_SEPARATOR . $requested_id
                ];
        }

        // if ( ! File::isDirectory(config('path.data.tmp') . DIRECTORY_SEPARATOR . 'minutiae') )
        // {
        //     File::makeDirectory(config('path.data.tmp') . DIRECTORY_SEPARATOR . 'minutiae', 0777);
        // }
        // else
        // {
        //     File::deleteDirectory(config('path.data.tmp') . DIRECTORY_SEPARATOR . 'minutiae', true);
        // }

        if ( $source == 'local' )
        {
            $candidate_data = $this->people->where('subject_id', $candidate_id)->firstOrFail();

            $candidate_path_data = [
                    'url' => asset("files/{$completed_folder_name}/{$candidate_data->demographic_id}/"),
                    'path' => config('path.data.tmp_compare') . DIRECTORY_SEPARATOR . $candidate_data->subject_id
                ];

            if ( File::exists($this->path['data']['complete'] . DIRECTORY_SEPARATOR . $candidate_data->demographic_id . DIRECTORY_SEPARATOR . $this->fileNaming['face_front']) ) 
            {
                $candidate_image = asset("files/{$completed_folder_name}/{$candidate_data->demographic_id}/{$this->fileNaming['face_front']}");
            }

            $formula = [
                'requested' => explode('|', !empty($request_data['formula']) ? $request_data['formula'] : '|||||' ),
                'candidate' => explode('|', $candidate_data['formula']),
            ];

            $formula_view = [
                'requested' => explode('|', !empty($request_data['formula_view']) ? $request_data['formula_view'] : '|||||' ),
                'candidate' => explode('|', $candidate_data['formula_view']),
            ];
        }
        else
        {
            // source server
            $candidate_data = get_data_from_file(config('path.data.tmp_compare') . DIRECTORY_SEPARATOR . $candidate_id . DIRECTORY_SEPARATOR . '_demographic.json');

            if ( array_has($candidate_data, 'results.content.0') )
            {
                $candidate_data = array_get($candidate_data, 'results.content.0');
                $comparePath = config('path.data.tmp_compare');

                $candidate_path_data = [
                    'url' => array_get($candidate_data, 'baseUrlAssets'),
                    'path' => $comparePath . DIRECTORY_SEPARATOR . $candidate_id
                ];

                if ( File::exists($comparePath . DIRECTORY_SEPARATOR .  $candidate_id . DIRECTORY_SEPARATOR . $this->fileNaming['face_front']) ) 
                {
                    $candidate_image = data_url("tmp/compare/{$candidate_id}/{$this->fileNaming['face_front']}");
                }

                $formula = [
                    'requested' => explode('|', !empty($request_data['formula']) ? $request_data['formula'] : '|||||' ),
                    'candidate' => explode('|', array_get($candidate_data, 'formula')),
                ];

                $formula_view = [
                    'requested' => explode('|', !empty($request_data['formula_view']) ? $request_data['formula_view'] : '|||||' ),
                    'candidate' => explode('|', array_get($candidate_data, 'formulaView')),
                ];
            }
            else
            {
                return abort(500, '[btn_close_window]Data tidak ditemukan pada server.');
            }
        }

        try
        {
            // create new PDF document
            $pdf = new COMPAREPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);

            // set document information
            $pdf->SetCreator('Biover Biometric');
            $pdf->SetAuthor('Inafis');
            $pdf->SetTitle('Biover Biometric Verification Station');
            $pdf->SetSubject('Biover Biometric Verification Station');
            // $pdf->SetKeywords('TCPDF, PDF, example, test, guide');

            // set default header data
            $pdf->SetHeaderData('', '', 'Laporan - Biover Biometric Verification Station', null, array(0,0,0), array(255,255,255));
            $pdf->setFooterData(array(0,0,0), array(0,0,0));

            $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 20));

            // set margins
            $pdf->SetMargins(10, 20, 10);
            $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $pdf->SetAutoPageBreak(TRUE, 0);

            // set image scale factor
            $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

            // set font
            $pdf->SetFont('times', '', 12);

            // add a page
            $pdf->AddPage();

            if ( $source == 'local' )
            {
                $data_print = view('export.compare', compact('candidate_data', 'request_data', 'request_image', 'candidate_image', 'request_folder', 'request_path_data', 'candidate_path_data', 'randDir', 'formula', 'formula_view'))->render();
            }
            else
            {
                $data_print = view('export.compare_server', compact('candidate_data', 'request_data', 'request_image', 'candidate_image', 'request_folder', 'request_path_data', 'candidate_path_data', 'randDir', 'formula', 'formula_view'))->render();
            }

            // print a block of text using Write()
            $pdf->writeHTML($data_print, false, false, true, false, '');

            if (ob_get_contents()) ob_end_clean();   

            // Close and output PDF document
            $pdf->Output(public_path('pdf/verification-station.pdf'), 'F');
            
            return redirect(url('pdfjs/web/viewer.html?file=../../pdf/verification-station.pdf'));

            // if ( File::isDirectory(config('path.data.tmp') . DIRECTORY_SEPARATOR . $randDir) )
            // {
            //     File::deleteDirectory(config('path.data.tmp') . DIRECTORY_SEPARATOR . $randDir);
            // }
        }
        catch (\Exception $e)
        {
            record_error('Error in compare print.', $e, $request);
            return abort(500, "[btn_close_window]Ma'af terjadi kesalahan saat mencetak dokumen.");
        }
    }

    public function ak24Clear()
    {
        session()->forget('print_data');
        return redirect()->back()->with('status', ['type' => 'success', 'message' => 'Data AK24 terpilih telah dibersihkan']);
    }
}