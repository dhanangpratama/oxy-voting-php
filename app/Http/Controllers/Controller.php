<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View, File;
use App\Services\HardwareService;
use App\Utils\ToolUtils;
use Illuminate\Support\Facades\Route;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $queue;
    protected $path;
    protected $option;
    protected $urlPath;
    protected $networkStatus;

    public function __construct() 
    {
        $this->queue = app()->make('QueueService');
        $this->option = app()->make('OptionRepository');
    	$this->path = config('path');
        $this->networkStatus = ToolUtils::networkStatus();

        View::share('path', $this->path);
        View::share('option', $this->option);
        View::share('networkStatus', ToolUtils::networkStatus());
        View::share('helperui', '');
    }

    protected function checkDataFolders()
    {
        try
        {
            if ( !File::isDirectory(config('path.data.base')) )
            {
                throw new \Exception('Missing base path ' . config('path.data.base'));
            }
        }
        catch ( \Exception $e )
        {
            record_error('Missing base path folder', $e);
            return abort(500, '[btn_shutdown]Folder data tidak ditemukan.<br>Mohon untuk segera melapor.');
        }

        try
        {
            if ( !File::isDirectory(config('path.data.base') . DIRECTORY_SEPARATOR . 'files') )
            {
                File::makeDirectory(config('path.data.base') . DIRECTORY_SEPARATOR . 'files', 0777);
            }

            foreach (config('path.data') as $dir)
            {
                if ( !File::isDirectory($dir) )
                {
                    File::makeDirectory($dir, 0777);
                }
            }
        }
        catch ( \Exception $e )
        {
            record_error('Failed create data folders', $e);
            return abort(500, '[btn_shutdown]Gagal membuat data folder.<br>Mohon untuk segera melapor.');
        }
    }
}
