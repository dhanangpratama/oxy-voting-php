<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use View;
use Route;

class DemographicController extends MainController
{
    protected $routeName;

    public function __construct()
    {
        parent::__construct();

        $this->routeName = Route::currentRouteName();

        View::share('routeName', $this->routeName);

        /** to use session in constructor must place inside middleware for laravel 5.3 and above */
        $this->middleware(function ($request, $next) 
        {
            return $next($request);
        });
    }
}
