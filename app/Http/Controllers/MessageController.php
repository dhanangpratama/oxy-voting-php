<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MessageBroadcast;
use Validator;
use DB, Cache;
use Carbon\Carbon;

class MessageController extends MainController
{
    private $rules;

    private $request;

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->rules = [
            'start_date' => 'required',
            'end_date' => 'required',
            'type' => 'required',
            'message' => 'required'
        ];

        $this->request = $request;
    }

    public function index()
    {
        $title = 'Daftar Pesan';
        $page = 'list-messages';

        $messages = new MessageBroadcast;

        $messages = $messages->paginate(20);

        return view('message.index', compact('title', 'page', 'messages'));
    }

    public function create()
    {
        $title = 'Buat Pesan Baru';
        $page = 'new-message';

        return view('message.form', compact('title', 'page'));
    }

    public function edit($id = null)
    {
        $title = 'Ubah Pesan';
        $page = 'edit-message';

        try
        {
            $id = decrypt($id);
        }
        catch ( \Exception $e )
        {
            record_error('Error in decrypt ID in edit page', $e, $this->request);
            return abort(500, '[btn_back]Gagal menampilkan data pesan');
        }

        $message = MessageBroadcast::findOrFail($id);

        return view('message.form', compact('title', 'page', 'message'));
    }

    public function save()
    {
        $data = $this->request->only('start_date', 'end_date', 'type', 'message');

        $validator = Validator::make($data, $this->rules);

        if ($validator->fails()) 
        {
            return redirect()->route('message_create')->withErrors($validator, 'message')->withInput();
        }

        $data['start_date'] = db_format_date($data['start_date']);
        $data['end_date'] = db_format_date($data['end_date']);
        $data['created_by'] = $this->loginUserId;

        try
        {
            MessageBroadcast::create($data);

            Cache::forget('message_broadcast');

            $this->activity->log('message-broadcast-new', 'Membuat data baru message broadcast ', $this->loginUserId);

            return redirect()->route('message_create')->with('status', status('success', 'Pesan berhasil dibuat'));
        }
        catch (\Exception $e)
        {
            record_error('Error in create message broadcast', $e, $this->request);
            return abort(500, '[btn_back]Gagal membuat pesan');
        }
    }

    public function saveEdit($id = null)
    {
        if ( ! empty($id) )
        {
            $data = $this->request->only('start_date', 'end_date', 'type', 'message');

            $validator = Validator::make($data, $this->rules);

            if ($validator->fails()) 
            {
                return redirect()->route('message_edit', [encrypt($id)])->withErrors($validator, 'message')->withInput();
            }

            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error in decrypt ID in edit save page', $e, $this->request);
                return abort(500, '[btn_back]Gagal menampilkan data pesan');
            }

            $message = MessageBroadcast::findOrFail($id);

            $data['start_date'] = db_format_date($data['start_date']);
            $data['end_date'] = db_format_date($data['end_date']);
            $data['created_by'] = $this->loginUserId;

            try
            {
                foreach ($data as $column => $value)
                {
                    $message->$column = $value;
                }

                $message->save();

                $this->activity->log('message-broadcast-edit', 'Mengubah data message broadcast dengan ID ' . $id, $this->loginUserId);

                Cache::forget('message_broadcast');

                return redirect()->route('message_edit', [encrypt($id)])->with('status', status('success', 'Pesan berhasil diubah'));
            }
            catch (\Exception $e)
            {
                record_error('Error in edit message broadcast', $e, $this->request);
                return abort(500, '[btn_back]Gagal membuat pesan');
            }
        }
        else
        {
            return abort(404);
        }
    }

    public function remove($id = null)
    {
        $title = 'Ubah Pesan';
        $page = 'edit-message';

        try
        {
            $id = decrypt($id);
        }
        catch ( \Exception $e )
        {
            record_error('Error in decrypt ID in remove page', $e, $this->request);
            return abort(500, '[btn_back]Gagal menampilkan data pesan');
        }

        try
        {
            $message = MessageBroadcast::findOrFail($id);

            if ($message->delete() )
            {
                return redirect()->route('message')->with('status', ['type' => 'success', 'message' => 'Pesan berhasil dihapus.']);
            }

            return redirect()->route('message')->with('status', ['type' => 'error', 'message' => 'gagal menghapus pesan.']);
        }
        catch ( \Exception $e )
        {
            record_error('Error in remove message', $e, $this->request);
            return redirect()->route('message')->with('status', ['type' => 'error', 'message' => 'gagal menghapus pesan.']);
        }
    }
}
