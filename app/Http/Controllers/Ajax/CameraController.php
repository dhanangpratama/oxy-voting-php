<?php

namespace App\Http\Controllers\Ajax;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\CameraService;

class CameraController extends Controller
{
    private $request;

    private $cameraService;

    public function __construct(Request $request, CameraService $cameraService)
    {
        $this->request = $request;
        $this->cameraService = $cameraService;
    }

    public function process()
    {
        $data = $this->request->only('imageData');

        if ( ! empty($data) )
        {
            return $this->cameraService->process($data, $this->request);
        }

        return ['status' => 'error'];
    }

    public function remove()
    {
        $data = $this->request->only('image');
        
        if ( ! empty($data) )
        {
            return $this->cameraService->remove($data, $this->request);
        }
    }
}
