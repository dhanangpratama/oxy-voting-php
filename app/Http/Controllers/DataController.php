<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Skin;
use App\Models\Body;
use App\Models\Head;
use App\Models\HairColor;
use App\Models\Hair;
use App\Models\Face;
use App\Models\Forehead;
use App\Models\EyeColor;
use App\Models\EyeIrregularity;
use App\Models\Nose;
use App\Models\Lip;
use App\Models\Tooth;
use App\Models\Chin;
use App\Models\Ear;
use App\Models\Religion;
use App\Models\Education;
use App\Models\Status;
use App\Models\Location;
use App\Models\Role;
use DB, Validator, View;
use Carbon\Carbon;

class DataController extends MainController
{
    private $request;

    private $rules;

    private $dataRelation;

    public function __construct(Request $request)
    {
        parent::__construct();

        $this->request = $request;

        $this->dataRelation = config('ak23.dataRelation');

        View::share('dataRelation', $this->dataRelation);

        $this->rules = [
            'label' => 'required'
        ];
    }

    public function index($type = null)
    {
        if ( ! empty($type) )
        {
            $title = 'Data ' . $this->dataRelation[$type]['label'];
            $page = 'list-'.$type;

            $data = $this->initClass($type)->paginate(10);

            $primaryKey = $this->dataRelation[$type]['id'];

            return view('data.index', compact('title', 'page', 'data', 'type', 'primaryKey'));
        }
        else
        {
            return abort(404);
        }
    }

    public function location()
    {
        $title = 'Data Lokasi';
        $page = 'list-location';

        $locations = new Location;
        $locations = $locations->orderBy('name', 'asc')->get();

        return view('data.location', compact('title', 'page', 'locations'));
    }

    public function locationCreate()
    {
        $title = 'Buat Lokasi Baru';
        $page = 'create-location';

        return view('data.location_form', compact('title', 'page'));
    }

    public function locationSave()
    {
        $data = $this->request->except('_token');

        $rules = [
            'name' => 'required'
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) 
        {
            return redirect()->route('data_location_create')->withErrors($validator, 'location')->withInput();
        }

        DB::beginTransaction();

        try
        {
            $location = new Location;

            $location->name = $data['name'];
            $location->activated = array_has($data, 'activated') ? $data['activated'] : 0;
            $location->save();
        }
        catch ( \Exception $e )
        {
            DB::rollBack();
            record_error('Error in create location', $e, $this->request);
            return abort(500, '[btn_back]Gagal menyimpan data lokasi');
        }

        DB::commit();

        return redirect()->route('data_location')->with('status', ['type' => 'success', 'message' => 'Lokasi '.$location->name.' berhasil dibuat']);
    }

    public function locationEdit($id = null)
    {
        if ( ! empty($id) )
        {
            $title = 'Ubah Lokasi';
            $page = 'edit-location';

            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error in decrypt ID in edit data location', $e, $this->request);
                return abort(500, '[btn_back]Gagal menampilkan data');
            }

            $location = Location::findOrFail($id);

            return view('data.location_form', compact('title', 'page', 'location'));
        }
        else
        {
            return abort(404);
        }
    }

    public function role()
    {
        $title = 'Data Level';
        $page = 'list-level';

        $roles = new Role;
        $roles = $roles->paginate(10);

        return view('data.role', compact('title', 'page', 'roles'));
    }

    public function roleCreate()
    {
        $title = 'Buat Level Baru';
        $page = 'create-role';

        return view('data.role_form', compact('title', 'page'));
    }

    public function roleEdit($id = null)
    {
        if ( ! empty($id) )
        {
            $title = 'Ubah Level';
            $page = 'edit-role';

            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error in decrypt ID in edit data level', $e, $this->request);
                return abort(500, '[btn_back]Gagal menampilkan data');
            }

            $role = Role::findOrFail($id);

            return view('data.role_form', compact('title', 'page', 'role'));
        }
        else
        {
            return abort(404);
        }
    }

    public function create($type = null)
    {
        if ( ! empty($type) )
        {
            $title = 'Buat Baru Data ' . $this->dataRelation[$type]['label'];
            $page = 'create-data';

            $primaryKey = $this->dataRelation[$type]['id'];

            return view('data.form', compact('title', 'page', 'type', 'primaryKey'));
        }
        else
        {
            return abort(404);
        }
    }

    public function edit($type = null, $id = null)
    {
        if ( ! empty($type) && ! empty($id) )
        {
            $title = 'Ubah Data';
            $page = 'edit-data';

            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error in decrypt ID in edit data', $e, $this->request);
                return abort(500, '[btn_back]Gagal menampilkan data data');
            }

            $data = $this->initClass($type)->findOrFail($id);

            $primaryKey = $this->dataRelation[$type]['id'];

            return view('data.form', compact('title', 'page', 'data', 'type', 'primaryKey'));
        }
        else
        {
            return abort(404);
        }
    }

    public function save($type = null)
    {
        if ( ! empty($type) )
        {
            $data = $this->request->only('label');
            $data['created_at'] = $data['updated_at'] = Carbon::now();

            $validator = Validator::make($data, $this->rules);

            if ($validator->fails()) 
            {
                return redirect()->route('data_create')->withErrors($validator, 'data')->withInput();
            }

            try
            {
                DB::table($type)->insert($data);

                $this->activity->log('demographic-sinyalemen-new', 'Membuat data baru sinyalemen ' . $type, $this->loginUserId);

                return redirect()->route('data', [$type])->with('status', status('success', 'Data baru berhasil dibuat'));
            }
            catch (\Exception $e)
            {
                record_error('Error in save data relation', $e, $this->request);
                return abort(500, '[btn_back]Gagal membuat data');
            }
        }
        else
        {
            return abort(404);
        }
    }

    public function saveEdit($type = null, $id = null)
    {
        if ( ! empty($id) && ! empty($type) )
        {
            $data = $this->request->only('label');

            $validator = Validator::make($data, $this->rules);

            if ($validator->fails()) 
            {
                return redirect()->route('data_edit', [$type, encrypt($id)])->withErrors($validator, 'data')->withInput();
            }

            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error in decrypt ID in edit save data relation', $e, $this->request);
                return abort(500, '[btn_back]Gagal menampilkan data');
            }

            $dataUpdate = $this->initClass($type)->findOrFail($id);

            try
            {
                foreach ($data as $column => $value)
                {
                    $dataUpdate->$column = $value;
                }

                $dataUpdate->save();

                $this->activity->log('demographic-sinyalemen-edit', 'Mengubah data sinyalemen ' . $type . ' dengan ID ' . $id, $this->loginUserId);

                return redirect()->route('data', [$type])->with('status', status('success', 'Data berhasil diubah'));
            }
            catch (\Exception $e)
            {
                record_error('Error in edit data', $e, $this->request);
                return abort(500, '[btn_back]Gagal mengubah data');
            }
        }
        else
        {
            return abort(404);
        }
    }

    public function locationSaveEdit($id = null)
    {
        if ( ! empty($id) )
        {
            $data = $this->request->only('name', 'activated');

            if ( ! empty($data['activated']) )
            {
                $data['activated'] = 1;
            }
            else
            {
                $data['activated'] = 0;
            }

            $rules = [
                'name' => 'required'
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) 
            {
                return redirect()->route('data_location_edit', [encrypt($id)])->withErrors($validator, 'location')->withInput();
            }

            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error in decrypt ID in edit save data location', $e, $this->request);
                return abort(500, '[btn_back]Gagal menampilkan data');
            }

            $dataUpdate = Location::findOrFail($id);

            try
            {
                foreach ($data as $column => $value)
                {
                    $dataUpdate->$column = $value;
                }

                $dataUpdate->save();

                $this->activity->log('demographic-location-edit', 'Mengubah data location dengan ID ' . $id, $this->loginUserId);

                return redirect()->route('data_location')->with('status', status('success', 'Data berhasil diubah'));
            }
            catch (\Exception $e)
            {
                record_error('Error in edit data location', $e, $this->request);
                return abort(500, '[btn_back]Gagal mengubah data');
            }
        }
        else
        {
            return abort(404);
        }
    }

    public function roleCreateSubmit()
    {
        $data = $this->request->only('name', 'display_name', 'description');

        $rules = [
            'name' => 'required|unique:roles'
        ];

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) 
        {
            return redirect()->route('data_role_create')->withErrors($validator, 'role')->withInput();
        }

        try
        {
            Role::create($data);
            $this->activity->log('role-new', 'Menambah level baru ' . json_encode($data), $this->loginUserId);
            return redirect()->route('data_role')->with('status', status('success', 'Data baru berhasil dibuat'));
        }
        catch (\Exception $e)
        {
            record_error('Error in save data level', $e, $this->request);
            return abort(500, '[btn_back]Gagal membuat data');
        }
    }

    public function roleSaveEdit($id = null)
    {
        if ( ! empty($id) )
        {
            $data = $this->request->only('name', 'display_name', 'description');

            $nameUnique = '';

            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error in decrypt ID in edit data role', $e, $this->request);
                return abort(500, '[btn_back]Gagal menampilkan data role');
            }

            $role = Role::findOrFail($id);

            if ($data['name'] != $role->name)
            {
                $nameUnique = '|unique:roles';
            }

            $rules = [
                'name' => 'required'.$nameUnique
            ];

            $validator = Validator::make($data, $rules);

            if ($validator->fails()) 
            {
                return redirect()->route('data_role_edit', [encrypt($id)])->withErrors($validator, 'role')->withInput();
            }
            
            try
            {
                foreach ($data as $column => $value)
                {
                    $role->$column = $value;
                }

                $role->save();

                $this->activity->log('role-edit', 'Mengubah data level dengan ID ' . $id, $this->loginUserId);

                return redirect()->route('data_role')->with('status', status('success', 'Data berhasil diubah'));
            }
            catch (\Exception $e)
            {
                record_error('Error in edit data role', $e, $this->request);
                return abort(500, '[btn_back]Gagal mengubah data');
            }
        }
        else
        {
            return abort(404);
        }
    }

    public function remove(Request $request, $type = null, $id = null)
    {
        if ( ! empty($id) && ! empty($type) )
        {
            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error decrypt ID', $e, $request);
                return abort(500, '[btn_back]Gagal menghapus data.');
            }

            if ( $type == 'location' )
            {
                $data = Location::findOrFail($id);
            }
            else
            {
                $data = $this->initClass($type)->findOrFail($id);
            }

            if ( $this->auth->hasRole('administrator') )
            {
                try
                {
                    $message = $data->delete() ? status('success', 'Data berhasil dihapus') : status('error', 'Data gagal dihapus');

                    $this->activity->log('data-delete', 'Menghapus data ' . $type . ' dengan ID ' . $id, $this->loginUserId);

                    return redirect()->back()->with('status', $message);
                }
                catch ( \Exception $e )
                {
                    record_error('Error delete data', $e, $request);

                    if ( strpos($e->getMessage(), 'Cannot delete or update a parent row') !== false )
                    {
                        return abort(500, '[btn_back]Tidak diijinkan menghapus data dikarenakan data memiliki relasi.');
                    }

                    return abort(500, '[btn_back]Gagal menghapus data.');
                }
            }

            return abort(500, '[btn_back]Tidak diijinkan melakukan hal ini');
        }

        return abort(404);
    }

    private function initClass($type)
    {
        $init = new Body;

        if ( $type == 'chin' )
        {
            $init = new Chin;
        }

        if ( $type == 'skin' )
        {
            $init = new Skin;
        }

        if ( $type == 'head' )
        {
            $init = new Head;
        }

        if ( $type == 'hair_color' )
        {
            $init = new HairColor;
        }

        if ( $type == 'hair' )
        {
            $init = new Hair;
        }

        if ( $type == 'face' )
        {
            $init = new Face;
        }

        if ( $type == 'forehead' )
        {
            $init = new Forehead;
        }

        if ( $type == 'eye_color' )
        {
            $init = new EyeColor;
        }

        if ( $type == 'eye_irregularity' )
        {
            $init = new EyeIrregularity;
        }

        if ( $type == 'nose' )
        {
            $init = new Nose;
        }

        if ( $type == 'lip' )
        {
            $init = new Lip;
        }

        if ( $type == 'tooth' )
        {
            $init = new Tooth;
        }

        if ( $type == 'ear' )
        {
            $init = new Ear;
        }

        if ( $type == 'education' )
        {
            $init = new Education;
        }

        if ( $type == 'religion' )
        {
            $init = new Religion;
        }

        if ( $type == 'status' )
        {
            $init = new Status;
        }

        return $init;
    }
}
