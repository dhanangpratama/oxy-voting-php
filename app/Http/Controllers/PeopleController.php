<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Services\Api\JavaServerService;
use App\Models\People;
use App\Models\Skin;
use App\Models\Body;
use App\Models\Head;
use App\Models\HairColor;
use App\Models\Hair;
use App\Models\Face;
use App\Models\Forehead;
use App\Models\EyeColor;
use App\Models\EyeIrregularity;
use App\Models\Nose;
use App\Models\Lip;
use App\Models\Tooth;
use App\Models\Chin;
use App\Models\Ear;
use App\Models\Henry;
use App\Models\Religion;
use App\Models\Education;
use App\Models\Status;
use App\Models\AK24_Print;
use GuzzleHttp\Client;
use App\Utils\ApiUtils;
use App\Utils\ImageUtils;
use Illuminate\Http\Request;
use Session;
use Crypt;
use Log;
use DB;
use Cache;
use File;
use Image;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class PeopleController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */

    protected $data = [];

    protected $search_server;

    protected $is_datacenter = true;

    private $javaServerService;

    public function __construct(JavaServerService $javaServerService)
    {
        parent::__construct();

        $this->data['workstation.location'] = null;

        $this->search_server = env('SEARCH_SERVER', true);

        $this->javaServerService = $javaServerService;
    }

    public function index(Request $request)
    {
        $fullName       = $request->get('FullName');
        $pob            = $request->get('pob');
        $dateOfBirth    = $request->get('DateOfBirth');
        $idcard         = $request->get('IDCard');
        $status         = $request->get('status');
        $limit          = $request->get('limit', 50);
        $page           = $request->get('page', 1);
        $source         = $request->get('source');
        $status_data    = [];

        $fields = $request->except('source');

        foreach ($fields as $key => $field)
        {
            if (empty($field))
            {
                unset($fields[$key]);
            }
        }

        if ( empty($fields) )
        {
            return redirect()->route('people.search')->with('status', ['type' => 'error', 'message' => 'Mohon untuk mengisi salah satu kriteria pencarian']);
        }

        $this->activity->log('demographic-search', 'Melakukan pencarian data dengan parameter ' . json_encode($request->query()), $this->loginUserId);

        $ektpFieldsSearch = [
            'nama_lengkap' => '',
            'nama_lengkap_ayah' => '',
            'nama_lengkap_ibu' => '',
            'pekerjaan' => '',
            'tmpt_lahir' => '',
            'tgl_lahir' => '',
            'alamat' => '' 
        ];

        $osInafisFieldSearch = [
            "nama_lengkap_ayah" => "",
            "nama_lengkap_ibu" => "",
            "tmpt_lahir" => "",
            "nama_lengkap" => "",
            "source" => "",
            "tgl_lahir" => "",
            "alamat" => ""
        ];

        if ( ! empty($fullName) )
        {
            $this->data['fullname'] = $osInafisFieldSearch['nama_lengkap'] = $ektpFieldsSearch['nama_lengkap'] = $fullName;
        }

        if ( ! empty($pob) )
        {
            $this->data['pob'] = $osInafisFieldSearch['tmpt_lahir'] = $ektpFieldsSearch['tmpt_lahir'] = $pob;
        }

        if ( ! empty($dateOfBirth) )
        {
            $osInafisFieldSearch['tgl_lahir'] = $this->data['dob'] = $this->data['birthDate'] = date('Y-m-d', strtotime($dateOfBirth));
            $ektpFieldsSearch['tgl_lahir'] = date('m/d/Y', strtotime($dateOfBirth));
        }

        if ( ! empty($idcard) )
        {
            $this->data['idCard'] = $this->data['idcard'] = $idcard;
            $this->data['nik'] = $ektpFieldsSearch['nik'] = $idcard;
        }

        if ( ! empty($status) )
        {
            $this->data['status'] = $this->data['status_id'] = $status;
        }

        if ( ! empty($limit) )
        {
            $this->data['size'] = $this->data['limit'] = $limit;
        }

        if ( $source == 'AK23' )
        {
            $ak24           = new AK24_print;
            $print_data     = session()->has('print_data') ? session()->get('print_data') : [];

            array_forget($this->data, ['status', 'birthDate', 'idCard']);
            
            if ( $this->networkStatus != 'off' )
            {
                $currentPage = $request->get('page', 1);

                if ( ! empty($currentPage) )
                {
                    $this->data['page'] = $currentPage - 1;
                }
                
                foreach (Status::all()->toArray() as $key => $value) 
                {
                    $status_data[$value['status_id']] = $value['label'];
                }

                $people = $this->javaServerService->search($this->data);

                $server = true;

                if ( $people['status'] == 'OK' )
                {
                    $paging_limit = ! empty($limit) ? $limit : $people['results']['page']['size'];
                    $paginator = new Paginator($people['results']['page']['number'], $people['results']['page']['totalElements'], $paging_limit, $currentPage, [
                        'path'  => $request->url(),
                        'query' => $request->query(),
                    ]);

                    return view('people.index', compact('people', 'print_data', 'ak24', 'status_data', 'paginator', 'server'));
                }
                
                return abort(500, '[btn_back]Tidak dapat terhubung ke server.');
            }

            $people = new People;

            $server = false;

            if(!empty($fullName))
            {
                $people = $people->where('fullname', 'like', '%' . $fullName . '%');
            }

            if(!empty($idcard))
            {
                $people = $people->where('nik', $idcard);
            }

            if(!empty($pob))
            {
                $people = $people->where('pob', $pob);
            }

            if(!empty($dateOfBirth))
            {
                $format_date = date('Y-m-d', strtotime($dateOfBirth));
                $people = $people->whereDate('dob', '=', $format_date);
            }

            if(!empty($status))
            {
                $people = $people->where('status_id', $status);
            }

            $people = $people->orderBy('fullname')->paginate(20);

            if(!empty($fullName))
            {
                $people = $people->appends(['FullName' => $fullName]);
            }

            if(!empty($dateOfBirth))
            {
                $people = $people->appends(['DateOfBirth' => $dateOfBirth]);
            }

            if(!empty($pob))
            {
                $people = $people->appends(['pob' => $pob]);
            }

            if(!empty($idcard))
            {
                $people = $people->appends(['IDCard' => $idcard]);
            }

            if(!empty($status))
            {
                $people = $people->appends(['status' => $status]);
            }

            return view('people.index', compact('people', 'print_data', 'ak24', 'server'));
        }
        else if ( $source == 'EKTP' && config('ak23.ektp_search_to') == 'ektp' )
        {
            if ( config('ak23.access') == 'desktop' && $this->networkStatus == 'off' )
            {
                return redirect()->back()->with('status', ['type' => 'error', 'message' => "Tidak dapat terhubung ke server"]);;
            }
            
            if ( array_has($ektpFieldsSearch, 'nik') )
            {
                $sourceIndex = 'nik';

                $peoples = Cache::remember('search_nik_ektp_' . implode('_', $ektpFieldsSearch), 15, function() use($ektpFieldsSearch)
                {
                    return $this->javaServerService->OSInafisKtpSearchByNIK($ektpFieldsSearch['nik']);
                });
            }
            else
            {
                $sourceIndex = 'detail';

                $peoples = Cache::remember('search_detail_ektp_' . implode('_', $ektpFieldsSearch), 15, function() use($ektpFieldsSearch)
                {
                    return $this->javaServerService->OSInafisKtpSearch(['json' => json_encode($ektpFieldsSearch)]);
                });
            }

            return view('people.ktp.index', compact('peoples', 'sourceIndex'));
        }
        else
        {
            $prev = $next = true;
            
            if ( config('ak23.access') == 'desktop' && $this->networkStatus == 'off' )
            {
                return redirect()->back()->with('status', ['type' => 'error', 'message' => "Tidak dapat terhubung ke server"]);;
            }

            $osInafisFieldSearch['source'] = $source;

            if ( $source == 'EKTP' && !empty($idcard) )
            {                
                return $this->OSInafisDetail(encrypt("{$source}_{$idcard}"));
            }

            // if ( array_key_exists('status', $this->data) )
            // {
            //     $statusMap = [
            //         1 => 5923,
            //         2 => 5924,
            //         3 => 5925,
            //         4 => 5926,
            //         5 => 5927,
            //         6 => 5928,
            //         7 => 5929,
            //         8 => 5930,
            //         9 => 5931,
            //         10 => 5932
            //     ];

            //     if ( array_key_exists($this->data['status'], $statusMap) )
            //     {
            //         $this->data['status'] = $statusMap[$this->data['status']];
            //     }
            //     else
            //     {
            //         unset($this->data['status']);
            //     }
            // }

            $prevUrl = $request->fullUrlWithQuery(['page' => $page - 1]);
            $nextUrl = $request->fullUrlWithQuery(['page' => $page + 1]);

            // $currentPage = Paginator::resolveCurrentPage();
            // $col = collect(array_get($this->javaServerService->OSInafisSearch($osInafisFieldSearch, 10000), 'result'));
            // $perPage = 100;
            // $currentPageItems = $col->slice(($currentPage - 1) * $perPage, $perPage)->all();
            // $peoples = new Paginator($currentPageItems, count($col), $perPage);
            // $peoples->setPath($request->url());
            // $peoples->appends($request->all());

            $offset = ($limit * $page) - $limit;
            $peoples = array_get($this->javaServerService->OSInafisSearch($osInafisFieldSearch, $limit, $offset), 'result');

            if ( $page < 2 || empty($peoples) )
            {
                $prev = false;
            }

            if ( count($peoples) < $limit )
            {
                $next = false;
            }

            return view('people.osinafis.index', compact('peoples', 'prev', 'next', 'limit', 'nextUrl', 'prevUrl', 'source'));
        }
    }


    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search()
    {
        if ( session()->has('people_data') )
        {
           session()->forget(['people_data']);
        }

        $status_holder = [];
        $status = Status::all();
        foreach ($status as $value) 
        {
            $status_holder[$value->status_id] = $value->label;
        }
        $other = $status_holder[10];
        
        asort($status_holder); // sort array alphabetically and without change the key

        // unset key 10 which is "Lain-lain" and move to the last index
        unset($status_holder[10]);
        $status_holder[10] = $other;

        return view('people.search', ['status' => $status_holder]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function show($id)
    {
        $id =  Crypt::decrypt($id);
        $people = People::findOrFail($id);

        return view('people.show', compact('people'));
    }

    public function view(Request $request, $id)
    {
        try 
        {
            $id =  Crypt::decrypt($id);
        }
        catch (\Exception $e)
        {
            Log::error('Error decrypt ID in People Controller -> view(). Message: ' . $e->getMessage());
            return abort(500, '[btn_back]Data tidak valid.');
        }

        //$this->search_server = false; // override sementara karena belum selesai

        $page = 'front';
        $status= new Status;
        $status = $status->all();

        $front = $id.'/front.jpg';
        $back = $id.'/back.jpg';

        $skin = Cache::remember('data_skin', 15, function() 
        {
            return Skin::all();
        });
        $body = Cache::remember('data_body', 15, function() 
        {
            return Body::all();
        });
        $head = Cache::remember('data_head', 15, function() 
        {
            return Head::all();
        });
        $hair_color = Cache::remember('data_hair_color', 15, function() 
        {
            return HairColor::all();
        });
        $hair = Cache::remember('data_hair', 15, function() 
        {
            return Hair::all();
        });
        $face = Cache::remember('data_face', 15, function() 
        {
            return Face::all();
        });
        $forehead = Cache::remember('data_forehead', 15, function() 
        {
            return Forehead::all();
        });
        $eye_color = Cache::remember('data_eye_color', 15, function() 
        {
            return EyeColor::all();
        });
        $eye_irregularity = Cache::remember('data_eye_irregularity', 15, function() 
        {
            return EyeIrregularity::all();
        });
        $nose = Cache::remember('data_nose', 15, function() 
        {
            return Nose::all();
        });
        $lip = Cache::remember('data_lip', 15, function() 
        {
            return Lip::all();
        });
        $tooth = Cache::remember('data_tooth', 15, function() 
        {
            return Tooth::all();
        });
        $chin = Cache::remember('data_chin', 15, function() 
        {
            return Chin::all();
        });
        $ear = Cache::remember('data_ear', 15, function() 
        {
            return Ear::all();
        });
        $religions = Cache::remember('data_religion', 15, function() 
        {
            return Religion::all();
        });
        $educations = Cache::remember('data_education', 15, function() 
        {
            return Education::all();
        });

        $sinyalemen = [
                'skin' => $skin->all(),
                'body' => $body->all(),
                'head' => $head->all(),
                'hair_color' => $hair_color->all(),
                'hair' => $hair->all(),
                'face' => $face->all(),
                'forehead' => $forehead->all(),
                'eye_color' => $eye_color->all(),
                'eye_irregularity' => $eye_irregularity->all(),
                'nose' => $nose->all(),
                'lip' => $lip->all(),
                'tooth' => $tooth->all(),
                'chin' => $chin->all(),
                'ear' => $ear->all(),
            ];  

        if ( $request->get('source') == 'server' )
        {
            if ( $this->networkStatus != 'off' )
            {
                $this->data['id'] = $id;

                $people = $this->javaServerService->search($this->data);

                if ( array_get($people, 'status') === 'OK' && count(array_get($people, 'results.content')) > 0 )
                {
                    $people = array_get($people, 'results.content.0');

                    $finger_json = json_decode(array_get($people, 'biometricsData'), true);

                    return view('people.server.view', compact('page', 'status', 'sinyalemen', 'finger_json', 'people'))->with(['is_datacenter' => true, 'religions' => $religions->all(), 'educations' => $educations->all()]);
                }
                else
                {
                    return abort(404);
                }
            }
            else
            {
                return abort(500, '[btn_back]Gagal menampilkan data server. Aplikasi tidak dapat terhubung dengan server.');
            }
        }
        else if ($request->get('source') == 'local')
        {
            $people = People::findOrFail($id);

            $file = array(
                'status' => true,
                'front' => array(
                    'name' => $front,
                    'path' => config('path.data.complete').'/'.$front,
                    'url' => url('/').'/files/complete/'.$front
                ),
                'back' => array(
                    'name' => $back,
                    'path' => config('path.data.complete').'/'.$back,
                    'url' => url('/').'/files/complete/'.$back
                )
            );

            $filename = $id;
            $file = array(
                'status' => true,
                'file' => $filename,
                'file_front' => $file['front']['name'],
                'file_back' => $file['back']['name'],
                'folder' => $filename,
                'path_front' => $file['front']['path'],
                'path_back' => $file['back']['path'],
                'url_front' => $file['front']['url'],
                'url_back' => $file['back']['url'],
                'link_front' => url('front'),
                'link_back' => url('back'),
                'link_finger' => url('finger')
            );

            $this->activity->log('demographic-search-view-detail', 'Melihat detail data demographic dengan subject ID ' . $people->subject_id, $this->loginUserId);

            $finger_json = json_decode($people->biometrics_data, true);

            return view('people.view', compact('people', 'file', 'page', 'status', 'sinyalemen', 'finger_json'))->with(['religions' => $religions->all(), 'educations' => $educations->all(), 'is_datacenter' => false]);
        }
        else
        {
            return abort(404);
        }
    }

    public function ktpDetail($nik = null)
    {
        try 
        {
            $nik =  decrypt($nik);
        }
        catch (\Exception $e)
        {
            Log::error('Error decrypt NIK in People Controller -> ktpDetail(). Message: ' . $e->getMessage());
            return abort(500, '[btn_back]Gagal menampilkan data');
        }

        if ( empty($nik) )
        {
            return abort(404);
        }

        try
        {
            $people = Cache::remember('detail_ektp_nik_' . $nik, 15, function() use($nik)
            {
                return $this->javaServerService->OSInafisKtpSearchByNIK($nik);
            });
        }
        catch ( \Exception $e)
        {
            Log::error('Error search NIK -> ktpDetail(). Message: ' . $e->getMessage());
            return abort(500, '[btn_back]Gagal menampilkan data. Server tidak merespon.');
        }

        if ( empty($people['result']) )
        {
            return abort(500, '[btn_back]Gagal menampilkan data. Data tidak ditemukan pada server KTP.');
        }

        $alamat = '';

        if ( config('ak23.access') == 'desktop' )
        {
            $religions = Cache::remember('data_religion', 15, function() 
            {
                return Religion::all();
            });

            $religionsData = [];

            foreach($religions as $religion)
            {
                $religionsData[strtolower($religion['label'])] = $religion['religion_id'];
            }

            $religionData = array_has($religionsData, strtolower($people['result']['Religion'])) ? $religionsData[strtolower($people['result']['Religion'])] : 0;

            $dataDemographic = [
                'fullname' => $people['result']['Name'],
                'nik' => $people['result']['NIK'],
                'sex' => strtolower($people['result']['Sex']) != 'perempuan' ? 'm' : 'f',
                'blood_type' => ! empty($people['result']['BloodType']) ? $people['result']['BloodType'] : '',
                'dob' => $people['result']['DOB'],
                'pob' => $people['result']['POB'],
                'address' => $people['result']['Address'],
                'rt' => $people['result']['RT'],
                'rw' => $people['result']['RW'],
                'village' => $people['result']['Kecamatan'],
                'regent' => $people['result']['Kabupaten'],
                'province' => $people['result']['Propinsi'],
                'postal_code' => $people['result']['pos'],
                'father_name' => $people['result']['Ayah'],
                'mother_name' => $people['result']['Ibu'],
                'job' => $people['result']['Job'],
                'religion_id' => $religionData
            ];

            create_demographic_temp_data($dataDemographic);
        }
        
        if ( ! empty($people['result']['Address']) )
        { 
            $alamat .= $people['result']['Address'];
        }

        if ( ! empty($people['result']['RT']) )
        {
            $alamat .= ' RT. ' . $people['result']['RT'];
        }

        if ( ! empty($people['result']['RW']) )
        {
            $alamat .= ' RW. ' . $people['result']['RW'];
        }

        if ( ! empty($people['result']['dusun']) ) 
        {
            $alamat .= ' Dusun ' . $people['result']['dusun'];
        }

        if ( ! empty($people['result']['Kelurahan']) )
        {
            $alamat .= ' KEL. ' . $people['result']['Kelurahan'];
        }

        if ( ! empty($people['result']['Kecamatan']) )
        {
            $alamat .= ' KEC. ' . $people['result']['Kecamatan'];
        }

        if ( ! empty($people['result']['Kabupaten']) )
        {
            $alamat .= ' KAB. ' . $people['result']['Kabupaten'];
        }

        if ( ! empty($people['result']['Propinsi']) )
        {
            $alamat .= ' ' . $people['result']['Propinsi'];
        }

        if ( ! empty($people['result']['pos']) )
        {
            $alamat .= ' ' . $people['result']['pos'];
        }

        // Log activity
        $this->activity->log('ktp-view-detail', 'Pengguna melihat detail data NIK ' . $nik, $this->loginUserId);

        return view('people.ktp.view', compact('people', 'alamat', 'page'));
    }

    public function OSInafisDetail($externalId = null)
    {
        try 
        {
            $externalId =  decrypt($externalId);
        }
        catch (\Exception $e)
        {
            Log::error('Error decrypt external ID in People Controller -> OSInafisDetail(). Message: ' . $e->getMessage());
            return abort(500, '[btn_back]Gagal menampilkan data');
        }

        try
        {
            $peoples = $this->javaServerService->OSInafisSearchDetailByExternalId([
                'externalId' => $externalId
            ]);
        }
        catch ( \Exception $e)
        {
            Log::error('Error search ExternalId -> OSInafisDetail(). Message: ' . $e->getMessage());
            return abort(500, '[btn_back]Gagal menampilkan data. Server tidak merespon.');
        }

        $people = array_get($peoples, 'candidates.0');

        if ( empty($people) )
        { 
            return view('errors.nodata');
        }

        try
        {
            $images = $this->javaServerService->OSInafisGetImages([
                'external_id' => $externalId,
                'source' => array_get($people, 'source')
            ]);
        }
        catch ( \Exception $e)
        {
            Log::error('Error get images -> OSInafisDetail(). Message: ' . $e->getMessage());
        }

        $alamat = '';

        if ( config('ak23.access') == 'desktop' )
        {
            $religions = Cache::remember('data_religion', 15, function() 
            {
                return Religion::all();
            });

            $religionsData = [];

            foreach($religions as $religion)
            {
                $religionsData[strtolower($religion['label'])] = $religion['religion_id'];
            }

            $religionData = array_has($religionsData, strtolower(array_get($people, 'Religion'))) ? $religionsData[strtolower(array_get($people, 'Religion'))] : 0;

            $dataDemographic = [
                'fullname' => array_get($people, 'full_name'),
                'nik' => array_get($people, 'ktp_number'),
                'sex' => array_get($people, 'sex'),
                'dob' => array_get($people, 'date_of_birth'),
                'pob' => array_get($people, 'birth_place'),
                'address' => array_get($people, 'address'),
                'rt' => array_get($people, 'rt_number'),
                'rw' => array_get($people, 'rw_number'),
                'village' => array_get($people, 'village'),
                'race' => array_get($people, 'race'),
                'job' => array_get($people, 'occupation'),
                'postal_code' => array_get($people, 'postal_code'),
                'religion_id' => $religionData
            ];

            create_demographic_temp_data($dataDemographic);
        }
        
        if ( ! empty(array_get($people, 'address')) )
        { 
            $alamat .= array_get($people, 'address');
        }

        if ( ! empty(array_get($people, 'rt_number')) )
        {
            $alamat .= ' RT. ' . array_get($people, 'rt_number');
        }

        if ( ! empty(array_get($people, 'rw_number')) )
        {
            $alamat .= ' RW. ' . array_get($people, 'rw_number');
        }

        if ( ! empty(array_get($people, 'postal_code')) )
        {
            $alamat .= ' KODEPOS. ' . array_get($people, 'postal_code');
        }

        // Log activity
        $this->activity->log('osinafis-view-detail', 'Pengguna melihat detail data OS Inafis ' . $externalId, $this->loginUserId);

        return view('people.osinafis.view', compact('people', 'alamat', 'page', 'images'));
    }
}
