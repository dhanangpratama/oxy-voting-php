<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Services\DemographicService;
use App\Utils\ApiUtils;
use Illuminate\Http\Request;
use Log, DB, Cache, File, Image;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class MemberController extends MainController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */

    protected $data = [];

    protected $search_server;

    private $demographicService;

    public function __construct(DemographicService $demographicService)
    {
        parent::__construct();
        $this->demographicService = $demographicService;
    }

    public function index(Request $request)
    {
        $data = $request->except('_token');

        $members = $this->demographicService->paginate($data);

        return view('member.index', compact('members', 'data'));
    }

    public function view($id = null)
    {
        if ( empty($id) )
        {
            return abort(404);
        }
        
        try
        {
            $id = decrypt($id);
        }
        catch ( \Exception $e )
        {
            return abort(500, '[btn_back]Gagal menampilkan data.');
        }

        $data = $this->demographicService->findById($id);
        $photo = File::exists(config('path.data.complete') . DIRECTORY_SEPARATOR . array_get($data, 'demographic_id') . DIRECTORY_SEPARATOR . 'photo.jpg');

        return view('member.view', compact('data', 'photo'));
    }
}
