<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator, DB, Auth, Activity;
use App\Utils\ApiUtils;

class RegisterController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $title = 'Register Operator';
        $page = 'register-operator';

        return view('auth.create', compact('title', 'page'));
    }

    public function save(Request $request)
    {
        $data = $request->only('name', 'email', 'phone_mobile', 'location_id', 'password', 'rpassword');

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:user|max:255',
            'phone_mobile' => 'required|numeric|unique:user',
            'password' => 'required|max:255|min:6',
            'rpassword' => 'required|same:password|max:255'
        ];

        try
        {
            $validator = Validator::make($data, $rules, config('formerrormessages'));

            if ($validator->fails()) 
            {
                return redirect()->route('default_operator')->withErrors($validator, 'defoperator')->withInput();
            }

            $data['password'] = bcrypt($data['password']);

            $data['activated'] = 1;

            $userHiValue = userIncrementNumber($this->option->get('user_hi_value'));

            // $data['user_id'] =  ApiUtils::getHardwareHashId() . '-' . $userHiValue;

            $data['user_id'] = 'test-' . $userHiValue;            

            DB::transaction(function() use ($userHiValue, $data)
            {
                User::create($data)->attachRole(2);
                $this->option->update('default_login_operator', '0');
                $this->option->update('user_hi_value', $userHiValue);
            });

            session()->flush();
            
            return redirect()->route('user_login')->with('status', ['type' => 'success', 'message' => 'Operator baru berhasil dibuat. Silahkan log in.']);
        }
        catch (\Exception $e)
        {
            record_error('Error in default operator create user', $e, $request);
            return redirect()->route('default_operator', ['page' => 'register'])->with('status', ['type' => 'error', 'message' => 'Gagal membuat operator baru.']);
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect()->route('user_login');
    }
}
