<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Validator, Image, File;
use AppOption;
use App\Utils\ApiUtils;
use Carbon\Carbon;
use App\Services\Api\JavaServerService;
use App\Events\AfterUserLogin;
use App\Events\AfterUserLogout;

class LoginController extends Controller
{
    private $java;

    public function __construct()
    {
        parent::__construct();

        $this->java = new JavaServerService;
    }

    public function index(Request $request)
    {
        $title = 'Login Pengguna';
        $page = 'user-login';

        $this->checkDataFolders();

        // // Get ip public from java server and store to session
        // ApiUtils::getIpPublic();

        // $this->java->getToken();

        return view('auth.login', compact('title', 'page'));
    }

    public function submit(Request $request)
    {
        $data = $request->only('email', 'password');

        $rules = [
            'email' => 'required|max:255',
            'password' => 'required|max:255'
        ];

        try
        {
            $validator = Validator::make($data, $rules, config('formerrormessages'));

            if ($validator->fails()) 
            {
                return redirect()->route('user_login')->withErrors($validator, 'login')->withInput();
            }

            if (    
                $this->option->get('default_login_operator') == 1 && 
                $data['email'] == config('ak23.default_login_operator_username') && 
                $data['password'] == config('ak23.default_login_operator_password') 
            )
            {
                session(['default_operator' => true]);
                return redirect()->route('default_operator')->with('status', ['type' => 'success', 'message' => 'Log in default operator berhasil']);
            }

            if ( ! Auth::attempt(['email' => $data['email'], 'password' => $data['password'], 'activated' => 1]) ) 
            {
                return redirect()->route('user_login')->with('status', ['type' => 'error', 'message' => 'Login gagal'])->withInput();
            }

            // Share user ID to java client API here
            // ApiUtils::shareUserIdSession(Auth::id());

            // Log activity
            // $this->activity->log('log in', 'Pengguna masuk ke aplikasi', Auth::id());

            event(new AfterUserLogin);

            return redirect()->route('event')->with('status', ['type' => 'success', 'message' => 'Login berhasil']);
        }
        catch (\Exception $e)
        {
            record_error('User login failed.', $e, $request);
            return redirect()->route('user_login')->with('status', ['type' => 'error', 'message' => 'Login gagal. Ada masalah pada sistem login.'])->withInput();
        }
    }

    public function logout(Request $request)
    {
        // Log activity
        // $this->activity->log('log out', 'Pengguna keluar dari aplikasi', Auth::id());

        Auth::logout();

        session()->flush();

        event(new AfterUserLogout);

        return redirect()->route('user_login')->with('status', ['type' => 'success', 'message' => 'Berhasil keluar aplikasi Silahkan log in kembali']);
    }
}
