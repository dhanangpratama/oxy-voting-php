<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Activity;
use Auth;
use Validator;

class ForgotPasswordController extends Controller
{
    protected $user;

    public function __construct()
    {
        parent::__construct();

        $this->user = new User;
    }

    public function submit(Request $request)
    {
        $data = $request->only('name' ,'email', 'phone_mobile', 'location_id');

        $rules = [
            'email' => 'required|email|max:255',
            'phone_mobile' => 'required|numeric',
            'location_id' => 'required'
        ];

        $validator = Validator::make($data, $rules, config('formerrormessages'));

        if ($validator->fails()) 
        {
            return redirect()->route('user_login', ['page' => 'forget'])->withErrors($validator, 'forget')->withInput();
        }

        $user = User::where($data);

        if ( $user->count() > 0 )
        {
            return redirect()->route('user_forget_new_password', [encrypt($user->first()->user_id)])->with('status', ['type' => 'success', 'message' => 'Masukkan password baru anda']);
        }

        return redirect()->route('user_login', ['page' => 'forget'])->with('status', ['type' => 'error', 'message' => 'Pengguna tidak ditemukan'])->withInput();
    }

    public function newPassword(Request $e, $id = null)
    {
        if ( ! empty($id) )
        {
            try
            {
                $id = decrypt($id);
            }
            catch (\Exception $e)
            {
                record_error('Error decrypt ID in forget password', $e, $request);
                return abort(404);
            }

            $user = User::findOrFail($id);

            $title = 'Masukkan Password Baru';
            $page = 'new-password';

            return view('auth.newpassword', compact('user'));
        }

        return abort(404);
    }

    public function newPasswordSubmit(Request $request, $id = null)
    {
        if ( ! empty($id) )
        {
            $data = $request->only('password', 'rpassword');

            $rules = [
                'password' => 'required|max:255|min:6',
                'rpassword' => 'required|same:password|max:255'
            ];

            $validator = Validator::make($data, $rules, config('formerrormessages'));

            if ($validator->fails()) 
            {
                return redirect()->back()->withErrors($validator, 'newpassword')->withInput();
            }

            try
            {
                $id = decrypt($id);
            }
            catch (\Exception $e)
            {
                record_error('Error decrypt ID in forget password', $e, $request);
                return abort(500, '[btn_back]Gagal mengubah password');
            }

            try
            {
                $user = User::findOrFail($id);

                $user->password = bcrypt($data['password']);

                if ($user->save())
                {
                    return redirect()->route('user_login')->with('status', ['type' => 'success', 'message' => 'Password berhail diubah. Silahkan log in kembali.']);
                }

                return redirect()->route('user_login')->with('status', ['type' => 'error', 'message' => 'Gagal mengubah password. Mohon untuk mencoba kembali beberapa saat lagi.']);
            }
            catch (\Exception $e)
            {
                record_error('Error save new password in forget password', $e, $request);
                return redirect()->back()->with('status', ['type' => 'error', 'message' => 'Gagal mengubah password. Mohon ulangi beberapa saat lagi.']);
            }
        }

        return abort(404);
    }
}
