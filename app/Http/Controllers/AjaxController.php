<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use File, Image, Cache, Auth;
use Carbon\Carbon;
use App\Services\PhotoService;

class AjaxController extends Controller
{
    protected $request; 
    protected $photoService;

    public function __construct(Request $request, PhotoService $photoService)
    {
        // constructor
        parent::__construct();

        $this->request = $request; 
        $this->photoService = $photoService;
    }

    public function processPhoto()
    {
        return $this->photoService->process($this->request->get('imageData'));
    }

    public function serverStatus()
    {
        $file = storage_path('tool/network-status.txt');
        
        try
        {
            if ( File::exists($file) )
            {
                return ['status' => file_get_contents($file)];
            }

            return['status' => 'off'];
        }
        catch (Exception $e)
        {
            return ['status' => 'off'];
        }
    }
}