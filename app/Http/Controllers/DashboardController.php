<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB, Cache;
use App\Services\DashboardService;

class DashboardController extends MainController
{
    private $request;

    private $dashboardService;

    public function __construct(Request $request, DashboardService $dashboardService)
    {
        parent::__construct();

        $this->request = $request;

        $this->dashboardService = $dashboardService;
    }

    public function index()
    {
        $title = 'Dashboard';
        $page = 'dashboard';
    
        $dataChart = $this->dashboardService->chartCount();

        $activityLogs = $this->dashboardService->getActivityLogs();

        return view('dashboard', compact('title', 'page', 'messages', 'dataChart', 'activityLogs'));
    }
}
