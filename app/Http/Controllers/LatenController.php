<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\LatenRequest;
use App\Utils\ImageUtils;
use App\Utils\ApiUtils;
use App\Services\LatenService;
use File;
use Image;
use Activity;

class LatenController extends MainController
{
	protected $request;

    protected $requestedDir;

    private $latenService;

   	public function __construct(Request $request, LatenService $latenService)
   	{
   		parent::__construct();

        $this->request = $request;
        
        $this->latenService = $latenService;

        $this->requestedDir = $this->path['data']['laten'] . DIRECTORY_SEPARATOR . 'requested' . DIRECTORY_SEPARATOR;

        if ( ! File::isDirectory($this->path['data']['laten']) )
        {
            File::makeDirectory($this->path['data']['laten'], 0777);
        }
   	}

   	public function index()
   	{   
        $fingerOptions = [
                'Tidak Diketahui',
                'Jempol Kanan',
                'Telunjuk Kanan',
                'Tengah Kanan',
                'Manis Kanan',
                'Kelingking Kanan',
                'Jempol Kiri',
                'Telunjuk Kiri',
                'Tengah Kiri',
                'Manis Kiri',
                'Kelingking Kiri'
            ];

        $drive = driveFind();

        // dd(is_dir($drive), scandir($drive));

   		return view('laten.index', compact('liveFingers', 'fingerOptions', 'drive'));
    }
       
    public function scan()
    {
        $title = 'Laten Scan';
        $page = 'laten-scan';

        if ( File::isDirectory($this->path['data']['laten']) )
        {
            File::deleteDirectory($this->path['data']['laten'], true);
        }
        else
        {
            File::makeDirectory($this->path['data']['laten'], 0777);
        }
        
        if ( ! File::isDirectory($this->requestedDir) )
        {
            File::makeDirectory($this->requestedDir, 0777);
        }
   
        return view('laten.scan', compact('title', 'page'));
    }

    public function result($key = null)
    {
        try
        {
            if ( !empty($key) )
            {
                $candidatesDataFile = $this->path['data']['laten'].DIRECTORY_SEPARATOR.'_candidates.json';

                if ( ! file_exists($candidatesDataFile) ) throw new \Exception('Candidates data file ('.$candidatesDataFile.') not found.');

                $foundedFingers = $resultsLog = $requestedFingers = $requestedFingersSorted = [];
                $foundedImageFirst = $requestedImageFirst = $scoreClass = '';
                $osFinger = config('ak23.os_inafis_finger_key');
                $fingersMap = config('ak23.fingermap_en');

                try
                {
                    $key = decrypt($key);
                }
                catch(\Exception $e)
                {
                    return abort(500, '[btn_back]Halaman tidak valid!');
                }

                $latenPath = $this->path['data']['laten'].DIRECTORY_SEPARATOR.$key.DIRECTORY_SEPARATOR;

                if ( ! File::isDirectory($this->requestedDir) ) throw new \Exception('Directory '.$this->requestedDir.' not found.');

                $candidatesData = json_decode(file_get_contents($candidatesDataFile), true);

                $requestedImages = File::glob($this->requestedDir . '*.jpg');

                $searchType = $candidatesData['type'];

                array_forget($candidatesData, 'type');

                if ( ! empty($candidatesData) )
                {
                    $countRequested = 1;
                    foreach ($requestedImages as $requestedImage) 
                    {
                        if ( File::exists($requestedImage) )
                        {
                            $requestedKey = str_replace('.jpg', '', basename($requestedImage));
                            $requestedKey = str_replace('-', '_', basename($requestedKey));

                            $requestedFingers[$requestedKey] = data_url('laten/requested/' . basename($requestedImage));

                            $countRequested++;
                        }
                        else
                        {
                            $requestedFingers[$requestedKey] = asset("assets/images/unknown-square.png");
                        }
                    }

                    $countFounded = 1;
                    foreach (config('ak23.finger_index') as $fingerName) 
                    {
                        $removeFinger = trim(str_replace('_finger', '', $fingerName));

                        $requestedFingersSorted[$removeFinger] = array_key_exists($removeFinger, $requestedFingers) ? $requestedFingers[$removeFinger] : asset("assets/images/unknown-square.png");

                        if ( file_exists($latenPath . str_replace('_', '-', $removeFinger) . '.jpg') )
                        {
                            $foundedFingers[$removeFinger] = data_url('laten/' . $key . '/' . str_replace('_', '-', $removeFinger) . '.jpg');
                        }
                        else
                        {
                            $foundedFingers[$removeFinger] = null;
                        }
                    }

                    $requestedFingers = $requestedFingersSorted;
                    $requestedImageFirst = $requestedFingers['right_thumb'];
                    $foundedImageFirst = $foundedFingers['right_thumb'];

                    $scoreClass = $candidatesData[$key]['scoreFlag'];
                }

                $candidateDataDetail = (strpos(array_get($candidatesData, "{$key}.source"), 'AK-23') !== false) ? $this->ak23Map(array_get($candidatesData, $key)) : $this->osInafisMap(array_get($candidatesData, $key));

                $minutiaeUrl = data_url('laten/requested/minutiae/') . '/';

                return view('laten.result', compact('searchType', 'key', 'foundedFingers', 'foundedImageFirst', 'requestedFingers', 'requestedImageFirst', 'scoreClass', 'latenPath', 'candidatesData', 'fingersMap', 'alamat', 'candidateDataDetail', 'minutiaeUrl'));
            }
            else
            {
                return abort(404);
            }
        }
        catch (\Exception $e)
        {
            record_error('Error in laten search result page.', $e, new Request);
            return view('errors.custom', ['data' => ['title' => "Ma'af terjadi kesalahan.", 'description' => 'Aplikasi tidak berjalan semestinya. Mohon untuk mencoba kembali beberapa saat.']]);

        }
    }

    private function ak23Map($data)
    {
        $alamat = '';
        $getSex = array_get($data, 'sex');
        
        if ( ! empty($data['detail']['address']) )
        { 
            $alamat .= $data['detail']['address'];
        }

        if ( ! empty($data['detail']['rt']) )
        {
            $alamat .= ' RT. ' . $data['detail']['rt'];
        }

        if ( ! empty($data['detail']['rw']) )
        {
            $alamat .= ' RW. ' . $data['detail']['rw'];
        }

        if ( ! empty($data['detail']['village']) )
        {
            $alamat .= ' KEC. ' . $data['detail']['village'];
        }

        if ( ! empty($data['detail']['regent']) )
        {
            $alamat .= ' KAB. ' . $data['detail']['regent'];
        }

        if ( ! empty($data['detail']['province']) )
        {
            $alamat .= ' ' . $data['detail']['province'];
        }

        if ( ! empty($data['detail']['postalCode']) )
        {
            $alamat .= ' ' . $data['detail']['postalCode'];
        }

        $sex = (strtolower($getSex) == 'm' || strtolower($getSex) == 'f') ? sex_label($getSex) : $getSex;

        return [
            'blood_type' => ! empty($data['detail']['bloodType']) ? $data['detail']['bloodType'] : '-',
            'dob' => ! empty($data['detail']['dob']) ? format_date($data['detail']['dob']) : '-',
            'sex' => $sex,
            'address' => $alamat
        ];
    }

    private function osInafisMap($data)
    {
        $alamat = '';
        $getSex = array_get($data, 'sex');
        
        if ( ! empty($data['address']) )
        { 
            $alamat .= $data['address'];
        }

        if ( ! empty($data['rt_number']) )
        {
            $alamat .= ' RT. ' . $data['rt_number'];
        }

        if ( ! empty($data['rw_number']) )
        {
            $alamat .= ' RW. ' . $data['rw_number'];
        }

        if ( ! empty($data['village']) )
        {
            $alamat .= ' KEC. ' . $data['village'];
        }

        if ( ! empty($data['regent']) )
        {
            $alamat .= ' KAB. ' . $data['regent'];
        }

        if ( ! empty($data['province']) )
        {
            $alamat .= ' ' . $data['province'];
        }

        if ( ! empty($data['postal_code']) )
        {
            $alamat .= ' ' . $data['postal_code'];
        }

        $sex = (strtolower($getSex) == 'm' || strtolower($getSex) == 'f') ? sex_label($getSex) : $getSex;

        return [
            'blood_type' => '-',
            'dob' => ! empty($data['dob']) ? $data['dob'] : '-',
            'sex' => $sex,
            'address' => $alamat
        ];
    }
}
