<?php

namespace App\Http\Controllers\Demographic;

use Illuminate\Http\Request;
use App\Http\Controllers\DemographicController;
use App\Http\Requests;
use App\Repositories\ReligionRepository;
use App\Services\DemographicService;
use File;

class DataController extends DemographicController
{
    public $operator;

    private $religions;
    private $demographicService;

    public function __construct(ReligionRepository $religions, DemographicService $demographicService)
    {
        parent::__construct();

        $this->religions = $religions;
        $this->demographicService = $demographicService;
    }

    public function index(Request $request)
    {
        $title = 'Rekam Data';
        $page = 'demographic_create';
        $religions = $this->religions->all();

        session(['current_indicator' => array_search_inner(config('ak23.indicator'), 'id', 'demographic_create')]);

        if ($request->get('action') == 'new')
        {
            session()->forget('demographic');
            File::deleteDirectory(config('path.data.demographic'), true);
        }

        return view('demographic.data', compact('title', 'page', 'religions'));
    }

    public function submit(Request $request)
    {
        $data = $request->except('_token');

        if ( !session()->has('demographic.db_id') )
        {
            $searchData = $this->demographicService->searchNik(array_get($data, 'nik'));

            if ( !is_null($searchData) )
            {
                session(['found_data' => $searchData->toArray()]);
                return redirect()->route('demographic_found_nik');
            }
        }

        session(['demographic.data' => $data]);

        return redirect()->route('demographic_photo');
    }

    public function nik()
    {
        $title = 'Data Ditemikan';
        $page = 'found_nik';

        $photo = File::exists(config('path.data.complete') . DIRECTORY_SEPARATOR . session('found_data.demographic_id') . DIRECTORY_SEPARATOR . 'photo.jpg');
        $data = session('found_data');

        return view('demographic.found_nik', compact('title', 'page', 'photo', 'data'));
    }
}