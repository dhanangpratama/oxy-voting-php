<?php

namespace App\Http\Controllers\Demographic;

use App\Http\Controllers\DemographicController;
use App\Models\People;
use App\Http\Requests;
use App\Utils\ImageUtils;
use Illuminate\Http\Request;
use Log;
use File;
use Image;
use Activity;
use App\Utils\ApiUtils;

class FingerFlatController extends DemographicController
{
    protected $type;
    protected $source;

    public function __construct()
    {
        parent::__construct(); // Call parent constructor
    }

    public function index(Request $request, $type = '')
    {
        $this->file = session('process');

        $scanType = $this->type;
        
        if ( empty($type) || ! array_key_exists($type, config('ak23.finger_data')) )
        {
            return abort('404');
        }

        $this->source = $this->path['data']['edit'];
        
        if ( session('process.mode') != 'edit' ) 
        {
            $this->source = ($scanType != 'card') ? $this->path['data']['live'] : $this->path['data']['card'];

            if ( $scanType == 'card' )
            {
                if ( !flagCheck('crop_flat_four_fingers_process_done') )
                {
                    return  redirect()->route('demographic_card_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses scan kartu terlebih dahulu']);
                }
            }
        }

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', 'flat')]);

        if ( ! File::isDirectory(session('process.basePath')) )
        {
            return view(config('const.error_folder_working_view'));
        }

        $side = (strpos($type, 'left') === FALSE) ? config('filename.flat_right_four_fingers') : config('filename.flat_left_four_fingers'); // assign $side to right if $left is false, and left if $left is true
        $side = (strpos($type, 'thumb') === FALSE) ? $side : config('filename.flat_thumbs'); // Replace $side with "thumbs" if string jempol exist

        $file = session('process');
        $finger = config('ak23.finger_data');
        $title = $finger[$type]['title'].' - Flat Finger';
        
        $fingerKey = array_keys($finger);
        $nextKey = array_search($type, $fingerKey)+1;
        $nextFinger = array_key_exists($nextKey, $fingerKey) ? $fingerKey[$nextKey] : FALSE;
        $beforeKey = array_search($type, $fingerKey)-1;
        $beforeFinger = array_key_exists($beforeKey, $fingerKey) ? $fingerKey[$beforeKey] : FALSE;

        $dataFinger = get_data_from_file($file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.flat_biometrics_data'));

        if ( session('process.mode') == 'edit' )
        {
            $dataFinger = get_data_from_file($file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'));
        }

        if ( $request->isMethod('post') )
        {
            $data = $request->except('_token');

            $skip = $request->get('skip');

            if ( session('process.mode') == 'edit' && array_key_exists('formula', $data) )
            {
                return redirect()->route('demographic_formula');
            }

            if ( array_key_exists('identification', $data) )
            {
                return redirect()->route('demographic_identification');
            }

            if ( ! isset($skip) ) 
            {
                $requiredData = ['x', 'y', 'w', 'h', 'r'];
                
                if ( empty(array_diff(array_keys($data), $requiredData)) )
                {
                    // doing this because rotate position in js and php is not same.
                    // in js 90, php -90, so we need to reverse it
                    if ( $data['r'] > 0 ) 
                    {
                        $data['r'] = -$data['r'];
                    }
                    else if ( $data['r'] < 0 )
                    {
                        $data['r'] = abs($data['r']);
                    }

                    try
                    {
                        Image::make($file['basePath'] . DIRECTORY_SEPARATOR . $side)->rotate($data['r'], '#FFFFFF')->crop(intval($data['w']), intval($data['h']), intval($data['x']), intval($data['y']))->brightness(config('ak23.finger_brightness'))->contrast(config('ak23.finger_contrast'))->save($file['basePath'] . DIRECTORY_SEPARATOR . "flat_{$type}.{$this->ext}");
                    }
                    catch(\Exception $e)
                    {
                        record_error('Error cropping flat finger in card.', $e, $request);
                        return redirect()->back()->with('status', ['type' => 'danger', 'message' => 'Gagal memproses gambar. Mohon ulangi beberapa saat lagi.']);
                    }

                    $requestData = [];

                    if ( file_exists($file['basePath'] . DIRECTORY_SEPARATOR . "flat_{$type}.{$this->ext}") )
                    {
                        $requestData['fingers'][] = [
                            'finger_position' => strtoupper("flat_{$type}"),
                            'path' => $file['basePath'] . DIRECTORY_SEPARATOR . "flat_{$type}.{$this->ext}"
                        ];
                    }

                    $classification = false;
                    $tryNumber = 1;

                    do 
                    {
                        if ( $tryNumber < 4 )
                        {
                            $henry = ApiUtils::patternClassification($requestData);
                            
                            if ( array_get($henry, 'status') == 'OK' && !empty(array_get($henry, 'results.fingers')) )
                            {
                                foreach (array_get($henry, 'results.fingers') as $henryDataResult)
                                {
                                    if ( in_array($henryDataResult['pattern_classification'], config('ak23.whorl_map')) )
                                    {
                                        $henryDataResult['pattern_classification'] = 'WHORL';
                                    }
                                    else if ( in_array($henryDataResult['pattern_classification'], config('ak23.ulnar_map')) )
                                    {
                                        $henryDataResult['pattern_classification'] = 'ULNAR_LOOP';
                                    }

                                    $henryDataResult['point_crop'] = $data;

                                    $theKey = strtolower($henryDataResult['finger_position']); 
                                    $dataFinger['fingers'][$theKey] = $henryDataResult;
                                }

                                $classification = true;
                            }

                            $tryNumber++;
                        }
                        else
                        {
                            $classification = true;
                            $dataFinger['fingers']["flat_{$type}"]['image_quality'] = 0;
                            $dataFinger['fingers']["flat_{$type}"]['point_crop'] = $data;
                        }
                    } 
                    while ( !$classification );

                    $dataFinger['fingers']["flat_{$type}"]['missing'] = 0;
                }
                else
                {
                    return redirect()->back()->with('status', ['type' => 'info', 'message' => 'Mohon untuk mengulang proses pemotongan. Data foto tidak valid.']);
                }
            }
            else
            {
                unset($dataFinger['fingers']["flat_{$type}"]);
                $dataFinger['fingers']["flat_{$type}"]['missing'] = 1;

                /**
                * Remove file who not have finger section if before provided!
                */
                if ( file_exists($file['basePath'].DIRECTORY_SEPARATOR."flat_{$type}.{$this->ext}") )
                {
                    File::delete($file['basePath'].DIRECTORY_SEPARATOR."flat_{$type}.{$this->ext}");
                }
            }

            if ( session('process.mode') == 'edit' )
            {
                File::put($file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), json_encode($dataFinger));
            }
            else
            {
                File::put($file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.flat_biometrics_data'), json_encode($dataFinger));
                flag('flat_'.$type.'_process_done');
            }
            
            if ( ! $nextFinger )
            {
                foreach ( $dataFinger['fingers'] as $fingerKey => $fingerData)
                {
                    if ( strpos($fingerKey, 'flat_') === false ) continue;

                    if ( ! array_has($dataFinger, "fingers.{$fingerKey}.missing") )
                    {
                        $fingerNoData = str_replace('flat_', '', $fingerKey);
                        return redirect()->route('demographic_finger_flat', [$fingerNoData])->with('status', ['type' => 'info', 'message' => 'Mohon untuk menyelesaikan memotong keselurahan jari rata']);
                    }
                }

                flag('finger_flat_process_done');

                if ( file_exists($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.flat_biometrics_data')) )
                {
                    $biometricData = get_data_from_file($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'));
                    $dataFingerMerge = ['fingers' => array_merge($biometricData['fingers'], $dataFinger['fingers'])];

                    File::put($file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), json_encode($dataFingerMerge));
                }

                if ( session('process.mode') == 'edit' )
                {
                    return redirect()->route('demographic_formula');
                }

                return redirect()->route('demographic_identification');
            }
            else 
            {
                return redirect()->route('demographic_finger_flat', [$nextFinger]);
            }
        }

        $cropFlatStatus = flagCheck('finger_flat_process_done');

        switch ($type) 
        {
            case 'right_thumb':
                $selection = '[700, 427, 582, 776]';
                break;
            case 'right_index_finger':
                $selection = '[32, 431, 351, 636]';
                break;
            case 'right_middle_finger':
                $selection = '[341, 137, 385, 601]';
                break;
            case 'right_ring_finger':
                $selection = '[740, 290, 388, 588]';
                break;
            case 'right_little_finger':
                $selection = '[1150, 863, 406, 604]';
                break;
            case 'left_thumb':
                $selection = '[78, 387, 602, 0]';
                break;
            case 'left_index_finger':
                $selection = '[1250, 599, 381, 0]';
                break;
            case 'left_middle_finger':
                $selection = '[893, 260, 452, 0]';
                break;
            case 'left_ring_finger':
                $selection = '[461, 329, 495, 0]';
                break;
            case 'left_little_finger':
                $selection = '[0, 812, 408, 0]';
                break;
            default:
                $selection = '';
                break;
        }

        $noFinger = array_has($dataFinger['fingers'], "flat_{$type}.missing") && ! empty($dataFinger['fingers']["flat_{$type}"]['missing']) ? true : false;

        return view('demographic.finger_flat', compact('file', 'title', 'side', 'type', 'finger', 'nextFinger', 'scanType', 'selection', 'cropFlatStatus', 'beforeFinger', 'noFinger', 'dataFinger'));
    }
}
