<?php

namespace App\Http\Controllers\Demographic;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Models\People;
use App\Models\Operator;
use File;
use DB, Image;
use App\Utils\ImageUtils;
use GuzzleHttp\Client;
use App\Utils\ApiUtils;
use App\Http\Controllers\DemographicController;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use App\Services\LiveService;

class LiveController extends DemographicController
{
    public $folderName;

    private $liveService;

    public function __construct(Request $request, LiveService $liveService)
    {
        parent::__construct();

        $this->folderName = config('filename.live_folder');

        $this->liveService = $liveService;

        $this->request = $request;
    }

    public function index()
    {
        $title = 'Live Scan';
        $page = 'live-scan';

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', 'record_finger')]);

        return view('record.livescanfinger', compact('title', 'page', 'dummyImage'));
    }

    public function submit()
    {
        $this->liveService->createImage($this->request->except('_token'));

        $files = File::glob(session('process.basePath').DIRECTORY_SEPARATOR.'*.{jpg}', GLOB_BRACE);

        if ( empty($files) )
        {
            return redirect()->route('demographic_finger_scan_record')->with('status', ['type' => 'error', 'message' => 'Tidak diijinkan melanjutkan, data jari tidak ditemukan.']);
        }

        $redirectRoute = session('process.mode') == 'edit' ? 'demographic_formula' : 'demographic_identification';

        return redirect()->route($redirectRoute);
    }

    public function liveScan()
    {
        $title = 'Live Scan';
        $page = 'live-scan';

        return view('record.livescan', compact('title', 'page'));
    }

    public function createProcessSession(Request $request)
    {
        $pathLive = config('path.data.live');

        File::deleteDirectory($pathLive, true);
        File::deleteDirectory(config('path.data.tmp_minutiae'), true);
        File::deleteDirectory(config('path.data.tmp_compare'), true);

        foreach ( config('ak23.finger_index') as $finger )
        {
            $dataFingers['fingers'][$finger] = $dataFingers['fingers']['flat_'.$finger] = array();
        }

        File::put($pathLive . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), json_encode($dataFingers));

        $getDataTemp = ($request->input('get_data') == 1) ? true : false;

        File::put($pathLive . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'), $this->liveService->getDemographicData($getDataTemp));

        flag('processedby_'.$this->loginUserId, true, $pathLive);

        // Create session data
        session([
            'process' => [
                'status' => true,
                'file' => basename($pathLive),
                'file_front' => null,
                'file_back' => null,
                'folder' => basename($pathLive),
                "basePath" => $pathLive,
                "baseUrl" => data_url(basename($pathLive)),
                'path_front' => null,
                'path_back' => null,
                'url_front' => null,
                'url_back' => null,
                'type' => 'live',
                'mode' => 'new'
            ]
        ]);

        return redirect()->route('demographic_finger_scan_record');
    }
}