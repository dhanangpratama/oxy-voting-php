<?php

namespace App\Http\Controllers\Demographic;

use Illuminate\Http\Request;
use App\Http\Controllers\DemographicController;
use App\Http\Requests;
use App\Repositories\DemographicRepository;
use File;

class PrintController extends DemographicController
{
    public $demographicRepository;

    public function __construct(DemographicRepository $demographicRepository)
    {
        parent::__construct();
        $this->demographicRepository = $demographicRepository;
    }

    public function index(Request $request)
    {
        if ( !session()->has('demographic.data') )
        {
            return redirect()->route('demographic_create')->with('status', ['type' => 'info', 'message' => 'Mohon untuk menyelesaikan proses perekaman data terlebih dahulu.']);
        }

        $title = 'Cetak Kartu';
        $page = 'demographic_print';

        session(['current_indicator' => array_search_inner(config('ak23.indicator'), 'id', 'demographic_print')]);

        $photo = File::exists(config('path.data.demographic') . DIRECTORY_SEPARATOR . 'photo.jpg');
        $data = $this->demographicRepository->findById(session('demographic.db_id'));

        return view('demographic.print', compact('title', 'page', 'photo', 'data'));
    }

    public function submit(Request $request)
    {
        $data = $request->except('_token');

        return redirect()->route('demographic_print');
    }
}