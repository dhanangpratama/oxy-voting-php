<?php

namespace App\Http\Controllers\Demographic;

use Illuminate\Http\Request;
use App\Http\Controllers\DemographicController;
use App\Services\PhotoService;
use App\Services\DemographicService;
use App\Http\Requests;
use App\Services\PDFService;
use File;

class PhotoController extends DemographicController
{
    protected $demographicService;

    public function __construct(DemographicService $demographicService)
    {
        parent::__construct();
        $this->demographicService = $demographicService;
    }

    public function index(Request $request)
    {
        if ( !session()->has('demographic.data') )
        {
            return redirect()->route('demographic_create')->with('status', ['type' => 'info', 'message' => 'Mohon untuk menyelesaikan proses perekaman data terlebih dahulu.']);
        }

        $title = 'Rekam Foto';
        $page = 'demographic_photo';

        $file = File::exists(config('path.data.demographic') . DIRECTORY_SEPARATOR . 'photo.jpg');

        session(['current_indicator' => array_search_inner(config('ak23.indicator'), 'id', 'demographic_photo')]);

        return view('demographic.photo', compact('title', 'page', 'file'));
    }

    public function submit()
    {
        PDFService::generateIdCard(session('demographic.data'));
        
        if ( !session()->has('demographic.db_id') )
        {
            $this->demographicService->create();
        }
        else
        {
            $this->demographicService->update();
        }

        return redirect()->route('demographic_print');
    }
}