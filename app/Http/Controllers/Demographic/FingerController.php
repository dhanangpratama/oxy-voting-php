<?php

namespace App\Http\Controllers\Demographic;

use Validator;
use App\Http\Controllers\DemographicController;
use Illuminate\Http\Request;
use File;
use Crypt;
use App\Models\People;
use Image;
use ImageUtils;
use Activity;
use View;
use App\Utils\ApiUtils;

class FingerController extends DemographicController
{
    protected $type;
    protected $source;
    protected $request;
    protected $dataFinger;
    protected $demographicData;

    public function __construct(Request $request)
    {
        parent::__construct(); // Call parent constructor

        $this->request = $request;

        View::share('request', $this->request);

        /** to use session in constructor must place inside middleware for laravel 5.3 and above */
        $this->middleware(function ($request, $next) 
        {
            $this->dataFinger = get_data_from_file($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'));

            $this->demographicData = get_data_from_file($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'));

            View::share('dataFinger', $this->dataFinger);

            View::share('demographicData', $this->demographicData);

            return $next($request);
        });
    }

    public function index($type='')
    {
        $scanType = $this->type;

        $this->source = $this->path['data']['edit'];

        $source_folder = 'edit';

        if (session('process.mode') != 'edit')
        {
            $source_folder = ($scanType != 'card') ? config('filename.live_folder') : config('filename.card_folder');
            $this->source = ($scanType != 'card') ? $this->path['data']['live'] : $this->path['data']['card'];

            if ( $scanType == 'card' )
            {
                if ( ! flagCheck('crop_flat_four_fingers_process_done') )
                {
                    return redirect()->route('demographic_card_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses scan kartu terlebih dahulu']);
                }

                if ( ! flagCheck('finger_flat_process_done') )
                {
                    return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Mohon melakukan cropping jari rata dulu']);
                }

                if ( ! flagCheck('cropping_card_process_done')  )
                {
                    return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Aplikasi sedang memproses kartu, mohon menunggu beberapa saat lagi']);
                }

                if ( ! flagCheck('verification_process_done') && str_contains($this->request->path(), 'formula') )
                {
                    return  redirect()->route('demographic_identification')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses verifikasi terlebih dahulu']);
                }
            }
            else
            {
                if ( ! flagCheck('enrollment_process_done')  && str_contains($this->request->path(), 'formula') )
                {
                    return redirect()->route('demographic_finger_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses perekaman sidik jari terlebih dahulu']);
                }

                if ( ! flagCheck('verification_process_done') && str_contains($this->request->path(), 'formula') )
                {
                    return  redirect()->route('demographic_identification')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses identifikasi terlebih dahulu']);
                }
            }
        }

        if ( ! File::isDirectory($this->file['basePath']) )
        {
            return view(config('const.error_folder_working_view'));
        }

        if ( ! empty($type) && array_key_exists($type, config('ak23.finger_data')) )
        {
            $finger = config('ak23.finger_data');

            if ( $this->request->isMethod('post') )
            {
                $validator = Validator::make($this->request->all(), [
                    'henry' => 'required'
                ], [
                    'henry.required' => 'Anda belum memilih jenis Henry'
                ]);

                if ( $validator->fails() )
                {
                    return redirect()->back()->withErrors($validator);
                }

                $this->dataFinger['fingers'][$type]['pattern_classification'] = $this->request->input('henry');
                File::put($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), json_encode($this->dataFinger));

                return redirect()->route('demographic_formula')->with('status', ['type' => 'success', 'message' => "{$finger[$type]['title']} telah diset."]);
            }

            $sess_finger = '';
            if ( session()->has($type) )
            {
                $sess_finger = session($type);
            }

            return view('demographic.fingerdetail', compact('finger', 'type', 'sess_finger', 'scanType', 'source_folder'))->with(['file' => $this->file]);
        }

        if ( $this->request->isMethod('post') )
        {
            if ( str_contains($this->request->path(), 'formula') )
            {
                $dataInput = $this->request->all();
                $formula = implode('|', $this->request->input('formula'));
                $formula_view = implode('|', $this->request->input('formula_view'));

                $this->demographicData['formula'] = $formula;
                $this->demographicData['formula_view'] = $formula_view;

                File::put($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'), json_encode($this->demographicData));

                if ( session('process.mode') != 'edit' )
                {
                    flag('formula_process_done');
                }

                if ( $this->type == 'card' )
                {
                    if ( array_has($dataInput, 'back') )
                    {
                        if ( session('process.mode') == 'edit' )
                        {
                            return redirect()->route('demographic_finger_flat', ['right_thumb']);
                        }

                        return redirect()->route('demographic_identification');
                    }

                    return redirect()->route('demographic_picture');
                }

                if ( array_has($dataInput, 'back') )
                {
                    if ( session('process.mode') == 'edit' )
                    {
                        return redirect()->route('demographic_finger_scan_record');
                    }

                    return redirect()->route('demographic_identification');
                }
            
                return redirect()->route('demographic_camera');
            }

            flag('finger_rolled_process_done');

            return redirect()->route('demographic_finger_flat', ['right_thumb']);
        }

        $file = session('process');
        $title = 'Finger';

        //dd(session('status'));

        $finger_data = session('front_data');

        // |== READY TO REMOVE ==| 
        $allowed_finger_types = ['W', 'A', 'T', 'R', 'U'];
        $error_status = 0;
        $finger_formula = '[';
        foreach (config('ak23.finger_data') as $key => $value) 
        {
            if ( ! empty(session($key)) )
            {
                $finger = strtolower($key);
                $henry = config('ak23.henry');
                $count_ridge = ($henry[session($finger)] != 'W') ? '1 + RandInt(20)' : 'RandElement(whorl_trace_values)';
                $finger_formula .= "['".$henry[session($finger)]."', ".$count_ridge."],";

                if ( ! in_array($henry[session($finger)], $allowed_finger_types) )
                {
                    $error_status++;
                }
            }
        }
        $finger_formula .= ']';

        $breadcrumbId = str_contains($this->request->path(), 'formula') ? 'formula' : 'rolled';

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', $breadcrumbId)]);

        return view('demographic.finger', compact('file', 'title', 'scanType', 'finger_data', 'source_folder', 'finger_formula', 'error_status'))->with(['fileNaming' => $this->fileNaming]);
    }

    public function crop($finger = null)
    {
        if ( is_null($finger) )
        {
            return redirect()->back();
        }

        $this->file = session('process');
        $source = ($this->type != 'card') ? $this->path['data']['live'] : $this->path['data']['card'];
        $source_folder = ($this->type != 'card') ? config('filename.live_folder') : config('filename.card_folder');
        $title = 'Potong Sidik Jari';
        $scanType = $this->type;
        $fingerData = config('ak23.finger_data');

        switch ($finger) 
        {
            case 'right_thumb':
                $selectedPoint = '[343, 1490, 726, 726]';
                break;
            case 'right_index_finger':
                $selectedPoint = '[1217, 1490, 726, 726]';
                break;
            case 'right_middle_finger':
                $selectedPoint = '[2057, 1490, 726, 726]';
                break;
            case 'right_ring_finger':
                $selectedPoint = '[2885, 1490, 726, 726]';
                break;
            case 'right_little_finger':
                $selectedPoint = '[3756, 1490, 726, 726]';
                break;
            case 'left_thumb':
                $selectedPoint = '[357, 2403, 726, 726]';
                break;
            case 'left_index_finger':
                $selectedPoint = '[1221, 2403, 726, 726]';
                break;
            case 'left_middle_finger':
                $selectedPoint = '[2053, 2403, 726, 726]';
                break;
            case 'left_ring_finger':
                $selectedPoint = '[2906, 2403, 726, 726]';
                break;
            case 'left_little_finger':
                $selectedPoint = '[3742, 2403, 726, 726]';
                break;
            default:
                $selectedPoint = '';
                break;
        }

        $selectedPoint = explode(',', trim($selectedPoint, '[]'));

        $this->request->session()->flash('status_growl', ['type' => 'info', 'message' => 'Silahkan potong ' . $fingerData[$finger]['title']]);

        return view('demographic.rolledcrop', compact('title', 'source', 'source_folder', 'scanType', 'finger', 'fingerData', 'selectedPoint'))->with(['file' => $this->file, 'fileNaming' => $this->fileNaming]);
    }

    public function doCrop($finger = null)
    {
        if ( is_null($finger) )
        {
            return redirect()->back();
        }

        $data = $this->request->only('x','y','w','h','r');
        $file = session('process');
        $fingerData = config('ak23.finger_data');
        $requestData = [];

        // doing this because rotate position in js and php is not same.
        // in js 90, php -90, so we need to reverse it
        if ( $data['r'] > 0 ) 
        {
            $data['r'] = -$data['r'];
        }
        else if ( $data['r'] < 0 )
        {
            $data['r'] = abs($data['r']);
        }

        $card_name_folder = config('filename.card_folder');

        try
        {
            if ( $this->type == 'card' )
            {
                Image::make($file['basePath'] . DIRECTORY_SEPARATOR . "front.{$this->ext}")->rotate($data['r'], '#FFFFFF')->crop(intval($data['w']), intval($data['h']), intval($data['x']), intval($data['y']))->save($file['basePath'] . DIRECTORY_SEPARATOR . "{$finger}.{$this->ext}");
            }
            else
            {
                // Crop foto for live scan here
            }
        }
        catch(\Exception $e)
        {
            record_error('Error cropping rolled finger.', $e, new Request);
            return redirect()->back()->with('status', ['type' => 'danger', 'message' => 'Gagal memproses gambar. Mohon ulangi beberapa saat lagi.']);
        }

        try
        {
            ImageUtils::saveJpegAsWsq($file['basePath'] . DIRECTORY_SEPARATOR . "{$finger}.{$this->ext}");
        }
        catch(\Exception $e)
        {
            record_error('Error convert form JPG to WSQ in cropping flat finger card.', $e, $this->request);
        }

        if ( file_exists($file['basePath'] . DIRECTORY_SEPARATOR . "{$finger}.{$this->ext}") )
        {
            $requestData['fingers'][] = [
                'finger_position' => strtoupper($finger),
                'path' => $file['basePath'] . DIRECTORY_SEPARATOR . "{$finger}.{$this->ext}"
            ];
        }

        $classification = false;
        $tryNumber = 1;

        do 
        {
            if ( $tryNumber < 4 )
            {
                $henry = ApiUtils::patternClassification($requestData);
                
                if ( array_get($henry, 'status') == 'OK' && ! empty(array_get($henry, 'results.fingers')) )
                {
                    foreach (array_get($henry, 'results.fingers') as $henryDataResult)
                    {
                        if ( in_array($henryDataResult['pattern_classification'], config('ak23.whorl_map')) )
                        {
                            $henryDataResult['pattern_classification'] = 'WHORL';
                        }
                        else if ( in_array($henryDataResult['pattern_classification'], config('ak23.ulnar_map')) )
                        {
                            $henryDataResult['pattern_classification'] = 'ULNAR_LOOP';
                        }

                        $henryDataResult['point_crop'] = $data;

                        $theKey = strtolower($henryDataResult['finger_position']); 
                        $this->dataFinger['fingers'][$theKey] = $henryDataResult;
                    }

                    $classification = true;
                }

                $tryNumber++;
            }
            else
            {
                $classification = true;
                $this->dataFinger['fingers'][$finger]['image_quality'] = 0;
                $this->dataFinger['fingers'][$finger]['point_crop'] = $data;
            }
        } 
        while ( !$classification );

        // $this->dataFinger['fingers'][$finger]['missing'] = 0; // uncomment if use for tracking missing finger

        File::put($file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), json_encode($this->dataFinger));

        return redirect()->route('demographic_formula')->with('status', ['type' => 'success', 'message' => $fingerData[$finger]['title'] . ' berhasil dipotong.']);

    }    
}
