<?php

namespace App\Http\Controllers\Demographic;

use App\Http\Controllers\DemographicController;
use App\Models\People;
use App\Models\DemographicQueue;
use App\Models\Operator;
use App\Http\Requests;
use App\Utils\ImageUtils;
use Illuminate\Http\Request;
use App\Http\Requests\AddOperatorRequest;
use Carbon\Carbon;
use Validator, File, Sync, DB, Log, Crypt;
use App\Events\DemographicSaved;
use App\Utils\ApiUtils;

class ActionController extends DemographicController
{
    protected $source;
    protected $demographicData;

    public function __construct()
    {
        parent::__construct(); // Call parent constructor

        /** to use session in constructor must place inside middleware for laravel 5.3 and above */
        $this->middleware(function ($request, $next) 
        {
            $this->demographicData = get_data_from_file($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'));

            return $next($request);
        });
    }

    public function save(Request $request)
    {
        $data_push = $data_push_finger = $data_finger = $data_push_photo = [];

        $faceFiles = File::glob(session('process.basePath').DIRECTORY_SEPARATOR.'face-*.{jpg}', GLOB_BRACE);

        $this->people = new People;

        $mode = session('process.mode');

        if ( ! File::isDirectory($this->file['basePath']) )
        {
            return view(config('const.error_folder_working_view'));
        }

        if ( ! file_exists($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic')) )
        {
            return abort(500, '[btn_back]Gagal menyimpan data kedatabase. File data demographic tidak ditemukan');
        }

        foreach ($this->demographicData as $rmKey => $rmVal)
        {
            if ( empty($rmVal) ) unset($this->demographicData[$rmKey]);
        }

        if ( isset($this->demographicData['taken_date']) )
        {
            $this->demographicData['taken_date'] = date('Y-m-d', strtotime($this->demographicData['taken_date']));
        }

        if ( isset($this->demographicData['dob']) )
        {
            $this->demographicData['dob'] = date('Y-m-d', strtotime($this->demographicData['dob']));
        }

        if ( $mode == 'edit' || session()->has('subject_id_to_update') )
        {
            $this->people = People::where('subject_id', session('subject_id_to_update'))->first();
            
            if ( is_null($this->people) )
            {
                $this->people = new People;
                $this->people->subject_id = session('subject_id_to_update');
            }

            $this->people->updated_at = Carbon::now()->toDateTimeString();
            $this->people->sync_status = 'UPDATED';
        }

        if ( file_exists($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic')) )
        {
            /** Save data to biometric database via API */
            foreach (config('ak23.finger_data') as $finger => $atts) 
            {
                if ( file_exists($this->file['basePath'] . DIRECTORY_SEPARATOR .$atts['roll_name']) )
                {
                    $data_push_finger[] = [
                        'finger_position' => strtoupper($finger),
                        'path' => $this->file['basePath'] . DIRECTORY_SEPARATOR . $atts['roll_name']
                    ];
                }

                if ( file_exists($this->file['basePath'] . DIRECTORY_SEPARATOR . $atts['flat_name']) )
                {
                    $data_push_finger[] = [
                        'finger_position' => 'FLAT_'.strtoupper($finger),
                        'path' => $this->file['basePath'] . DIRECTORY_SEPARATOR . $atts['flat_name']
                    ];
                }
            }

            if ( session()->has('subject_id_to_update') )
            {
                $data_push['subject_id'] = session('subject_id_to_update');
            }

            // save fingers data from biometric return to array
            $data_push['fingers'] = $data_push_finger;

            /** Save data to local database */
            foreach ($this->demographicData as $colomn => $value)
            {
                if ( $this->people->$colomn != 'fingers' )
                {
                    $this->people->$colomn = $value;
                }
            }

            /**
             * assign to type colomn
             * @var string
             */
            $this->people->enrollment_type = session('process.type');

            /**
             * assign to biometrics_data colomn
             * @var json
             */
             if ( file_exists($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data')) )
             {
                $this->people->biometrics_data = file_get_contents($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'));
             }

            $this->people->user_id = $this->loginUserId;

            /**
             * assign to operator colomn
             * @var integer
             */
            // $this->people->operator = session('operator');

            $message = $mode != 'edit' ? 'Data <strong>'.$this->demographicData['fullname'].'</strong> telah di simpan.' : 'Data berhasil diubah';

            if ( session()->has('is_verified_local') )
            {
                $this->people->is_verified_local = 1;
                $message = 'Data <strong>'.$this->demographicData['fullname'].'</strong> telah di simpan dan di verifikasi lokal.';
            }

            if ( session()->has('is_verified_server') )
            {
                $this->people->is_verified_server = 1;
                $message = 'Data <strong>'.$this->demographicData['fullname'].'</strong> telah di simpan dan di verifikasi server.';
            }

            try
            {
                $res = ApiUtils::insert($data_push);

                if ( $res['status'] != 'OK' )
                {
                    throw new \Exception($res['message']);
                }

                if ( ! session()->has('subject_id_to_update') ) $data_push['id'] = $res['results']['subject_id'];
            }
            catch (\Exception $e)
            {
                $message = '[btn_back]Terjadi kesalahan. Gagal menyimpan template ke database. Mohon ulangi beberapa saat lagi.';
                record_error('Error in save template.', $e, $request);

                if ( strpos($e->getMessage(), 'Image is blank') !== false )
                {
                    $message = "[btn_back]Ma'af gagal menyimpan data dikarenakan tidak ditemukan wajah di Foto Wajah Depan. Mohon untuk memeriksa kembali.";
                }

                return abort(500, $message);
            }

            if ( isset($data_push['id']) )
            {
                /**
                 * assign to SubjectId colomn
                 * @var integer
                 */
                $this->people->subject_id = $data_push['id'];
            }

            DB::beginTransaction();
            
            try
            {
                /**
                 * save data to database
                 */
                $this->people->save(); 

                $peopleUpdate = People::find($this->people->demographic_id)->toArray();
                $demographicQueue = DemographicQueue::create($peopleUpdate);
                $this->option->update('demographic_queue_insert_id', $demographicQueue->demographic_queue_id);

                DB::commit();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                record_error('Error save data to database.', $e, $request);
                return abort(500, "[btn_back]Ma'af terjadi kesalahan ketika menyimpan data ke database");
            }

            event(new DemographicSaved($this->file['basePath'], $this->people->demographic_id, $this->people->subject_id, $this->option->get('demographic_queue_insert_id')));

            return redirect()->route('people.view', [encrypt($this->people->demographic_id), 'source' => 'local', 'noback' => 1])->with('status', ['type' => 'success', 'message' => $message]);
        }
        else
        {
            Log::error('Data Demographics tidak tersedia. Mohon untuk mengikuti langkah demi langkah.');
            return abort(500, '[btn_back]Terjadi kesalahan, sesi data tidak ditemukan. Mohon ulangi beberapa saat lagi.');
        }
    }
}
