<?php

namespace App\Http\Controllers\Demographic;

use App\Http\Controllers\DemographicController;
use App\Models\People;
use Data;
use App\Http\Requests;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\FrontRequest;
use App\Utils\ApiUtils;
use Log;
use Crypt;
use File;
use Activity;
use View;
use Carbon\Carbon;
use App\Services\Api\JavaServerService;
use App\Services\ZipService;

class FrontController extends DemographicController
{
    protected $source;
    protected $demographicData;
    private $javaServerService;
    private $zip;

    public function __construct(JavaServerService $javaServerService, ZipService $zip)
    {
        parent::__construct(); // Call parent constructor

        $this->javaServerService = $javaServerService;

        $this->zip = $zip;

        /** to use session in constructor must place inside middleware for laravel 5.3 and above */
        $this->middleware(function ($request, $next) 
        {
            $this->demographicData = get_data_from_file($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'));

            View::share('demographicData', $this->demographicData);

            return $next($request);
        });
    }

    public function index()
    {
        $scanType = $this->type;

        if ( ! File::isDirectory($this->file['basePath']) )
        {
            return view(config('const.error_folder_working_view'));
        }

        if ( $scanType == 'card' && session('process.mode') != 'edit' )
        {
            if ( ! flagCheck('crop_flat_four_fingers_process_done') )
            {
                return redirect()->route('demographic_card_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses scan kartu terlebih dahulu']);
            }

            if ( ! flagCheck('finger_flat_process_done') )
            {
                return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Mohon melakukan cropping jari rata dulu']);
            }

            if ( ! flagCheck('cropping_card_process_done')  )
            {
                return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Aplikasi sedang memproses kartu, mohon menunggu beberapa saat lagi']);
            }

            if ( ! flagCheck('verification_process_done') && str_contains($this->request->path(), 'formula') )
            {
                return  redirect()->route('demographic_identification')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses identifikasi terlebih dahulu']);
            }

            if ( ! flagCheck('formula_process_done') )
            {
                return redirect()->route('demographic_formula')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan perumusan terlebih dahulu']);
            }

            if ( ! flagCheck('picture_process_done') )
            {
                return redirect()->route('demographic_picture')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan proses rekam foto terlebih dahulu']);
            }
        }
        
        if ( $scanType == 'live' && session('process.mode') != 'edit' )
        {
            if ( ! flagCheck('enrollment_process_done') )
            {
                return redirect()->route('demographic_finger_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses perekaman sidik jari terlebih dahulu']);
            }

            if ( ! flagCheck('verification_process_done') )
            {
                return  redirect()->route('demographic_identification')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses identifikasi terlebih dahulu']);
            }

            if ( ! flagCheck('formula_process_done') )
            {
                return redirect()->route('demographic_formula')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan perumusan terlebih dahulu']);
            }

            if ( ! flagCheck('camera_process_done') )
            {
                return redirect()->route('demographic_camera')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan perekaman foto terlebih dahulu']);
            }
        }

        $title = 'Tampilan Depan';
        $page = 'front';
        $status_holder = [];
        $status = $this->dataService->status();
        foreach ($status as $value) 
        {
            $status_holder[$value->status_id] = $value->label;
        }
        $other = $status_holder[10];
        
        asort($status_holder); // sort array alphabetically and without change the key

        // unset key 10 which is "Lain-lain" and move to the last index
        unset($status_holder[10]);
        $status_holder[10] = $other;

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', 'data')]);

        $takenDate = $takenBy = '';

        if ($scanType == 'live') 
        {
            $takenDate = date('d-m-Y');
            $takenBy = $this->auth->name;
        }

        return view('demographic.front', compact('title', 'scanType', 'page', 'takenDate', 'takenBy'))->with(['file' => $this->file, 'status' => $status_holder]);
    }

    public function edit(Request $request, $id = null)
    {
        if ( empty($id) )
        {
            return abort(404);
        }

        $people = new People;

        $flagUpdate = $request->get('flag');

        $title = 'Ubah data ' . $people->fullname;
        $status =  app()->make('DataService')->statusArray();
        $other = $status[10];

        // sort array alphabetically and without change the key
        asort($status); 

        // unset key 10 which is "Lain-lain" and move to the last index
        unset($status[10]);

        $status[10] = $other;

        if ( $this->networkStatus == 'on' && ($flagUpdate != 'local') )
        {
            $people_server = $this->javaServerService->search(['subjectId' => $id, 'includeTemplate' => 'true']);

            if ( array_get($people_server, 'status') === 'OK' && !empty(array_get($people_server, 'results.content')) )
            {
                // TO DO if people subjectId not found here
                $getData = array_get($people_server, 'results.content.0');

                $zipFile = config('url.demographicZip') . "/{$getData['id']}.zip";
                $zipFileInfo = pathinfo($zipFile);
                $zipTemp = config('path.data.tmp') . DIRECTORY_SEPARATOR . $id . '.' . array_get($zipFileInfo, 'extension');

                $updatedAtServer = $getData['updatedAt'] / 1000; // divide to 1000 because return miliseconds

                $people_local = $people->where('subject_id', $id)->first();

                if ( count($people_local) > 0 && (int)$updatedAtServer <= strtotime($people_local->updated_at) )
                {
                    return redirect("demographic/edit/{$id}/front?flag=local");
                }
                else
                {
                    // change to update if data server is newer
                    if ( count($people_local) > 0 && (int)$updatedAtServer > strtotime($people_local->updated_at) )
                    {
                        $people = People::findOrFail($people_local->demographic_id);
                    }

                    try
                    {
                        download_file($zipFile, $zipTemp); // Doing download process
                    }
                    catch ( \Exception $e )
                    {
                        return abort(500, '[btn_back]'.$e->getMessage());
                    }

                    $fieldsToRemove = ['demographic_id', 'date_uploaded'];

                    $relationsData = ['religion_id', 'status_id', 'education_id', 'skin_id', 'body_id', 'head_id', 'hair_color_id', 'hair_id', 'face_id', 'forehead_id', 'eye_color_id', 'eye_irregularity_id', 'nose_id', 'lip_id', 'chin_id', 'ear_id', 'tooth_id', 'user_id'];

                    $demographic_fields = \Schema::getColumnListing('demographics'); 

                    foreach ($fieldsToRemove as $colomn) 
                    {
                        $key = array_search($colomn, $demographic_fields);
                        unset($demographic_fields[$key]);
                    }

                    foreach ($relationsData as $relationsDataColomn) 
                    {
                        $key = array_search($relationsDataColomn, $demographic_fields);
                        unset($demographic_fields[$key]);
                    }

                    foreach ($getData as $key => $value) 
                    {
                        $getData[snake_case($key)] = $value;
                        $getData[snake_case($key).'_id'] = $value;
                    }

                    foreach ($demographic_fields as $colomn) 
                    {
                        if ( isset($getData[$colomn]) )
                        {
                            $people->$colomn = $getData[$colomn];
                        }
                    }

                    foreach ($relationsData as $relationColomn) 
                    {
                        if ( isset($getData[$relationColomn]) )
                        {
                            $people->$relationColomn = $getData[$relationColomn]['id'];
                        }
                    }

                    $people->created_at = ! is_null($getData['createdAt']) ? Carbon::createFromTimeStamp($getData['createdAt'] / 1000)->toDateTimeString() : null;

                    $people->updated_at = ! is_null($getData['updatedAt']) ? Carbon::createFromTimeStamp($getData['updatedAt'] / 1000)->toDateTimeString() : null;

                    $people->user_id = $this->loginUserId;

                    if ( isset($getData['personRace']) )
                    {
                         $people->race = $getData['personRace'];
                    }

                    $people->save();

                    $dir = $people->demographic_id;
                    $completePath = $this->path['data']['complete'] . DIRECTORY_SEPARATOR . $dir;

                    if ( !File::isDirectory($completePath) )
                    {
                        File::makeDirectory($completePath, 0777);
                    }
                    else
                    {
                        File::deleteDirectory($completePath, true);
                    }

                    $this->zip->demographicServerExtractor($zipTemp, $completePath); // Doing unzip process

                    return redirect("demographic/edit/{$id}/front?flag=local");              
                }
            }
        }

        $people = $people->where('subject_id', $id)->firstOrFail();

        $scanType = $people->enrollment_type;

        $basePath = $this->path['data']['edit'].DIRECTORY_SEPARATOR.$people->demographic_id;

        if ( ! File::isDirectory($this->path['data']['edit']) )
        {
            File::makeDirectory($this->path['data']['edit'], 0777);
        }

        File::copyDirectory($this->path['data']['complete'] . DIRECTORY_SEPARATOR . $people->demographic_id, $this->path['data']['edit'] . DIRECTORY_SEPARATOR . $people->demographic_id);

        $frontData = $backData = $flatFinger = [];

        $fields = config('ak23.form_fields');

        foreach ( $fields['front'] as $fieldFrontKey => $valueFront )
        {
            $fieldFrontKey = str_replace('-', '_', str_replace('.jpg', '', $fieldFrontKey));
            $frontData[$fieldFrontKey] = $people->$fieldFrontKey;
        }

        foreach ( $fields['back'] as $fieldBackKey => $valueBack )
        {
            $fieldBackKey = str_replace('-', '_', str_replace('.jpg', '', $fieldBackKey));

            if (strpos($fieldBackKey, 'child') === false)
            {
                $backData[$fieldBackKey] = $people->$fieldBackKey;
            }
        }

        $frontData['taken_date'] = ! empty($frontData['taken_date']) ? format_date($frontData['taken_date']) : '';
        $backData['dob'] = ! empty($backData['dob']) ? format_date($backData['dob']) : '';
        $backData['children'] = $people->children;

        $file = $this->cardService->data($people->demographic_id, 'edit');

        removeDemographicSession();

        session([
            'process' => [
                'status' => true,
                'file' => $people->demographic_id,
                'file_front' => $people->enrollment_type != 'card' ? null : $file['front']['name'],
                'file_back' => $people->enrollment_type != 'card' ? null : $file['back']['name'],
                'folder' => $people->demographic_id,
                'basePath' => $basePath,
                'baseUrl' => data_url('edit/'.$people->demographic_id),
                'path_front' => $people->enrollment_type != 'card' ? null : $file['front']['path'],
                'path_back' => $people->enrollment_type != 'card' ? null : $file['back']['path'],
                'url_front' => $people->enrollment_type != 'card' ? null : $file['front']['url'],
                'url_back' => $people->enrollment_type != 'card' ? null : $file['back']['url'],
                'type' => $people->enrollment_type,
                'mode' => 'edit',
            ],
            'subject_id_to_update' => $people->subject_id
        ]);

        File::put($basePath . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'), json_encode(array_merge($frontData, $backData)));

        File::put($basePath . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), $people->biometrics_data);

        $this->activity->log('demographic-'.$people->enrollment_type.'-edit', 'Melakukan edit data demographic dengan subject ID ' . $people->subject_id, $this->loginUserId);

        return redirect()->route('demographic_front');
    }

    public function submit(FrontRequest $request)
    {
        $data = $request->except('_token');  

        try
        {    
            if ( ! array_key_exists($data['status_id'], app()->make('DataService')->statusArray()) )
            {
                $data['status_id'] = null;
            }

            if ( ! empty($data['taken_date']) )
            {
                $data['taken_date'] = format_date($data['taken_date']);
            }
            else
            {
                unset($data['taken_date']);
            }

            foreach ($data as $dataKey => $dataVal)
            {
                $this->demographicData[$dataKey] = $dataVal;
            }

            File::put($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'), json_encode($this->demographicData));
            
            if ( session('process.mode') != 'edit' )
            {
                flag('front_data_process_done');
            }

            if ( array_has($data, 'back') )
            {
                if (session('process.type') == 'card')
                {
                    return redirect()->route('demographic_picture');
                }

                return redirect()->route('demographic_camera');
            }

            return redirect()->route('demographic_back');
        }
        catch (\Exception $e)
        {
            record_error('Error in submit front data', $e, $request);
            redirect()->back();
        }
    }
}