<?php

namespace App\Http\Controllers\Demographic;

use App\Http\Controllers\DemographicController;
use App\Models\People;
use Illuminate\Http\Request;
use App\Http\Requests\PictureRequest;
use Illuminate\Support\Facades\Input;
use GuzzleHttp\Client;
use Log, Crypt, File, Image, Activity;
use App\Services\BiometricService;

class PictureController extends DemographicController
{
    protected $destinationPath;
    private $biometricService;

    public function __construct(BiometricService $biometricService)
    {
        parent::__construct(); // Call parent constructor  
        
        $this->biometricService = $biometricService;
    }

    public function index(Request $request)
    {
        $this->file = session('process');
        $scanType = $this->type;

        if ( session('process.mode') != 'edit' ) 
        {
            if ( $scanType == 'card'  )
            {
                if ( ! flagCheck('crop_flat_four_fingers_process_done') )
                {
                    return redirect()->route('demographic_card_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses scan kartu terlebih dahulu']);
                }

                if ( ! flagCheck('finger_flat_process_done') )
                {
                    return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Mohon melakukan cropping jari rata dulu']);
                }

                if ( ! flagCheck('cropping_card_process_done')  )
                {
                    return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Aplikasi sedang memproses kartu, mohon menunggu beberapa saat lagi']);
                }

                if ( ! flagCheck('verification_process_done') && str_contains($this->request->path(), 'formula') )
                {
                    return  redirect()->route('demographic_identification')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses identifikasi terlebih dahulu']);
                }

                if ( ! flagCheck('formula_process_done') )
                {
                    return redirect()->route('demographic_formula')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan perumusan terlebih dahulu']);
                }
            }
        }

        if ( ! File::isDirectory(session('process.basePath')) )
        {
            return view(config('const.error_folder_working_view'));
        }

        $title = 'Foto';

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', 'picture')]);

        return view('demographic.picture', compact('title', 'scanType'))->with(['file' => $this->file, 'fileNaming' => $this->fileNaming]);
    }

    public function submit(PictureRequest $request, $scanType = 'live')
    {
        $this->file = session('process');
        $source = ($scanType != 'card') ? $this->path['data']['live'] : $this->path['data']['card'];
        $this->destinationPath = $source. DIRECTORY_SEPARATOR . $this->file['folder']; // upload path

        $uploadError = $messages = [];
        
        $left_side = Input::file('picture_left_side');
        $front_side = Input::file('picture_front_side');
        $right_side = Input::file('picture_right_side');

        try
        {
            if (!empty($left_side))
            {
                $left_side->move($this->destinationPath, $this->fileNaming['face_left']); // uploading file to given path

                img($this->destinationPath . DIRECTORY_SEPARATOR . $this->fileNaming['face_left'])->resize(2048, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($this->destinationPath . DIRECTORY_SEPARATOR . $this->fileNaming['face_left']);

                if ( file_exists(config('const.photo_source') . DIRECTORY_SEPARATOR . $left_side->getClientOriginalName()))
                {
                    unlink(config('const.photo_source') . DIRECTORY_SEPARATOR . $left_side->getClientOriginalName());
                }

                array_push($messages, 'Upload foto wajah kiri berhasil');
            }

            if (!empty($front_side))
            {
                $front_side->move($this->destinationPath, $this->fileNaming['face_front']); // uploading file to given path

                img($this->destinationPath . DIRECTORY_SEPARATOR . $this->fileNaming['face_front'])->resize(2048, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($this->destinationPath . DIRECTORY_SEPARATOR . $this->fileNaming['face_front']);

                if ( file_exists(config('const.photo_source') . DIRECTORY_SEPARATOR . $front_side->getClientOriginalName()))
                {
                    unlink(config('const.photo_source') . DIRECTORY_SEPARATOR . $front_side->getClientOriginalName());
                }

                array_push($messages, 'Upload foto wajah depan berhasil');
            }
            if (!empty($right_side))
            {
                $right_side->move($this->destinationPath, $this->fileNaming['face_right']); // uploading file to given path

                img($this->destinationPath . DIRECTORY_SEPARATOR . $this->fileNaming['face_right'])->resize(2048, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->save($this->destinationPath . DIRECTORY_SEPARATOR . $this->fileNaming['face_right']);

                if ( file_exists(config('const.photo_source') . DIRECTORY_SEPARATOR . $right_side->getClientOriginalName()))
                {
                    unlink(config('const.photo_source') . DIRECTORY_SEPARATOR . $right_side->getClientOriginalName());
                }

                array_push($messages, 'Upload foto wajah kanan berhasil');
            }

            $text_message = 'Tidak ada file yang di upload.';
            $status = 'info';

            if ( !empty($messages) )
            {
                $text_message = '<ul>';

                foreach ($messages as $message) 
                {
                    $text_message .= '<li>'.$message.'</li>';
                }

                $text_message .= '</ul>';
                $status = 'success';
            }

            return redirect(config("url.{$scanType}.picture"))->with('status', ['class' => $status, 'message' => $text_message])->header('Cache-Control', 'no-store, no-cache, must-revalidate');
        }
        catch (\Exception $e)
        {
            record_error('Error in upload photos.', $e, $request);
            return redirect(config("url.{$scanType}.picture"))->with('status', ['type' => 'danger', 'message' => $e->getMessage()]);
        }
    }

    public function submit_next(Request $request)
    {
        flag('picture_process_done');

        return redirect()->route('demographic_front');
    }

    public function remove($position)
    {
        $this->file = session('process');

        if ( file_exists($this->file['basePath'].DIRECTORY_SEPARATOR."{$position}.{$this->ext}") )
        {
            if ( ! File::delete($this->file['basePath'].DIRECTORY_SEPARATOR."{$position}.{$this->ext}") )
            {
                return redirect()->back()->with('status', ['type' => 'error', 'message' => "Gambar gagal dihapus. Mohon coba beberapa saat lagi."]);
            }
            else
            {
                return redirect()->back()->with('status', ['type' => 'success', 'message' => facePositionName($position) . " berhasil dihapus."]);
            }
        }

        return redirect()->back();
    }

    public function crop($position = null)
    {
        if ( is_null($position) )
        {
            return redirect()->back();
        }
        
        $this->file = session('process');

        $source = $this->path['data']['edit'];
        $source_folder = 'edit';

        if ( session('process.mode') != 'edit')
        {
            $source = ($this->type != 'card') ? $this->path['data']['live'] : $this->path['data']['card'];
            $source_folder = ($this->type != 'card') ? config('filename.live_folder') : config('filename.card_folder');
        }

        $title = 'Potong Foto';
        $scanType = $this->type;

        $imageUrl = $this->file['baseUrl'] . "/back.{$this->ext}";
        $cancelUrl = route('demographic_picture', [$this->type]);

        return view('demographic.picturecrop', compact('title', 'source', 'source_folder', 'scanType', 'position', 'imageUrl', 'cancelUrl'))->with(['file' => $this->file, 'fileNaming' => $this->fileNaming]);
    }
    
    public function doCrop(Request $request, $position = null)
    {
        if ( is_null($position) )
        {
            return redirect()->back();
        }

        $data = $request->all();
        $file = session('process');

        // doing this because rotate position in js and php is not same.
        // in js 90, php -90, so we need to reverse it
        if ( $data['r'] > 0 ) 
        {
            $data['r'] = -$data['r'];
        }
        else if ( $data['r'] < 0 )
        {
            $data['r'] = abs($data['r']);
        }

        $card_name_folder = config('filename.card_folder');

        try
        {
            if ( $this->type == 'card' )
            {
                $people = session('people_data');

                Image::make($file['basePath'] . DIRECTORY_SEPARATOR . "back.{$this->ext}")->rotate($data['r'], '#FFFFFF')->crop(intval($data['w']), intval($data['h']), intval($data['x']), intval($data['y']))->save($file['basePath'] . DIRECTORY_SEPARATOR . "{$position}.{$this->ext}");

                return redirect()->route('demographic_picture')->with('status', ['type' => 'success', 'message' => facePositionName($position) . ' berhasil dipotong.']);
            }
            else
            {
                // Crop foto for live scan here
            }
        }
        catch(\Exception $e)
        {
            record_error('Error cropping picture.', $e, new Request);
            return redirect()->back()->with('status', ['type' => 'error', 'message' => 'Gagal memproses gambar. Mohon ulangi beberapa saat lagi.']);
        }
    }          
}
