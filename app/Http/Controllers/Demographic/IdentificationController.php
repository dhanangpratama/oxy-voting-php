<?php

namespace App\Http\Controllers\Demographic;

use App\Http\Controllers\DemographicController;
use App\Models\People;
use App\Http\Requests;
use Illuminate\Http\Request;
use Log;
use Crypt;
use File;
use Activity;
use App\Utils\ApiUtils;
use App\Services\BiometricService;
use App\Services\IdentificationService;

class IdentificationController extends DemographicController
{
    protected $type;
    protected $source;
    private $biometricService;
    private $identificationService;

    public function __construct(BiometricService $biometricService, IdentificationService $identificationService)
    {
        parent::__construct(); // Call parent constructor   
        
        $this->biometricService = $biometricService;

        $this->identificationService = $identificationService;
    }

    public function index(Request $request, $key = null)
    {
        $dataFingerMatching = $resultMatching = $peopleMatching = $candidateData = [];

        $dataSession = session()->all();

        $file = session('process');

        $verificationFile = $file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data')['verification_data'];

        $dataFinger = get_data_from_file($file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'));

        $server = false;

        if ( is_null($key) && session()->has('verification_key') )
        {
            $key = session('verification_key');
        }
        else if ( is_null($key) )
        {
            $key = 0;
        }
        else
        {
            $key = decrypt($key);
        }

        $scanType = $this->type;

        $this->source = $this->path['data']['edit'];

        if (session('process.mode') != 'edit')
        {
            $this->source = ($scanType != 'card') ? $this->path['data']['live'] : $this->path['data']['card'];

            if ( $scanType == 'card' )
            {
                if ( ! flagCheck('crop_flat_four_fingers_process_done') )
                {
                    return redirect()->route('demographic_card_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses scan kartu terlebih dahulu']);
                }

                if ( ! flagCheck('finger_flat_process_done') )
                {
                    return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Mohon melakukan cropping jari rata dulu']);
                }

                if ( ! flagCheck('cropping_card_process_done')  )
                {
                    return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Aplikasi sedang memproses kartu, mohon menunggu beberapa saat lagi']);
                }
            }
            else
            {
                if ( ! flagCheck('enrollment_process_done') )
                {
                    return redirect()->route('demographic_finger_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses perekaman sidik jari terlebih dahulu']);
                }
            }
        }

        if (  ! File::isDirectory($file['basePath']) )
        {
            return view(config('const.error_folder_working_view'));
        }

        if ( ! empty($dataFinger) )
        {
            if ( $this->networkStatus == 'on' )
            {
                $server = true; // Set server to true if online
            }

            if ( $server )
            {
                // Matching server when online
                try
                {
                    $results = $this->identificationService->server($key);
                }
                catch ( \Exception $e )
                {
                    record_error('Error matching server in identification page', $e);
                    $server = false; // if error with matching server, set $server var to false
                }
            }

            if ( !$server )
            {
                // if $server var false do local matching
                try
                {
                    $results = $this->identificationService->local($key, $dataFinger['fingers']);
                }
                catch ( \Exception $e )
                {
                    record_error('Error matching local in identification page', $e);
                    $results = []; // if any errors in local matching set it to no results
                }
            }
        }
        
        $finger = config('ak23.finger_data');
        $title = 'Verifikasi - Flat Finger';

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', 'identification')]);

        if ( ! empty($results['content']) )
        {
            $candidateData = [
                'status' => ! empty($results['content'][$key]['status']) ? $results['content'][$key]['status'] : '-',
                'fullname' => ! empty($results['content'][$key]['fullname']) ? $results['content'][$key]['fullname'] : '-',
                'nickname' => ! empty($results['content'][$key]['detail']['nickname']) ? $results['content'][$key]['detail']['nickname'] : '-',
                'sex' => ! empty($results['content'][$key]['detail']['sex']) ? sex_label($results['content'][$key]['detail']['sex']) : '-',
                'bloodType' => ! empty($results['content'][$key]['detail']['blood_type']) ? $results['content'][$key]['detail']['blood_type'] : '-',
                'job' => ! empty($results['content'][$key]['detail']['job']) ? $results['content'][$key]['detail']['job'] : '-',
                'pob' => ! empty($results['content'][$key]['detail']['pob']) ? $results['content'][$key]['detail']['pob'] : '-',
                'dob' => ! empty($results['content'][$key]['detail']['dob']) ? format_date($results['content'][$key]['detail']['dob']) : '-',
                'nik' => ! empty($results['content'][$key]['detail']['nik']) ? $results['content'][$key]['detail']['nik'] : '-',
                'address' =>  ! empty($results['content'][$key]['full_address']) ? $results['content'][$key]['full_address'] : '-',
                'score' => ! empty($results['content'][$key]['score']) ? $results['content'][$key]['score'] : '-',
                'scoreFlag' => ! empty($results['content'][$key]['scoreFlag']) ? $results['content'][$key]['scoreFlag'] : '-',
                'baseUrl' => ! empty($results['content'][$key]['base_url']) ? $results['content'][$key]['base_url'] : '-',
                'subjectId' => ! empty($results['content'][$key]['id']) ? $results['content'][$key]['id'] : '-'
            ];
        }
        
        return view('demographic.identification', compact('file', 'title', 'side', 'finger', 'scanType', 'key', 'results', 'candidateData','dataSession', 'address'));
    }

    public function submit_edit(Request $request)
    {
        $this->file = session('process');
        
        $data = $request->all();

        $people = new People;
        $people_found = $people->where('SubjectId', $data['SubjectId_found'])->firstorfail();
        $people = $people->where('SubjectId', $data['SubjectId'])->firstorfail();
        $people_array = $people->toArray();
        $people_id = $people_array['id'];
        
        unset($people_array['id'], $people_array['SubjectId']);

        foreach ($people_array as $colomn => $value) 
        {
            $people_found->$colomn = $people_array[$colomn];
        }

        $people_found->save();

        File::deleteDirectory(config('path.data.complete') . DIRECTORY_SEPARATOR . $people_found->id, true);
        File::copyDirectory( config('path.data.complete') . DIRECTORY_SEPARATOR . $people_id, config('path.data.complete') . DIRECTORY_SEPARATOR . $people_found->id );

        /**
         * Run soft delete
         */
        $people->deleted_at = date('Y-m-d H:i:s');
        $people->save();

        $encrypt_id = Crypt::encrypt($people_found->id);

        return redirect('people/view/'.$encrypt_id)->with('status', ['type' => 'success', 'message' => 'Data telah diubah.']);
    }

    public function chooseCandidate(Request $request)
    {
        $inputAll = $request->all();
        $subjectId = $request->get('subject_id');
        $key = $request->get('key');
        $this->file = session('process');
        $verificationFile = session('process.basePath') . DIRECTORY_SEPARATOR . config('ak23.file_data.verification_data');
        $dataVerification = get_data_from_file($verificationFile);

        // $fingerFile = File::glob(session('process.basePath').DIRECTORY_SEPARATOR.'*.{wsq}', GLOB_BRACE);
        // $biometricData = get_data_from_file(session('process.basePath') . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'));

        // if ( ! empty($fingerFile) && ! array_has($biometricData['fingers'], 'right_thumb.pattern_classification') )
        // {
        //     // Doing pattern classification again
        //     $this->biometricService->patternClassification(session('process.basePath'));
        // }
        
        if ( ! array_has($inputAll, 'skip') )
        {
            $demographicData = get_data_from_file($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'));

            if ( array_has($dataVerification, 'content.'.$key.'.detail') )
            {
                if ( ! is_null($takenDate = array_get($dataVerification, "content.{$key}.detail.taken_date")) )
                {
                    $dataVerification['content'][$key]['detail']['taken_date'] = format_date_null($takenDate);
                }

                if ( ! is_null($dob = array_get($dataVerification, "content.{$key}.detail.dob")) )
                {
                    $dataVerification['content'][$key]['detail']['dob'] = format_date_null($dob);
                }

                $demographicData = array_merge($demographicData, array_get($dataVerification, "content.{$key}.detail"));
            }
            else
            {
                 $demographicData['fullname'] = array_get($dataVerification, "content.{$key}.fullname");
            }

            File::put($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'), json_encode($demographicData));

            session(['subject_id_to_update' => $subjectId, 'verification_key' => $key]);
        }
        else
        {
            session()->forget(['subject_id_to_update', 'verification_key']);
        }

        if ( array_has($dataVerification, 'source') )
        {
            if ( array_get($dataVerification, 'source') == 'server' )
            {
                session(['is_verified_server' => 1]);
            }

            if ( array_get($dataVerification, 'source') == 'local' )
            {
                session(['is_verified_local' => 1]);
            }

            File::put(config('path.data.tmp_compare') . DIRECTORY_SEPARATOR . 'process.json', json_encode(['source' => array_get($dataVerification, 'source'), 'subjectId' => $subjectId, 'id' => basename(array_get($dataVerification, "content.{$key}.base_url"))]));

            session(['identification_source' =>array_get($dataVerification, 'source')]);
        }

        flag('verification_process_done');

        return redirect()->route('demographic_formula');
    }
}
