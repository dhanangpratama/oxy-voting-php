<?php

namespace App\Http\Controllers\Demographic;

use App\Http\Controllers\DemographicController;
use App\Models\People;
use App\Models\Henry;
use App\Http\Requests;
use DB;
use Illuminate\Http\Request;
use App\Http\Requests\BackRequest;
use Log;
use Crypt;
use File;
use Activity;
use View;

class BackController extends DemographicController
{
    protected $source;
    protected $sinyalemen;
    protected $data;
    protected $demographicData;

    public function __construct()
    {
        parent::__construct(); // Call parent constructor

        /** to use session in constructor must place inside middleware for laravel 5.3 and above */
        $this->middleware(function ($request, $next) 
        {
            $this->demographicData = get_data_from_file($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'));

            View::share('demographicData', $this->demographicData);

            return $next($request);
        });

        $this->data = app()->make('DataService');

        $this->sinyalemen = array(
                'skin' => $this->data->skin(),
                'body' => $this->data->body(),
                'head' => $this->data->head(),
                'hair_color' => $this->data->hairColor(),
                'hair' => $this->data->hair(),
                'face' => $this->data->face(),
                'forehead' => $this->data->forehead(),
                'eye_color' => $this->data->eyeColor(),
                'eye_irregularity' => $this->data->eyeIrregularity(),
                'nose' => $this->data->nose(),
                'lip' => $this->data->lip(),
                'tooth' => $this->data->tooth(),
                'chin' => $this->data->chin(),
                'ear' => $this->data->ear()
            );
    }

    public function index()
    {
        $scanType = $this->type;

        if (session('process.mode') == 'edit')
        {
            $this->source = $this->path['data']['edit'];
        }

        if ( ! File::isDirectory($this->file['basePath']) )
        {
            return view(config('const.error_folder_working_view'));
        }

        if ( $scanType == 'card' && session('process.mode') != 'edit' )
        {
            if ( ! flagCheck('crop_flat_four_fingers_process_done') )
            {
                return redirect()->route('demographic_card_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses scan kartu terlebih dahulu']);
            }

            if ( ! flagCheck('finger_flat_process_done') )
            {
                return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Mohon melakukan cropping jari rata dulu']);
            }

            if ( ! flagCheck('cropping_card_process_done')  )
            {
                return  redirect()->route('demographic_finger_flat', ['right_thumb'])->with('status', ['type' => 'info', 'message' => 'Aplikasi sedang memproses kartu, mohon menunggu beberapa saat lagi']);
            }

            if ( ! flagCheck('verification_process_done') && str_contains($this->request->path(), 'formula') )
            {
                return  redirect()->route('demographic_identification')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses identifikasi terlebih dahulu']);
            }

            if ( ! flagCheck('formula_process_done') )
            {
                return redirect()->route('demographic_formula')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan perumusan terlebih dahulu']);
            }

            if ( ! flagCheck('picture_process_done') )
            {
                return redirect()->route('demographic_picture')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan proses rekam foto terlebih dahulu']);
            }

            if ( ! flagCheck('front_data_process_done') )
            {
                return redirect()->route('demographic_front')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan pengisian data kartu bagian depan terlebih dahulu']);
            }
        }

        if ( session()->has('front_data') )
        {
            $frontdata = session('front_data');

            if ( empty($frontdata['fullname']) || empty($frontdata['sex']) )
            {
                return redirect()->route('demographic_front')->with('status', ['type' => 'error', 'message' => 'Mohon untuk mengisi field wajib (Nama dan Jenis Kelamin).']);
            }
        }

        $back_data = session('back_data');

        $title = 'Tampilan Belakang';
        $page = 'back';

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', 'data')]);

        return view('demographic.back', compact('title', 'scanType', 'back_data', 'page'))->with(['file' => $this->file, 'religion' => $this->data->religion(), 'educations' => $this->data->education(), 'sinyalemen' => $this->sinyalemen]);
    }

    public function submit(BackRequest $request)
    {
        $this->file = session('process');
        $data = $request->except('_token');
        $childs = implode(', ', array_filter($data['children']));
        $data['children'] = $childs;

        if ( ! empty($data['dob']) )
        {
            $data['dob'] = format_date($data['dob']);
        }
        else
        {
            unset($data['dob']);
        }

        foreach ($data as $dataKey => $dataVal)
        {
            $this->demographicData[$dataKey] = $dataVal;
        }

        File::put($this->file['basePath'] . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'), json_encode($this->demographicData));

        if ( session('process.mode') != 'edit' )
        {
            flag('back_data_process_done');
        }

        if ( array_has($data, 'back') )
        {
            return redirect()->route('demographic_front');
        }
        
        return redirect()->route('demographic_save');
    }
}
