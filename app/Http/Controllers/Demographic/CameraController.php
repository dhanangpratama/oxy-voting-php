<?php

namespace App\Http\Controllers\Demographic;

use Illuminate\Http\Request;
use App\Http\Controllers\DemographicController;
use App\Services\CameraService;

class CameraController extends DemographicController
{
    private $request;

    private $cameraService;

    public function __construct(Request $request, CameraService $cameraService)
    {
        parent::__construct();

        $this->request = $request;
        $this->cameraService = $cameraService;
    }

    public function index()
    {   
        if ( session('process.mode') != 'edit' )
        {
            if ( ! flagCheck('enrollment_process_done') )
            {
                return redirect()->route('demographic_finger_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses perekaman sidik jari terlebih dahulu']);
            }

            if ( ! flagCheck('verification_process_done') )
            {
                return  redirect()->route('demographic_identification')->with('status', ['type' => 'info', 'message' => 'Mohon menyelesaikan proses identifikasi terlebih dahulu']);
            }

            if ( ! flagCheck('formula_process_done') )
            {
                return redirect()->route('demographic_formula')->with('status', ['type' => 'info', 'message' => 'Mohon melakukan perumusan terlebih dahulu']);
            }
        }

        $title = 'Pengambilan Foto';
        $page = 'camera';

        $file = session('process');

        $drive = driveFind();

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', 'camera')]);

        return view('record.camera', compact('title', 'page', 'file', 'drive'));
    }

    public function submit()
    {
        $dataImages = $this->request->except('_token');

        $this->cameraService->submit($dataImages, $this->request);

        if ( array_has($dataImages, 'back') )
        {
            return redirect()->route('demographic_formula');
        }

        return redirect()->route('demographic_front');
    }
}
