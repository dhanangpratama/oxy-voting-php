<?php

namespace App\Http\Controllers\Demographic;

use Illuminate\Http\Request;
use App\Http\Controllers\DemographicController;
use App\Http\Requests;
use App\Models\People;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;
use App\Services\CardService;
use Activity, File, Auth;

class CardController extends DemographicController
{
    public $operator;

    public $dir_to_remove;

    protected $cardService;

    public function __construct()
    {
        parent::__construct();

        $this->cardService = new CardService;
    }

    public function index(Request $request)
    {
        $title = 'Card Scan';
        $page = 'card-scan';

        session(['current_indicator' => array_search_inner(($this->type == 'card') ? config('ak23.card_breadcrumbs') : config('ak23.live_breadcrumbs'), 'id', 'scan_card')]);

        return view('record.card', compact('title', 'page'));
    }

    public function submit(Request $request)
    {
        if ( ! File::exists(session('process.path_front')) || ! File::exists(session('process.path_back')) )
        {
            return redirect()->route('demographic_card_scan_record')->with('status', ['type' => 'info', 'message' => 'Mohon untuk menyelesaikan scan Kartu AK-23']);
        }

        $this->cardService->doProcess('flat_four');

        flagDelete('cropping_card_process_done');

        return redirect()->route('demographic_finger_flat', ['right_thumb']);
    }

    public function createProcessSession(Request $request)
    {
        $pathCard = config('path.data.card');

        File::put($this->path['data']['tmp'] . DIRECTORY_SEPARATOR . 'delete_flag.txt', time());

        File::deleteDirectory($pathCard, true);
        File::deleteDirectory(config('path.data.tmp_minutiae'), true);
        File::deleteDirectory(config('path.data.tmp_compare'), true);

        $dataFingers = $dataFingersFlat = [];

        foreach ( config('ak23.finger_index') as $finger )
        {
            $dataFingers['fingers'][$finger] = $dataFingers['fingers']['flat_'.$finger] = $dataFingersFlat['fingers']['flat_'.$finger] = array('point_crop' => null);
        }

        File::put($pathCard . DIRECTORY_SEPARATOR . config('ak23.file_data.biometrics_data'), json_encode($dataFingers));

        File::put($pathCard . DIRECTORY_SEPARATOR . config('ak23.file_data.flat_biometrics_data'), json_encode($dataFingersFlat));

        $demographicData = demographic_fields();
        
        if ( array_has($demographicData, 'sex') )
        {
            $demographicData['sex'] = 'm';
        }

        File::put($pathCard . DIRECTORY_SEPARATOR . config('ak23.file_data.demographic'), json_encode($demographicData));

        flag('processedby_'.$this->loginUserId, true, $pathCard);

        // Create session data
        session([
            'process' => [
                'status' => true,
                'file' => basename($pathCard),
                'file_front' => 'front.jpg',
                'file_back' => 'back.jpg',
                'folder' => basename($pathCard),
                "basePath" => $pathCard,
                "baseUrl" => data_url(basename($pathCard)),
                'path_front' => $pathCard . DIRECTORY_SEPARATOR . 'front.jpg',
                'path_back' => $pathCard . DIRECTORY_SEPARATOR . 'back.jpg',
                'url_front' => data_url(basename($pathCard) . "/front.jpg"),
                'url_back' => data_url(basename($pathCard) . "/back.jpg"),
                'type' => 'card',
                'mode' => 'new'
            ]
        ]);

        return redirect()->route('demographic_card_scan_record');
    }
}