<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Models\BioOptions;
use GuzzleHttp\Client;
use App\Utils\ToolUtils;
use Carbon\Carbon;
use Auth, File, App, View, Artisan, Cache;

class MainController extends Controller
{
    protected $people;
    protected $file;
    protected $fileNaming;
    protected $ext;
    protected $options = [];
    protected $workstation = [];
    protected $auth;
    protected $loginUserId;
    protected $messageBroadcast;
    
    public function __construct()
    {
        parent::__construct();

        $this->shareVariables();

        $this->nonAjaxRequests();
        
        /** to use session in constructor must place inside middleware for laravel 5.3 and above */
        $this->middleware(function ($request, $next) 
        {
            $this->auth = Auth::user();

            $this->loginUserId = Auth::id();

            View::share('auth', $this->auth);

            View::share('loginUserId', $this->loginUserId);

            return $next($request);
        });
    }

    public function globalNotification()
    {
        return Cache::remember('global_notification', 120, function() 
        {
            $notificationUrl = config('url.updateUrl') . 'notification.txt';

            return $notificationUrl;

            if ( $this->networkStatus == 'on' )
            {
                if ( checkRemoteFile($notificationUrl, false) )
                {
                    try
                    {
                        $getData = file_get_contents($notificationUrl);
                        return isJson($getData) ? null : $getData;
                    }
                    catch(\Exception $e)
                    {
                        return null;
                    }
                }
            }

            return null;
        });
    }  

    private function nonAjaxRequests()
    {
        $request = new Request;

        if ( ! $request->ajax() )
        {
            $this->writeLatestRequest();
        }
    }

    private function shareVariables()
    {
        View::share('globalNotification', $this->globalNotification());
        View::share('messageBroadcast', $this->messageBroadcast);
    }

    private function writeLatestRequest()
    {
        try
        {
            $file = fopen(storage_path('tool/latest-request.txt'),"w");
            fwrite($file, Carbon::now());
            fclose($file);
        }
        catch (\Exception $e)
        {
            // do nothing here
        }
    }
}