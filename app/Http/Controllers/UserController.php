<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Activity;
use Validator;
use App\Models\Role;
use App\Utils\ApiUtils;
use Illuminate\Support\Facades\Hash;
use DB;

class UserController extends MainController
{
    public function __construct() 
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $title = 'Daftar Operator';
        $page = 'lists-operator';

        $data = $request->all();

        $users = new User;

        if ( array_has($data, 'location_id') )
        {
            $users = $users->where('location_id', $data['location_id']);
        }

        $users = $users->orderBy('name', 'asc')->with('roles')->paginate(20);

        return view('user.index', compact('title', 'page', 'users'));
    }

    public function create()
    {
        $title = 'Buat Pengguna Baru';
        $page = 'new-user';

        return view('user.create', compact('title', 'page'));
    }

    public function edit($id = null)
    {
        $title = 'Profile';
        $page = 'profile';

        $userId = $this->loginUserId;

        if ( ! empty($id) )
        {
            // if ( ! $this->auth->hasRole('administrator') )
            // {
            //     return redirect()->route('user_profile')->with('status', status('info', 'Anda tidak memiliki hak akses untuk mengakses halaman tersebut'));
            // }

            try
            {
                $userId = decrypt($id);
            }
            catch ( \Exception $e )
            {
                return abort(404);
            }
        }

        $user = User::with('roles')->findOrFail($userId);

        return view('user.edit', compact('title', 'page', 'user'));
    }

    public function createSubmit(Request $request)
    {
        $data = $request->only('name', 'email', 'phone_mobile', 'password', 'rpassword', 'activated');

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:user|max:255',
            'phone_mobile' => 'required|numeric|unique:user',
            'password' => 'required|max:255|min:6',
            'rpassword' => 'required|same:password|max:255'
        ];

        try
        {
            $validator = Validator::make($data, $rules, config('formerrormessages'));

            if ($validator->fails()) 
            {
                return redirect()->route('user_create')->withErrors($validator, 'register')->withInput();
            }

            $data['password'] = bcrypt($data['password']);

            if ( ! empty($data['activated']) )
            {
                $data['activated'] = 1;
            }
            else
            {
                $data['activated'] = 0;
            }

            $userHiValue = userIncrementNumber($this->option->get('user_hi_value'));

            $data['user_id'] = config('ak23.access') == 'desktop' ? ApiUtils::getHardwareHashId() . '-' . $userHiValue : 'web' . '-' . $userHiValue;

            $data['created_by'] = $this->loginUserId;

            DB::transaction(function() use ($userHiValue, $data)
            {
                User::create($data)->attachRole(2);
                $this->option->update('user_hi_value', $userHiValue);
            });
            
            return redirect()->route('user_create')->with('status', ['type' => 'success', 'message' => 'Operator baru berhasil dibuat']);
        }
        catch (\Exception $e)
        {
            record_error('Error in create user', $e, $request);
            return redirect()->route('user_create')->with('status', ['type' => 'error', 'message' => 'Gagal membuat pengguna baru.'])->withInput();
        }
    }

    public function editSubmit(Request $request, $id = null)
    {
        $data = $request->only('name', 'email', 'phone_mobile', 'old_password', 'password', 'rpassword', 'activated');

        $emailUnique = $phoneMobileUnique = '';

        $idRequest = $id;

        $id = ! empty($id) ? decrypt($id) : $this->loginUserId;

        $user = User::findOrFail($id);

        $role = 2; // default to operator

        $activated = 0;

        if ($data['email'] != $user->email)
        {
            $emailUnique = '|unique:user';
        }

        if ($data['phone_mobile'] != $user->phone_mobile)
        {
            $phoneMobileUnique = '|unique:user';
        }

        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|max:255'.$emailUnique,
            'phone_mobile' => 'required|numeric'.$phoneMobileUnique
        ];

        // if ( $this->auth->hasRole('administrator') )
        // {
        //     $rules['role'] = 'required';
        // }

        if ( ! empty($data['old_password']) )
        {
            $rules['old_password'] = 'required|max:255';
            $rules['password'] = 'required|max:255|min:6';
            $rules['rpassword'] = 'required|same:password|max:255';

            $password = $data['password'];

            if ( ! Hash::check($data['old_password'], $user->password)) 
            {
                return redirect()->route('user_profile', [encrypt($id)])->with('status', status('error', 'Gagal mengubah data. Password lama yang anda masukkan salah.'))->withInput();
            }
        }

        try
        {
            $validator = Validator::make($data, $rules, config('formerrormessages'));

            if ($validator->fails()) 
            {
                return redirect()->route('user_profile', [encrypt($id)])->withErrors($validator, 'edit')->withInput()->with('status', status('error', 'Gagal mengubah data.'));
            }

            // if ( ! empty($data['role']) )
            // {
            //     $role = $data['role'];
            //     $user->roles()->sync($role);
            // }

            if ( ! empty($data['activated']) )
            {
                $activated = $data['activated'];
            }

            array_forget($data, ['_token', 'rpassword', 'role', 'old_password', 'password', 'activated']);

            if ( isset($password) )
            {
                $data['password'] = bcrypt($password);
            }

            // if ( $this->auth->hasRole('administrator') )
            // {
            //     $data['activated'] = $activated;
            // }

            foreach ( $data as $column => $value )
            {
                $user->$column = $value;
            }

            $user->save();

            if ( array_key_exists('password', $data) )
            {
                return redirect()->route('user_logout');
            }

            $redirect = ! empty($idRequest) ? redirect()->route('user_profile', [encrypt($id)]) : redirect()->route('user_profile');
            
            return $redirect->with('status', ['type' => 'success', 'message' => 'Data berhasil diubah']);
        }
        catch (\Exception $e)
        {
            dd($e);
            record_error('Error in edit user', $e, $request);
            return redirect()->route('user_profile', [encrypt($id)])->with('status', status('error', 'Gagal mengubah data.'))->withInput();
        }
    }

    public function remove(Request $request, $id = null)
    {
        if ( ! empty($id) )
        {
            try
            {
                $id = decrypt($id);
            }
            catch ( \Exception $e )
            {
                record_error('Error decrypt ID', $e, $request);
                return abort(500, '[btn_back]Gagal menghapus data.');
            }

            $user = User::findOrFail($id);

            if ( ! $user->activated && $user->demographics->count() < 1 && $this->auth->hasRole('administrator') )
            {
                try
                {
                    $message = $user->delete() ? status('success', 'Data berhasil dihapus') : status('error', 'Data gagal dihapus');
                    $this->activity->log('user-remove', 'Menghapus data pengguna ' . $id, $this->loginUserId);
                    return redirect()->back()->with('status', $message);
                }
                catch ( \Exception $e )
                {
                    record_error('Error in delete user', $e, $request);

                    if ( strpos($e->getMessage(), 'Cannot delete or update a parent row') !== false )
                    {
                        return abort(500, '[btn_back]Tidak diijinkan menghapus data dikarenakan data memiliki relasi.');
                    }

                    return abort(500, '[btn_back]Gagal menghapus data.');
                }
            }

            return abort(500, '[btn_back]Tidak diijinkan melakukan hal ini');
        }

        return abort(404);
    }
}
