<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Utils\ReportUtils;

use App\Models\People;
use App\Models\AK24_Print;
use App\Services\Api\JavaServerService;
use Crypt, Log, Excel, DB, View;
use Carbon\Carbon;
use App\Services\LocationService;

class ReportController extends MainController
{
    public $request;

    public $people;

    protected $ak24;

    private $javaServerService;

    private $locationService;

    public function __construct(Request $request, JavaServerService $javaServerService, LocationService $locationService)
    {
        parent::__construct();

        $this->request = $request;

        $this->javaServerService = $javaServerService;

        $this->locationService = $locationService;

        $this->people = new People;

        $this->ak24 = new AK24_Print;
    }

    public function index()
    {
        $startDate = $this->request->get('startDate', date('Y-m-d'));
        $endDate = $this->request->get('endDate', date('Y-m-d'));
        $location = $this->request->get('location', $this->location);
        $workstations = [];
        $server = true;

        $location = $location == 'all' ? null : $location;

        if ( config('ak23.access') == 'desktop' ) // Desktop version
        {   
            if ( $this->networkStatus == 'off' )
            {
                /**
                 * Connect to local database
                 */
                $data = ReportUtils::generate($startDate, $endDate);
                
                $server = false;
            }   
            else
            {     
                if ( $location == 'all' )
                {
                    $location = null;
                }

                /**
                 * Connect to database server
                 */
                $data = $this->javaServerService->report($startDate, $endDate, $location);  

                $workstations = $this->locationService->getAll();
            }
        }
        else // Web version
        {
            try
            {
                $data = $this->javaServerService->report($startDate, $endDate, $location);  
            }
            catch(\Exception $e)
            {
                record_error('Error request server API report', $e, $this->request);

                return abort(500, 'Gagal meminta data laporan');
            }

            $workstations = $this->locationService->getAll();
        }

        $rangeDate = encrypt( json_encode(['startDate' => $startDate, 'endDate' => $endDate, 'location' => $location]) );

        return view('report.index', compact('startDate', 'endDate', 'data', 'rangeDate', 'workstations', 'location', 'server'));
    }

    public function export_csv(Request $request, $dateRange)
    {
        $data_export = [];

        $drive = driveFind();

        if ( ! $drive )
        {
            return redirect()->back()->with('status', ['type' => 'error', 'message' => 'USB Drive tidak ditemukan']);
        }

        try
        {
            $dateRange =  Crypt::decrypt($dateRange);
            $rangeArray = json_decode($dateRange, true);
        }
        catch(\Exception $e)
        {
            record_error('Error decrypt date range.', $e, $request);
            return abort(404);
        }

        try 
        {
            if ( $this->networkStatus == 'off' )
            {
                throw new \Exception("Error connection to datacenter.");
            }

            /**
             * Connect to database server
             */
            $data = $this->javaServerService->report($rangeArray['startDate'],  $rangeArray['endDate'], $rangeArray['location']);            
        }
        catch (\Exception $e)
        {
            
            $data = ReportUtils::generate($rangeArray['startDate'], $rangeArray['endDate']);
        }

        foreach ($data['days'] as $key => $value) 
        {
            $data_export[] = [
                'Tanggal' => $key,
                'Kriminal' => $value['criminal_day'],
                'Non Kriminal' => $value['non_criminal_day'],
                'AK-24' => $value['ak24_day']
            ];
        }

        Excel::create('report.'.time(), function($excel) use($data_export) {
            $excel->setTitle('Inafis AK-23');
            $excel->setCreator('Inafis')->setCompany('Inafis');
            $excel->setDescription('Export CSV');
            $excel->sheet('Report', function($sheet) use($data_export) {
                $sheet->fromArray($data_export, null, 'A1', true, true);
            });
        })->store('csv', $drive);

        return redirect()->back()->with('status', ['type' => 'info', 'message' => 'Report berhasil di export']);
    }

    public function search()
    {
        return view('report/search');
    }

    public function submit()
    {
        $data = $this->request->except('_token');

        if ( ! session()->has('report_params') )
        {
            session(['report_params' => ['startDate' => $data['startDate'], 'endDate' => $data['endDate']]]);
        }
        else
        {
            session()->put('report_params.startDate', $data['startDate']);
            session()->put('report_params.endDate', $data['endDate']);
        }

        if ( empty($data['location']) )
        {
            unset($data['location']);
        }

        return redirect()->route('report', $data);
    }
}