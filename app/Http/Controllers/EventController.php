<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB, Cache;
use App\Services\EventService;
use App\Http\Requests\EventRequest;

class EventController extends MainController
{
    protected $eventService;

    public function __construct(EventService $eventService)
    {
        parent::__construct();
        $this->eventService = $eventService;
    }

    public function index()
    {
        $title = 'Pilih Event';
        $page = 'event';

        $eventsAutocompletion = $this->eventService->autoCompletion();

        return view('event', compact('title', 'page', 'eventsAutocompletion'));
    }

    public function submit(EventRequest $request)
    {
        $name = $request->get('name');
        $data = $this->eventService->getAll();

        if ( in_array($name, $data) )
        {
            $eventData = ['id' => array_search($name, $data), 'name' => $name];
        }
        else
        {
            $created = $this->eventService->create($name);
            $eventData = ['id' => $created->event_id, 'name' => $created->name];
        }

        session(['event_data' => $eventData]);
        return redirect()->route('demographic_create', ['action' => 'new'])->with('status', ['type' => 'info', 'message' => 'Registrasi pada acara '. $name]);
    }
}
