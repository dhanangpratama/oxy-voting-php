<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PDFService;

class BarcodeController extends Controller
{
    public function generateCard()
    {
        PDFService::generateIdCard(['name' => 'Maesa Randi', 'nik' => '3575023003880003']);

        return 'Done';
    }
}
