<?php

namespace App\Http\Controllers\Tools;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Artisan;

class UpdateController extends Controller
{	
	public function __construct()
	{
		parent::__construct();

        Artisan::call('view:clear');
	}

    public function app(Request $request)
    {
        $do = $request->get('do');

    	$serverVersion = $this->tool->getServerVersion();
    	$localVersion = $this->tool->getLocalVersion();

        $serverChangesLog = $this->tool->getUpdateChangesLog();
        $localChangesLog = $this->tool->getLocalChangesLog();

    	$updateAvailable = false;

    	if ( ! is_null($serverVersion) && ! is_null($localVersion) && ! empty($serverVersion['checker']) && ((int)$serverVersion['checker'] > (int)$localVersion['checker']) )
    	{
    		$updateAvailable = true;

            if ( $do === 'yes' )
            {
                return $this->doAppUpdate();
            }
    	}

    	return view('tools.update', compact('updateAvailable', 'serverChangesLog', 'localChangesLog', 'serverVersion'));
    }

    protected function doAppUpdate()
    {
        $process = $this->tool->updateSoftware();

        if ( $process['status'] )
        {
            return redirect('tool/update/software')->with('update_status', ['type' => 'success', 'message' => $process['message']]);
        }

        return redirect('tool/update/software')->with('update_status', ['type' => 'danger', 'message' => $process['message']]);
    }

    public function downloadUpdate()
    {
        try
        {
            if ( file_exists($this->tool->localUpdateFile()) )
            {
                unlink($this->tool->localUpdateFile());
            }

            if ( checkRemoteFile($this->tool->updateUrlFile(), false) )
            {
                getFile($this->tool->updateUrlFile(), $this->tool->localUpdateFile(), 10800);
            }
            else
            {
                return view('errors.custom', ['data' => ['title' => "Ma'af terjadi kesalahan.", 'description' => 'File update tidak ditemukan.']]);
            }
        }
        catch (\Exception $e)
        {
            Log::error('Error manual download software.', ['message' => $e->getMessage()]);

            return view('errors.custom', ['data' => ['title' => "Ma'af terjadi kesalahan.", 'description' => 'Pesan error: ' . $e->getMessage()]]);
        }

        if ( file_exists($this->tool->localUpdateFile()) )
        {
            return response()->download($this->tool->localUpdateFile());
        }
        else
        {
            return abort(404);
        }
    }

    public function checkEnv()
    {
        return $this->tool->checkEnv();
    }
}