<?php

namespace App\Http\Controllers\Tools;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class ToolController extends Controller
{	
	public function __construct()
	{
		parent::__construct();
	}

    public function viewSession()
    {
        dd(session()->all());
    }
}