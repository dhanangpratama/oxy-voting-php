<?php

namespace App\Http\Controllers\Tools;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\MainController;

class SettingController extends MainController
{	
	public function __construct()
	{
		parent::__construct();
	}

    public function index()
    {
        if ( ! $this->auth->hasRole('administrator') )
        {
            return redirect()->route('demographic_card_scan_record');
        }

        $title = 'Halaman Pengaturan';
        $page = 'setting';

        return view('tools.setting', compact('title', 'page'));
    }

    public function save(Request $request)
    {
        if ( ! $this->auth->hasRole('administrator') )
        {
            return redirect()->route('demographic_card_scan_record');
        }

        return 'Do setting save';
    }

    public function keepLive()
    {
        return ['status' => 'OK'];
    }
}