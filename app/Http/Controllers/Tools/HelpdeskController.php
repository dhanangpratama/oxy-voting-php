<?php

namespace App\Http\Controllers\Tools;

use Illuminate\Http\Request;
use App\Http\Controllers\MainController;

class HelpdeskController extends MainController
{
    public function __contruct() {}

    public function index()
    {
        $title = 'HelpDesk';
        $page = 'helpdesk';

        return view('tools.helpdesk', compact('title', 'page'));
    }
}
