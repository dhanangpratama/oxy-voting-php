<?php

namespace App\Http\Controllers\Tools;

use Illuminate\Http\Request;
use App\Http\Controllers\MainController;

class DocController extends MainController
{
    public function __contruct() {}

    public function index()
    {
        $title = 'Dokumentasi';
        $page = 'documentation';

        return view('tools.doc', compact('title', 'page'));
    }
}
