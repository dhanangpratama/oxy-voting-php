<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Services\Api\JavaServerService;
use App\Utils\ApiUtils;

use App\Http\Requests;
use File;
use App\Services\DownloadService;

class TestController extends Controller
{
    private $javaServerService;

    private $download;

    public function __construct(JavaServerService $javaServerService, DownloadService $download)
    {
        $this->javaServerService = $javaServerService;

        $this->download = $download;
    }

    public function downloadZip($id = null)
    {
        $startTime = date('H:i:s');

        $src = config('url.demographicZip') . "/{$id}.zip";
        $dest = 'C:\Biover\download' . DIRECTORY_SEPARATOR . $id . '.zip';

        $process = $this->download->demographicZipFileServer($src, $dest);

        $finishTime = date('H:i:s');

        dd($src, $dest, $startTime, $finishTime, $process);
    }

    public function detail()
    {
        $data = [
            'nama_lengkap' => 'dhanang arfian pratama',
            'nama_lengkap_ayah' => '',
            'nama_lengkap_ibu' => '',
            'pekerjaan' => '',
            'tmpt_lahir' => '',
            'tgl_lahir' => '',
            'alamat' => '' 
        ];

        try
        {
            return $this->javaServerService->OSInafisKtpSearch(['json' => json_encode($data)]);
        }
        catch ( \Exception $e)
        {
            dd($e);
        }
    }

    public function nik($nik)
    {
        return $this->javaServerService->OSInafisKtpSearchByNIK($nik);
    }

    public function OSInafisSearch()
    {
        $start = date('H:i:s');
        $download = download_file('http://www.dhanangpratama.com/download/file.zip', 'C:\aa\down\file.zip');

        if ( $download !== false )
        {
            $zip = new \ZipArchive;
            $res = $zip->open('C:\aa\down\file.zip');
            if ($res === TRUE) {
            $zip->extractTo('C:\aa\down\files');
            $zip->close();
            } else {
            echo 'doh!';
            }

            File::delete('C:\aa\down\file.zip');

            $end = date('H:i:s');

            dd('satu', $start, $end);
        }
        else
        {
            return 'Gagal';
        }

        // $data = [
        //     "nama_lengkap_ayah" => "",
        //     "nama_lengkap_ibu" => "",
        //     "tmpt_lahir" => "",
        //     "nama_lengkap" => "test",
        //     "source" => "DERM", // DERM, INP, AK-23 2.0, AK-23 1.0
        //     "tgl_lahir" => "",
        //     "alamat" => ""
        // ];

        // dd($this->javaServerService->OSInafisSearch($data, 20, 0));
    }

    public function OSInafisSearchDirect()
    {
        $data = [
            "nama_lengkap_ayah" => "",
            "nama_lengkap_ibu" => "",
            "tmpt_lahir" => "",
            "nama_lengkap" => "dhanang",
            "source" => "EKTP", // DERM, INP, AK-23 2.0, AK-23 1.0
            "tgl_lahir" => "",
            "alamat" => ""
        ];

        $response = ApiUtils::OSInafisSearchDirect($data);

        dd(json_decode($response->getBody(), true));
    }

    public function OSInafisSearchDetailByExternalId()
    {
        $data = [
            // 'externalId' => 'AK23_23da2386205377a7edefc9c79f78843c-109',
            'externalId' => 'AK23_81dc9bdb52d04dc20036dbd8313ed055-19' 
            // 'externalId' => 'EKTP_3511115902960001'           
        ];
        
        $response = $this->javaServerService->OSInafisSearchDetailByExternalId($data);
        // $response = ApiUtils::searchDetailByExternalIdDirect($data);

        dd($response);
    }

    public function OSInafisGetImages()
    {
        $data = [
            'external_id' => 'EKTP_3276022912850005',
            // 'external_id' => 'AK23_23da2386205377a7edefc9c79f78843c-109',
            'source' => 'EKTP'
        ];

        $response = $this->javaServerService->OSInafisGetImages($data);

        dd($response);
    }

    public function OSInafisGetImagesDirect()
    {
        $data = [
            'external_id' => 'AK23_218',
            'source' => 'AK23'
        ];

        $response = ApiUtils::OSInafisGetImagesDirect($data);

        dd(json_decode($response->getBody(), true));
    }

    public function search($nik)
    {
        dd($this->javaServerService->search(['subjectId' => '23da2386205377a7edefc9c79f78843c-77']));
    }

    public function OSInafisLatenSearch()
    {
        $data = [[
            'name' => 'fingerprint_0',
            'contents' => fopen('C:\Biover\files\right_thumb.jpg', 'r')
        ]];

        dd($this->javaServerService->OSInafisLatenSearch($data));
    }

    public function OSInafisLatenSearchDirect()
    {
        $data = [[
            'name' => 'fingerprint_0',
            'contents' => fopen('C:\Biover\files\live-sample\right_thumb.jpg', 'r')
        ]];
        
        dd(ApiUtils::OSInafisLatenSearchDirect($data));
    }

    public function reportServer(Request $request)
    {
        dd($this->javaServerService->report($request->get('startDate', date('Y-m-d')), $request->get('endDate', date('Y-m-d')), $request->get('location')));
    }
}