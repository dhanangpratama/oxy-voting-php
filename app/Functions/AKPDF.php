<?php

namespace App\Functions;

use TCPDF;

class AKPDF extends TCPDF 
{
    //Page header
    public function Header() 
    {
        $img_file = config('path.data.tmp') . DIRECTORY_SEPARATOR . 'watermark.jpg';

        if ( file_exists($img_file) )
        {
            // get the current page break margin
            $bMargin = $this->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $this->AutoPageBreak;
            // disable auto-page-break
            $this->SetAutoPageBreak(false, 0);
            // set bacground image
            
            $this->Image($img_file, 0, 0, 200, 200, '', '', '', false, 300, '', false, false, 0);
            // restore auto-page-break status
            $this->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $this->setPageMark();
        }
    }
}
