<?php

use App\Utils\ToolUtils;

/**
 * Set active page
 *
 * @param string $uri
 * @return string
 */
function set_active($uri)
{
    return Request::is($uri) ? 'active' : '';
}

function replace_slug($str)
{
	return str_replace('-', ' ', $str);
}

function get_current_url()
{
	$protocol = (isset($_SERVER['HTTPS'])) ? 'https' : 'http';
	return $protocol.'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
}

function format_date($date)
{
	return ! empty($date) ? date('d-m-Y', strtotime($date)) : '-';
}

function format_date_null($date)
{
	return ! empty($date) ? date('d-m-Y', strtotime($date)) : null;
}

function db_format_date($date)
{
	return date('Y-m-d', strtotime($date));
}

function img($path = "")
{
	if (!empty($path))
	{
		try 
        {
        	$img = Image::make($path);
			return $img;
		} 
        catch(\Exception $e) 
        {
        	error_log($e->getMessage(), 0);
        	return false;
		}
	}

	return null;
}

function data_file($file) 
{
	$exp_name = explode('_', $file);
	return  $exp_name[0];
}

function roll_fingers_name()
{
	return Finger::roll_name();
}

function flat_fingers_name()
{
	return Finger::flat_name();
}

function is_image($file = '')
{
	$status = false;
	$get_mime = mime_content_type($file);

	if ( $get_mime == 'image/jpeg' && !empty($file) )
	{
		$status = true;
	}

	return $status;
}

function card_data($filename, $ext = 'jpg')
{
	return Card::data($filename, $ext);
}

function db_source($source)
{
	switch ($source) {
		case 'INP':
			return 'Inafis Portable';
			break;

		case 'DERM':
			return 'Dermalog';
			break;
		
		default:
			return $source;
			break;
	}
}

function data_url($content) 
{
	return asset("files/{$content}");
}

function finger_search_done($type, $data)
{
	return is_array($data) && array_key_exists($type, $data) ? 'done' : '';
}

function henry_checked($type, $henry, $data)
{
	return array_has($data, "{$type}.pattern_classification") && $data[$type]['pattern_classification'] == $henry ? 'checked' : '';
}

function displayRoles($roles)
{
	$dataRoles = [];
	foreach ( $roles as $role ) $dataRoles[] = $role->display_name;
	return implode(', ', $dataRoles);
}

function facePositionName($key)
{
	$map = [
		'face-front' => 'Foto Wajah Depan',
		'face-right' => 'Foto Wajah Samping Kanan',
		'face-left' => 'Foto Wajah Samping Kiri'
	];

	return array_key_exists($key, $map) ? $map[$key] : '-';
}

/**
 * Convert image to base64 encode
 * @param  string $image
 * @return string       
 */
function image_base64($image = '', $mime = TRUE)
{
	/*if ( empty($image) && ! checkRemoteFile($image) )
	{
		return;
	}*/

	/**
	 * If anything is fine
	 */
	try
	{
		// Read image path, convert to base64 encoding
		$imageData = base64_encode(file_get_contents($image));
	}
	catch (\Exception $e)
	{
		$imageData = base64_encode(file_get_contents(public_path().'/assets/images/blank-finger.png'));
	}

	if ($mime)
		return 'data: '.img($image)->get_original_info()['exif']['MimeType'].';base64,'.$imageData; // Format the image SRC:  data:{mime};base64,{data};

	return $imageData;
}

/**
 * Check URL remote
 * @param  string $url 
 * @return boolean     
 */
function checkRemoteFile($url, $exception = true)
{
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL,$url);
    // don't download content
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    if(curl_exec($ch)!==FALSE)
    {
        return true;
    }
    else
    {
    	if ( $exception )
    	{
        	throw new Exception('"'. $url . '" tidak dapat diakses!');
        }
        else
        {
        	return false;
        }
    }
}

function empty_filter($string, $output = '')
{
	return ! empty($string) ? $output : '-';
}

function check_empty($string, $output = '')
{
	$output = ! empty($output) ? $output : '-';
	return ! empty($string) ? $string : $output;
}

function is_json($string){
	return is_string($string) && is_array(json_decode($string, true)) ? true : false;
 }

function get_data_from_file($path)
{
	return ! file_exists($path) ? null : json_decode(file_get_contents($path), true);
}

function ak23_get_contents($data)
{
	$arrContextOptions=array(
		"ssl"=>array(
			"verify_peer"=>false,
			"verify_peer_name"=>false,
		),
	); 
	
	return file_get_contents($data, false, stream_context_create($arrContextOptions));
}

function download_file($src, $dest)
{
	if ( !checkRemoteFile($src, false) )
	{
		Log::error('Cannot access given URL.', ['url' => $src]);
		throw new \Exception('File foto demographic tidak bisa diakses.');
	}

	return file_put_contents($dest, fopen($src, 'r'));
}

function status($type, $message)
{
	return ['type' => $type, 'message' => $message];
}

function phone_format($string)
{
	return trim(chunk_split($string, 4, '-'), '-');
}

function sex_label($string)
{
	return $string != 'm' ? 'Perempuan' : 'Laki-laki';
}

function yesNo($string)
{
	return $string != 0 ? 'Ya' : 'Tidak';
}

function image_quality_class($val)
{
	if ( $val >= 80 )
	{
		return 'excellent';
	}
	else if ( $val >= 60 && $val < 80)
	{
		return 'very-good';
	}
	else if ( $val >= 40 && $val < 60)
	{
		return 'good';
	}
	else if ( $val >= 20 && $val < 40)
	{
		return 'fair';
	}
	else if ( $val >= 0 && $val < 20)
	{
		return 'poor';
	}
	else
	{
		return 'unknown';
	}
}

function sess_operator_id()
{
	return ( session()->has('operator') ) ? session('operator') : null;
}

// function changeDPI($path, $dpi = 500, $dest = null)
// {
//     $image = file_get_contents($path);
    
//     $image = substr_replace($image, pack("cnn", 1, $dpi, $dpi), 13, 5);

//     if ( is_null($dest) )
//     {
//         $dest = $path;
//     }
    
//     file_put_contents($dest, $image);
// }

function format_text($text)
{
	return ucwords(strtolower($text));
}

function ktp_el_data($key)
{
	if ( session()->has('ktp_el_data') )
	{
		$data = session('ktp_el_data');

		if ( ! isset($data['data'][$key]) )
		{
			return null;
		}

		return $data['data'][$key];
	}

	return null;
}

function record_error($text, $exception, $request = null)
{
	$details = [
        'exception_message' => $exception->getMessage(),
        'exception_file' => $exception->getFile(),
        'exception_code' => $exception->getCode(),
        'exception_line' => $exception->getLine(),
        'request_url' => url()->full(),
        'request_function' => Route::currentRouteAction()
	];
	
	if ( ! is_null($request) )
	{
		$details['request_path'] = $request->path();
	}

	Log::error($text, $details);
}

function getServerStatus($ip, $port)
{
	$fp = @fsockopen($ip, $port, $errno, $errstr, 2);

	if (!$fp) 
	{
	    return false;
	}
	else 
	{ 
		return true;
	}
}

function getFile($src, $dest, $timeout = 3600)
{
	set_time_limit(0);

    //This is the file where we save the    information
    $fp = fopen ($dest, 'w+');

    //Here is the file we are downloading, replace spaces with %20
    $ch = curl_init(str_replace(" ", "%20", $src));
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);

    // write curl response to file
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    // get curl response
    curl_exec($ch); 
    curl_close($ch);

    fclose($fp);
}

function drmSex($sexid = null)
{
	if ( empty($sexid) || ! is_numeric($sexid) )
	{
		return '-';
	}

	return $sexid == 57 ? 'Laki-laki' : 'Perempuan';
}

function array_search_inner ($array, $attr, $val, $strict = FALSE) {
  // Error is input array is not an array
  if (!is_array($array)) return FALSE;
  // Loop the array
  foreach ($array as $key => $inner) {
    // Error if inner item is not an array (you may want to remove this line)
    if (!is_array($inner)) return FALSE;
    // Skip entries where search key is not present
    if (!isset($inner[$attr])) continue;
    if ($strict) {
      // Strict typing
      if ($inner[$attr] === $val) return $key;
    } else {
      // Loose typing
      if ($inner[$attr] == $val) return $key;
    }
  }
  // We didn't find it
  return NULL;
}

function userIncrementNumber($val)
{
	return (int) $val + 1;
}

function demographic_fields()
{
	$data = [];

	foreach (array_diff(\Schema::getColumnListing('demographics'), config('ak23.hidden_demographic_fields')) as $column)
	{
		$data[$column] = '';
	}

	return $data;
}

function create_demographic_temp_data($data = [])
{
	if ( ! empty($data) )
	{
		$data = array_merge(demographic_fields(), $data);
		File::put(config('path.data.tmp') . DIRECTORY_SEPARATOR . '_demographic_data.json', json_encode($data));
	}
}

function createFlagFile($file)
{
	if ( ! file_exists($file) && session('process.mode') != 'edit' )
	{
		File::put($file, '');
	}
}

function flag($data = null, $forcedWrite = false, $overridePath = null)
{
	$pathFile = ! is_null($overridePath) ? $overridePath : session('process.basePath');
	$flagFile = $pathFile . DIRECTORY_SEPARATOR . '_flag.txt';

	createFlagFile($flagFile);

	if ( file_exists($flagFile) && ! empty($data) )
	{
		try
		{
			if ( ! flagCheck($data) || $forcedWrite )
			{
				$fp = fopen($flagFile, 'a');
				fwrite($fp, $data . PHP_EOL);
				fclose($fp);
			}
		}
		catch (\Exception $e)
		{
			// do nothing
		}
	}
}

function flagCheck($data = null, $overridePath = null)
{
	$pathFile = ! is_null($overridePath) ? $overridePath : session('process.basePath');
	$flagFile = $pathFile . DIRECTORY_SEPARATOR . '_flag.txt';

	createFlagFile($flagFile);

	if ( file_exists($flagFile) && ! empty($data) )
	{
		try
		{
			if (strpos(file_get_contents($flagFile), $data) !== false) 
			{
				return true;
			}
		}
		catch (\Exception $e)
		{
			// do nothing
		}

		return false;
	}

	return false;
}

function flagDelete($data = null, $overridePath = null)
{
	$pathFile = ! is_null($overridePath) ? $overridePath : session('process.basePath');
	$flagFile = $pathFile . DIRECTORY_SEPARATOR . '_flag.txt';

	if ( file_exists($flagFile) && ! empty($data) )
	{
		$file = file_get_contents($flagFile);
		
		try
		{
			if (strpos($file, $data) !== false) 
			{
				$file = str_replace($data, '', $file);
				file_put_contents($flagFile, $file);
			}
		}
		catch (\Exception $e)
		{
			// do nothing
		}
	}
}

function driveFind()
{
	$letters = range(config('ak23.drive_letter_start'), config('ak23.drive_letter_end'));

	foreach ($letters as $letter) 
	{
		$drive = $letter.':\\';
		
		try
		{
			if( is_dir($drive) ) 
			{
				scandir($drive);

				return $drive;
			}
		}
		catch (\Exception $e)
		{
			Log::error('Error in read drives letter', ['message' => $e->getMessage()]);
			continue;
		}
	}
	
	return false;
}

function driveRead($path, $arrayList)
{
	$limitFileSize = 5 * 1024 * 1024;	//	LIMIT FILE SIZE TO A MAXIMUM OF 5 MB
	
	$files = scandir($path);

	unset($files[array_search('.', $files, true)]);
	unset($files[array_search('..', $files, true)]);
	
	if (count($files) < 1)
		return;
	
	foreach($files as $file)
	{
		$pathFileName = $path.'/'.$file;
		
		// if(is_dir($pathFileName))
		// {
		// 	$arrayList = driveRead($path.'/'.$file, $arrayList);
		// }
		// else
		// {
			$ext = pathinfo($file, PATHINFO_EXTENSION);;
				
			if(in_array($ext, array('png', 'jpg', 'wsq')) && filesize($pathFileName) < $limitFileSize)
				$arrayList[] = array('name' => $file, 'ext' => $ext, 'full' => $pathFileName);
		// }
	}
	
	return $arrayList;
}

function removeDemographicSession()
{
	session()->forget([
		'process',
		'current_indicator',
		'verification_data',
		'result_matching',
		'subject_id_to_update',
		'verification_key',
		'is_verified_local',
		'is_verified_server',
		'identification_source',
		'ktp_el_data'
	]);
}