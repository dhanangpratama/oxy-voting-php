<?php

return [
	'location' => 'Pusat',
	'card_name_folder' => 'card',
	'live_name_folder' => 'live',
	'complete_name_folder' => 'complete',
	'AppName' => 'Biover Biometric',
	'photo_source' => env('PHOTO_SOURCE', 'C:\AK23\photo'),
	'error_folder_working_view' => 'errors.folder_working',
];