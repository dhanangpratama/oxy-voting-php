<?php

$serverUrl = env('URL_SERVER', 'https://two.ak23inafis.com');
$osInafisServerUrl = env('OSINAFIS_URL_SERVER', 'http://202.52.13.116:8181/api/');
// $osInafisServerUrl = env('OSINAFIS_URL_SERVER', 'http://202.52.13.254:8181/api/');

return [
	'serverAddress' => $serverUrl,
	'tmp' => 'files/tmp',
	'phpApiServer' => env('URL_PHP_API_SERVER', $serverUrl.':55580/webapi/'),
	'javaApiClient' => env('URL_JAVA_API_CLIENT', 'http://localhost:7083/api/'),
	'javaApiServer' => env('URL_JAVA_API_SERVER', $serverUrl . ':8181/api/'),
	'latenApiServer' => env('URL_LATEN_API_SERVER', $serverUrl.':8000/'),
	'latenApiServerNew' => env('URL_LATEN_API_SERVER', $osInafisServerUrl),
	'updateUrl' => env('URL_UPDATE', $serverUrl.':55580/webapi/update/'),
	'demographic' => env('URL_DEMOGRAPHIC', $serverUrl . ':8181/assets/box/demographic/'), 
	'demographicZip' => env('URL_DEMOGRAPHIC_ZIP', $serverUrl . ':8181/assets/box/demographic-zip') 
];