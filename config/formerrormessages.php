<?php

return [
    'required' => 'Tidak boleh kosong',
    'email' => 'Format email salah',
    'max' => 'Melebihi batas maksimum karakter',
    'rpassword.same' => 'Harus sama dengan password',
    'email.unique' => 'Email sudah terdaftar',
    'phone_mobile.unique' => 'No. Handphone sudah terdaftar'
];