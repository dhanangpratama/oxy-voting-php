<?php

return [
	'frontName' => 'front.jpg',
	'backName' => 'back.jpg',
	'frontSmallName' => 'front-small.jpg',
	'backSmallName' => 'back-small.jpg',
	'face_front' => 'face-front.jpg',
	'face_left' => 'face-left.jpg',
	'face_right' => 'face-right.jpg',
	'flat_left_four_fingers' => 'flat_left_four_fingers.jpg',
	'flat_right_four_fingers' => 'flat_right_four_fingers.jpg',
	'flat_thumbs' => 'flat_thumbs.jpg',
    'card_folder' => 'card',
	'live_folder' => 'live',
	'complete_folder' => 'complete'
];