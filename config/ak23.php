<?php

$right_thumb = 'right_thumb.jpg';
$right_index = 'right_index_finger.jpg';
$right_middle = 'right_middle_finger.jpg';
$right_ring = 'right_ring_finger.jpg';
$right_little = 'right_little_finger.jpg';
$left_thumb = 'left_thumb.jpg';
$left_index = 'left_index_finger.jpg';
$left_middle = 'left_middle_finger.jpg';
$left_ring = 'left_ring_finger.jpg';
$left_little = 'left_little_finger.jpg';

return [
    'access' => env('APP_ACCESS', 'desktop'), // desktop, web

    'zip_server_password' => '1!2@3#4$5%',

    'ektp_search_to' => env('EKTP_SEARCH_TO', 'osinafis'), // ektp or osinafis

    /** Setting for finger crop brightness */
    'finger_brightness' => 0,

    /** Setting for finger crop contrast */
    'finger_contrast' => -10,

    'drive_letter_start' => env('DRIVE_LETTER_START', 'F'),

    'drive_letter_end' => env('DRIVE_LETTER_END', 'Z'),

    'image_ext' => 'jpg',

    'default_login_operator_username' => 'operator',

    'default_login_operator_password' => 'operator',

    'pusat_name' => env('NAMA_PUSAT', 'PUSAT'),

    'report_location_activated' => env('REPORT_LOCATION_ACTIVATED', false),

    'serverToken' => 'Bearer ee861a81-a852-4a68-83d6-6531655e3f50',

    'latenSearchTimeout' => 300000, // 5 minutes

    'nikSearchTimeout' => 300000, // 5 minutes

    'finger_index' =>  [
        'right_thumb',
	    'right_index_finger',
	    'right_middle_finger',
	    'right_ring_finger',
	    'right_little_finger',
		'left_thumb',
	    'left_index_finger',
	    'left_middle_finger',
	    'left_ring_finger',
	    'left_little_finger'
	],

    'file_data' => [
        'finger_classification' => '_finger_classification_data.json',
        'flat_finger' => '_flat_finger_data.json',
        'finger_data' => '_formula_data.json',
        'front_data' => '_front_data.json',
        'back_data' => '_back_data.json',
        'verification_data' => '_verification_data.json',
        'verification_source' => '_verification_source.txt',
        'demographic' => '_demographic_data.json',
        'biometrics_data' => '_biometrics_data.json',
        'flat_biometrics_data' => '_flat_biometrics_data.json',
        'field_ocr' => '_fields.json'
    ],

    'henry' => [
		'WHORL' => 'W',
		// 'RIGHT_SLANT_LOOP' => 'U', Ulnar
		// 'LEFT_SLANT_LOOP' => "U", Ulnar
		'PLAIN_ARCH' => 'A',
		'TENTED_ARCH' => 'A',
		'RADIAL_LOOP' => 'U',
		'ULNAR_LOOP' => 'U',
		// 'PLAIN_WHORL' => 'W', Whorl
		// 'CENTRAL_POCKET_LOOP' => 'W', Whorl
		// 'DOUBLE_LOOP' => 'W', Whorl
		// 'ACCIDENTAL_WHORL' => 'W', Whorl
		'UNKNOWN' => 'UN',
		'SCAR' => 'SC',
		'AMPUTATION' => 'W'
	],

    'fingermap_en' => [
        'right_thumb' => 'Jempol Kanan',
        'right_index' => 'Telunjuk Kanan',
        'right_middle' => 'Tengah Kanan',
        'right_ring' => 'Manis Kanan',
        'right_little' => 'Kelingking Kanan',
        'left_thumb' => 'Jempol Kiri',
        'left_index' => 'Telunjuk Kiri',
        'left_middle' => 'Tengah Kiri',
        'left_ring' => 'Manis Kiri',
        'left_little' => 'Kelingking Kiri'
    ],

    'fingermap_key' => [
        1 => 'right_thumb',
        'right_index',
        'right_middle',
        'right_ring',
        'right_little',
        'left_thumb',
        'left_index',
        'left_middle',
        'left_ring',
        'left_little'
    ],

    'os_inafis_finger_key' => [
		'right_thumb' => ['alt' => 'jempol_kanan', 'key' => 1],
	    'right_index' => ['alt' => 'telunjuk_kanan', 'key' => 2],
	    'right_middle' => ['alt' => 'tengah_kanan', 'key' => 3],
	    'right_ring' => ['alt' => 'manis_kanan', 'key' => 4],
	    'right_little' => ['alt' => 'kelingking_kanan', 'key' => 5],
		'left_thumb' => ['alt' => 'jempol_kiri', 'key' => 6],
	    'left_index' => ['alt' => 'telunjuk_kiri', 'key' => 7],
	    'left_middle' => ['alt' => 'tengah_kiri', 'key' => 8],
	    'left_ring' => ['alt' => 'manis_kiri', 'key' => 9],
	    'left_little' => ['alt' => 'kelingking_kiri', 'key' => 10]
	],

    'form_fields' => [
		'front' => [
			'status-id.jpg' => ['x1' => 349, 'y1' => 61, 'x2' => 1207, 'y2' => 178],
			'sex.jpg' => ['x1' => 573, 'y1' => 56, 'x2' => 1693, 'y2' => 310],
			'fullname.jpg' => ['x1' => 722, 'y1' => 77, 'x2' => 499, 'y2' => 421],
			'nickname.jpg' => ['x1' => 708, 'y1' => 98, 'x2' => 504, 'y2' => 492],
			'job.jpg' => ['x1' => 689, 'y1' => 88, 'x2' => 499, 'y2' => 575],
			'taken-by.jpg' => ['x1' => 443, 'y1' => 88, 'x2' => 521, 'y2' => 1743],
			'viewed-by.jpg' => ['x1' => 433, 'y1' => 74, 'x2' => 526, 'y2' => 1816],
			'taken-date.jpg' => ['x1' => 385, 'y1' => 59, 'x2' => 577, 'y2' => 1688],
			'location.jpg' => ['x1' => 362, 'y1' => 69, 'x2' => 143, 'y2' => 1684],
			'note.jpg' => ['x1' => 837, 'y1' => 153, 'x2' => 1415, 'y2' => 1740],
			'formula.jpg' => ['x1' => 797, 'y1' => 83, 'x2' => 1538, 'y2' => 417],
			'formula-view.jpg' => ['x1' => 767, 'y1' => 96, 'x2' => 1537, 'y2' => 532]
		],
		'back' => [
            'blood-type.jpg' => ['x1' => 709, 'y1' => 85, 'x2' => 716, 'y2' => 128],
			'dob.jpg' => ['x1' => 709, 'y1' => 85, 'x2' => 716, 'y2' => 128],
			'pob.jpg' => ['x1' => 712, 'y1' => 88, 'x2' => 716, 'y2' => 194],
			'nationality.jpg' => ['x1' => 703, 'y1' => 82, 'x2' => 716, 'y2' => 266],
            'race.jpg' => ['x1' => 703, 'y1' => 82, 'x2' => 716, 'y2' => 266],
			'religion-id.jpg' => ['x1' => 712, 'y1' => 85, 'x2' => 716, 'y2' => 332],
			'address.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
            'rt.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
            'rw.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
            'village.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
            'postal-code.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
            'province.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
            'regent.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
            'district.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
            'country.jpg' => ['x1' => 706, 'y1' => 163, 'x2' => 731, 'y2' => 404],
			'nik.jpg' => ['x1' => 703, 'y1' => 85, 'x2' => 719, 'y2' => 557],
            'passport-number.jpg' => ['x1' => 703, 'y1' => 85, 'x2' => 719, 'y2' => 557],
            'information.jpg' => ['x1' => 715, 'y1' => 79, 'x2' => 710, 'y2' => 923],
			'education-id.jpg' => ['x1' => 700, 'y1' => 76, 'x2' => 713, 'y2' => 629],
			'father-name.jpg' => ['x1' => 706, 'y1' => 73, 'x2' => 716, 'y2' => 695],
			'mother-name.jpg' => ['x1' => 721, 'y1' => 73, 'x2' => 707, 'y2' => 809],
			'mate-name.jpg' => ['x1' => 715, 'y1' => 79, 'x2' => 710, 'y2' => 923],
			'father-address.jpg' => ['x1' => 706, 'y1' => 67, 'x2' => 713, 'y2' => 752],
			'mother-address.jpg' => ['x1' => 712, 'y1' => 73, 'x2' => 710, 'y2' => 863],
			'mate-address.jpg' => ['x1' => 697, 'y1' => 94, 'x2' => 716, 'y2' => 983],
			'child-1.jpg' => ['x1' => 514, 'y1' => 94, 'x2' => 281, 'y2' => 1133],
			'child-2.jpg' => ['x1' => 640, 'y1' => 82, 'x2' => 785, 'y2' => 1136],
			'child-3.jpg' => ['x1' => 514, 'y1' => 103, 'x2' => 281, 'y2' => 1208],
			'child-4.jpg' => ['x1' => 643, 'y1' => 100, 'x2' => 776, 'y2' => 1205],
			'child-5.jpg' => ['x1' => 532, 'y1' => 97, 'x2' => 266, 'y2' => 1289],
			'child-6.jpg' => ['x1' => 658, 'y1' => 103, 'x2' => 767, 'y2' => 1286],
			'height.jpg' => ['x1' => 97, 'y1' => 61, 'x2' => 1838, 'y2' => 140],
			'weight.jpg' => ['x1' => 79, 'y1' => 49, 'x2' => 1835, 'y2' => 215],
			'skin-id.jpg' => ['x1' => 1839, 'y1' => 282, 'x2' => 2237, 'y2' => 361],
			'body-id.jpg' => ['x1' => 1839, 'y1' => 361, 'x2' => 2338, 'y2' => 431],
			'head-id.jpg' => ['x1' => 1839, 'y1' => 428, 'x2' => 2338, 'y2' => 511],
			'hair-color-id.jpg' => ['x1' => 1839, 'y1' => 506, 'x2' => 2338, 'y2' => 582],
			'hair-id.jpg' => ['x1' => 1839, 'y1' => 571, 'x2' => 2338, 'y2' => 659],
			'face-id.jpg' => ['x1' => 1839, 'y1' => 642, 'x2' => 2338, 'y2' => 735],
			'forehead-id.jpg' => ['x1' => 1839, 'y1' => 720, 'x2' => 2338, 'y2' => 806],
			'eye-color-id.jpg' => ['x1' => 1839, 'y1' => 794, 'x2' => 2338, 'y2' => 878],
			'eye-irregularity-id.jpg' => ['x1' => 1839, 'y1' => 880, 'x2' => 2338, 'y2' => 945],
			'nose-id.jpg' => ['x1' => 1839, 'y1' => 942, 'x2' => 2338, 'y2' => 1025],
			'lip-id.jpg' => ['x1' => 1839, 'y1' => 1010, 'x2' => 2338, 'y2' => 1096],
			'tooth-id.jpg' => ['x1' => 1839, 'y1' => 1089, 'x2' => 2228, 'y2' => 1169],
			'chin-id.jpg' => ['x1' => 1839, 'y1' => 1158, 'x2' => 2338, 'y2' => 1245],
			'ear-id.jpg' => ['x1' => 1839, 'y1' => 1238, 'x2' => 2338, 'y2' => 1321],
			'tattoo.jpg' => ['x1' => 481, 'y1' => 70, 'x2' => 1832, 'y2' => 1205],
			'scars-and-handicap.jpg' => ['x1' => 487, 'y1' => 76, 'x2' => 1838, 'y2' => 1268],
			'provision-code.jpg' => ['x1' => 487, 'y1' => 76, 'x2' => 1838, 'y2' => 1340]
		]
	],

    'finger_data' => [
        'right_thumb' => array(
            'title' => 'Jempol Kanan',
            'rolled_crop_area' => ['w' => 885, 'h' => 885, 'x' => 267, 'y' => 1375],
            'flat_crop_area' => ['w' => 700, 'h' => 427, 'x' => 582, 'y' => 776],
            'num' => 16,
            'roll_name' => $right_thumb,
            'flat_name' => 'flat_' . $right_thumb,
            'minutiae_name' => 'minutiae_' . $right_thumb
        ),
        'right_index_finger' => array(
            'title' => 'Telunjuk Kanan',
            'rolled_crop_area' => ['w' => 812, 'h' => 812, 'x' => 1177, 'y' => 1375],
            'flat_crop_area' => ['w' => 32, 'h' => 431, 'x' => 351, 'y' => 636],
            'num' => 16,
            'roll_name' => $right_index,
            'flat_name' => 'flat_' . $right_index,
            'minutiae_name' => 'minutiae_' . $right_index
        ),
        'right_middle_finger' => array(
            'title' => 'Jari Tengah Kanan',
            'rolled_crop_area' => ['w' => 808, 'h' => 808, 'x' => 2017, 'y' => 1375],
            'flat_crop_area' => ['w' => 341, 'h' => 137, 'x' => 385, 'y' => 601],
            'num' => 8,
            'roll_name' => $right_middle,
            'flat_name' => 'flat_' . $right_middle,
            'minutiae_name' => 'minutiae_' . $right_middle
        ),
        'right_ring_finger' => array(
            'title' => 'Jari Manis Kanan',
            'rolled_crop_area' => ['w' => 819, 'h' => 819, 'x' => 2846, 'y' => 1378],
            'flat_crop_area' => ['w' => 740, 'h' => 290, 'x' => 388, 'y' => 588],
            'num' => 8,
            'roll_name' => $right_ring,
            'flat_name' => 'flat_' . $right_ring,
            'minutiae_name' => 'minutiae_' . $right_ring
        ),
        'right_little_finger' => array(
            'title' => 'Kelingking Kanan',
            'rolled_crop_area' => ['w' => 842, 'h' => 842, 'x' => 3686, 'y' => 1378],
            'flat_crop_area' => ['w' => 1150, 'h' => 863, 'x' => 406, 'y' => 604],
            'num' => 4,
            'roll_name' => $right_little,
            'flat_name' => 'flat_' . $right_little,
            'minutiae_name' => 'minutiae_' . $right_little
        ),
        'left_thumb' => array(
            'title' => 'Jempol Kiri',
            'rolled_crop_area' => ['w' => 892, 'h' => 892, 'x' => 259, 'y' => 2288],
            'flat_crop_area' => ['w' => 78, 'h' => 387, 'x' => 602, 'y' => 0],
            'num' => 4,
            'roll_name' => $left_thumb,
            'flat_name' => 'flat_' . $left_thumb,
            'minutiae_name' => 'minutiae_' . $left_thumb
        ),
        'left_index_finger' => array(
            'title' => 'Telunjuk Kiri',
            'rolled_crop_area' => ['w' => 817, 'h' => 817, 'x' => 1177, 'y' => 2291],
            'flat_crop_area' => ['w' => 1250, 'h' => 599, 'x' => 381, 'y' => 0],
            'num' => 2,
            'roll_name' => $left_index,
            'flat_name' => 'flat_' . $left_index,
            'minutiae_name' => 'minutiae_' . $left_index
        ),
        'left_middle_finger' => array(
            'title' => 'Jari Tengah Kiri',
            'rolled_crop_area' => ['w' => 808, 'h' => 808, 'x' => 2014, 'y' => 2291],
            'flat_crop_area' => ['w' => 893, 'h' => 260, 'x' => 452, 'y' => 0],
            'num' => 2,
            'roll_name' => $left_middle,
            'flat_name' => 'flat_' . $left_middle,
            'minutiae_name' => 'minutiae_' . $left_middle
        ),
        'left_ring_finger' => array(
            'title' => 'Jari Manis Kiri',
            'rolled_crop_area' => ['w' => 818, 'h' => 818, 'x' => 2843, 'y' => 2291],
            'flat_crop_area' => ['w' => 461, 'h' => 329, 'x' => 495, 'y' => 0],
            'num' => 1,
            'roll_name' => $left_ring,
            'flat_name' => 'flat_' . $left_ring,
            'minutiae_name' => 'minutiae_' . $left_ring
        ),
        'left_little_finger' => array(
            'title' => 'Kelingking Kiri',
            'rolled_crop_area' => ['w' => 837, 'h' => 837, 'x' => 3687, 'y' => 2296],
            'flat_crop_area' => ['w' => 0, 'h' => 812, 'x' => 408, 'y' => 0],
            'num' => 1,
            'roll_name' => $left_little,
            'flat_name' => 'flat_' . $left_little,
            'minutiae_name' => 'minutiae_' . $left_little
        )
    ],

    'messageType' => [
        'info' => 'Info',
        'warning' => 'Warning'
    ],

    'dataRelation' => [
        'body' => [
            'label' => 'Bentuk Tubuh',
            'id' => 'body_id'
        ],
        'chin' => [
            'label' => 'Bentuk Dagu',
            'id' => 'chin_id'
        ],
        'ear' => [
            'label' => 'Bentuk Telinga',
            'id' => 'ear_id'
        ],
        'eye_color' => [
            'label' => 'Warna Mata',
            'id' => 'eye_color_id'
        ],
        'eye_irregularity' => [
            'label' => 'Kelainan Mata',
            'id' => 'eye_irregularity_id'
        ],
        'face' => [
            'label' => 'Bentuk Wajah',
            'id' => 'face_id'
        ],
        'forehead' => [
            'label' => 'Bentuk Dahi',
            'id' => 'forehead_id'
        ],
        'hair' => [
            'label' => 'Bentuk Rambut',
            'id' => 'hair_id'
        ],
        'hair_color' => [
            'label' => 'Warna Rambut',
            'id' => 'hair_color_id'
        ],
        'head' => [
            'label' => 'Bentuk Kepala',
            'id' => 'head_id'
        ],
        'lip' => [
            'label' => 'Bentuk Bibir',
            'id' => 'lip_id'
        ],
        'nose' => [
            'label' => 'Bentuk Hidung',
            'id' => 'nose_id'
        ],
        'skin' => [
            'label' => 'Warna Kulit',
            'id' => 'skin_id'
        ],
        'tooth' => [
            'label' => 'Bentuk Gigi',
            'id' => 'tooth_id'
        ],
        'education' => [
            'label' => 'Pendidikan',
            'id' => 'education_id'
        ],
        'religion' => [
            'label' => 'Agama',
            'id' => 'religion_id'
        ],
        'status' => [
            'label' => 'Status',
            'id' => 'status_id'
        ]
    ],

    'indicator' => [
        [
            'id' => 'demographic_create',
            'title' => 'Rekam Data',
            'route' => 'demographic_create'
        ],
        [
            'id' => 'demographic_photo',
            'title' => 'Rekam Foto',
            'route' => 'demographic_photo'
        ],
        [
            'id' => 'demographic_print',
            'title' => 'Cetak Kartu',
            'route' => 'demographic_print'
        ]
    ],
    'live_breadcrumbs' => [
        [
            'id' => 'record_finger',
            'title' => 'Rekam Jari',
            'route' => 'demographic_finger_scan_record'
        ],
        [
            'id' => 'identification',
            'title' => 'Identifikasi',
            'route' => 'demographic_identification'
        ],
        [
            'id' => 'formula',
            'title' => 'Perumusan',
            'route' => 'demographic_formula'
        ],
        [
            'id' => 'camera',
            'title' => 'Rekam Foto',
            'route' => 'demographic_camera'
        ],
        [
            'id' => 'data',
            'title' => 'Rekam Data',
            'route' => 'demographic_front'
        ]
    ],
    'hidden_demographic_fields' => ['deleted_at', 'created_at', 'updated_at', 'date_uploaded', 'user_id', 'is_verified_local', 'is_verified_server', 'sync_status', 'finger_image_data', 'fingers', 'demographic_id', 'enrollment_type'],
    'whorl_map' => ['PLAIN_WHORL', 'CENTRAL_POCKET_LOOP', 'DOUBLE_LOOP', 'ACCIDENTAL_WHORL'],
    'ulnar_map' => ['RIGHT_SLANT_LOOP', 'LEFT_SLANT_LOOP']
];