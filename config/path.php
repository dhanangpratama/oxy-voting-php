<?php

$basePathData = env('PATH_BASE_DATA', 'D:\Voting');

return [
    'data' => [
        'base' => $basePathData,
        'demographic' => env('PATH_FILES_LIVE', $basePathData.'\files\demographic'),
        'edit' => env('PATH_FILES_EDIT', $basePathData.'\files\edit'),
        'complete' => env('PATH_FILES_COMPLETE', $basePathData.'\files\complete'),
        'complete_queue' => env('PATH_FILES_COMPLETE_QUEUE', $basePathData.'\files\complete_queue'),
        'tmp' => env('PATH_TMP', $basePathData.'\files\tmp')
    ],
    'latestRequestFile' => storage_path('tool/latest-request.txt'),
    'serverStatusFile' => storage_path('tool/network-status.txt'),
    'tesseractExecPath' => env('PATH_TESSERACT_EXE', 'C:\Bin\ak23bin\tesseract\tesseract.exe'),
    'hardwareStatus' => env('PATH_HARDWARE_STATUS', 'E:\01-Test-Apps\HardwareDetector\HardwareStatus.txt')
];