var popupWindowHelpdesk = null,
    popupWindowDoc = null;

function updateCoords(c) {
    $('#x').val(c.x);
    $('#y').val(c.y);
    $('#w').val(c.w);
    $('#h').val(c.h);
};

function checkCoords() {
    if (parseInt($('#w').val())) return true;
    swal({
        text: 'Mohon untuk memilih bagian yang akan di potong',
        type: 'error',
        allowOutsideClick: false
    });
    return false;
};

function go(url, target) {
    if (target === undefined) {
        target = '_self';
    }

    window.open(url, target);
}

function reload() {
    App.blockUI();
    window.location.reload(false);
}

function checkConnection() {
    $.ajax({
        url: home_url + '/ajax/get-server-status',
        type: 'GET',
        dataType: 'json',
        cache: false,
        error: function(xhr, status, errorThrown) {
            $('#network_status, body').removeClass('online').removeClass('part').addClass('offline');
        },
        success: function(response, status, xhr) {
            if (response.status == 'off') {
                $('#network_status').removeClass('online').removeClass('part').addClass('offline').attr('data-original-title', 'Server Offline');
                $('body').removeClass('online').removeClass('part').addClass('offline');
            } else if (response.status == 'part') {
                $('#network_status').removeClass('online').removeClass('offline').addClass('part').attr('data-original-title', 'OS Inafis Server Offline');
                $('body').removeClass('online').removeClass('offline').addClass('part');
            } else {
                $('#network_status').removeClass('offline').removeClass('part').addClass('online').attr('data-original-title', 'Server Online');
                $('body').removeClass('offline').removeClass('part').addClass('online');
            }
        }
    });

    setTimeout(checkConnection, 5000);
}

function sidebarToggler() {
    $('.sidebar-toggler').on('click', function(e) {
        console.log('Sidebar toggler clicked');
    })
}

function PopupWindowCenter(url, title, w, h) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'fullscreen=yes,location=no,titlebar=no,menubar=no,status=no,scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }

    return newWindow;
}

function show_logs()
{
    PopupWindowCenter(home_url + '/tool/logs', 'Logs', winWidth, winHeight);
    return 'Open logs data...';
}

function show_session()
{
    PopupWindowCenter(home_url + '/tool/session', 'Sessions', winWidth, winHeight);
    return 'Open sessions data...';
}

// head.ready(function() {
    jQuery(document).ready(function($) {
        checkConnection();
        $('img[data-src]').addClass('hidden');
        $('img[data-src]').after('<div class="image-loading"><div></div></div>');

        
        
        $('input.icheck').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });

        if ( appAccess == 'desktop' )
        {
            var $dOut = $('#date'),
            $hOut = $('#hours'),
            $mOut = $('#minutes'),
            $sOut = $('#seconds'),
            $ampmOut = $('#ampm');
            var months = [
            'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
            ];
            
            var days = [
            'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'
            ];
            
            function update(){
            var date = new Date();
            
            var ampm = date.getHours() < 12
                        ? 'AM'
                        : 'PM';
            
            var hours = date.getHours();
            
            var minutes = date.getMinutes() < 10 
                            ? '0' + date.getMinutes() 
                            : date.getMinutes();
            
            var seconds = date.getSeconds() < 10 
                            ? '0' + date.getSeconds() 
                            : date.getSeconds();
            
            var dayOfWeek = days[date.getDay()];
            var month = months[date.getMonth()];
            var day = date.getDate();
            var year = date.getFullYear();
            
            var dateString = dayOfWeek + ', ' + month + ' ' + day + ', ' + year;
            
            $dOut.text(dateString);
            $hOut.text(hours);
            $mOut.text(minutes);
            $sOut.text(seconds);
            //   $ampmOut.text(ampm);
            } 
            
            update();
            window.setInterval(update, 1000);

            $(window).load(function() {
                setTimeout(function() {
                    $('body').animate({
                        opacity: 1
                    }, 250, function() {
                        App.unblockUI();
                        $('body').addClass('visible');

                        $('img[data-src]').each(function(index, value) {
                            var $this = $(this)
                            var imagesrc = $this.data('src');

                            $.ajax({
                                url: home_url+'/ajax/check-url-exists',
                                data: {
                                    url: imagesrc
                                },
                                success: function(data) 
                                {
                                    if ( data.status )
                                    {
                                        $this.attr('src', imagesrc).removeAttr('data-src').addClass('img-fullwidth').removeClass('hidden');
                                    }
                                    else
                                    {
                                        $this.attr('src', unknownImage).removeAttr('data-src').addClass('img-fullwidth').removeClass('hidden');
                                    }
                                },
                                error: function () 
                                { 
                                    $this.attr('src', unknownImage).removeAttr('data-src').addClass('img-fullwidth').removeClass('hidden');
                                }
                            })
                            .done(function() {
                                $this.next('.image-loading').remove();
                                $.fn.matchHeight._apply('.flat-equal-height, .rolled-equal-height');
                            });
                        });
                    });
                }, 50);
            });
        }
        else
        {
            $('body').animate({
                opacity: 1
            }, 250, function() {
                App.unblockUI();
                $('body').addClass('visible');
            });
        }

        $(document).on("click", "a", function(e) {
            var self = $(this);
            var attr = "";

            try {
                attr = self.attr('href');

            } catch(err) {}

            if ( /^http/.test(attr) && !self.hasClass('uiblock-on') )
            {
                App.blockUI();
            }
        });

        $('.pagination a').on('click', function() {
            App.blockUI();
        });
        
        sidebarToggler();

        $('#helpdesk').on('click', function(e) {
            e.preventDefault();

            if (popupWindowHelpdesk) {
                popupWindowHelpdesk.close();
                popupWindowHelpdesk = PopupWindowCenter($(this).attr('href'), "_blank", winWidth, winHeight);
            } else {
                popupWindowHelpdesk = PopupWindowCenter($(this).attr('href'), "_blank", winWidth, winHeight);
            }
        });

        $('#documentation').on('click', function(e) {
            e.preventDefault();

            if (popupWindowDoc) {
                popupWindowDoc.close();
                popupWindowDoc = PopupWindowCenter($(this).attr('href'), "_blank", winWidth, winHeight);
            } else {
                popupWindowDoc = PopupWindowCenter($(this).attr('href'), "_blank", winWidth, winHeight);
            }
        });

        $('.image-link, .image-popup').magnificPopup({
            type: 'image',
            gallery: {
                enabled: true
            }
        });

        $('.open-message').magnificPopup({
            type: 'inline',
            midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
        });

        $('.scroller').slimScroll({
            height: '275px'
        });

        $('.uiblock-on').click(function() {
            // $('.uiblock').fadeIn(200);
            App.blockUI();
        });

        $('.equal-height').matchHeight();

        $('.badge-message').addClass('animated shake');

        $('#helper_toggler').click(function() {
            $('.helper-ui').fadeToggle('fast');
        });

        $('.helper-ui').click(function() {
            $(this).fadeOut('fast');
        });

        $('#theme_toggler').click(function(e) {
            e.preventDefault();
            var $this = $(this);
            App.blockUI();
            $.post(home_url + "/ajax/change-theme", function(data, status)
            {
                App.unblockUI();
                if ( status == 'success' )
                {
                    if ( data == 'on' )
                    {
                        $this.addClass('light');
                        $('body').addClass('light');
                    }
                    else
                    {
                        $this.removeClass('light');
                        $('body').removeClass('light');
                    }
                }
            });            
        });

        $('.card-preview').each(function() { // the containers for all your galleries
            $(this).magnificPopup({
                delegate: 'a', // the selector for gallery item
                type: 'image',
                gallery: {
                    enabled: true
                }
            });
        });

        $('[data-toggle="popover"]').popover();
        $('[data-toggle="tooltip"]').tooltip();
        $('#datepicker, #birthday').datetimepicker({
            format: 'DD-MM-YYYY',
            ignoreReadonly: true
        });

        $('.datepicker').datetimepicker({
            format: 'DD-MM-YYYY',
            // defaultDate: moment(),
            ignoreReadonly: true,
            allowInputToggle: true
        });

        $('.select2').select2();

        $("#checkAll").change(function() {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });

        /*$('.overlay').click(function(event) {
            $('#overlay').addClass('show');
        }); */

        $('.chkbox').click(function() {
            //var chkbox = $(this).find('.formcheck');
            var henry = $(this).attr('data-henry');
            $('.chkbox').removeClass('checked');
            $('#henry').val(henry);
            $(this).addClass('checked');
        });

        $(".date-mask").inputmask({ 'alias': 'dd-mm-yyyy', 'clearIncomplete': true });

        $(".rumus-mask").inputmask({ "mask": "*** *** *** *** *** ***", 'clearIncomplete': false });

        var focusPop = function(selector) {
            $('#' + selector).focusin(function(event) {
                var popId = $(this).attr('data-id');
                $('#' + popId).css('display', 'block');
            });

            $('#' + selector).focusout(function(event) {
                var popId = $(this).attr('data-id');
                $('#' + popId).css('display', 'none');
            });
        }

        focusPop('mask_date2');

        // $(".overlay, ul.pagination > li > a").on('click', function(event) {
        //     App.blockUI();
        // });

        $("form.overlay").submit(function(event) {
            App.blockUI();
        });

        $('#form_scroll').on('scroll', function() {
            $('#img_scroll').scrollTop($(this).scrollTop());
            // console.log($(this).scrollTop());
        });

        $('#split_toggle').on('click', function(e) {
            e.preventDefault();
            $(this).toggleClass('active');
            $('#form_scroll, #img_scroll').toggleClass('active');

            if ($('#form_scroll').hasClass('active')) {
                $('body').addClass('pop-disable');
                $('.page-sidebar-menu').hide();
            } else {
                $('body').removeClass('pop-disable');
                $('.page-sidebar-menu').show();
            }
        });

        $('#sinyalemen_tab').on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            $('#img_scroll').addClass('sinyalemen');
        });

        $('#demographic_tab').on('click', function(event) {
            event.preventDefault();
            /* Act on the event */
            $('#img_scroll').removeClass('sinyalemen');
        });

        wheelzoom($('img.zoom'));

    });

    var FormInputMask = function() {
        var a = function() {
                $("#mask_date").inputmask("d/m/y", {
                    autoUnmask: !0
                }), $("#mask_date1").inputmask("d/m/y", {
                    placeholder: "*"
                }), $(".mask_date2").inputmask("d-m-y", {
                    placeholder: "dd-mm-yyyy"
                }), $("#mask_phone").inputmask("mask", {
                    mask: "(999) 999-9999"
                }), $("#mask_tin").inputmask({
                    mask: "99-9999999",
                    placeholder: ""
                }), $("#idcard").inputmask({
                    mask: "9",
                    repeat: 16,
                    greedy: !1
                }), $("#mask_decimal").inputmask("decimal", {
                    rightAlignNumerics: !1
                }), $("#mask_currency").inputmask("â‚¬ 999.999.999,99", {
                    numericInput: !0
                }), $("#mask_currency2").inputmask("â‚¬ 999,999,999.99", {
                    numericInput: !0,
                    rightAlignNumerics: !1,
                    greedy: !1
                }), $("#mask_ssn").inputmask("999-99-9999", {
                    placeholder: " ",
                    clearMaskOnLostFocus: !0
                })
            },
            n = function() {
                $("#input_ipv4").ipAddress(), $("#input_ipv6").ipAddress({
                    v: 6
                })
            };
        return {
            init: function() {
                a(), n()
            }
        }
    }();
    App.isAngularJsApp() === !1 && jQuery(document).ready(function() {
        FormInputMask.init()
    });

// });