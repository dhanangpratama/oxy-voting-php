// Copyright (C) 2008 Seth Golub
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// To obtain a copy of the GNU General Public License, see
// <http://www.gnu.org/licenses/>.
//
// With my apologies.  I haven't written much javascript.
// This code lives at http://www.sethoscope.net/henryjs/
//
// One major caveat about this code is that the Henry system numbers
// fingers 1-10, and this code does the usual thing of starting at zero.
// All the comments are in 1-based Henry lingo, while all the code is
// zero-based.  Sorry about that.  Maybe one of these days I'll rewrite
// the code with a dummy position, just so the numbers match.
//
//
// The Henry system of classification is a little complicated.
//
// finger order:
//   1: R thumb
//   2: R index
//   3: R middle
//   4: R ring
//   5: R pinky
//   6: L thumb
//   7: L index
//   8: L middle
//   9: L ring
//  10: L pinky/little
//
//  Generally presented in two rows:
//    1  2  3  4  5
//    6  7  8  9 10
//
//  The value given for each finger is a capital letter for the class of print:
// W - Whorl
// A - Arch
// T - Tented Arch
// R - Radial Loop
// U - Ulnar Loop
finger_types = ['W', 'A', 'T', 'R', 'U'];

//
// Whorls are accompanied by their trace value:
// I - Inner
// M - Meet
// O - Outer
//
// Radial Loops and Ulnar Loops are accompanied by their ridge count.
whorl_trace_values = ['I', 'M', 'O'];

function Finger() {
  // I've read that this way of defining class methods is memory
  // inefficient in javascript, but we'll generally only have ten
  // fingers at a time, and with all the monster javascript apps out
  // there, I imagine the garbage collector can keep up.

  this.IsLoop = function() {
    return this.type == 'U' || this.type == 'R';
  };
  this.IsRadialLoop = function() {
    return this.type == 'R';
  };
  this.IsUlnarLoop = function() {
    return this.type == 'U';
  };
  this.IsWhorl = function() {
    return this.type == 'W';
  };
  this.IsArch = function() {
    return this.type == 'A';
  };
  this.IsTentedArch = function() {
    return this.type == 'T';
  };
}

// Returns a random integer 0 .. n-1
function RandInt(n) {
  return Math.floor(n * Math.random());
}

// Returns a random element of the supplied array
function RandElement(arr) {
  return arr[RandInt(arr.length)];
}

// Returns a finger with random features.  Features are chosen with
// uniform probability from the given options, which does not reflect
// their likelihood in the real world.
function RandomFinger() {
  finger = new Finger();
  finger.type = RandElement(finger_types);
  if ( finger.IsLoop() ) {
    finger[1] = 1 + RandInt(20);    // Give it a ridge count
  } else if ( finger == 'W' ) {
    finger[1] = RandElement(whorl_trace_values);
  }
  return finger;
}

function RandomNFingers(n) {
  // Let's pretend for the sake of style that n won't always be 10
  arr = new Array(n);
  for ( i=0; i<n; ++i ) {
    arr[i] = RandomFinger();
  }
  return arr;
}


// The general form of the Henry Classification is this:
//
//     Key Major Primary Secondary Subsecondary Final
//     ----------------------------------------------
//         Major Primary Secondary Subsecondary Final
//
// though some elements do not always appear.
//

// Key: the ridge count of the first loop, not counting fingers 5 or 10.
// If there are none, there is no key.
function Key(fingers) {
  for ( i=0; i<9; ++i ) {
    if ( i != 4 ) {
      if ( fingers[i][0] == 'U' || fingers[i][0] == 'R' ) {
        return [fingers[i][1], null];
      }
    }
  }
  return [null, null];
}

// Major: If finger 1 or 6 is an Arch or Tented Arch, there is no Major.
// Otherwise, we first compute the bottom part of the Major using the left
// thumb (finger 6).  If finger 6 is a Whorl, we use its trace value.
// Otherwise it's a Loop and we look up its ridge count in this table:
//  1-11 = S
// 12-16 = M
// 17+   = L
//
// Then we compute the top part of the Major using the right thumb.
// If it's a Whorl, we use its trace value.  Otherwise, we look up its
// ridge count in a table, but which table we use depends on what we
// got for the bottom part of the Major.  If we got an S or M, we use
// the same table as above.  Otherwise, we use this table:
//  1-17 = S
// 18-22 = M
// 23+   = L
function _small_major_table(ridge_count) {
  if ( ridge_count <= 11 ) {
    return 'S';
  } else if ( ridge_count <= 16 ) {
    return 'M';
  }
  return 'L';
}

function _large_major_table(ridge_count) {
  if ( ridge_count <= 17 ) {
    return 'S';
  } else if ( ridge_count <= 22 ) {
    return 'M';
  }
  return 'L';
}

function Major(fingers) {
  values = [null, null];
  if ( fingers[0][0] == 'U' || fingers[0][0] == 'R' || fingers[0][0] == 'T' || fingers[5] == 'A' || fingers[5] == 'T' ) {
    return values;
  }
  if ( fingers[5] == 'W' ) {
    values[1] = fingers[5][1];
  } else {
    values[1] = _small_major_table(fingers[5][1]);
  }
  if ( fingers[0][0] == 'W' ) {
    values[0] = fingers[0][1];
  } else {
    if ( values[1] == 'L' ) {
      values[0] = _large_major_table(fingers[0][1]);
    } else {
      values[0] = _small_major_table(fingers[0][1]);
    }
  }
  return values;
}


// Primary
//
// Fingers 1-10 are valued thusly: 16 16 8 8 4 4 2 2 1 1.  The Primary
// above the line is 1 plus the sum of the value of each even finger
// (2,4,6,8,10) whose class is a Whorl.  Below the line is the same
// thing for the odd fingers.
primary_values = [16,16,8,8,4,4,2,2,1,1];
function Primary(fingers) {
  values = [1, 1];
  for ( i=0; i<10; ++i ) {
    if ( fingers[i][0] == 'W' ) {
      values[1 - (i & 1)] += primary_values[i];
    }
  }
  return values;
}


// Subsecondary: If any finger in 2,3,4, 7,8,9 is any of R/A/T, there
// is no Subsecondary.  Otherwise, the Subsecondary consists of three
// uppercase letters on top and three on bottom, corresponding to
// fingers 2,3,4 and 7,8,9.  Which letters we use is a bit complicated.
// For a Whorl, use the trace value (I/M/O).
// For a Loop (R/U), we use either O or I, depending on whether the
// ridge count is above a certain threshold.  For some reason, the
// threshold dependings on which finger we're talking about.
// Fingers 2/7: 1-9  = I, 10+ = O.
// Fingers 3/8: 1-10 = I; 11+ = O.
// Fingers 4/9: 1-13 = I; 14+ = O.
subsecondary_threshold = [0,9,10,13];
function _Subsecondary1Hand(fingers) {
  buf = '';
  for ( i=1; i < 4; ++i ) {
    if ( fingers[i][0] == 'A' || fingers[i][0] == 'T' || fingers[i][0] == 'R') {
      return null;
    }
    if ( fingers[i][0] == 'W' ) {
      val = fingers[i][1];
    } else { // Ulnar Loop
      if ( fingers[i][1] <= subsecondary_threshold[i] ) {
        val = 'I';
      } else {
        val = 'O';
      }
    }
    buf += val;
  }
  return buf;
}
function Subsecondary(fingers) {
  right = _Subsecondary1Hand(fingers.slice(0,5));
  left = _Subsecondary1Hand(fingers.slice(5));
  if ( right == null || left == null ) {
    return [null, null];
  }
  return [right, left];
}


// Secondary: Use the following algorithm for each hand (right hand
// above the line, left below).
//
// 1. Start by simply copying the class letter of each finger (WATRU),
//    so we have a sequence of five letters.
// 2. Keep the index finger uppercase, but make the others all lowercase.
// 3. Replace all lowercase W and U with a dash.
// 4. Remove all leading dashes.
// 5. Remove all trailing dashes.
// 6. Run length encode any letters in the sequence (but omit 1s).
//    This means: if there's a sequence of the same letter repeated,
//    replace the whole sequence with a number indicating the length of
//    the sequence, followed by the letter.  For example, tt becomes 2t
//    and aaa becomes 3a.
//
// (If no finger other than 2 or 7 is R, A, or T, the Secondary is
// just the class of finger 2 over the class of finger 7, both in
// caps.)
function _RLE_letters(arr) {
  // run-length-encode remaining lowercase letters
  // We go backwards so that we can do triples more simply.  Also, we're
  // guaranteed to have a non-run at the start of the string, so this
  // way we don't need any special cases to tidy up a run in progress.
  count = 1;
  for ( i=arr.length-2; i >= 0; --i ) {
    if ( arr[i] == arr[i+1] && arr[i] != '-' ) {
      ++count;
      arr[i+1] = '';
    } else {
      if ( count > 1 ) {
        // we just came off a run
        arr[i+1] = '' + count + arr[i+1];
        count = 1;
      }
    }
  }
  return arr;
}

function _Secondary1Hand(fingers, subsecondary) {
  if ( subsecondary[0] != null ) {
    return fingers[2][0];
  }
  buf = new Array(fingers.length);
  for ( i=0; i < fingers.length; ++i ) {
    tmp = fingers[i][0];
    if ( i != 1 ) {
      if ( tmp == 'W' || tmp == 'U' ) {
        if ( i > 0 ) {
          tmp = '-';
        } else {
          tmp = '';
        }
      } else {
        tmp = tmp.toLowerCase();
      }
    }
    buf[i] = tmp;
  }
  // remove trailing dashes
  while ( buf[buf.length-1] == '-' ) {
    buf = buf.slice(0,buf.length-1);
  }
  buf = _RLE_letters(buf);
  return buf.join('');
}

function Secondary(fingers) {
  subsecondary = Subsecondary(fingers);  // CPU is cheap.
  return [_Secondary1Hand(fingers.slice(0,5), subsecondary),
          _Secondary1Hand(fingers.slice(5), subsecondary)];
}

// Final: If finger 5 is an Ulnar Loop, the Final is its ridge count
// and is written over the line.  Else, if finger 10 is an Ulnar Loop,
// the Final is its ridge count and is written below the line.  Else
// there is no Final.
function Final(fingers) {
  if ( fingers[4][0] == 'U' ) {
    return [fingers[4][1], null];
  }
  if ( fingers[9][0] == 'U' ) {
    return [null, fingers[9][1]];
  }
  return [null, null];
}

function FingersToDebugString(fingers) {
  buf = '';
  for ( i in fingers ) {
    buf += fingers[i][0];
    if ( fingers[i][1] != undefined ) {
      buf += '(' + fingers[i][1] + ')';
    }
    buf += ' ';
  }
  return buf;
}

function FingersToHTMLDebugString(fingers) {
  buf = '';
  for ( i in fingers ) {
    buf += fingers[i][0];
    if ( fingers[i][0] != undefined ) {
      buf += '<SUP><SMALL>' + fingers[i][1] + '</SMALL></SUP>';
    }
    buf += ' ';
    if ( i == 4 ) {
      buf += "<BR>";
    }
  }
  return buf;
}

function FingersToHTMLDebugTable(fingers) {
  buf = '<TABLE class=fingers><TR>';
  for ( i in fingers ) {
    buf += "<TD>";
    buf += fingers[i][0];
    if ( fingers[i][0] != undefined ) {
      buf += '<SUP><SMALL>' + fingers[i][1] + '</SMALL></SUP>';
    }
    buf += '</TD> ';
    if ( i == 4 ) {
      buf += "</TR><TR>";
    }
  }
  buf += "</TR></TABLE>";
  return buf;
}

