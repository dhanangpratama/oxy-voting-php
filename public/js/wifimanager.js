/**!
 * OXY Creative Wifi manager
 * Javascript library to interact with Wireless connection
 *
 * @license Propietary
 * @author OXY Creative
 * @version 0.0.1
 * @date Aug 30, 2017
 **/

jQuery(document).ready(function ($) {

    var $headerWifiMenu = $("#header_wifi_menu_ul");

    BioverWifiScan.Init({
        OnDataReceived: function (wifi) {
            /**
             *retrieve wifi list and write to ul menu
             */

            $headerWifiMenu.html('');

            if (wifi.length > 0)
            {
                $('.badge-wifi').addClass('available');

                for (var i = 0; i < wifi.length; i++) {
                    var obj = wifi[i];

                    var idLink = "wifiManagerItem_" + obj.Name + "_" + i;
                    var connected = obj.ConnectionStatus !== "" ? " (" + obj.ConnectionStatus + ")" : "";
                    var li = $('<li id="' + idLink + '"><a href="javascript:;"><span class="message">' + obj.Name + connected + ' </span></a></li>');
                    $headerWifiMenu.append(li);

                    //var li = $("#" + idLink);
                    if (obj.ConnectionStatus !== "Connecting" && obj.ConnectionStatus !== "Connected") {

                        li.data('wifi', obj);
                        li.on("click", function () {
                            var data = $(this).data('wifi');
                            BioverWifiScan.Connect(data.Name);
                        });

                    } else if(obj.ConnectionStatus === "Connected") {
                        li.on("click", function () {
                            BioverWifiScan.Disconnect();
                        });
                    }
                }
            }
            else
            {
                var li = $('<li><a href="javascript:;"><span class="message">Koneksi WiFi tidak tersedia</span></a></li>');
                $('.badge-wifi').removeClass('available');
                $headerWifiMenu.append(li);
            }

            var liMac = $('<li><a class="mac-popup" href="#mac_address">Lihat Informasi MAC Address</li>');
            $headerWifiMenu.append(liMac);

            liMac.on("click", function () {
                $.magnificPopup.open({
                    items: {
                      src: '#mac_address'
                    },
                    type: 'inline'
                  
                    // You may add options here, they're exactly the same as for $.fn.magnificPopup call
                    // Note that some settings that rely on click event (like disableOn or midClick) will not work here
                  }, 0);
                console.log($(this).children('a').attr('href'));
            });
        },
        OnStartWait: function () {
            $headerWifiMenu.html('');
            $headerWifiMenu.append('<li>Mohon menunggu...</li>');

        },
        OnStopWait: function () {
            App.unblockUI();
        },
        OnStatusMessage: function(message) {
            swal({
                text: message,
                type: 'warning',
                allowOutsideClick: false
            });

        },
        OnConnectionStatusChanged: function(connectionStatus) {
            var message = "";

            if(connectionStatus === "Disconnected") {
                message = "Koneksi WiFi terputus";

            } /*else if(connectionStatus === "Connected") {
                message = "Berhasil tersambung ke wifi";

            }*/

            if(message !== "") {
                swal({
                    text: message,
                    type: 'warning',
                    allowOutsideClick: false
                });
            }
        },
        OnConnectAuthRequired: function(nameWifi) {
            swal({
                title: 'Masukkan password untuk WiFi ' + nameWifi,
                input: 'password',
                showCancelButton: true,
                confirmButtonText: 'Submit',
                showLoaderOnConfirm: true,
                preConfirm: function (password) {
                    if (password !== "") {
                        BioverWifiScan.ConnectWithPassword(nameWifi, password);
                    }
                },
                allowOutsideClick: false
            });

        }
    });

    $("#header_wifi_menu a").hover(
        function () {
            BioverWifiScan.ReadAvailWifi();
        },
        function(){}
    );

    BioverWifiScan.ReadAvailWifi();
});