/**!
 * OXY Creative Hardware Diagnostic
 * Javascript library to interact with Wireless connection
 *
 * @license Propietary
 * @author OXY Creative
 * @version 0.0.1
 * @date Sep 6, 2017
 **/

jQuery(document).ready(function ($) {

    var $hardwareStatusEl = $("#header_hardware_status a");
    var $hardwareDataEl = $("#header_hardware_status_data");
    var $hardwareStatusIconEl = $("#header_hardware_status .badge-hardware");
    var $isCheckingHardware = false;

    $hardwareStatusEl.hover(
        function () {
            if ($isCheckingHardware) return;
            $isCheckingHardware = true;

            Biover.RunHardwareDetector();
            $hardwareDataEl.html('<li><a href="javascript:;">Loading...</a></li>');

            $.ajax({
                url: home_url + '/ajax/get-hardware-status',
                type: 'get',
                dataType: 'json',
                /*beforeSend: function() {
                    App.blockUI();
                },*/

                cache: false,
                error: function (xhr, status, errorThrown) {
                    alertify.alert("Gagal mendapatkan info hardware");
                },
                success: function (response, status, xhr) {
                    //App.unblockUI();
                    $hardwareDataEl.html('');
                    var hardwareStatusClass = "success";
                    var hardwarecount = 0;
                    var hardwareErrorCount = 0;

                    jQuery.each(response, function (key, val) {
                        var status = (val === "0" ? "off" : "");
                        var el =
                            '<li>' +
                            '   <a href="javascript:void(0)">' +
                            '       <span class="hardware-indicator ' + status + '"></span>' +
                            '       <span class="message"> ' + key + ' </span>' +
                            '   </a>' +
                            '</li>';

                        $hardwareDataEl.append(el);
                        hardwarecount++;
                        if (val === "0") hardwareErrorCount++;
                    });

                    if(hardwareErrorCount > 0 && hardwareErrorCount < 6) hardwareStatusClass = 'warning';
                    else if(hardwareErrorCount >= 6) hardwareStatusClass = 'error';

                    $hardwareStatusIconEl.removeClass("success");
                    $hardwareStatusIconEl.removeClass("warning");
                    $hardwareStatusIconEl.removeClass("error");
                    $hardwareStatusIconEl.addClass(hardwareStatusClass);
                    $hardwareStatusIconEl.html(hardwarecount);
                    $isCheckingHardware = false;
                }
            });
        },
        function(){}
    );






});