/**!
 * OXY Creative Livescan Fingerprint capture
 * Javascript library to interact with Fingerprint devices
 *
 * @license Propietary
 * @author OXY Creative
 * @version 0.0.1
 * @date July 31, 2017
 **/

(function($) {

    $.fn.livescanfinger = function(options) {
        'use strict';

        var methods = {};

        if (methods[options]) {
            return methods[options].apply(this, Array.prototype.slice.call(arguments, 1));
        }

        var $settings = $.extend({
            elementFingerScreenPreview: null,
            elementDeviceMessage: null,
            elementButtonStart: null,
            elementButtonNext: null,
            elementButtonRepeat: null,
            elementButtonSkip: null,
            elementFingerSeqName: null,
            elementFingerSeqImage: null,
            elementFingerQuality: null,
            urlBaseImage: null,
            urlBaseImageFingerSequence: null
        }, options);

        $settings.elementButtonStart.on("click", startCapture);
        $settings.elementButtonRepeat.on("click", startCapture);
        $settings.elementButtonSkip.on("click", skipCurrentCapture);

        $("a")
            .not($settings.elementButtonStart)
            .not($settings.elementButtonRepeat)
            .on('click', function(){ BioverFingerprint.Close(); });

        var $container = $settings.elementFingerScreenPreview;
        var $offset = $container.offset();

        var param = {
            FrameWidth: $container.width(),
            FrameHeight: $container.height(),
            FrameTop: $offset.top,
            FrameLeft: $offset.left,
            OnAfterInit: OnAfterInit,
            OnUpdateStatusMessage: OnUpdateStatusMessage,
            OnUpdateFingerSequence: OnUpdateFingerSequence,
            OnImageResultAvailable: OnImageResultAvailable,
            OnCaptureCompleted: OnCaptureCompleted,
            OnUpdateFingerQuality: OnUpdateFingerQuality
        };
        BioverFingerprint.Init(param);

        function startCapture() {
            $settings.elementDeviceMessage.show();
            $settings.elementButtonStart.hide();
            $settings.elementFingerQuality.show();

            $settings.elementButtonNext
                .attr('disabled', 'disabled')
                .show();

            // $settings.elementButtonRepeat
            //     .attr('disabled', 'disabled')
            //     .show();

            BioverFingerprint.Start();

        }

        function skipCurrentCapture() {
            App.blockUI({boxed: !0});
            $(this).hide();
            BioverFingerprint.SkipCurrentCapture();
            App.unblockUI();
        }

        function OnAfterInit() {
            BioverFingerprint.GetConnectedDevice().then(function(connectedDevice) {
                var information = "Tolong hubungkan <em>Fingerprint Scanner</em> dan tekan tombol <strong>Ulangi</strong> di bawah ini.";

                if (connectedDevice === "") {

                    /*
                     var result = confirm(information);
                     if(result) {
                     OnAfterInit();
                     }
                     */


                    // alertify.okBtn("Ulangi").cancelBtn("Batalkan").confirm(information).then(function(resolvedValue) {
                    //     resolvedValue.event.preventDefault();
                    //     // alertify.alert("You clicked the " + resolvedValue.buttonClicked + " button!");
                    //     if (resolvedValue.buttonClicked === 'ok') {
                    //         OnAfterInit();
                    //     }
                    // });

                    swal({
                        html: information,
                        title: 'Perangkat Tidak Terhubung',
                        type: 'error',
                        padding: 30,
                        width: 650,
                        showCancelButton: true,
                        confirmButtonColor: '#0376B8',
                        cancelButtonColor: '#e12330',
                        confirmButtonText: 'Ulangi',
                        cancelButtonText: 'Batal',
                        allowOutsideClick: false
                    }).then(function () {
                        OnAfterInit();
                    }, function() {
                        App.blockUI();
                        go(home_url + '/dashboard');
                    });

                } else {
                    //$("#button-start").removeAttr("disabled");
                    BioverFingerprint.Start();
                }

            });

        }


        function OnUpdateStatusMessage(msg) {
            $settings.elementDeviceMessage.html(msg);

        }

        function OnUpdateFingerSequence(fingerPosition) {

            var fingerTitle = "";
            var fingerImage = $settings.urlBaseImage + "palm.png";
            $settings.elementButtonSkip.hide();

            switch (fingerPosition) {
                case "FLAT_RIGHT_FOUR_FINGERS":
                    fingerTitle = "Empat Jari Kanan";
                    $settings.elementButtonSkip.show();
                    break;

                case "FLAT_LEFT_FOUR_FINGERS":
                    fingerTitle = "Empat Jari Kiri";
                    $settings.elementButtonSkip.show();
                    break;

                case "FLAT_THUMBS":
                    fingerTitle = "Dua Jari Jempol";
                    $settings.elementButtonSkip.show();
                    break;

                case "RIGHT_THUMB":
                    fingerTitle = "Jempol Kanan";
                    break;

                case "RIGHT_INDEX_FINGER":
                    fingerTitle = "Telunjuk Kanan";
                    break;

                case "RIGHT_MIDDLE_FINGER":
                    fingerTitle = "Tengah Kanan";
                    break;

                case "RIGHT_RING_FINGER":
                    fingerTitle = "Manis Kanan";
                    break;

                case "RIGHT_LITTLE_FINGER":
                    fingerTitle = "Kelingking Kanan";
                    break;

                case "LEFT_THUMB":
                    fingerTitle = "Jempol Kiri";
                    break;

                case "LEFT_INDEX_FINGER":
                    fingerTitle = "Telunjuk Kiri";
                    break;

                case "LEFT_MIDDLE_FINGER":
                    fingerTitle = "Tengah Kiri";
                    break;

                case "LEFT_RING_FINGER":
                    fingerTitle = "Manis Kiri";
                    break;

                case "LEFT_LITTLE_FINGER":
                    fingerTitle = "Kelingking Kiri";
                    break;


            }

            if (fingerTitle !== "") {
                fingerImage = $settings.urlBaseImageFingerSequence + fingerPosition.toLowerCase() + ".png";
            }

            $settings.elementFingerSeqName.html(fingerTitle);
            $settings.elementFingerSeqImage.attr("src", fingerImage);

        }

        function OnImageResultAvailable(data) {
            try {
                $("#" + data.FingerName).html(data.Image);
            } catch (e) {
                console.error(e);
            }

        }

        function OnCaptureCompleted() {

            $settings.elementDeviceMessage.hide();
            $settings.elementButtonStart.hide();
            $settings.elementFingerQuality.hide();
            $settings.elementButtonSkip.hide();

            $settings.elementButtonNext
                .removeAttr("disabled")
                .show();

            $settings.elementButtonRepeat
                .removeAttr("disabled")
                .show();

            $settings.elementFingerSeqName.html("Selesai");
            $settings.elementFingerSeqImage.attr("src", $settings.urlBaseImage + "palm.png");

        }

        /**
         *
         * @param message possible value UNKNOWN, NOT_PRESENT, GOOD, FAIR, POOR, INVALID_AREA
         * Bagus, Cukup, Buruk
         */
        function OnUpdateFingerQuality(message) {

            switch(message) {
                case 'GOOD':
                    message = 'Bagus';
                    break;

                case 'FAIR':
                    message = 'Cukup';
                    break;

                case 'POOR':
                    message = 'Buruk';
                    break;

                default:
                    message = '';
            }

            $settings.elementFingerQuality.removeClass();
            $settings.elementFingerQuality.addClass(message);
            $settings.elementFingerQuality.html(message);
        }

        return this;

    };

}(jQuery));