/**!
 * OXY Creative webcam library
 * Javascript library to interact with webcam
 *
 * @license Propietary
 * @author OXY Creative
 * @version 0.0.1
 * @date July 7, 2017
 **/

(function($) {

    $.fn.webcam = function(options, callbackEvent) {
        'use strict';

        var element = this;
        var videoDevices = [];
        var videoDeviceId = null;
        // var videoResolution = { width: 1280, height: 720 }; //HD Resolution
        var videoResolution = { width: 1920, height: 1080 }; //FullHD Resolution

        var methods = {
            captureImage: captureImage
        };

        if (methods[options]) {
            return methods[options].apply(this, Array.prototype.slice.call(arguments, 1));
        }

        var settings = $.extend({
            videoSelect: null,
            videoDeviceName: null,
            onError: function() {},
            onHideVideoSelect: function() {},
        }, options);


        navigator.mediaDevices.enumerateDevices()
            .then(gotDevices)
            .then(getStream)
            .catch(onError);

        if (settings.videoSelect !== null && "onchange" in settings.videoSelect) {
            settings.videoSelect.onchange = getStream;
        }

        function gotDevices(deviceInfos) {
            for (var i = 0; i !== deviceInfos.length; ++i) {
                var deviceInfo = deviceInfos[i];


                if (deviceInfo.kind === 'videoinput') {
                    var option = document.createElement('option');
                    var deviceId = deviceInfo.deviceId;
                    var deviceLabel = deviceInfo.label || 'camera ' + (videoDevices.length + 1);

                    videoDevices.push({ id: deviceId, label: deviceLabel });

                    option.value = deviceId;
                    option.text = deviceLabel;
                    if (settings.videoSelect !== null && $.isFunction(settings.videoSelect.appendChild)) {
                        settings.videoSelect.appendChild(option);
                    }


                    if (settings.videoDeviceName !== null && deviceLabel.indexOf(settings.videoDeviceName) !== -1) {
                        videoDeviceId = deviceId;
                    }

                }
            }
        }

        function getStream() {
            if (window.stream) {
                window.stream.getTracks().forEach(function(track) {
                    track.stop();
                });
            }

            var _videoDeviceId = videoDeviceId;

            if (videoDeviceId === null && settings.videoSelect !== null) {
                _videoDeviceId = settings.videoSelect.value;

            } else if (videoDeviceId === null && videoDevices.length > 0) {
                _videoDeviceId = videoDevices[0].id;
            }

            if (videoDeviceId !== null) {
                if (settings.videoSelect !== null) {
                    $(settings.videoSelect).hide();
                }
                settings.onHideVideoSelect.call();
            }


            var constraints = {
                video: {
                    deviceId: { exact: _videoDeviceId },
                    /*
                    width: videoResolution.width,
                    height: videoResolution.height
                    */
                    width: {exact: videoResolution.width},
                    height: {exact: videoResolution.height}


                }
            };

            navigator.mediaDevices.getUserMedia(constraints).then(gotStream).catch(onError);
        }

        function gotStream(stream) {
            window.stream = stream; // make stream available to console

            element.each(function() {
                if ("srcObject" in this) {
                    this.srcObject = stream;
                }

            });
        }

        function captureImage() {

            element.each(function() {

                if (window.stream) {
                    var canvas = document.createElement('canvas');
                    var img = document.createElement('img');
                    canvas.width = this.videoWidth;
                    canvas.height = this.videoHeight;
                    canvas
                        .getContext('2d')
                        .drawImage(this, 0, 0, canvas.width, canvas.height);


                    // "image/webp" works in Chrome.
                    // Other browsers will fall back to image/png.
                    //img.src = canvas.toDataURL('image/png');
                    //document.body.appendChild(img);

                    if ($.isFunction(callbackEvent)) {
                        callbackEvent(canvas.toDataURL('image/png'))
                    }
                }

            });


        }

        function onError(error) {
            settings.onError.call(error);
            console.error('Error: ', error);
        }

        return this;

    };

}(jQuery));