var gulp = require('gulp'),
    gutil = require('gulp-util'),
    autoprefix = require('gulp-autoprefixer'),
    minifyCSS = require('gulp-minify-css'),
    concat = require('gulp-concat');

gulp.task('cssVendor', function() {
    var css = [
        'public/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'public/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'public/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'public/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css',
        'public/assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css',
        'public/assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css',
        'public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
        'public/assets/global/plugins/morris/morris.css',
        'public/assets/global/plugins/fullcalendar/fullcalendar.min.css',
        'public/assets/global/plugins/jqvmap/jqvmap/jqvmap.css',
        'public/assets/vendor/alertifyjs/dist/css/alertify.css',
        'public/assets/global/plugins/select2/css/select2.min.css',
        'public/assets/global/plugins/select2/css/select2-bootstrap.min.css',
        'public/assets/global/css/components.min.css',
        'public/assets/global/css/plugins.min.css',
        'public/assets/layouts/layout/css/layout.min.css',
        'public/assets/layouts/layout/css/themes/darkblue.min.css',
        'public/assets/layouts/layout/css/custom.min.css',
        'public/assets/vendor/magnific-popup/dist/magnific-popup.css',
        'public/assets/css/cropper.min.css',
        'public/assets/vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
        'public/assets/css/sweetalert2.min.css',
        'public/css/animate.css'
    ];

    gulp.src(css)
        .pipe(concat('vendor.min.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('public/css'));
});

gulp.task('customCss', function() {
    gulp.src(['public/css/style.css'])
        .pipe(concat('style.min.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('public/css'));
});

gulp.task('jsVendorLogin', function() {
    var loginJs = [
        'public/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'public/assets/global/plugins/js.cookie.min.js',
        'public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'public/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
        'public/assets/global/plugins/select2/js/select2.full.min.js'
    ];

    gulp.src(loginJs)
        .pipe(concat('vendor-login.js'))
        .pipe(gulp.dest('public/js/'));
});

gulp.task('jsVendor', function() {
    gulp.src([
            'public/assets/global/plugins/bootstrap/js/bootstrap.min.js',
            'public/assets/global/plugins/js.cookie.min.js',
            'public/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js',
            'public/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
            'public/assets/global/plugins/jquery.blockui.min.js',
            'public/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
            'public/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
            'public/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
            'public/assets/global/plugins/moment.min.js',
            'public/assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js',
            'public/assets/global/plugins/morris/morris.min.js',
            'public/assets/global/plugins/morris/raphael-min.js',
            'public/assets/global/plugins/counterup/jquery.waypoints.min.js',
            'public/assets/global/plugins/counterup/jquery.counterup.min.js',
            'public/assets/global/plugins/fullcalendar/fullcalendar.min.js',
            'public/assets/global/plugins/flot/jquery.flot.min.js',
            'public/assets/global/plugins/flot/jquery.flot.resize.min.js',
            'public/assets/global/plugins/flot/jquery.flot.categories.min.js',
            'public/assets/global/plugins/flot/jquery.flot.pie.min.js',
            'public/assets/global/plugins/flot/jquery.flot.stack.min.js',
            'public/assets/global/plugins/flot/jquery.flot.crosshair.min.js',
            'public/assets/global/plugins/flot/jquery.flot.axislabels.js',
            'public/assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js',
            'public/assets/global/plugins/jquery.sparkline.min.js',
            'public/assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
            'public/assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
            'public/assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
            'public/assets/global/plugins/jquery.blockui.min.js',
            'public/assets/global/plugins/select2/js/select2.min.js',
            'public/assets/global/scripts/app.min.js',
            'public/assets/layouts/layout/scripts/layout.min.js',
            'public/assets/layouts/global/scripts/quick-sidebar.min.js',
            'public/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
            'public/assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js',
            'public/assets/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'public/assets/vendor/magnific-popup/dist/jquery.magnific-popup.min.js',
            'public/assets/vendor/alertifyjs/dist/js/alertify.js',
            'public/assets/js/sweetalert2.min.js',
            'public/assets/js/jquery.flot.orderBars.js',
            'public/assets/js/wheelzoom.js',
            'public/assets/js/jquery.matchHeight-min.js',
            'public/js/jquery.pwstrength.min.js',
            'public/js/noty.min.js',
            'public/assets/js/cropper.min.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('public/js'));

    gulp.src([
            'public/assets/vendor/jquery/dist/jquery.min.js'
        ])
        .pipe(gulp.dest('public/js'));
});

gulp.task('copyImg', function() {
    gulp.src([
            'public/assets/global/img/*'
        ])
        .pipe(gulp.dest('public/img'));
});

gulp.task('copyFonts', function() {
    gulp.src([
            'public/assets/global/plugins/font-awesome/fonts/*',
        ])
        .pipe(gulp.dest('public/fonts/'));

    gulp.src([
            'public/assets/global/plugins/simple-line-icons/fonts/*'
        ])
        .pipe(gulp.dest('public/css/fonts'));
});

gulp.task('default', ['cssVendor', 'jsVendor', 'jsVendorLogin', 'copyFonts', 'copyImg', 'customCss'], function() {
    // place code for your default task here
    //gulp.src('resources/assets/css/*.css') // Matches 'client/js/somedir/somefile.js' and resolves `base` to `client/js/`
    //.pipe(gulp.dest('public/css'));
    gutil.log('Gulp is finished!');
});

gulp.task('watch', function() {
    gulp.watch('public/css/style.css', ['customCss']);
});