<?php

// Route::get('test/os/detail', ['as' => 'os_inafis', 'uses' => 'Api\TestController@detail']);
// Route::get('test/os/match', ['as' => 'os_inafis_match', 'uses' => 'Api\TestController@OSInafisLatenSearch']);
// Route::get('test/os/match/direct', ['as' => 'os_inafis_match', 'uses' => 'Api\TestController@OSInafisLatenSearchDirect']);
Route::get('test/os/nik/{nik}', ['as' => 'os_inafis_nik', 'uses' => 'Api\TestController@nik']);
Route::get('barcode/generate', ['as' => 'sample_barcode_generate', 'uses' => 'BarcodeController@generateCard']);
// Route::get('test/os/search', ['as' => 'os_inafis_search', 'uses' => 'Api\TestController@OSInafisSearch']);
// Route::get('test/os/search/direct', ['as' => 'os_inafis_search_direct', 'uses' => 'Api\TestController@OSInafisSearchDirect']);
// Route::get('test/os/search/image', ['as' => 'os_inafis_search_image', 'uses' => 'Api\TestController@OSInafisGetImages']);
// Route::get('test/os/search/image/direct', ['as' => 'os_inafis_search_image_direct', 'uses' => 'Api\TestController@OSInafisGetImagesDirect']);
// Route::get('test/os/search/externalid', ['as' => 'os_inafis_search_externalid', 'uses' => 'Api\TestController@OSInafisSearchDetailByExternalId']);
// Route::get('test/report', ['as' => 'test_report_server', 'uses' => 'Api\TestController@reportServer']);
// Route::get('test/download/zip/{id}', ['as' => 'test_download_ak23', 'uses' => 'Api\TestController@downloadZip']);

/**
* Route guest user
*/
Route::group(['middleware' => 'guest'], function () 
{
	Route::get('/', ['as' => 'user_login', 'uses' => 'Auth\LoginController@index']);
	Route::post('/', ['as' => 'user_login_submit', 'uses' => 'Auth\LoginController@submit']);
	Route::post('forgot-password', ['as' => 'user_forget_submit', 'uses' => 'Auth\ForgotPasswordController@submit']);
	Route::get('forgot-password/new-password/{id}', ['as' => 'user_forget_new_password', 'uses' => 'Auth\ForgotPasswordController@newPassword']);
	Route::post('forgot-password/new-password/{id}', ['as' => 'user_forget_new_password_submit', 'uses' => 'Auth\ForgotPasswordController@newPasswordSubmit']);
});

/**
* Route first register user operator
*/
Route::group(['middleware' => 'default_operator'], function () 
{
	Route::get('operator/register', ['as' => 'default_operator', 'uses' => 'Auth\RegisterController@index']);
	Route::post('operator/register', ['as' => 'default_operator_submit', 'uses' => 'Auth\RegisterController@save']);
	Route::get('operator/register/logout', ['as' => 'default_operator_logout', 'uses' => 'Auth\RegisterController@logout']);
});

/**
* Route auth user
*/
Route::group(['middleware' => 'auth'], function () 
{
	Route::get('event', ['as' => 'event', 'uses' => 'EventController@index']);
	Route::post('event', ['as' => 'event_submit', 'uses' => 'EventController@submit']);

	Route::get('demographic/create', ['as' => 'demographic_create', 'uses' => 'Demographic\DataController@index']);
	Route::post('demographic/create', ['as' => 'demographic_create_submit', 'uses' => 'Demographic\DataController@submit']);

	Route::get('demographic/photo', ['as' => 'demographic_photo', 'uses' => 'Demographic\PhotoController@index']);
	Route::post('demographic/photo', ['as' => 'demographic_photo_submit', 'uses' => 'Demographic\PhotoController@submit']);

	Route::get('demographic/print', ['as' => 'demographic_print', 'uses' => 'Demographic\PrintController@index']);

	Route::get('demographic/nik', ['as' => 'demographic_found_nik', 'uses' => 'Demographic\DataController@nik']);
	Route::get('members', ['as' => 'members', 'uses' => 'MemberController@index']);
	Route::get('member/view/{id}', ['as' => 'member_view', 'uses' => 'MemberController@view']);

	Route::post('keep-alive', ['as' => 'keep_live', 'uses' => 'Tools\SettingController@keepLive']);

	/** Route group for administrator page */
	Route::group(['middleware' => 'auth.admin'], function () 
	{
		// Route::get('users', ['as' => 'users', 'uses' => 'UserController@index']);
		// Route::get('user/remove/{id}', ['as' => 'user_remove', 'uses' => 'UserController@remove']);

		// Route::get('messages', ['as' => 'message', 'uses' => 'MessageController@index']);
		// Route::get('message/create', ['as' => 'message_create', 'uses' => 'MessageController@create']);
		// Route::post('message/create', ['as' => 'message_create_submit', 'uses' => 'MessageController@save']);

		// Route::get('message/{id}', ['as' => 'message_edit', 'uses' => 'MessageController@edit']);
		// Route::post('message/{id}', ['as' => 'message_edit_submit', 'uses' => 'MessageController@saveEdit']);
		// Route::get('message/remove/{id}', ['as' => 'message_remove', 'uses' => 'MessageController@remove']);

		// Route::get('data/location', ['as' => 'data_location', 'uses' => 'DataController@location']);
		// Route::get('data/location/create', ['as' => 'data_location_create', 'uses' => 'DataController@locationCreate']);
		// Route::post('data/location/create', ['as' => 'data_location_create_submit', 'uses' => 'DataController@locationSave']);
		// Route::get('data/location/{id}', ['as' => 'data_location_edit', 'uses' => 'DataController@locationEdit']);
		// Route::post('data/location/{id}', ['as' => 'data_location_edit_submit', 'uses' => 'DataController@locationSaveEdit']);
		// Route::get('data/role', ['as' => 'data_role', 'uses' => 'DataController@role']);
		// Route::get('data/role/create', ['as' => 'data_role_create', 'uses' => 'DataController@roleCreate']);
		// Route::post('data/role/create', ['as' => 'data_role_create_submit', 'uses' => 'DataController@roleCreateSubmit']);
		// Route::get('data/role/{id}', ['as' => 'data_role_edit', 'uses' => 'DataController@roleEdit']);
		// Route::post('data/role/{id}', ['as' => 'data_role_edit_submit', 'uses' => 'DataController@roleSaveEdit']);

		// Route::get('data/{type}', ['as' => 'data', 'uses' => 'DataController@index']);
		// Route::get('data/create/{type}', ['as' => 'data_create', 'uses' => 'DataController@create']);
		// Route::post('data/create/{type}', ['as' => 'data_create_submit', 'uses' => 'DataController@save']);
		// Route::get('data/edit/{type}/{id}', ['as' => 'data_edit', 'uses' => 'DataController@edit']);
		// Route::post('data/edit/{type}/{id}', ['as' => 'data_edit_submit', 'uses' => 'DataController@saveEdit']);
		// Route::get('data/remove/{type}/{id}', ['as' => 'data_remove', 'uses' => 'DataController@remove']);
	});
	
	Route::get('logout', ['as' => 'user_logout', 'uses' => 'Auth\LoginController@logout']);

	Route::get('user/create', ['as' => 'user_create', 'uses' => 'UserController@create']);
	Route::post('user/create', ['as' => 'user_create_submit', 'uses' => 'UserController@createSubmit']);

	Route::get('user/profile/{id?}', ['as' => 'user_profile', 'uses' => 'UserController@edit']);
	Route::post('user/profile/{id?}', ['as' => 'user_profile_submit', 'uses' => 'UserController@editSubmit']);

	// Route::get('setting', ['as' => 'setting', 'uses' => 'Tools\SettingController@index']);
	// Route::get('doc', ['as' => 'doc', 'uses' => 'Tools\DocController@index']);
	// Route::get('helpdesk', ['as' => 'helpdesk', 'uses' => 'Tools\HelpdeskController@index']);

	// Route::get('create/card-session', ['as' => 'create_scan_card_process', 'uses' => 'Demographic\CardController@createProcessSession']);
	// Route::get('create/live-session', ['as' => 'create_finger_scan_process', 'uses' => 'Demographic\LiveController@createProcessSession']);

	/**
	* Edit Controller
	*/
	// Route::get('demographic/edit/{id}/front', ['as' => 'demographic_edit_front', 'uses' => 'Demographic\FrontController@edit']);

	// Route::get('report/export/csv/{dateRange}', 'ReportController@export_csv');

	/**
	* Demographic Controller Routes
	*/
	// Route::group(['middleware' => 'processed'], function () 
	// {
	// 	Route::get('demographic/card', ['as' => 'demographic_card_scan_record', 'uses' => 'Demographic\CardController@index']);
	// 	Route::get('demographic/live', ['as' => 'demographic_finger_scan_record', 'uses' => 'Demographic\LiveController@index']);
	// 	Route::post('demographic/card', ['as' => 'demographic_scan_card_submit', 'uses' => 'Demographic\CardController@submit']);
	// 	Route::post('demographic/live', ['as' => 'demographic_finger_scan_submit', 'uses' => 'Demographic\LiveController@submit']);

	// 	Route::get('demographic/front', ['as' => 'demographic_front', 'uses' => 'Demographic\FrontController@index']);
	// 	Route::post('demographic/front', ['as' => 'demographic_front_submit', 'uses' => 'Demographic\FrontController@submit']);

	// 	Route::get('demographic/back', ['as' => 'demographic_back', 'uses' => 'Demographic\BackController@index']);
	// 	Route::post('demographic/back', ['as' => 'demographic_front_submit', 'uses' => 'Demographic\BackController@submit']);

	// 	Route::get('demographic/camera', ['as' => 'demographic_camera', 'uses' => 'Demographic\CameraController@index']);
	// 	Route::post('demographic/camera', ['as' => 'demographic_camera_submit', 'uses' => 'Demographic\CameraController@submit']);

	// 	Route::get('demographic/picture', ['as' => 'demographic_picture', 'uses' => 'Demographic\PictureController@index']);
	// 	Route::post('demographic/picture', ['as' => 'demographic_picture_submit_upload', 'uses' => 'Demographic\PictureController@submit']);
	// 	Route::get('demographic/picture/next', ['as' => 'demographic_picture_submit_next', 'uses' => 'Demographic\PictureController@submit_next']);
	// 	Route::get('picture/remove/{position}', ['as' => 'demographic_picture_remove', 'uses' => 'Demographic\PictureController@remove']);
	// 	Route::get('picture/crop/{position}', ['as' => 'demographic_picture_crop', 'uses' => 'Demographic\PictureController@crop']);
	// 	Route::post('picture/crop/{position}/do', ['as' => 'demographic_picture_crop_do', 'uses' => 'Demographic\PictureController@doCrop']);

	// 	Route::match(['get', 'post'], 'demographic/finger/roll/{type?}', ['as' => 'demographic_finger_roll', 'uses' => 'Demographic\FingerController@index']);
	// 	Route::match(['get', 'post'], 'demographic/formula', ['as' => 'demographic_formula', 'uses' => 'Demographic\FingerController@index']);
	// 	Route::get('demographic/formula_session', ['as' => 'demographic_formula_session', 'uses' => 'Demographic\FingerController@formula_session']);
	// 	Route::match(['get', 'post'], 'demographic/finger/flat/{type?}', ['as' => 'demographic_finger_flat', 'uses' => 'Demographic\FingerFlatController@index']);
	// 	Route::get('rolled/crop/{finger}', ['as' => 'demographic_rolled_crop', 'uses' => 'Demographic\FingerController@crop']);
	// 	Route::post('rolled/crop/{finger}/do', ['as' => 'demographic_rolled_crop_do', 'uses' => 'Demographic\FingerController@doCrop']);

	// 	Route::get('demographic/identification/{key?}', ['as' => 'demographic_identification', 'uses' => 'Demographic\IdentificationController@index']);
	// 	Route::post('demographic/identification/{key?}', ['as' => 'demographic_identification_candidate', 'uses' => 'Demographic\IdentificationController@chooseCandidate']);

	// 	Route::match(['get','post'], 'demographic/save', ['as' => 'demographic_save', 'uses' => 'Demographic\ActionController@save']);
	// 	Route::post('demographic/update', ['as' => 'demographic_update', 'uses' => 'Demographic\ActionController@update']);
	// });

	/**
	* People Controller Routes
	*/
	Route::match(['get', 'post'], 'people/search', ['as' => 'people.search', 'uses' => 'PeopleController@search'] );
	Route::get('people/view/{id}', ['as' => 'people.view', 'uses' => 'PeopleController@view']);
	Route::resource('people', 'PeopleController');
	Route::get('people/dermalog/front/{afisid}', ['as' => 'people.dermalog.front', 'uses' => 'PeopleController@dermalogFront']);
	Route::get('people/dermalog/back/{afisid}', ['as' => 'people.dermalog.back', 'uses' => 'PeopleController@dermalogBack']);
	Route::get('people/dermalog/photo/{afisid}', ['as' => 'people.dermalog.photo', 'uses' => 'PeopleController@dermalogPhoto']);
	Route::get('people/dermalog/finger/{afisid}', ['as' => 'people.dermalog.finger', 'uses' => 'PeopleController@dermalogFinger']);

	Route::get('people/ktp/{nik}', ['as' => 'people_ktp_detail', 'uses' => 'PeopleController@ktpDetail']);
	Route::get('people/osinafis/{id}', ['as' => 'people_osinafis_detail', 'uses' => 'PeopleController@OSInafisDetail']);

	/**
	* Report Controller Routes
	*/
	Route::get('report', ['as' => 'report', 'uses' => 'ReportController@index']);
	Route::post('report', 'ReportController@submit');
	Route::get('report/search', ['as' => 'report.search', 'uses' => 'ReportController@search']);

	Route::get('laten', ['as' => 'laten', 'uses' => 'LatenController@index']);
	Route::get('laten/scan', ['as' => 'laten_scan', 'uses' => 'LatenController@scan']);
	// Route::post('laten', 'LatenController@submit');
	Route::get('laten/process', 'LatenController@liveProcess');
	Route::get('laten/result/{key}', ['as' => 'laten_result', 'uses' => 'LatenController@result']);

	/**
	* Export Controller Routes
	*/
	// Route::get('export/ak24', 'ExportController@ak24_multiple');
	// Route::get('export/ak24-server', 'ExportController@ak24MultipleServer');
	// Route::get('clear/ak24', ['as' => 'ak24_clear', 'uses' => 'ExportController@ak24Clear']);
	// Route::get('export/ak24/{id?}', 'ExportController@ak24');
	// Route::get('export/ak23/{id?}', 'ExportController@ak23');
	// Route::get('export/compare/{data}', ['as' => 'demographic_compare_print', 'uses' => 'ExportController@compare_print']);
	// Route::get('export/dermalog/{afisid}', 'ExportController@dermalog');

	// Route::group(['prefix' => 'tool', 'namespace' => 'Tools'], function ()
	// {
	// 	Route::get('env', ['as' => 'compatible_check', 'uses' => 'UpdateController@checkEnv']);
	// 	Route::get('update/software', ['as' => 'update_software', 'uses' => 'UpdateController@app']);
	// 	Route::get('update/software/download', ['as' => 'update_software_download', 'uses' => 'UpdateController@downloadUpdate']);
	// 	Route::post('update/software', 'UpdateController@doAppUpdate');
	// });
});

Route::group(['middleware' => 'checkAjax'], function ()
{
	Route::get('ajax/get-server-status', "AjaxController@serverStatus");

	Route::group(['middleware' => 'auth'], function ()
	{
		// Route::get('ajax/verification', 'AjaxController@matching');
		// Route::get('ajax/verification/{finger_id}', 'AjaxController@matchingDetail');
		// Route::get('ajax/verification/update/{id}', 'AjaxController@update_local_verification'); 
		// Route::get('ajax/slv', 'AjaxController@session_local_verification');

		// Route::get('ajax/edit/verification/{id}', 'AjaxController@matchingEdit');
		// Route::get('ajax/edit/verification/detail/{finger_id}/{id}', 'AjaxController@matchingDetailEdit'); 

		// Route::get('ajax/search/ektp', "AjaxController@findEKtp");
		Route::get('ajax/get-hardware-status', "AjaxController@getHardwareStatus");
		Route::post('ajax/search/create-demographics', ['as' => 'ajax_ektp_create_demographic', 'uses' => 'AjaxController@ektpCreateDemographic']);
		Route::post('ajax/search/upload-live-photo', ['as' => 'ajax_upload_live_photo', 'uses' => 'AjaxController@uploadLivePhoto']);

		Route::post('ajax/process-photo', 'AjaxController@processPhoto');
		Route::post('ajax/remove-photo', 'Ajax\CameraController@remove');

		/**
		* Route for add and remove session print AK-24 data
		*/
		Route::post('ajax/print', 'AjaxController@create_session_print_ak24');

		Route::post('laten/scan-search', ['as' => 'ajax_laten_scan_submit', 'uses' => 'AjaxController@latenSearchLive']);

		Route::post('ajax/laten-search', ['as' => 'ajax_laten_search', 'uses' => 'AjaxController@latenSearch']);
		Route::post('ajax/laten-process-detail', ['as' => 'ajax_laten_process_detail', 'uses' => 'AjaxController@latenProcessDetail']);
		Route::post('ajax/laten-search-live', ['as' => 'ajax_laten_search_live', 'uses' => 'AjaxController@latenSearchLive']);
		Route::post('ajax/laten-preview', ['as' => 'ajax_laten_preview', 'uses' => 'AjaxController@latenPreview']);

		Route::post('ajax/change-theme', ['as' => 'ajax_change_theme', 'uses' => 'AjaxController@changeTheme']);

		Route::post('ajax/remove-card-flag', ['as' => 'ajax_card_remove_flag', 'uses' => 'AjaxController@flagRemoveCard']);
		Route::post('ajax/check-card-complete', ['as' => 'ajax_card_check_complete', 'uses' => 'AjaxController@checkCompleteCardFile']);
		Route::get('ajax/check-url-exists', ['as' => 'ajax_check_url_exists', 'uses' => 'AjaxController@checkUrlExists']);
	});

	Route::group(['namespace' => 'Ajax', 'middleware' => 'reflashSession'], function ()
	{
		// ajax route here
	});
});

if ( env('APP_DEBUG') )
{
	Route::get('tool/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
	Route::get('tool/session', 'Tools\ToolController@viewSession');
}