@extends('layouts.main')
@section('content')
<?php $bg = ($scanType != 'card') ? '' : 'style="background-image: url('.$file['url_back'].');"'; ?>

@if (session('process.type') == 'card')
<a class="image-link" data-toggle="tooltip" title="Lihat kartu" data-placement="right" href="{{ $file['url_back'] }}"><i class="fa fa-file-image-o"></i></a>
@include('layouts.card_indicator')
@else
@include('layouts.live_scan_indicator')
@endif

<div id="img_scroll" class="back">
    <img src="{{ $file['url_back'] }}" class="img-responsive">
</div>

{!! Form::open(['method'=>'POST']) !!}
<div id="form_scroll" class="back">
    <div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">Tampilan Belakang</div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a id="demographic_tab" href="#demographic" aria-controls="page-1" role="tab" data-toggle="tab">Demographic</a></li>
                <li role="presentation"><a id="sinyalemen_tab" href="#sinyalemen" aria-controls="page -2" role="tab" data-toggle="tab">Sinyalemen</a></li>
            </ul>
        </div>
        <div class="portlet-body form">
          <div class="form-body">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="demographic">
                    <div class="row data">
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('pob', old('pob', array_get($demographicData, 'pob')), ['id' => 'tempat_lahir', 'tabindex' => 1,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo tempat-lahir">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="tempat_lahir">Tempat Lahir</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <div id="birthday" class="input-group date">
                                    {{ Form::text('dob', old('dob', array_get($demographicData, 'dob')), ['id' => 'mask_date2', 'tabindex' => 2,'class' => 'form-control infoable', 'autocomplete' => 'off', 'data-id' => 'birthdate']) }} 

                                    <label for="mask_date2">Tanggal Lahir</label>

                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>

                                @if ($scanType == 'card')
                                <div id="birthdate" class="popinfo birthdate">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <?php $defaultNationality = ! empty($demographicData['nationality']) ? $demographicData['nationality'] : 'Indonesia'; ?>
                                {{ Form::text('nationality', old('nationality', ! empty($defaultNationality) ? $defaultNationality : ktp_el_data('nationality')), ['id' => 'nationality', 'tabindex' => 3,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo nationality">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="nationality">Kebangsaan</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('race', old('race', array_get($demographicData, 'race')), ['id' => 'nationality', 'tabindex' => 3,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo nationality">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="nationality">Suku</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="religion_id" id="religion" tabindex="4">
                                    <option value="0">-- Pilih Agama --</option>
                                    @foreach ($religion as $value)
                                    <?php 
                                    $select_religion = '';

                                    $select_religion = $value->religion_id == ktp_el_data('religion') ? 'selected' : '';

                                    if (isset($demographicData['religion_id']))
                                    {
                                        $select_religion = $value->religion_id == $demographicData['religion_id'] ? 'selected' : '';
                                    } ?>
                                    <option value="{{ $value->religion_id }}" {{ $select_religion }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo religion">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="religion">Agama</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <?php
                                $full_address = '';
                                $address = ktp_el_data('address');
                                $rt_rw = ktp_el_data('rt_rw');
                                $kel = ktp_el_data('kel');
                                $kec = ktp_el_data('kec');
                                $kab = ktp_el_data('kab');
                                $provinsi = ktp_el_data('provinsi');

                                if ( ! empty($address) )
                                {
                                    $full_address .= format_text($address);
                                }

                                if ( ! empty($rt_rw) )
                                {
                                    $full_address .= strtoupper(' RT/RW. ' . $rt_rw);
                                }

                                if ( ! empty($kel) )
                                {
                                    $full_address .= format_text(' Kelurahan ' . $kel . ',');
                                }

                                if ( ! empty($kec) )
                                {
                                    $full_address .= format_text(' Kecamatan ' . $kec . ',');
                                }

                                if ( ! empty($kab) )
                                {
                                    $full_address .= format_text(' ' . $kab . ',');
                                }

                                if ( ! empty($provinsi) )
                                {
                                    $full_address .= format_text(' ' . $provinsi);
                                }
                                ?>
                                {{ Form::text('address', old('address', isset($demographicData['address']) ? $demographicData['address'] : $full_address), ['id' => 'lastaddress', 'tabindex' => 5,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="lastaddress">Alamat</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('rt', old('rt', isset($demographicData['rt']) ? $demographicData['rt'] : ktp_el_data('rt')), ['id' => 'rt', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="rt">RT</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('rw', old('rw', isset($demographicData['rw']) ? $demographicData['rw'] : ktp_el_data('rw')), ['id' => 'rw', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="rw">RW</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('village', old('village', isset($demographicData['village']) ? $demographicData['village'] : ktp_el_data('village')), ['id' => 'village', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="village">Desa</label>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('regent', old('regent', isset($demographicData['regent']) ? $demographicData['regent'] : ktp_el_data('regent')), ['id' => 'regent', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="regent">Kabupaten / Kota</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('district', old('district', isset($demographicData['district']) ? $demographicData['district'] : ktp_el_data('district')), ['id' => 'district', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="district">Wilayah</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('province', old('province', isset($demographicData['province']) ? $demographicData['province'] : ktp_el_data('province')), ['id' => 'province', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="province">Provinsi</label>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <?php $defaultCountry = ! empty($demographicData['country']) ? $demographicData['country'] : 'Indonesia'; ?>
                                {{ Form::text('country', old('country', ! empty($defaultCountry) ? $defaultCountry : ktp_el_data('country')), ['id' => 'country', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="country">Negara</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('postal_code', old('postal_code', isset($demographicData['postal_code']) ? $demographicData['postal_code'] : ktp_el_data('postal_code')), ['id' => 'postalcode', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo lastaddress">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="postalcode">Kodepos</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('nik', old('nik', isset($demographicData['nik']) ? $demographicData['nik'] : ktp_el_data('nik')), ['id' => 'idcard', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo id-card">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="idcard">No. KTP</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                {{ Form::text('passport_number', old('passport_number', isset($demographicData['passport_number']) ? $demographicData['passport_number'] : ktp_el_data('passport_number')), ['id' => 'paspor', 'tabindex' => 6,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                @if ($scanType == 'card')
                                <div class="popinfo id-card">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="paspor">No. Paspor</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <select class="form-control pophover infoable" name="education_id" id="education" tabindex="7">
                                    <option value="0">-- Pilih Pendidikan --</option>
                                    @foreach ($educations as $value)
                                    <option {{ $value->education_id == $demographicData['education_id'] ? 'selected' : '' }} value="{{ $value->education_id }}">{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo education">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="education">Pendidikan Terakhir</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="father_name" name="father_name" value="{{ old('father_name', $demographicData['father_name']) }}" tabindex="8">

                                @if ($scanType == 'card')
                                <div class="popinfo father-name">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="father_name">Nama Ayah</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="father_address" name="father_address" value="{{ old('father_address', $demographicData['father_address']) }}" tabindex="9">

                                @if ($scanType == 'card')
                                <div class="popinfo father-address">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="father_address">Alamat Ayah</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="mother_name" name="mother_name" value="{{ old('mother_name', $demographicData['mother_name']) }}" tabindex="10">

                                @if ($scanType == 'card')
                                <div class="popinfo mother-name">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="mother_name">Nama Ibu</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="mother_address" name="mother_address" value="{{ old('mother_address', $demographicData['mother_address']) }}" tabindex="11">

                                @if ($scanType == 'card')
                                <div class="popinfo mother-address">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="mother_address">Alamat Ibu</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="wh_name" name="mate_name" value="{{ old('mate_name', $demographicData['mate_name']) }}" tabindex="12">

                                @if ($scanType == 'card')
                                <div class="popinfo wh-name">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="wh_name">Nama Istri/Suami</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="wh_address" name="mate_address" value="{{ old('mate_address', $demographicData['mate_address']) }}" tabindex="13">

                                @if ($scanType == 'card')
                                <div class="popinfo wh-address">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="wh_address">Alamat Istri/Suami</label>
                            </div>
                        </div>
                        
                        <?php $child = !empty($demographicData['children']) ? explode(',', $demographicData['children']) : []; ?>
                        @for ($i = 0; $i <= 5; $i++)
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="child_{{$i+1}}" name="children[]" value="{{ old('childs[]', array_key_exists($i, $child) ? trim($child[$i]) : '') }}" tabindex="{{ 16+$i }}">

                                @if ($scanType == 'card')
                                <div class="popinfo childs-name-{{$i+1}}">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="child_{{$i+1}}">Anak {{$i+1}}</label>
                            </div>
                        </div>
                        @endfor
                        
                        {{--
                        <div class="col-sm-12">
                            <div class="form-group form-md-line-input form-md-floating-label ">

                                {!! Form::text('information', old('information', $demographicData['information']), ['class' => 'form-control infoable', 'tabindex' => 22]) !!}

                                <label for="information">Information</label>
                            </div>
                        </div>
                        --}}

                        {{-- <div class="col-sm-12">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists"> Change </span>
                                        <input type="file" name="picture"> 
                                    </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="sinyalemen">
                    <div class="row data">
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <div class="input-group">
                                    <input type="text" class="form-control infoable" autocomplete="off" id="body_height" name="height" value="{{ old('height', $demographicData['height']) }}" tabindex="20"> 

                                    @if ($scanType == 'card')
                                    <div class="popinfo body-height">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="body_height">Tinggi Badan</label>

                                    <span class="input-group-addon">
                                        CM
                                    </span>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <div class="input-group">
                                    <input type="text" class="form-control infoable" autocomplete="off" id="body_weight" name="weight" value="{{ old('weight', $demographicData['weight']) }}" tabindex="21"> 

                                    @if ($scanType == 'card')
                                    <div class="popinfo body-weight">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="body_weight">Berat Timbangan</label>

                                    <span class="input-group-addon">
                                        KG
                                    </span>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="mother_name" name="blood_type" value="{{ old('blood_type', $demographicData['blood_type']) }}" tabindex="22">

                                @if ($scanType == 'card')
                                <div class="popinfo mother-name">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="mother_name">Golongan Darah</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="skin_id" id="skin_color" tabindex="22">
                                <option value="0">-- Pilih Warna Kulit --</option>
                                    @foreach ($sinyalemen['skin'] as $value)
                                    <option {{ $value->skin_id == $demographicData['skin_id'] ? 'selected' : '' }} value="{{ $value->skin_id }}">{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo skin-color">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="skin_color">Warna Kulit</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="body_id" id="body_shape" tabindex="23">
                                    <option value="0">-- Pilih Bentuk Tubuh --</option>
                                    @foreach ($sinyalemen['body'] as $value)
                                    <option value="{{ $value->body_id }}" {{ $value->body_id == $demographicData['body_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo body-shape">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="body_shape">Bentuk Tubuh</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="head_id" id="shape_of_head" tabindex="23">
                                    <option value="0">-- Pilih Bentuk Kepala --</option>
                                    @foreach ($sinyalemen['head'] as $value)
                                    <option value="{{ $value->head_id }}" {{ $value->head_id == $demographicData['head_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach 
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo shape-of-head">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="shape_of_head">Bentuk Kepala</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="hair_color_id" id="hair_color" tabindex="24">
                                    <option value="0">-- Pilih Warna Rambut --</option>
                                    @foreach ($sinyalemen['hair_color'] as $value)
                                    <option value="{{ $value->hair_color_id }}" {{ $value->hair_color_id == $demographicData['hair_color_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach 
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo hair-color">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="hair_color">Warna Rambut</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="hair_id" id="type_of_hair" tabindex="25">
                                    <option value="0">-- Pilih Jenis Rambut --</option>
                                    @foreach ($sinyalemen['hair'] as $value)
                                    <option value="{{ $value->hair_id }}" {{ $value->hair_id == $demographicData['hair_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo type-of-hair">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="type_of_hair">Jenis Rambut</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="face_id" id="type_of_face" tabindex="26">
                                    <option value="0">-- Pilih Bentuk Muka --</option>
                                    @foreach ($sinyalemen['face'] as $value)
                                    <option value="{{ $value->face_id }}" {{ $value->face_id == $demographicData['face_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo type-of-face">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="type_of_face">Bentuk Muka</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="forehead_id" id="forehead" tabindex="27">
                                    <option value="0">-- Pilih Dahi --</option>
                                    @foreach ($sinyalemen['forehead'] as $value)
                                    <option value="{{ $value->forehead_id }}" {{ $value->forehead_id == $demographicData['forehead_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo forehead">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="forehead">Dahi</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="eye_color_id" id="eyes_color" tabindex="28">
                                    <option value="0">-- Pilih Warna Mata --</option>
                                    @foreach ($sinyalemen['eye_color'] as $value)
                                    <option value="{{ $value->eye_color_id }}" {{ $value->eye_color_id == $demographicData['eye_color_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo eyes-color">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="eyes_color">Warna Mata</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="eye_irregularity_id" id="irregularitesoneyes" tabindex="29">
                                    <option value="0">-- Pilih Kelainan pada Mata --</option>
                                    @foreach ($sinyalemen['eye_irregularity'] as $value)
                                    <option value="{{ $value->eye_irregularity_id }}" {{ $value->eye_irregularity_id == $demographicData['eye_irregularity_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo irregularitesoneyes">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="irregularitesoneyes">Kelainan pada mata</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="nose_id" id="nose" tabindex="30">
                                    <option value="0">-- Pilih Hidung --</option>
                                    @foreach ($sinyalemen['nose'] as $value)
                                    <option value="{{ $value->nose_id }}" {{ $value->nose_id == $demographicData['nose_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo nose">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="nose">Hidung</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="lip_id" id="lips" tabindex="31">
                                    <option value="0">-- Pilih Bentuk Bibir --</option>
                                    @foreach ($sinyalemen['lip'] as $value)
                                    <option value="{{ $value->lip_id }}" {{ $value->lip_id == $demographicData['lip_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo lips">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="lips">Bibir</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="tooth_id" id="tooth" tabindex="32">
                                    <option value="0">-- Pilih Bentuk Gigi --</option>
                                    @foreach ($sinyalemen['tooth'] as $value)
                                    <option value="{{ $value->tooth_id }}" {{ $value->tooth_id == $demographicData['tooth_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo tooth">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="tooth">Gigi</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="chin_id" id="chin" tabindex="33">
                                    <option value="0">-- Pilih Bentuk Dagu --</option>
                                    @foreach ($sinyalemen['chin'] as $value)
                                    <option value="{{ $value->chin_id }}" {{ $value->chin_id == $demographicData['chin_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo chin">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="chin">Dagu</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="ear_id" id="ear" tabindex="34">
                                    <option value="0">-- Pilih Bentuk Telinga --</option>
                                    @foreach ($sinyalemen['ear'] as $value)
                                    <option value="{{ $value->ear_id }}" {{ $value->ear_id == $demographicData['ear_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                @if ($scanType == 'card')
                                <div class="popinfo ear">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="ear">Telinga</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="tattoo" autocomplete="off" name="tattoo" value="{{ old('note', $demographicData['tattoo']) }}" tabindex="35">

                                @if ($scanType == 'card')
                                <div class="popinfo tattoo">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="tattoo">Tatoo</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="scars_and_handicap" name="scars_and_handicap" value="{{ old('note', $demographicData['scars_and_handicap']) }}" tabindex="35">

                                @if ($scanType == 'card')
                                <div class="popinfo scars-handicap">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif

                                <label for="scars_and_handicap">Dipotong dan Cacat</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" autocomplete="off" id="provision_code" name="provision_code" value="{{ old('provision_code', $demographicData['provision_code']) }}" tabindex="36">

                                @if ($scanType == 'card')
                                <div class="popinfo provision-code">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif
                                
                                <label for="provision_code">Kode Pasal</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <textarea class="form-control infoable" rows="3" id="information" name="information" tabindex="37">{{ old('information', $demographicData['information']) }}</textarea>

                                @if ($scanType == 'card')
                                <div class="popinfo note">
                                    <div class="image" <?php echo $bg; ?>></div>
                                </div>
                                @endif
                                
                                <label for="information">Keterangan</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
          </div>  
        </div>
    </div>
    </div>
</div>

<div class="text-center">
    <div class="btn-group m-b-30">
        <button class="btn red uiblock-on  btn-lg uppercase" type="submit" name="back" tabindex="39"><i class="fa fa-angle-left m-r-15"></i> Kembali</button>

        @if ( session()->has('subject_id_to_update') && session('process.mode') != 'edit' && !File::exists(config('path.data.tmp_compare') . DIRECTORY_SEPARATOR . session('subject_id_to_update') . DIRECTORY_SEPARATOR . 'processing.txt') )
        <button class="btn red btn-lg uppercase" id="printCompare" onclick="PopupWindowCenter('{{ route("demographic_compare_print", [encrypt(0 . '|' . session('subject_id_to_update'))]) }}', '_blank', 1930, 1080)" tabindex="40" type="button"><i class="fa fa-print m-r-15"></i>Cetak Perbandingan</button>
        @endif   

        <button class="btn blue btn-lg uppercase" id="saveEditBtn" tabindex="38" type="submit">Simpan <i class="fa fa-check m-l-15"></i></button>
    </div>
</div>

{!! Form::close() !!}
    
@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        $('#tempat_lahir').focus();

        $('#saveEditBtn').click(function(event) {
            event.preventDefault();

            swal({
                title: 'Konfirmasi',
                html: 'Sudahkah informasi diperiksa ulang?',
                type: 'warning',
                padding: 50,
                width: 600,
                showCancelButton: true,
                confirmButtonColor: '#0376B8',
                cancelButtonColor: '#e12330',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                allowOutsideClick: false
            }).then(function () {
                App.blockUI();    
                $('form').submit();
            });
        });
    });
});
</script>
@endpush