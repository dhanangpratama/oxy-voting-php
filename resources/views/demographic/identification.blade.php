@extends('layouts.main')
@section('content')

@if ( $scanType == 'card' )
@include('layouts.card_indicator')
<a style="z-index: 99" class="image-link" data-toggle="tooltip" title="Lihat kartu bagian depan" data-placement="right" href="{{ $file['url_front'] }}"><i class="fa fa-file-image-o"></i></a>
<a class="image-link" data-toggle="tooltip" title="Lihat kartu bagian belakang" data-placement="right" href="{{ $file['url_back'] }}"><i class="fa fa-file-image-o"></i></a>
@else
@include('layouts.live_scan_indicator')
@endif

<?php extract($candidateData); ?>

{!! Form::open(['method' => 'post']) !!}
    <div class="portlet box dark">
        <div class="portlet-body">
            @if ( ! empty($results['content']) )
            <div class="row">
                <div class="col-sm-4">
                    <div class="laten-result-title">KANDIDAT</div>
                    <ul class="laten-name-list list-group rd-0">
                        @foreach($results['content'] as $keyArray => $candidate)
                        <?php $keyArrayEnc = encrypt($keyArray); ?>
                        <li class="key-{{ $keyArray }} list-group-item rd-0 {{ $key == $keyArray ? 'active' : '' }}">
                            <a class="uiblock-on" href="{{ route('demographic_identification', [$keyArrayEnc]) }}"><span class="{{ $candidate['scoreFlag'] }}">{{ ! empty($candidate['score']) ? str_replace('.', ',', ($candidate['score'])) : 0 }}</span>{{ !empty($candidate['fullname']) ? strtoupper(str_limit($candidate['fullname'], 26)) : '-' }}</a>
                        </li>
                        @endforeach
                    </ul>
                </div>

                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="laten-score {{ $scoreFlag }}">NILAI: <strong>{{ $score }}</strong></div>
                        </div>

                        @if (session('verification_key') === $key)
                        <div class="col-sm-12">
                            <div class="alert alert-info">Kandidat terpilih</div>
                        </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="col-sm-4">
                            <img data-src="{{ $baseUrl . 'face-front.jpg?' }}{{ time() }}" class="img-fullwidth" alt="">
                        </div>

                        <div class="col-sm-8">
                            <table class="table table-compare">
                                <tbody>
                                    <tr>
                                        <th width="180" align="right">Status</th>
                                        <td>{{ $status }}</td>
                                    </tr>
                                    <tr>
                                        <th width="180" align="right">Nama</th>
                                        <td>{{ $fullname }}</td>
                                    </tr>
                                    <tr>
                                        <th width="180" align="right">Alias</th>
                                        <td>{{ $nickname }}</td>
                                    </tr>
                                    <tr>
                                        <th width="180" align="right">Jenis Kelamin</th>
                                        <td>{{ $sex }}</td>
                                    </tr>
                                    <tr>
                                        <th width="180" align="right">Golongan Darah</th>
                                        <td>{{ $bloodType }}</td>
                                    </tr>
                                    <tr>
                                        <th width="180" align="right">Pekerjaan</th>
                                        <td>{{ $job }}</td>
                                    </tr>
                                    <tr>
                                        <th width="180" align="right">Lahir</th>
                                        <td>{{ $pob }}, {{ $dob }}</td>
                                    </tr>
                                    <tr>
                                        <th width="180" align="right">N.I.K</th>
                                        <td>{{ $nik }}</td>
                                    </tr>
                                    <tr>
                                        <th width="180" align="right">Alamat</th>
                                        <td>{{ $address }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            @if ( empty($results['content']) )
            <div class="text-center verify-page-wrapper">
                <p class="white">Data tidak ditemukan</p>
            </div>
             @endif
        </div>
    </div>

    <div class="text-center">
    <div class="btn-group">
        @if ( session()->has('status') && !empty($statusNotification['class']) && $statusNotification['class'] == 'danger' )
        <a class="btn yellow btn-lg uiblock-on uppercase" href="{{ (session('process.type') != 'card') ? url('live') : url('') }}">Batal</a>
        @endif
        <a class="btn red btn-lg uiblock-on uppercase" href="{{ (session('process.type') != 'card') ? route('demographic_finger_scan_record') : route('demographic_finger_flat', ['right_thumb']) }}"><i class="fa fa-angle-left m-r-15"></i> Kembali</a>
        {{-- <a class="btn green btn-lg" data-toggle="modal" href="#verificationModal">Verifikasi</a> --}}
        @if ( ! empty($results['content']) )
        <button type="submit" class="btn blue btn-lg uppercase" id="choose_candidate">Pilih Kandidat <i class="fa fa-angle-right m-l-15"></i></button>
        {!! Form::hidden('subject_id', $subjectId) !!}
        @endif
        <button type="submit" class="btn {{ ! empty($results['content']) ? 'red' : 'blue' }} btn-lg uiblock-on uppercase" name="skip">{{ ! empty($results['content']) ? 'Lewati' : 'Selanjutnya' }} <i class="fa fa-angle-right m-l-15"></i></button>
        <input type="hidden" name="key" value="{{ $key }}">
    </div>
    </div>
    {!! Form::close() !!}

    {{-- <div class="modal fade" id="verificationModal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Verifikasi</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <a href="#" type="button" id="btn_verification" class="btn green btn-outline" style="visibility: hidden;" data-dismiss="modal">Verifikasi</a>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div> --}}

@endsection

@push('js')
<script type="text/javascript">
head.ready(function() {
	jQuery(document).ready(function($) {
        $('#choose_candidate').click(function(event) {
            event.preventDefault();
            swal({
                title: 'Konfirmasi',
                html: 'Yakin akan memilih kandidat ini?',
                type: 'info',
                padding: 50,
                width: 600,
                showCancelButton: true,
                confirmButtonColor: '#0376B8',
                cancelButtonColor: '#e12330',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                allowOutsideClick: false
            }).then(function () {
                App.blockUI();    
                $('form').submit();
            });
        });
    });
});
</script>
@endpush