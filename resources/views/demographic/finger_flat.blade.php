@extends('layouts.main')
@section('content')

@if ( $scanType == 'live' )
@include('layouts.live_scan_indicator')
@endif

<?php $pos = explode(',', trim($selection, '[]')); ?>

@include('layouts.card_indicator')

{!! Form::open(['method'=>'POST']) !!}

    <div class="portlet box dark">
        <div class="portlet-body form">
        	<div class="form-body">
                <div class="list-flat-finger">
                    @foreach(config('ak23.finger_data') as $fingerName => $value)
                        <div class="{{ $fingerName == $type ? 'active' : '' }}">
                            <div class="equal-height">
                                <a data-toggle="tooltip" title="{{ $value['title'] }}" data-placement="top" href="{{ route('demographic_finger_flat', [$fingerName]) }}" class="holder uiblock-on">
                                    @if ( empty($dataFinger['fingers']['flat_'.$fingerName]['missing']) && file_exists($file['basePath'] . DIRECTORY_SEPARATOR . 'flat_' . $fingerName . '.jpg') )
                                    <img class="rd-0 img-responsive" src="{{ $file['baseUrl'] . '/' . $value['flat_name'] }}?{{ time() }}" alt="">
                                    @else
                                    <img src="{{ asset('assets/images/unknown.png') }}" class="img-responsive rd-0" alt="">
                                    @endif
                                </a>
                                <div class="btn-group" style="width: 100%">
                                    <button type="button" title="Kualitas" class="btn {{ array_has($dataFinger['fingers'], "flat_{$fingerName}.image_quality") ? image_quality_class($dataFinger['fingers']['flat_'.$fingerName]['image_quality']) : '' }} btn-xs" style="width: 50%">{{ array_has($dataFinger['fingers'], "flat_{$fingerName}.image_quality") ?$dataFinger['fingers']['flat_'.$fingerName]['image_quality'] : '?' }}</button>
                                    <a style="width: 50%" title="{{ $value['title'] }} | Kualitas: {{ array_has($dataFinger['fingers'], "flat_{$fingerName}.image_quality") ? $dataFinger['fingers']['flat_'.$fingerName]['image_quality'] : '?' }}" href="{{ $file['baseUrl']."/{$value['flat_name']}" }}?{{ time() }}" class="image-popup btn red  btn-xs"><i class="fa fa-search-plus"></i></a>
                                </div>
                                
                            </div>
                        </div>
                    @endforeach
                </div>
        	</div>
        </div>
    </div>

	<div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">{{ $finger[$type]['title'] }}</div>
        </div>
        <div class="portlet-body form">
        	<div class="form-body">
                {{-- Hide rotate button, still error in cropping result
                <div class="m-b-20 text-center">
                    <div class="btn-group">
                        <button type="button" id="rotate_left" class="btn blue" title="Rotate Left">
                            <span class="fa fa-rotate-left"></span>
                        </button>
                        <button type="button" id="rotate_right" class="btn blue" title="Rotate Right">
                            <span class="fa fa-rotate-right"></span>
                        </button>
                    </div>
                </div> --}}
	        	<div class="row">
	        		<div class="col-sm-12" style="height: 500px;">
	        			  <!-- This is the image we're attaching Jcrop to -->
      						<img src="{{ $file['baseUrl']."/{$side}" }}" class="" id="cropbox" />

      						<input type="hidden" id="x" name="x" value="{{ array_has($dataFinger['fingers'], "flat_{$type}.point_crop.x") ? trim($dataFinger['fingers']["flat_{$type}"]['point_crop']['x']) : trim($pos[0]) }}" />
      						<input type="hidden" id="y" name="y" value="{{ array_has($dataFinger['fingers'], "flat_{$type}.point_crop.y") ? trim($dataFinger['fingers']["flat_{$type}"]['point_crop']['y']) : trim($pos[1]) }}" />
      						<input type="hidden" id="w" name="w" value="{{ array_has($dataFinger['fingers'], "flat_{$type}.point_crop.w") ? trim($dataFinger['fingers']["flat_{$type}"]['point_crop']['w']) : trim($pos[2]) }}" />
      						<input type="hidden" id="h" name="h" value="{{ array_has($dataFinger['fingers'], "flat_{$type}.point_crop.h") ? trim($dataFinger['fingers']["flat_{$type}"]['point_crop']['h']) : trim($pos[3]) }}" />
                  <input type="hidden" id="r" name="r" value="0" />
	        		</div>
	        	</div>
        	</div>
        </div>
    </div>

    <div class="clearfix m-b-30">
        <div class="alignleft clearfix m-b-30">
            <div class="">
                <label class="label-checkbox"> {!! Form::checkbox('skip', 1, $noFinger, ['class' => 'icheck']); !!} Data jari tidak tersedia (Centang jika jari buntung)
                </label>
            </div>
        </div>
        <div class="text-center">
            <div class="btn-group">
                @if ( session('process.mode') != 'edit' )
                <a class="btn red uiblock-on btn-lg uppercase" tabindex="37" href="{{ route('demographic_card_scan_record') }}"><i class="fa fa-times m-r-15"></i> Batal</a>
                @endif

                @if ( $beforeFinger !== false )
                <a class="btn red uiblock-on btn-lg uppercase" tabindex="37" href="{{ route('demographic_finger_flat', [$beforeFinger]) }}"><i class="fa fa-angle-left m-r-15"></i> Kembali  ({{$finger[$beforeFinger]['title']}})</a>
                @endif

                @if (!$nextFinger)
                <button class="btn blue uiblock-on btn-lg uppercase" tabindex="1" type="submit">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></button>
                @else
                <button class="btn blue uiblock-on btn-lg uppercase" tabindex="1" type="submit">Selanjutnya ({{$finger[$nextFinger]['title']}}) <i class="fa fa-angle-right m-l-15"></i></button>
                @endif

                {{--  @if ( $cropFlatStatus )
                <button class="btn green uiblock-on btn-lg uppercase" tabindex="2" name="identification" type="submit">Lanjut ke Identifikasi <i class="fa fa-check m-l-15"></i></button> 
                @endif  --}}

                @if ( session('process.mode') == 'edit' )
                <a class="btn green uiblock-on btn-lg uppercase" tabindex="2" href="{{ route('demographic_formula') }}">Lanjut ke Perumusan <i class="fa fa-check m-l-15"></i></a> 
                @endif
            </div>
        </div>
    </div>

{!! Form::close() !!}

<script type="text/javascript">
var selection = {{ $selection }};
head.ready(function() {
    $(window).load(function() {
        /* Act on the event */
        var type = "<?php echo $finger[$type]['title']; ?>";
        setTimeout(function() {
            new Noty({
                type: 'info',
                layout: 'topRight',
                theme: 'metroui',
                text: "Silahkan crop <strong>" + type + "</strong>",
                timeout: 8000,
                progressBar: true,
                closeWith: ['button'],
                animation: {
                    open: 'noty_effects_open',
                    close: 'noty_effects_close'
                }
            }).show();
        }, 500);
    });

    jQuery(document).ready(function($) {
        $('.equal-height').matchHeight();

        $('#cropbox').cropper({
          aspectRatio: 3 / 4,
          background: false,
          zoomable: false,
          viewMode: 0,
          dragMode: 'move',
          data: {
            x: {{ array_has($dataFinger['fingers'], "flat_{$type}.point_crop.x") ? trim($dataFinger['fingers']["flat_{$type}"]['point_crop']['x']) : trim($pos[0]) }},
            y: {{ array_has($dataFinger['fingers'], "flat_{$type}.point_crop.y") ? trim($dataFinger['fingers']["flat_{$type}"]['point_crop']['y']) : trim($pos[1]) }},
            width: {{ array_has($dataFinger['fingers'], "flat_{$type}.point_crop.w") ? trim($dataFinger['fingers']["flat_{$type}"]['point_crop']['w']) : trim($pos[2]) }},
            height: {{ array_has($dataFinger['fingers'], "flat_{$type}.point_crop.h") ? trim($dataFinger['fingers']["flat_{$type}"]['point_crop']['h']) : trim($pos[3]) }}
          },
          crop: function(e) {
            // Output the result data for cropping image.
            $('#x').val(e.x);
            $('#y').val(e.y);
            $('#w').val(e.width);
            $('#h').val(e.height);
            $('#r').val(e.rotate);
          },
          built: function () {
            $('#rotate_right').click( function() {
                $('#cropbox').cropper('rotate', 3);
            });
            $('#rotate_left').click( function() {
                $('#cropbox').cropper('rotate', -3);
            });
          }
        });
    });
});
</script>

@endsection