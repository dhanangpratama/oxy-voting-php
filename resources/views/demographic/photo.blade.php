@extends('layouts.main')
@section('content')

@include('layouts.indicator')
    
{!! Form::open(['class' => 'overlay']) !!}
<div class="camera-wrapper bg-main-color p-20 m-b-20">
     <div id="camera" class="row {{ $file ? 'hide' : '' }}">
          <div class="col-sm-12">
          <select class="video-source"></select>    
               <div class="camera-preview" style="overflow: hidden">
                    <video muted autoplay class="video-webcam"></video>
                    <div class="face-holder front hide" ></div>
               </div>
          </div>
     </div>

     <img id="img_result" src="{{ $file ? data_url('demographic/photo.jpg') : '' }}?{{ time() }}" class="img-fullwidth {{ $file ? '' : 'hide' }}" alt="">
</div>

<div class="text-center m-b-30">
    <button type="button" title="Ambil Foto" id="photo" class="btn btn-lg blue {{ $file ? 'hide' : '' }}"><i class="fa fa-camera"></i></button>
</div>

<div class="text-center">
    <div class="btn-group">
        <a href="{{ route('demographic_create') }}" class="btn red uiblock-on btn-lg uppercase" name="back"><i class="fa fa-angle-left m-r-15"></i> Kembali</a>
        <button type="button" id="repeat" class="btn yellow btn-lg uppercase"><i class="fa fa-refresh m-r-15"></i> Ulangi</button>
        <button type="submit" class="btn green btn-lg uppercase">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></button>
    </div>
</div>
{!! Form::close() !!}

@endsection

@push('js')
<script type="application/javascript">
    head.load("{{ asset('js/jquery.webcam.js') }}?{{ time() }}", function(){

        $('#repeat').click(function()
        {
            $('#camera, #photo').removeClass('hide');
            $('#img_result').addClass('hide');
        });
        var camera = $('video');

        camera.webcam({
            videoDeviceName: "C922 Pro Stream Webcam",
            videoSelect: document.querySelector('select.video-source')
        });

        $('#photo').on('click', onClickCapture);

        function onClickCapture() {
            var btn = this;
            camera.webcam('captureImage', function(image)
            {
                $.ajax({
                    url: home_url + '/ajax/process-photo',
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function() {
                        App.blockUI();
                    },
                    data: {
                        imageData: image 
                    },
                    cache: false,
                    error: function(xhr, status, errorThrown) {
                        console.log(errorThrown);
                         alertify.alert("Gagal merekam gambar");
                    },
                    success: function(response, status, xhr) {
                        App.unblockUI();

                        if ( response.status == 'OK' )
                        {
                            $("#img_result").attr("src", response.image);
                            {{--   $("input[type=hidden]", btn).val(response.image);  --}}

                            $('#camera, #photo').addClass('hide');
                            $('#img_result').removeClass('hide');

                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                theme: 'metroui',
                                text: 'Foto berhasil direkam',
                                timeout: 3000,
                                progressBar: true,
                                closeWith: ['button'],
                                animation: {
                                    open: 'noty_effects_open',
                                    close: 'noty_effects_close'
                                }
                            }).show();
                        }
                        else
                        {
                            alertify.alert("Gagal merekam gambar");
                        }
                    }
                });
            });
        }

        $('.remove-image').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var thisButton = $(this);
            var textRemove = thisButton.attr('data-remove');

            swal({
                html: 'Yakin akan menghapus foto ini?',
                type: 'warning',
                padding: 50,
                width: 600,
                showCancelButton: true,
                confirmButtonColor: '#0376B8',
                cancelButtonColor: '#e12330',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                allowOutsideClick: false
            }).then(function () {
                $.ajax({
                    url: home_url + '/ajax/remove-photo',
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function() {
                        App.blockUI();
                    },
                    data: {
                        image: thisButton.attr('data-photo')
                    },
                    cache: false,
                    error: function(xhr, status, errorThrown) {
                            alertify.alert("Gagal menghapus foto");
                    },
                    success: function(response, status, xhr) {
                        App.unblockUI();

                        if (response.status == 'OK')
                        {
                            thisButton.closest('button').children('img').attr('src', response.noImageUrl);
                            $(textRemove).val('');

                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                theme: 'metroui',
                                text: 'Foto berhasil dihapus',
                                timeout: 8000,
                                progressBar: true,
                                closeWith: ['button'],
                                animation: {
                                    open: 'noty_effects_open',
                                    close: 'noty_effects_close'
                                }
                            }).show();
                        }
                        else
                        {
                            alertify.alert(response.message);
                        }
                    }
                });
            });
        });
    });

</script>
@endpush