@extends('layouts.main')
@section('content')

@include('layouts.indicator')
    
<div class="portlet box dark">
        <div class="portlet-body form">
            <div class="form-body">
                  <div class="row">
                        <div class="col-sm-6">
                            @if ( File::exists(config('path.data.complete') . DIRECTORY_SEPARATOR . array_get($data, 'demographic_id') . DIRECTORY_SEPARATOR . 'idcard.pdf' ) )
                            <iframe src="{{ url('pdfjs/web/viewer.html?file=' . data_url('demographic/idcard.pdf')) }}" frameborder="0"></iframe>
                            @endif
                        </div>
                        <div class="col-sm-6">
                          <div class="row data">
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    {!! Form::text('nik', array_get($data, 'nik'), ['id' => 'nik', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="nik">N.I.K</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('name', array_get($data, 'name'), ['id' => 'fullname', 'tabindex' => 2,'class' => 'form-control infoable', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}

                                    <label for="name">Nama</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    {!! Form::text('pob', array_get($data, 'pob'), ['id' => 'pob', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}

                                    <label for="pob">Tempat Lahir</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('dob', format_date(array_get($data, 'dob')), ['id' => 'dob', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}

                                    <label for="dob">Tanggal Lahir</label>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input "> 
                                    {!! Form::text('sex', sex_label(array_get($data, 'sex')), ['id' => 'sex', 'tabindex' => 2,'class' => 'form-control infoable', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}

                                    <label for="sex">Jenis Kelamin</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('religion', array_get($data, 'religion_data.label'), ['id' => 'religion', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="religion">Agama</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('blood_type', array_get($data, 'blood_type'), ['id' => 'blood_type', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="blood_type">Golongan Darah</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('marital_status', array_get($data, 'marital_status'), ['id' => 'marital_status', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="marital_status">Status Perkawinan</label>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group form-md-line-input  ">
                                    <textarea class="form-control infoable" rows="2" id="address" name="address" tabindex="11" disabled>{!! array_get($data, 'address') !!}</textarea>
                                    
                                    <label for="note">Alamat</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('rt', array_get($data, 'rt'), ['id' => 'rt', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="rt">RT</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('rw', array_get($data, 'rw'), ['id' => 'rw', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="rw">RW</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('village', array_get($data, 'village'), ['id' => 'village', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="village">Kelurahan</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('district', array_get($data, 'district'), ['id' => 'district', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="district">Kecamatan</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('regent', array_get($data, 'regent'), ['id' => 'regent', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="regent">Kabupaten</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('province', array_get($data, 'province'), ['id' => 'province', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="province">Provinsi</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('country', array_get($data, 'country'), ['id' => 'country', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="country">Negara</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('postal_code', array_get($data, 'postal_code'), ['id' => 'postal_code', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="postal_code">Kodepos</label>
                                </div>
                            </div>
                        </div>
                        </div>
                  </div>  
            </div>
        </div>
    </div>

<div class="text-center">
    <div class="btn-group m-b-30">
        <a href="{{ route('demographic_photo') }}" class="btn red uiblock-on btn-lg uppercase" tabindex="12"><i class="fa fa-angle-left m-r-10"></i>Kembali</a> 
        <a href="{{ route('demographic_create') }}" class="btn yellow uiblock-on btn-lg uppercase" tabindex="12"><i class="fa fa-pencil m-r-10"></i>Ubah</a> 
        <a href="{{ route('demographic_create', ['action' => 'new']) }}" class="btn blue uiblock-on btn-lg uppercase" tabindex="12"><i class="fa fa-print m-r-10"></i>Buat Baru</a> 
    </div>
</div>

@endsection