@extends('layouts.main')
@section('content')

@include('layouts.indicator')
    
{!! Form::open(['method'=>'POST', 'class' => 'overlay'])  !!}
<div id="form_scroll" class="front">
    <div class="portlet box dark">
        <div class="portlet-body form">
            <div class="form-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="row data">
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    {!! Form::text('nik', old('nik', session('demographic.data.nik')), ['id' => 'nik', 'tabindex' => 1,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="nik">NIK</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('name', old('name', session('demographic.data.name')), ['id' => 'name', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="name">Nama</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('dob', old('dob', session('demographic.data.dob')), ['id' => 'dob', 'tabindex' => 3,'class' => 'form-control mask_date2', 'autocomplete' => 'off']) !!}
                                    <label for="dob">Tanggal Lahir</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    {!! Form::text('pob', old('pob', session('demographic.data.pob')), ['id' => 'pob', 'tabindex' => 4,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="pob">Tempat Lahir</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('job', old('job', session('demographic.data.job')), ['id' => 'job', 'tabindex' => 5,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="job">Pekerjaan</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="square-label">Jenis Kelamin</label>
                                    <div class="pophover">
                                        <label class="radio-square-label">
                                            {!! Form::radio('sex', 'm', old('sex', session('demographic.data.sex') == 'm' ? true : !session()->has('demographic.data.sex') ? true : false), ['class' => 'icheck', 'tabindex' => 6, 'id' => 'male']) !!} Laki-laki
                                            <span></span>
                                        </label>
                                        <label class="radio-square-label">
                                            {!! Form::radio('sex', 'f', old('sex', session('demographic.data.sex') == 'f' ? true : false), ['class' => 'icheck', 'tabindex' => 7, 'id' => 'female']) !!} Perempuan
                                            <span></span>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    {!! Form::select('religion_id', $religions, old('religion', session('demographic.data.religion_id')), ['class' => 'form-control', 'tabindex' => 8]) !!}
                                    <label for="religion">Agama</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('blood_type', old('blood_type', session('demographic.data.blood_type')), ['id' => 'blood_type', 'tabindex' => 9,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="blood_type">Golongan Darah</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('marital_status', old('marital_status', session('demographic.data.marital_status')), ['id' => 'marital_status', 'tabindex' => 10,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="marital_status">Status Perkawinan</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('nationality', old('nationality', session('demographic.data.nationality')), ['id' => 'nationality', 'tabindex' => 11,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="nationality">Kewarganegaraan</label>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group form-md-line-input  ">
                                    {!! Form::textarea('address', old('address', session('demographic.data.address')), ['id' => 'address', 'tabindex' => 12,'class' => 'form-control', 'autocomplete' => 'off', 'rows' => 2]) !!}
                                    <label for="note">Alamat</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('rt', old('rt', session('demographic.data.rt')), ['id' => 'rt', 'tabindex' => 13,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="rt">RT</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('rw', old('rw', session('demographic.data.rw')), ['id' => 'rw', 'tabindex' => 14,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="rw">RW</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('village', old('village', session('demographic.data.village')), ['id' => 'village', 'tabindex' => 15,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="village">Kelurahan</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('district', old('district', session('demographic.data.district')), ['id' => 'district', 'tabindex' => 15,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="district">Kecamatan</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('regent', old('regent', session('demographic.data.regent')), ['id' => 'regent', 'tabindex' => 16,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="regent">Kabupaten</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('province', old('province', session('demographic.data.province')), ['id' => 'province', 'tabindex' => 17,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="province">Provinsi</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('country', old('country', session('demographic.data.country', 'Indonesia')), ['id' => 'country', 'tabindex' => 18,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="country">Negara</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('postal_code', old('postal_code', session('demographic.data.postal_code')), ['id' => 'postal_code', 'tabindex' => 19,'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                    <label for="postal_code">Kodepos</label>
                                </div>
                            </div>
                            
                            {{--  <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    <select name="koordinator" class="form-control pophover infoable" id="status" tabindex="0">
                                        <option>-- Pilih salah satu --</option>
                                        <option value="anang" selected>Anang Sitompul</option>
                                        <option value="Jaja Miharja">Jaja Miharja</option>
                                    </select>

                                    <label for="koordinator">Koordinator</label>
                                </div>
                            </div>  --}}
                        </div>
                    </div>
                  </div>  
            </div>
        </div>
    </div>
    </div>
</div>

<div class="text-center">
    <div class="btn-group m-b-30">
        <button type="submit" class="btn blue btn-lg uppercase" tabindex="12">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></button> 
    </div>
</div>
{!! Form::close() !!}

@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        var eKtp = $('#searchEKtp');
        var name = $('#fullname');
        var job = $('#job');

        BioverKtpel.Init({
            OnDataReceived: function(ktp){
                /**
                 * Available KTP Field Name:
                 * Nik, Name, Address, Neighbourhood, CommunityAssociation, District, Village,
                 * Gender, BloodType, Religion, MarriageStatus, Occupation, PlaceOfBirth,
                 * DateOfBirth, Province, City, DateOfExpiry, Nationality;
                 */

                if ( ktp.Nik != null )
                {
                    console.log(ktp);
                    name.val(ktp.Name);
                    job.val(ktp.Occupation);
                    $('#name').val(ktp.Name);
                    $('#nik').val(ktp.Nik);
                    $('#dob').val(ktp.DateOfBirth);
                    $('#pob').val(ktp.PlaceOfBirth);
                    $('#marital_status').val(ktp.MarriageStatus);
                    $('#province').val(ktp.Province);
                    $('#blood_type').val(ktp.BloodType);
                    $('#address').val(ktp.Address);
                    $('#village').val(ktp.Village);
                    $('#district').val(ktp.District);
                    $('#rw').val(ktp.CommunityAssociation);
                    $('#rt').val(ktp.Neighbourhood);
                    $('#regent').val(ktp.City);
                    $('#nationality').val(ktp.Nationality);
                    $('#regent').val(ktp.City);

                    if( ktp.Gender == 'PEREMPUAN' ) 
                    {
                        $('#female').iCheck('check');
                    }
                    else
                    {
                        $('#male').iCheck('check');
                    }
                }
            },
            OnStartWait: function(){ App.blockUI(); },
            OnStopWait: function() { App.unblockUI(); }
        });

        eKtp.click(function(e) {
            BioverKtpel.ReadKtpEl();
        });

        //Autoread on load, in case there is ktp on device
        BioverKtpel.ReadKtpEl();
    });
});
</script>
@endpush