@extends('layouts.main')
@section('content')

<div class="portlet box dark">
    <div class="portlet-body text-center white">
        <h2 class="m-t-10 uppercase">Pengguna Telah Terdaftar</h2>
    </div>
</div>

<div class="portlet box dark">
        <div class="portlet-body form">
            <div class="form-body">
                  <div class="row">
                        <div class="col-sm-5">
                            @if ($photo)
                            <img src="{{ data_url('complete/'.session('found_data.demographic_id').'/photo.jpg') }}" alt="photo" class="img-fullwidth">
                            @endif
                        </div>
                        <div class="col-sm-7">
                          <div class="row data">
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    {!! Form::text('nik', array_get($data, 'nik'), ['id' => 'nik', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="nik">N.I.K</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('name', array_get($data, 'name'), ['id' => 'fullname', 'tabindex' => 2,'class' => 'form-control infoable', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}

                                    <label for="name">Nama</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    {!! Form::text('pob', array_get($data, 'pob'), ['id' => 'pob', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}

                                    <label for="pob">Tempat Lahir</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('dob', format_date(array_get($data, 'dob')), ['id' => 'dob', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}

                                    <label for="dob">Tanggal Lahir</label>
                                </div>
                            </div>
                            
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input "> 
                                    {!! Form::text('sex', sex_label(array_get($data, 'sex')), ['id' => 'sex', 'tabindex' => 2,'class' => 'form-control infoable', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}

                                    <label for="sex">Jenis Kelamin</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('religion', array_get($data, 'religion_data.label'), ['id' => 'religion', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="religion">Agama</label>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('blood_type', array_get($data, 'blood_type'), ['id' => 'blood_type', 'tabindex' => 2,'class' => 'form-control', 'autocomplete' => 'off', 'disabled' => 'disabled']) !!}
                                    <label for="blood_type">Golongan Darah</label>
                                </div>
                            </div>
                            
                            <div class="col-sm-12">
                                <div class="form-group form-md-line-input  ">
                                    <textarea class="form-control infoable" rows="2" id="address" name="address" tabindex="11" disabled>{!! array_get($data, 'address') !!}</textarea>
                                    
                                    <label for="note">Alamat</label>
                                </div>
                            </div>
                        </div>
                        </div>
                  </div>  
            </div>
        </div>
    </div>

<div class="text-center">
    <div class="btn-group m-b-30">
        {{--  <a href="{{ route('demographic_create') }}" class="btn yellow uiblock-on btn-lg uppercase" tabindex="12"><i class="fa fa-pencil m-r-10"></i>Ubah</a>   --}}
        <a href="{{ route('demographic_create', ['action' => 'new']) }}" class="btn blue uiblock-on btn-lg uppercase" tabindex="12"><i class="fa fa-user m-r-10"></i>Buat Baru</a> 
    </div>
</div>

@endsection