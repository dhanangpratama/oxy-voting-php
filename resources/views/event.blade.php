@extends('layouts.main')
@section('content')

<div class="event-form-wrapper">
{!! Form::open(['class' => 'event-form', 'id' => 'event']) !!}
    <h1 class="white">Masukkan Nama Acara</h1>

    <div class="row">
        <div class="col-md-10">
            <div class="form-group form-md-line-input {{ ! empty($errors->first('name')) ? 'has-error' : '' }}">
                {!! Form::text("name",  "", ['class' => 'form-control input-lg', 'id' => 'name']) !!}
                {!! ! empty($errors->first('name')) ? '<span id="error-name" class="help-block">'.$errors->first('name').'</span>' : '' !!}
            </div>
        </div>

        <div class="col-md-2">
            <button type="submit" class="btn green btn-lg btn-block uppercase" style="margin-top: 25px">Pilih</button>
        </div>
    </div>
{!! Form::close() !!}
</div>

@endsection

@push('js')
<script type="application/javascript">
var Event = function() {

    var handleEvent = function() {
        jQuery('.event-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Tidak boleh kosong"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            submitHandler: function(form) 
            {
                App.blockUI();
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.event-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.event-form').validate().form()) {
                    App.blockUI();
                    $('.event-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {
            handleEvent();
        }

    };

}();
    
head.ready(function() {
    jQuery(document).ready(function($) {
        Event.init();

        $(window).load(function() {
            $('#name').focus();
        });

        var options = {
            data: {!! $eventsAutocompletion !!},
            list: {
                match: {
                    enabled: true
                }
            }
        };

        $("#name").easyAutocomplete(options);
    });
});
</script>
@endpush