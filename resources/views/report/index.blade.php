@extends('layouts.main')

@section('content')

{!! Form::open(['method'=>'POST', 'class' => 'm-b-30']) !!}
<div id="reportrange" class="btn default no-print">
    <i class="fa fa-calendar"></i> &nbsp;
    <span> </span>
    <b class="fa fa-angle-down"></b>
</div>
@if ( config('ak23.access') == 'web' || config('ak23.report_location_activated') && $networkStatus == 'on' && $server )
<div class="select2-location-wrapper report">
{!! Form::select('location', $workstations, is_null($location) ? 'all' : $location, ['class' => 'form-control select2 search-location', 'id' => 'location_report', 'tabindex' => 1]) !!}
</div>
@endif
{!! Form::hidden('startDate', $startDate, ['id' => 'startDate']) !!}
{!! Form::hidden('endDate', $endDate, ['id' => 'endDate']) !!}
{!! Form::submit('Pilih', ['class' => 'btn btn-circle blue no-print']) !!}

<div class="pull-right no-print">
    <a href="{{ url("report/export/csv/{$rangeDate}") }}" class="btn btn-circle blue uiblock-on">Export CSV</a>
    <button type="button" onclick="window.print()" class="btn btn-circle blue">Print</button>
</div>

{!! Form::close() !!}

<!-- BEGIN INTERACTIVE CHART PORTLET-->
<div class="portlet light portlet-fit no-print">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings "></i>
            <span class="caption-subject  sbold uppercase">Grafik Tahunan</span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="chart_annually" class="chart"></div>
    </div>
</div>
<!-- END INTERACTIVE CHART PORTLET-->

<div class="portlet light portlet-fit ">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-red sbold uppercase">Data Tahunan</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table table-scrollable table-scrollable-borderless">
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">Jumlah Input Sidik Jari</th>
                        <th class="text-center">Kriminal</th>
                        <th class="text-center">Non Kriminal</th>
                        <th class="text-center">AK-24</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['years'] as $year => $value)
                    <tr class="btn-show-location" data-loc-time="{{$year}}" data-loc-type="year" data-loc-id="{{ md5('year' . $year) }}">
                        <td><strong>{{ $year }}</strong></td>
                        <td class="text-center">{{ $value['input_fingers_year'] }}</td>
                        <td class="text-center">{{ $value['criminal_year'] }}</td>
                        <td class="text-center">{{ $value['non_criminal_year'] }}</td>
                        <td class="text-center">{{ $value['ak24_year'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- BEGIN INTERACTIVE CHART PORTLET-->
<div class="portlet light portlet-fit no-print">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings "></i>
            <span class="caption-subject  sbold uppercase">Grafik Bulanan</span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="chart_monthly" class="chart"></div>
    </div>
</div>
<!-- END INTERACTIVE CHART PORTLET-->

<div class="portlet light portlet-fit ">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-red sbold uppercase">Data Bulanan</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table table-scrollable table-scrollable-borderless">
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">Jumlah Input Sidik Jari</th>
                        <th class="text-center">Kriminal</th>
                        <th class="text-center">Non Kriminal</th>
                        <th class="text-center">AK-24</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['months'] as $keyMonth => $value)
                    <tr class="btn-show-location" data-loc-time="{{$keyMonth}}" data-loc-type="month" data-loc-id="{{ md5('month' . $keyMonth) }}">
                        <td><strong>{{ $keyMonth }}</strong></td>
                        <td class="text-center">{{ $value['input_fingers_month'] }}</td>
                        <td class="text-center">{{ $value['criminal_month'] }}</td>
                        <td class="text-center">{{ $value['non_criminal_month'] }}</td>
                        <td class="text-center">{{ $value['ak24_month'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- BEGIN INTERACTIVE CHART PORTLET-->
<div class="portlet light portlet-fit no-print">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings "></i>
            <span class="caption-subject  sbold uppercase">Grafik Mingguan</span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="chart_weekly" class="chart"></div>
    </div>
</div>
<!-- END INTERACTIVE CHART PORTLET-->

<div class="portlet light portlet-fit ">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-red sbold uppercase">Data Mingguan</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table table-scrollable table-scrollable-borderless">
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">Jumlah Input Sidik Jari</th>
                        <th class="text-center">Kriminal</th>
                        <th class="text-center">Non Kriminal</th>
                        <th class="text-center">AK-24</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['weeks'] as $keyWeeks => $value)
                    <tr class="btn-show-location" data-loc-time="{{$keyWeeks}}" data-loc-type="week" data-loc-id="{{ md5('week' . $keyWeeks) }}">
                        <td><strong>{{ $keyWeeks }}</strong></td>
                        <td class="text-center">{{ $value['input_fingers_week'] }}</td>
                        <td class="text-center">{{ $value['criminal_week'] }}</td>
                        <td class="text-center">{{ $value['non_criminal_week'] }}</td>
                        <td class="text-center">{{ $value['ak24_week'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- BEGIN INTERACTIVE CHART PORTLET-->
<div class="portlet light portlet-fit no-print">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-settings "></i>
            <span class="caption-subject  sbold uppercase">Grafik Harian</span>
        </div>
    </div>
    <div class="portlet-body">
        <div id="chart_daily" class="chart"></div>
    </div>
</div>
<!-- END INTERACTIVE CHART PORTLET-->

<div class="portlet light portlet-fit ">
    <div class="portlet-title">
        <div class="caption">
            <span class="caption-subject font-red sbold uppercase">Data Harian</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table table-scrollable table-scrollable-borderless">
            <table class="table">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-center">Jumlah Input Sidik Jari</th>
                        <th class="text-center">Kriminal</th>
                        <th class="text-center">Non Kriminal</th>
                        <th class="text-center">AK-24</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($data['days'] as $keyDay => $value)
                    <tr class="btn-show-location" data-loc-time="{{$keyDay}}" data-loc-type="day" data-loc-id="{{ md5('day' . $keyDay) }}">
                        <td><strong>{{ $keyDay }}</strong></td>
                        <td class="text-center">{{ $value['input_fingers_day'] }}</td>
                        <td class="text-center">{{ $value['criminal_day'] }}</td>
                        <td class="text-center">{{ $value['non_criminal_day'] }}</td>
                        <td class="text-center">{{ $value['ak24_day'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection

@push('js')
<script> 
head.ready(function() {
    jQuery(document).ready(function($) {
        console.log(moment().startOf("year").format('MMMM D, YYYY'));
        var start = moment("{{ $startDate }}");
        var end = moment("{{ $endDate }}");

        function cb(start, end) 
        {
            $('#reportrange span').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            linkedCalendars: false,
            ranges: {
            'Hari Ini': [moment(), moment()],
            'Kemarin': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 Hari Terakhir': [moment().subtract(6, 'days'), moment()],
            '30 Hari Terakhir': [moment().subtract(29, 'days'), moment()],
            'Bulan Ini': [moment().startOf('month'), moment().endOf('month')],
            'Bulan Kemarin': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
            'Tahun Ini': [moment().startOf('year'), moment().endOf('year')],
            'Tahun Kemarin': [moment().subtract(1, 'year').startOf('year'), moment().subtract(1, 'year').endOf('year')],
            },
            buttonClasses: ['btn'],
            applyClass: 'green',
            cancelClass: 'default',
            format: 'DD/MM/YYYY',
            separator: ' to ',
            locale: {
                applyLabel: 'Apply',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Tanggal Kustom',
                daysOfWeek: ['Mi', 'Se', 'Sel', 'Ra', 'Ka', 'Ju', 'Sa'],
                monthNames: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                firstDay: 1
            }
        }, cb);

        cb(start, end);

        $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
            console.log(picker.startDate.format('YYYY-MM-DD'));
            console.log(picker.endDate.format('YYYY-MM-DD'));

            $('#startDate').val(picker.startDate.format('YYYY-MM-DD'));
            $('#endDate').val(picker.endDate.format('YYYY-MM-DD'));
            // $('#date_range_form')
            });
    });
    var dataAnnually = {!! $data['input_fingers_year_data'] !!},
        dataCriminalAnnually = {!! $data['criminal_year_data'] !!},
        dataNonCriminalAnnually = {!! $data['non_criminal_year_data'] !!},
        dataAk24Annually = {!! $data['ak24_year_data'] !!},
        dataMonthly = {!! $data['input_fingers_month_data'] !!},
        dataCriminalMonthly = {!! $data['criminal_month_data'] !!},
        dataNonCriminalMonthly = {!! $data['non_criminal_month_data'] !!},
        dataAk24Monthly = {!! $data['ak24_month_data'] !!},
        dataWeekly = {!! $data['input_fingers_week_data'] !!},
        dataCriminalWeekly = {!! $data['criminal_week_data'] !!},
        dataNonCriminalWeekly = {!! $data['non_criminal_week_data'] !!},
        dataAk24Weekly = {!! $data['ak24_week_data'] !!},
        dataDaily = {!! $data['input_fingers_day_data'] !!},
        dataCriminalDaily = {!! $data['criminal_day_data'] !!},
        dataNonCriminalDaily = {!! $data['non_criminal_day_data'] !!};
        dataAk24Daily = {!! $data['ak24_day_data'] !!};

    var dataset_annually = [
        {
            label: "Input Sidik Jari",
            data: dataAnnually,
            shadowSize: 4
        },
        {
            label: "Kriminal",
            data: dataCriminalAnnually,
            shadowSize: 4
        },
        {
            label: "Non Kriminal",
            data: dataNonCriminalAnnually,
            shadowSize: 4
        },
        {
            label: "AK-24",
            data: dataAk24Annually,
            shadowSize: 4
        }
    ];

    var dataset_monthly = [
        {
            label: "Input Sidik Jari",
            data: dataMonthly,
            shadowSize: 4
        },
        {
            label: "Kriminal",
            data: dataCriminalMonthly,
            shadowSize: 4
        },
        {
            label: "Non Kriminal",
            data: dataNonCriminalMonthly,
            shadowSize: 4
        },
        {
            label: "AK-24",
            data: dataAk24Monthly,
            shadowSize: 4
        }
    ];

    var dataset_weekly = [
        {
            label: "Input Sidik Jari",
            data: dataWeekly,
            shadowSize: 4
        },
        {
            label: "Kriminal",
            data: dataCriminalWeekly,
            shadowSize: 4
        },
        {
            label: "Non Kriminal",
            data: dataNonCriminalWeekly,
            shadowSize: 4
        },
        {
            label: "AK-24",
            data: dataAk24Weekly,
            shadowSize: 4
        }
    ];

    var dataset_daily = [
        {
            label: "Input Sidik Jari",
            data: dataDaily,
            shadowSize: 4
        },
        {
            label: "Kriminal",
            data: dataCriminalDaily,
            shadowSize: 4
        },
        {
            label: "Non Kriminal",
            data: dataNonCriminalDaily,
            shadowSize: 4
        },
        {
            label: "AK-24",
            data: dataAk24Daily,
            shadowSize: 4
        }
    ];

    var options = {
        series: {
            bars: {
                show: true,
                barWidth: 0.2,
                lineWidth: 0,
                order: 1, 
                fillColor: {
                    colors: [{
                        opacity: 1
                    }, {
                        opacity: 1
                    }]
                }
            },
            shadowSize: 1
        },
        grid: {
            borderWidth: 0,
            hoverable: true,
            clickable: true
        },
        colors: ["#37b7f", "#FF0000", "#3dcc16", "#c49f47"],
        xaxis: {
            axisLabel: "Tanggal",
            mode: "categories",
            tickLength: 0,
            ticks: null,
            tickDecimals: 0,
            tickColor: "#2B3036"
        },
        yaxis: {
            axisLabel: "Jumlah Data",
            ticks: null,
            tickDecimals: 0,
            tickColor: "#2B3036"
        }
    };

    function showTooltip(x, y, color, contents) {
        $('<div id="tooltip">' + contents + '</div>').css({
            position: 'absolute',
            display: 'none',
            top: y + 5,
            left: x + 15,
            border: '2px solid ' + color,
            padding: '3px',
                'font-size': '9px',
                'border-radius': '5px',
                'background-color': '#fff',
                'font-family': 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
            opacity: 0.9
        }).appendTo("body").fadeIn(200);
    }

    var previousPoint = null,
        previousLabel = null;

    jQuery(document).ready(function() {
        $.plot($("#chart_annually"), dataset_annually, options);
        $.plot($("#chart_monthly"), dataset_monthly, options);
        $.plot($("#chart_weekly"), dataset_weekly, options);
        $.plot($("#chart_daily"), dataset_daily, options);
        $("#chart_monthly").on("plothover", function (event, pos, item) {
            if (item) {
                if ((previousLabel != item.series.label) || (previousPoint != item.dataIndex)) {
                    previousPoint = item.dataIndex;
                    previousLabel = item.series.label;
                    $("#tooltip").remove();

                    var x = item.datapoint[0];
                    var y = item.datapoint[1];

                    var color = item.series.color;

                    //console.log(item.series.xaxis.ticks[x].label);               

                    showTooltip(item.pageX,
                    item.pageY,
                    color,
                        "<strong>" + item.series.label + "</strong><br>Tanggal " + item.series.xaxis.ticks[x].label + " : <strong>" + y + "</strong> data");
                }
            } else {
                $("#tooltip").remove();
                previousPoint = null;
            }
        });
    });

    jQuery(document).ready(function () {
        var $location = '{{ $location }}';
        if ($location == '')
        {
            try {
                onBtnShowLocationClick();

            } catch (err) {
                $("body").css("cursor", "default");
                console.log(err);
            }


        }

        $('form').submit(function() {
            App.blockUI();
        });

        function onBtnShowLocationClick()
        {
            var $btn_show_location = $('.btn-show-location');

            $btn_show_location.addClass('btn-show-location-clickable');


            $btn_show_location.click(function () {

                var $el = $(this);
                var $location_type = $el.data('loc-type');
                var $location_time = $el.data('loc-time');
                var $location_id = $el.data('loc-id');
                var $showing = $el.data('showing');
                var $generated = $el.data('generated');

                if ($showing == true) {
                    $('[data-loc-parent-id="' + $location_id + '"]').hide();
                    $el.data('showing', false);
                    return true;
                }

                if ($showing == false && $generated == true) {
                    $('[data-loc-parent-id="' + $location_id + '"]').show();
                    $el.data('showing', true);
                    return true;

                }

                var $startDate = null;
                var $endDate = null;

                switch ($location_type) {
                    case 'year':
                        $startDate = $location_time + '-01-01';
                        $endDate = $location_time + '-12-31';
                        break;

                    case 'month':
                        var month = new Date(Date.parse($location_time)).getMonth() + 1;
                        var year = new Date(Date.parse("May 2016")).getFullYear();
                        var lastDay = new Date(year, month, 0);
                        lastDay = ("0" + lastDay.getDate()).slice(-2);
                        month = ("0" + (new Date(Date.parse($location_time)).getMonth() + 1)).slice(-2);

                        $startDate = year + '-' + month + '-01';
                        $endDate = year + '-' + month + '-' + lastDay;
                        break;

                    case 'week':
                        var array_week = $location_time.trim().split(' - ');
                        $startDate = array_week[0].trim();
                        $endDate = array_week[1].trim();
                        break;

                    case 'day':
                        var day = $location_time.split('/');
                        var this_day = day[2] + '-' + day[1] + '-' + day[0];
                        $startDate = this_day;
                        $endDate = this_day;
                        break;


                }

                $("body").css("cursor", "progress");


                var $urlFetchData = '{!! config('url.phpApiServer') !!}report/findAllByDate';
                var request = $.ajax({
                    url: $urlFetchData,
                    method: "GET",
                    data: {
                        startDate: $startDate,
                        endDate: $endDate,
                        reportType: $location_type
                    },
                    dataType: "json"
                });

                request.done(function ($data_locations) {

                    $.each($data_locations.locations, function ($loca_name, $value) {
                        var $tr = $('<tr class="location-child" data-loc-parent-id="' + $location_id + '">');


                        $tr.append('<td><strong>' + $loca_name + '<strong></td>');

                        $.each(['input_fingers', 'criminal', 'non_criminal', 'ak24'], function (i, data_name) {
                            var data_value = $value[$location_type + 's'][$location_time][data_name + '_' + $location_type];
                            $tr.append('<td class="text-center">' + data_value + '</td>');


                        });

                        $el.after($tr);


                    });
                });

                request.fail(function (jqXHR, textStatus) {
                    alert("Request failed: " + textStatus);
                });

                request.always(function (jqXHR, textStatus) {
                    $("body").css("cursor", "default");
                });


                $el.data('showing', true);
                $el.data('generated', true);

            });

        }

    });
});
</script>
@endpush
