@extends('layouts.main')
@section('content')

@if ( session()->has('laten_status') )
<div class="laten-status-wrapper">
    <div class="alert alert-{{ session('laten_status')['type'] }}">
        <p>{{ session('laten_status')['message'] }}</p>
    </div>    
</div>
@endif

@if (count($errors) > 0)
<div class="laten-status-wrapper">
    <div class="alert alert-danger">
        <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
</div>
@endif

<div class="master-laten-wrapper">
    @include('layouts.server_alert')
    @include('layouts.osinafis_alert')

    {!! Form::open(['id' => 'latenForm', 'class' => 'form-horizontal form-bordered', 'files' => true]) !!}
        <div class="laten-form-wrapper">
            <div class="form-body">
                {{--  <div class="form-group">
                    <label class="control-label col-md-3">Wajah</label>
                    <div class="col-md-9">
                        <button type="button" class="btn blue" data-toggle="modal" data-target="#chooseFace">Pilih File</button>
                        {!! Form::hidden('faceFile', null, ['id' => 'faceFile']) !!}
                        {!! Form::hidden('faceFileType', null, ['id' => 'faceFileType']) !!}
                        <div class="help-block">Pilih foto wajah <em>(JPG, PNG dan WSQ)</em></div>
                    </div>
                </div>  --}}

                <div class="form-group">
                    <label class="control-label col-md-3">Jari</label>
                    <div class="col-md-9">
                        <div id="fingerPreview" style="max-width: 150px"></div>
                        <button type="button" class="section-on btn blue btn-lg uppercase" data-toggle="modal" data-target="#chooseFinger">Pilih File</button>
                        <button type="button" class="section-off btn blue btn-lg uppercase" disabled>Pilih File</button>
                        {!! Form::hidden('fingerFile', null, ['id' => 'fingerFile']) !!}
                        {!! Form::hidden('fingerFileType', null, ['id' => 'fingerFileType']) !!}
                        <div class="help-block">Pilih foto sidik jari <em>(JPG, PNG dan WSQ)</em></div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Pilih Jari</label>
                    <div class="col-md-9">
                        {!! Form::select('fingerType', $fingerOptions, null, ['class' => 'form-control select2']) !!}
                        <div class="help-block">Pilih jika jenis sidik jari diketahui</div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-9 col-md-offset-3">
                        {!! Form::submit('CARI', ['class' => 'section-on btn btn-lg uppercase blue']) !!}
                        <button type="button" class="section-off btn blue btn-lg uppercase" disabled>CARI</button>
                    </div>
                </div>
            </div>
        </div>
    {!! Form::close() !!}

    <div class="clearfix"></div>
    
    <p class="text-center white">-- atau --</p>
    <p class="text-center">
    	<a class="section-on btn blue btn-lg uppercase uiblock-on" href="{{ route('laten_scan') }}">Scan Jari</a>
        <button type="button" class="section-off btn blue btn-lg uppercase uiblock-on" disabled>Scan Jari</button>
    </p>
</div>

<!-- Modal -->
<div class="modal fade" id="chooseFinger" tabindex="-1" role="dialog" aria-labelledby="chooseModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="chooseModalLabel">Cari File</h4>
      </div>
      <div class="modal-body">
        <div id="preview" class="pull-right" style="max-width: 100px;"></div>
        <?php 
	
            if($drive != '')
            {	
                $arrayList = driveRead($drive, $arrayList = array());
                
                if(count($arrayList) > 0) 
                {
                    asort($arrayList);
                
                    echo '<ul>';
                    
                    foreach($arrayList as $thisArray) 
                    {
                        foreach($thisArray as $label => $value)
                            $$label = $value;
                        
                        echo '<li><a href="#" class="path-file" data-full="'.$full.'" data-ext="'.$ext.'">'.$name.'</a></li>';
                    }
                    
                    echo '</ul>';	
                }
                else
                    echo 'Tidak menemukan file yang didukung';		
            }
            else
                echo '<p>Mohon untuk menghubungkan USB Drive.</p> <p>Klik tombol dibawah jika sudah menghubungkan USB Drive. Kemudian lakukan proses filih file lagi.</p><p><a href="javascript:reload()" class="btn btn-sm blue">Cek USB Drive</a></p>';	
        ?>

        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn red uppercase btn-lg" data-dismiss="modal">Batal</button>
        <button type="button" class="btn blue uppercase btn-lg" id="btnSelectFinger" disabled>Pilih</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="chooseFace" tabindex="-1" role="dialog" aria-labelledby="chooseModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="chooseModalLabel">Cari File</h4>
      </div>
      <div class="modal-body">
        <?php 
	
            if($drive != '')
            {	
                $arrayList = driveRead($drive, $arrayList = array());
                
                if(count($arrayList) > 0) 
                {
                    asort($arrayList);
                
                    echo '<ul>';
                    
                    foreach($arrayList as $thisArray) 
                    {
                        foreach($thisArray as $label => $value)
                            $$label = $value;
                        
                        echo '<li><a href="#" class="path-face" data-full="'.$full.'" data-ext="'.$ext.'">'.$name.'</a></li>';
                    }
                    
                    echo '</ul>';	
                }
                else
                    echo 'Tidak menemukan file yang didukung';		
            }
            else
                echo 'Mohon untuk menghubungkan USB Drive. <a href="javascript:reload()" class="btn btn-sm blue">Reload</a>';	
        ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn red uppercase btn-lg" data-dismiss="modal">Batal</button>
      </div>
    </div>
  </div>
</div>

@endsection

@push('js')
<script>
function getDetail() {
    $.ajax({
        type: 'POST',
        url: "{{ route('ajax_laten_process_detail') }}",
        cache: false,
        timeout: '{{ config("ak23.nikSearchTimeout") }}',
        async: true,
        dataType: 'json',
        data: {
            key: 0
        },
        beforeSend: function() {
            new Noty({
                type: 'info',
                layout: 'topRight',
                theme: 'metroui',
                text: 'Pencarian laten menemukan beberapa data yang cocok dengan sidik jari yang dicari. Mohon menunggu, aplikasi sedang mengambil detail data.',
                timeout: false,
                progressBar: true,
                closeWith: ['button'],
                animation: {
                    open: 'noty_effects_open',
                    close: 'noty_effects_close'
                }
            }).show();

            console.log('Starting search by NIK...');
        },
        error: function(xmlhttprequest, textstatus, message) {
            App.unblockUI();
            console.log(message);
            if( textstatus === "timeout" ) 
            {  
                swal({
				    title: 'Konfirmasi',
                    html: "<strong>Ma'af, akses ke server KTP-el timeout!</strong><br><br>Hal ini menyebabkan data yang ditampilkan tidak lengkap. Kemungkinan data yang didapatkan hanya <strong>Nama</strong> dan <strong>NIK</strong>.<br><br>Tetap melihat data hasil pencarian?",
                    type: 'info',
                    padding: 50,
                    width: 600,
                    showCancelButton: true,
                    confirmButtonColor: '#0376B8',
                    cancelButtonColor: '#e12330',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    allowOutsideClick: false
                }).then(function () {
                    App.blockUI();
                    window.location.href = '{{ url("laten/result") }}'+'/'+'{{ encrypt(0) }}';
                });  

                return;          
            } 
            else 
            {
                // console.log(message); 

                swal({
				title: 'Konfirmasi',
                    html: "<strong>Ma'af, terjadi kesalahan saat meminta detail data.</strong><br><br>Hal ini menyebabkan data yang ditampilkan tidak lengkap.<br><br>Tetap melihat data hasil pencarian?",
                    type: 'info',
                    padding: 50,
                    width: 600,
                    showCancelButton: true,
                    confirmButtonColor: '#0376B8',
                    cancelButtonColor: '#e12330',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    allowOutsideClick: false
                }).then(function () {
                    App.blockUI();
                    window.location.href = '{{ url("laten/result") }}'+'/'+'{{ encrypt(0) }}';
                });  

                return;
            }
        },
        success: function(data)
        {
            console.log('Search by NIK done, redirecting...');
            console.log(data);

            if ( data.status == 'OK' )
            {
                window.location.href = '{{ url("laten/result") }}'+'/'+'{{ encrypt(0) }}';
                return;
            }
            else
            {
                App.unblockUI();

                swal({
                    title: 'Terjadi Kesalahan',
                    html: '<strong>' + data.message + '</strong><br><br>Hal ini menyebabkan data yang ditampilkan tidak lengkap.<br><br>Tetap melihat data hasil pencarian?',
                    type: 'info',
                    padding: 50,
                    width: 600,
                    showCancelButton: true,
                    confirmButtonColor: '#0376B8',
                    cancelButtonColor: '#e12330',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    allowOutsideClick: false
                }).then(function () {
                    App.blockUI();
                    window.location.href = '{{ url("laten/result") }}'+'/'+'{{ encrypt(0) }}';
                });
            }
        }
    });
}

head.ready(function() {
    jQuery(document).ready(function($) {
        $('form').on('submit', function(e) {
            e.preventDefault();
            $formData = $(this);

            $.ajax({
                type: 'POST',
                url: "{{ route('ajax_laten_search') }}",
                cache: false,
                timeout: '{{ config("ak23.latenSearchTimeout") }}',
                data: $formData.serialize(),
                async: true,
                // dataType: 'json',
                beforeSend: function() {
                    console.log('Laten search started...');
                    App.blockUI();
                },
                error: function(xmlhttprequest, textstatus, message) {
                    App.unblockUI();

                    console.log(message);

                    swal({
                        type: 'error',
                        text: "Ma'af, permintaan ke server timeout!"
                    });
                    
                    return;
                },
                success: function(data)
                {
                    {{--  console.log(data);  --}}

                    if ( data.status == 'OK' )
                    {
                        getDetail();
                        return;
                    }

                    App.unblockUI();

                    if ( data.status == 'NOTFOUND' )
                    {
                        swal({
                            type: 'info',
                            text: "Ma'af, data tidak ditemukan"
                        });
                        return;
                    }

                    if ( data.status == 'ERROR' )
                    {
                        console.log(data.message);
                        swal({
                            type: 'error',
                            text: data.message
                        });
                        return;
                    }
                }
            });

        });

        $('#liveButton').on('click', function(e) {
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: "{{ route('ajax_laten_search_live') }}",
                cache: false,
                timeout: '{{ config("ak23.latenSearchTimeout") }}',
                async: true,
                dataType: 'json',
                beforeSend: function() {
                    console.log('Laten search started...');
                    App.blockUI();
                },
                error: function(xmlhttprequest, textstatus, message) {
                    App.unblockUI();
                    if( textstatus === "timeout" ) 
                    {
                        alertify.alert("<strong>Ma'af, permintaan ke server timeout!</strong><br><br>Mohon untuk mencoba beberapa saat lagi");
                        return;
                    } 
                    else 
                    {
                        console.log(message);
                        alertify.alert("Ma'af, terjadi kesalahan pada aplikasi");
                        return;
                    }
                },
                success: function(data)
                {
                    console.log('Laten search done...');

                    if ( data.status == 'OK' )
                    {
                        searchKtp();
                        return;
                    }

                    App.unblockUI();

                    if ( data.status == 'NOTFOUND' )
                    {
                        alertify.alert("Ma'af, data tidak ditemukan");
                        return;
                    }

                    if ( data.status == 'ERROR' )
                    {
                        console.log(data.message);
                        alertify.alert(data.message);
                        return;
                    }
                }
            });
        });

        $('.path-file').click(function(e)
        {
            e.preventDefault();
            var $this = $(this);

            $('.path-file').removeClass('selected');
            $this.addClass('selected');

            $.ajax({ 
				url: "{{ route('ajax_laten_preview') }}",
				data: {'filename' : $this.data('full'), 'ext' : $this.data('ext')},
				type: 'post',
				beforeSend: function(){
					/*	SHOW PLEASE WAIT MESSAGE OR A LOADING STATE	*/
					
					var thisHTML = '<p>Please wait...</p>';
					$('#preview').html(thisHTML).show();
			   },
				success: function(data)
				{
					/*	SHOW PREVIEW AND ASK THE USER TO CONFIRM IF THEY WANT TO PICKED FILE	*/

                    console.log(data);
					
					var thisHTML = '<img class="img-responsive" src="' + data + '" />';
					$('#preview').html(thisHTML).show();

                    $('#btnSelectFinger').prop("disabled", false);
				},
				error: function ()
				{
                    $('#chooseFinger').modal('hide');

                    swal({
                        html: "Ma'af, File sudah tidak ditemukan.",
                        type: 'error',
                        padding: 30,
                        width: 600,
                        showCancelButton: false,
                        confirmButtonColor: '#0376B8',
                        cancelButtonColor: '#e12330',
                        confirmButtonText: 'OK',
                        cancelButtonText: 'Tidak',
                        allowOutsideClick: false
                    }).then(function () {
                        App.blockUI();
                        window.location.reload(false); 
                    });

					$('#preview').hide();
				}
            });
        });

        $('.path-face').click(function(e)
        {
            e.preventDefault();
            var $this = $(this);
            $('#faceFile').val($this.data('full'));
            $('#faceFileType').val($this.data('ext'));
        });

        $('#btnSelectFinger').click(function(e) {
            e.preventDefault();

            $('#fingerPreview').html('');

            var $this = $('.path-file.selected');

            $('#fingerFile').val($this.data('full'));
            $('#fingerFileType').val($this.data('ext'));
            $('#chooseFinger').modal('hide');
            
            var $t = '{{ time() }}';
            var thisHTML = '<img class="img-responsive" src="'+home_url+'/files/tmp/file-preview.jpg?t='+Math.random()+'" />';
			$('#fingerPreview').html(thisHTML);
        });
    });
});
</script>
@endpush