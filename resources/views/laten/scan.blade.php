@extends('layouts.main')
@section('content')

    {!! Form::open() !!}
     <div id="sectionContainer" class="bg-main-color p-20" style="margin-bottom: 30px;">
          <div class="row">
               <div class="col-sm-6 ">
                    <div class="finger-preview"></div>
               </div>
               <div class="col-sm-6  text-center" style="overflow-y: scroll">
                    <div class="relative">
                        <div id="finger-quality" class=" white"></div>
                    </div>
                    <div id="device-message" class="alert alert-info"></div>
                    <h1 id="finger-seq-name" class="finger-title"></h1>
                    <div class="finger-image">
                         <img id="finger-seq-image" src="{{ asset('assets/images/palm.png') }}" class="img-responsive" alt="hand">
                    </div>
                    
                    <div class="button-wrapper">
                         <p>
                              <button type="button" id="button-start"  class="btn red hide" disabled>Mulai</button>
                         </p>
                    </div>

               </div>
          </div>
     </div>

     @foreach(config('ak23.finger_index') as $finger)
     <?php
     $flat = 'flat_'.$finger;
     ?>
     {!! Form::textarea($finger, null, ['id' => strtoupper($finger), 'class' => 'hidden']) !!}
     {{-- Form::textarea($flat, null, ['id' => strtoupper($flat), 'class' => 'hidden']) --}}
     @endforeach

     <div class="text-center">
        <div class="btn-group">
            <a href="{{ route('laten_scan') }}" class="btn red btn-lg uppercase uiblock-on" diabled><i class="fa fa-repeat m-r-15"></i> Ulangi</a>
            <button type="button" id="button-skip" class="btn red btn-lg uppercase">Lewati <i class="fa fa-chevron-right m-l-15"></i></button>
            <button type="submit" id="button-next"   class="btn blue btn-lg uppercase" disabled>Cari <i class="fa fa-search m-l-15"></i></button>
        </div>
     </div>

     {!! Form::close() !!}


@endsection

@push('js')
<script type="application/javascript">
    head.load("{{ asset('js/livescanfinger.js') }}?{{ time() }}", function(){

        var bodyHeight = $('body').height();
        $('.smhe').height(bodyHeight - 450);

        $.fn.livescanfinger({
            elementFingerScreenPreview: $('.finger-preview'),
            elementDeviceMessage: $("#device-message"),
            elementButtonStart: $("#button-start"),
            elementButtonNext: $("#button-next"),
            elementButtonRepeat: $("#button-repeat"),
            elementButtonSkip: $("#button-skip"),
            elementFingerSeqName: $("#finger-seq-name"),
            elementFingerSeqImage: $("#finger-seq-image"),
            elementFingerQuality: $("#finger-quality"),
            urlBaseImage: "{{ asset('assets/images/') }}/",
            urlBaseImageFingerSequence: "{{ asset('assets/images/fingersequence') }}/"

        });

    });

    function getDetail() {
        $.ajax({
            type: 'POST',
            url: "{{ route('ajax_laten_process_detail') }}",
            cache: false,
            timeout: '{{ config("ak23.nikSearchTimeout") }}',
            async: true,
            dataType: 'json',
            data: {
                key: 0
            },
            beforeSend: function() {
                new Noty({
                    type: 'info',
                    layout: 'topRight',
                    theme: 'metroui',
                    text: 'Pencarian laten menemukan beberapa data yang cocok dengan sidik jari yang dicari. Mohon menunggu, aplikasi sedang mengambil detail data.',
                    timeout: false,
                    progressBar: true,
                    closeWith: ['button'],
                    animation: {
                        open: 'noty_effects_open',
                        close: 'noty_effects_close'
                    }
                }).show();

                console.log('Starting search detail data...');
            },
            error: function(xmlhttprequest, textstatus, message) {
                App.unblockUI();
                console.log(message);
                if( textstatus === "timeout" ) 
                {  
                    swal({
                        title: 'Konfirmasi',
                        html: "<strong>Ma'af, akses ke server KTP-el timeout!</strong><br><br>Hal ini menyebabkan data yang ditampilkan tidak lengkap. Kemungkinan data yang didapatkan hanya <strong>Nama</strong> dan <strong>NIK</strong>.<br><br>Tetap melihat data hasil pencarian?",
                        type: 'info',
                        padding: 50,
                        width: 600,
                        showCancelButton: true,
                        confirmButtonColor: '#0376B8',
                        cancelButtonColor: '#e12330',
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak',
                        allowOutsideClick: false
                    }).then(function () {
                        App.blockUI();
                        window.location.href = '{{ url("laten/result") }}'+'/'+'{{ encrypt(0) }}';
                    });  

                    return;          
                } 
                else 
                {
                    // console.log(message); 

                    swal({
                    title: 'Konfirmasi',
                        html: "<strong>Ma'af, terjadi kesalahan saat meminta detail data.</strong><br><br>Hal ini menyebabkan data yang ditampilkan tidak lengkap.<br><br>Tetap melihat data hasil pencarian?",
                        type: 'info',
                        padding: 50,
                        width: 600,
                        showCancelButton: true,
                        confirmButtonColor: '#0376B8',
                        cancelButtonColor: '#e12330',
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak',
                        allowOutsideClick: false
                    }).then(function () {
                        App.blockUI();
                        window.location.href = '{{ url("laten/result") }}'+'/'+'{{ encrypt(0) }}';
                    });  

                    return;
                }
            },
            success: function(data)
            {
                console.log('Search detail data done, redirecting...');
                console.log(data);

                if ( data.status == 'OK' )
                {
                    window.location.href = '{{ url("laten/result") }}'+'/'+'{{ encrypt(0) }}';
                    return;
                }
                else
                {
                    App.unblockUI();

                    swal({
                        title: 'Terjadi Kesalahan',
                        html: '<strong>' + data.message + '</strong><br><br>Hal ini menyebabkan data yang ditampilkan tidak lengkap.<br><br>Tetap melihat data hasil pencarian?',
                        type: 'info',
                        padding: 50,
                        width: 600,
                        showCancelButton: true,
                        confirmButtonColor: '#0376B8',
                        cancelButtonColor: '#e12330',
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak',
                        allowOutsideClick: false
                    }).then(function () {
                        App.blockUI();
                        window.location.href = '{{ url("laten/result") }}'+'/'+'{{ encrypt(0) }}';
                    });
                }
            }
        });
    }

    head.ready(function() {
        jQuery(document).ready(function($) {
            $('form').on('submit', function(e) {
                e.preventDefault();
                
                $.ajax({
                    type: 'POST',
                    url: "{{ route('ajax_laten_scan_submit') }}",
                    cache: false,
                    timeout: 300000,
                    data: $(this).serialize(),
                    beforeSend: function() {
                        App.blockUI();
                    },
                    error: function(xmlhttprequest, textstatus, message) {
                        App.unblockUI();
                        if( textstatus === "timeout" ) 
                        {
                            alertify.alert("Ma'af, permintaan timeout");
                        } 
                        else 
                        {
                            alertify.alert(message);
                        }
                    },
                    success: function(data)
                    {
                        if ( data.status == 'OK' )
                        {
                            getDetail();
                            return;
                        }

                        App.unblockUI();

                        if ( data.status == 'NOTFOUND' )
                        {
                            swal({
                                type: 'info',
                                text: "Ma'af, data tidak ditemukan"
                            });
                            return;
                        }

                        if ( data.status == 'ERROR' )
                        {
                            console.log(data.message);
                            swal({
                                type: 'error',
                                text: (data.message == null) ? "Ma'af, tidak ada respon." : data.message
                            });
                            return;
                        }
                    }
                });
            });
        });
    });
</script>
@endpush