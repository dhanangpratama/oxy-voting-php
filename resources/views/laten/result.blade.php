@extends('layouts.main')
@section('content')

<div class="portlet light portlet-fit">
	<div class="portlet-title">
		<div class="caption">
            <span class="caption-subject sbold uppercase"><i class="icon-user"></i> &nbsp; Hasil Pencarian Laten</span>
        </div>
	</div>
	<div class="portlet-body">
		@if( ! empty($candidatesData) )
		<div class="m-b-30">
			<a href="{{ url('laten') }}" class="btn btn-md btn-circle blue uiblock-on" ><i class="fa fa-angle-left m-r-15"></i>Cari Lagi</a>
		</div>
		<div class="row">
		    <div class="col-md-4">
		    	<div class="laten-result-title">KANDIDAT</div>
		        <ul class="laten-name-list list-group">
		        	<?php $countList = 1; ?>
		        	@foreach($candidatesData as $keyArray => $candidate)
		        	<?php $keyArrayEnc = encrypt($keyArray); ?>
		        	@if ($countList <= 5)
		            <li class="list-group-item {{ $keyArray == $key ? 'active' : '' }}">
		                <a class="candidate-list" data-key="{{ $keyArray }}" href="{{ url("laten/result/{$keyArrayEnc}") }}"><span class="{{ $candidate['scoreFlag'] }}">{{ !empty($candidate['score']) ? str_replace('.', ',', ($candidate['score']/10)) : '-' }}</span>{{ strtoupper(check_empty(array_get($candidate, 'full_name'))) }} <i class="fa fa-angle-right list-arrow"></i></a>
		            </li>
		            @else
					<?php break; ?>
		            @endif
		            <?php $countList++ ?>
		            @endforeach
		        </ul>
		    </div>
		    <div class="col-md-8">
				<div class="content">
		        	<div class="laten-score {{ $scoreClass }}">NILAI: <strong>{{ !empty($candidatesData[$key]['score']) ? str_replace('.', ',', ($candidatesData[$key]['score']/10)) : '-' }}</strong></div>
		            <div class="row">
						<div class="col-md-4">
							<div class="panel panel-default">
								<div class="panel-body alt">
									@if ( file_exists($latenPath . 'photo.jpg') )
									<a href="{{ data_url('laten/' . $key . '/photo.jpg') }}" class="open-popup-link">
										<img src="{{ data_url('laten/' . $key . '/photo.jpg') }}" class="img-responsive">
									</a>
									@else
									<img src="{{ asset("assets/images/no-photo.png") }}?{{ time() }}" class="img-responsive">
									@endif
								</div>
							</div>
							

							@if ( file_exists($latenPath . 'signature.jpg') )
	                		<img src="{{ data_url('laten/' . $key . '/signature.jpg') }}" class="img-responsive img-signature" style="margin-top:10px;">
	                		@endif
	                	</div>
		                <div class="col-md-8">
		                	<table class="table laten-data uppercase">
								<tr>
		                			<td width="200">DATABASE</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.source")) }}</td>
		                		</tr>
								<tr>
		                			<td width="200">NIK</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.ktp_number")) }}</td>
		                		</tr>
								<tr>
		                			<td width="200">Passpor</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.passport_number")) }}</td>
		                		</tr>
		                		<tr>
		                			<td width="200">NAMA</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.full_name")) }}</td>
		                		</tr>
		                		<tr>
		                			<td width="200">JENIS KELAMIN</td> 
		                			<td>{{ check_empty(array_get($candidateDataDetail, "sex")) }}</td>
		                		</tr>
								<tr>
		                			<td width="200">Golongan Darah</td> 
		                			<td>{{ check_empty(array_get($candidateDataDetail, "blood_type")) }}</td>
		                		</tr>
		                		<tr>
		                			<td width="200">STATUS KAWIN</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.marital_status")) }}</td>
		                		</tr>
		                		<tr>
		                			<td width="200">AGAMA</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.religion")) }}</td>
		                		</tr>
								<tr>
		                			<td width="200">Suku</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.race")) }}</td>
		                		</tr>
		                		<tr>
		                			<td width="200">Tempat Lahir</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.birth_place")) }}</td>
		                		</tr>
								<tr>
		                			<td width="200">Tanggal Lahir</td> 
		                			<td>{{ check_empty(array_get($candidateDataDetail, "dob")) }}</td>
		                		</tr>
		                		<tr>
		                			<td width="200">PEKERJAAN</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.occupation")) }}</td>
		                		</tr>
		                		<tr>
		                			<td width="200">ALAMAT</td> 
		                			<td>
		                				{{ check_empty(array_get($candidateDataDetail, 'address')) }}
		                			</td>
		                		</tr>
								<tr>
		                			<td width="200">Nama Ayah</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.father_name")) }}</td>
		                		</tr>
								<tr>
		                			<td width="200">N.I.K Ayah</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.father_nik_number")) }}</td>
		                		</tr>
								<tr>
		                			<td width="200">Nama Ibu</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.mother_name")) }}</td>
		                		</tr>
								<tr>
		                			<td width="200">N.I.K Ibu</td> 
		                			<td>{{ check_empty(array_get($candidatesData, "{$key}.mother_nik_number")) }}</td>
		                		</tr>
		                	</table>
		                </div> 
		            </div>
		        </div>
		    </div> 
		</div>

		<div class="row" style="margin-top: 30px">
        	<div class="col-sm-6">
        		<div class="holder-inner">
        			<div class="laten-result-title">PERMINTAAN</div>
	        		<div class="requested-finger-preview text-center" style="margin-bottom: 20px;">
	        			@if($searchType == 'live')
						<?php $imageUrlBig = strpos(basename($requestedImageFirst), 'unknown') === false ? $minutiaeUrl . basename($requestedImageFirst) : $requestedImageFirst; ?>
	        			<a id="requested_zoom" class="open-popup-link" href="{{ $imageUrlBig }}?{{ time() }}">
	        				<img id="requested_preview_finger" src="{{ $imageUrlBig }}?{{ time() }}" class="aligncenter img-height-responsive" alt="">
	        			</a>
	        			@else
	        			<a class="open-popup-link" href="{{ ! file_exists($path['data']['laten'].DIRECTORY_SEPARATOR.'requested'.DIRECTORY_SEPARATOR.'minutiae'.DIRECTORY_SEPARATOR.'file.jpg') ? data_url('laten/requested/file.jpg') : data_url('laten/requested/minutiae/file.jpg') }}?{{ time() }}">
	        				<img id="requested_preview_finger" src="{{ ! file_exists($path['data']['laten'].DIRECTORY_SEPARATOR.'requested'.DIRECTORY_SEPARATOR.'minutiae'.DIRECTORY_SEPARATOR.'file.jpg') ? data_url('laten/requested/file.jpg') : data_url('laten/requested/minutiae/file.jpg') }}?{{ time() }}" class="aligncenter img-height-responsive" alt="">
	        			</a>
	        			@endif
	        			</a>
	        		</div>
					@if ($searchType === 'live')
	        		<div class="row gutter-10" style="margin-bottom: 10px;">
	        			<?php $countRequest = 1; ?>
	        			@foreach ($requestedFingers as $requestedKey => $path)
							<?php $imageUrl = strpos(basename($path), 'unknown') === false ? $minutiaeUrl . basename($path) : $path; ?>
		        			<div class="col-sm-15 m-b-15">
		        				<div class="requested-finger laten-image-list equal-height-laten-list">
		        					<a data-mh="laten-thumbnail" href="{{ $imageUrl }}?{{ time() }}" data-minutiae="{{ $imageUrl }}?{{ time() }}" data-requested-name="{{ $requestedKey }}" class="laten-link {{ $requestedImageFirst == $path && $countRequest == 1 ? 'active' : ''}} {{ $requestedKey }}">
		        						<div class="title text-center uppercase">{!! str_replace(' ', '<br>', $fingersMap[$requestedKey]) !!}</div>
		        						<img src="{{ $imageUrl }}?{{ time() }}" class="aligncenter img-responsive" alt="">
		        					</a>
		        				</div>
		        			</div>
		        			
		        		<?php $countRequest++; ?>
	        			@endforeach
	        		</div>
	        		@endif
	        	</div>
        	</div>

			<div class="col-sm-6">
				<div class="holder-inner">
					<div class="laten-result-title">KANDIDAT</div>
	        		<div class="founded-finger-preview text-center" style="margin-bottom: 20px;">
						@if (!empty($foundedImageFirst))
	        			<a id="founded_zoom" class="open-popup-link" href="{{ str_replace('_', '-', data_url("laten/{$key}/right-thumb-minutiae.jpg")) }}">
	        				<img id="founded_preview_finger" src="{{ str_replace('.jpg', '-minutiae.jpg', $foundedImageFirst) }}" class="aligncenter img-height-responsive" alt="">
	        			</a>
						@else
						<a id="founded_zoom" href="javascript:;">
	        				<img id="founded_preview_finger" src="{{ asset("assets/images/unknown-square.png") }}" class="aligncenter img-height-responsive" alt="">
	        			</a>
						@endif
	        		</div>

	        		<div class="row gutter-10" style="margin-bottom: 10px;">
	        			<?php $countFound = 1; ?>
	        			@foreach ($foundedFingers as $finger => $image)
		        			<div class="col-sm-15 m-b-15">
		        				<div class="founded-finger laten-image-list equal-height-laten-list">
									@if (!empty($image))
		        					<a data-mh="laten-thumbnail" href="{{ str_replace('.jpg', '-minutiae.jpg', $image) }}" data-founded-name="{{ trim(str_replace('_finger', '', $finger)) }}" data-minutiae="{{ str_replace('_', '-', data_url("laten/{$key}/{$finger}-minutiae.jpg")) }}" class="laten-link {{ $image == $foundedImageFirst && $countFound == 1 ? 'active' : ''}} {{ trim(str_replace('_finger', '', $finger)) }}">
		        						<div class="title text-center uppercase">{!! ! empty($fingersMap[$finger]) ? str_replace(' ', '<br>', $fingersMap[$finger]) : '-' !!}</div>
		        						<img src="{{ str_replace('.jpg', '-minutiae.jpg', $image) }}" class="aligncenter img-responsive" alt="">
		        					</a>
									@else
									<a data-mh="laten-thumbnail" href="{{ asset("assets/images/unknown-square.png") }}" data-founded-name="{{ trim(str_replace('_finger', '', $finger)) }}" data-minutiae="{{  asset("assets/images/unknown-square.png") }}" class="laten-link {{ $image == $foundedImageFirst && $countFound == 1 ? 'active' : ''}} {{ trim(str_replace('_finger', '', $finger)) }}">
										<div class="title text-center uppercase">{!! ! empty($fingersMap[$finger]) ? str_replace(' ', '<br>', $fingersMap[$finger]) : '-' !!}</div>
										<img src="{{ asset("assets/images/unknown-square.png") }}" class="aligncenter img-responsive" alt="">
									</a>
									@endif
		        				</div>
		        			</div>
		        		<?php $countFound++ ?>
	        			@endforeach
	        		</div>
	        	</div>
        	</div>
		</div>

		@else
		<div class="text-center">
			<h3>Data tidak ditemukan.</h3>
			<p><a href="{{ url('laten') }}" class="btn btn-sm green overlay" ><i class="fa fa-angle-left m-r-15"></i>Cari Lagi</a></p>
		</div>
		@endif
	</div>
</div>

@endsection

@if( ! empty($candidatesData) )
@push('js')
<script>
head.ready(function() {
	jQuery(document).ready(function($) {
		$('.laten-link').matchHeight({
			byRow: false
		});

		$('.founded-finger > a.laten-link').click(function(e) {
			e.preventDefault();
			$urlFounded = $(this).attr('href');
			$urlFoundedMinutiae = $(this).attr('data-minutiae');
			$indexFounded = $(this).attr('data-founded-name');

			$('.founded-finger > a.laten-link').removeClass('active');
			$(this).addClass('active');

			$('#founded_preview_finger').attr('src', $urlFounded);
			$('#founded_zoom').attr('href', $urlFoundedMinutiae);

			$('.requested-finger > a.laten-link').removeClass('active');
			$('.requested-finger > a.laten-link.' + $indexFounded).addClass('active');
			$('#requested_preview_finger').attr('src', $('.requested-finger > a.laten-link.' + $indexFounded).attr('href'));
			$('#requested_zoom').attr('href',  $('.requested-finger > a.laten-link.' + $indexFounded).attr('data-minutiae'));
		});

		$('.requested-finger > a.laten-link').click(function(e) {
			e.preventDefault();
			$url = $(this).attr('href');
			$urlMinutiae = $(this).attr('data-minutiae');
			$index = $(this).attr('data-requested-name');

			$('.requested-finger > a.laten-link').removeClass('active');
			$(this).addClass('active');

			$('#requested_preview_finger').attr('src', $url);
			$('#requested_zoom').attr('href', $urlMinutiae);

			$('.founded-finger > a.laten-link').removeClass('active');
			$('.founded-finger > a.laten-link.' + $index).addClass('active');
			$('#founded_preview_finger').attr('src', $('.founded-finger > a.laten-link.' + $index).attr('href'));
			$('#founded_zoom').attr('href', $('.founded-finger > a.laten-link.' + $index).attr('data-minutiae'));
			console.log($url, $urlMinutiae, $index);
		});

		$('.open-popup-link').magnificPopup({
			type: 'image',
			verticalFit: true,
			midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
		});

		 $('.candidate-list').on('click', function(e) 
		 {
            e.preventDefault();
			var candidate = $(this);

			$.ajax({
				type: 'POST',
				url: "{{ route('ajax_laten_process_detail') }}",
				cache: false,
				timeout: '{{ config("ak23.nikSearchTimeout") }}',
				async: true,
				dataType: 'json',
				data: {
					key: candidate.attr('data-key')
				},
				beforeSend: function() {
					console.log('Starting search detail data...');

					App.blockUI();
				},
				error: function(xmlhttprequest, textstatus, message) {
					App.unblockUI();
					if( textstatus === "timeout" ) 
					{  
						swal({
							html: "<strong>Ma'af, akses ke server!</strong><br><br>Hal ini menyebabkan data yang ditampilkan tidak lengkap.<br><br>Tetap melihat data hasil pencarian?",
							type: 'info',
							padding: 50,
							width: 600,
							showCancelButton: true,
							confirmButtonColor: '#0376B8',
							cancelButtonColor: '#e12330',
							confirmButtonText: 'Ya',
							cancelButtonText: 'Tidak',
							allowOutsideClick: false
						}).then(function () {
							App.blockUI();
							window.location.href = candidate.attr('href');
						});

						return;           
					} 
					else 
					{
						console.log(message);
						alertify.alert("Ma'af, terjadi kesalahan pada aplikasi");
						return;
					}
				},
				success: function(data)
				{
					console.log('Search detail data done, redirecting...');

					if ( data.status == 'OK' )
					{
						window.location.href = candidate.attr('href');
						return;
					}

					App.unblockUI();

					if ( data.status == 'ERROR' )
                    {
                        // console.log(data.message);

						swal({
							html: "<strong>"+data.message+"</strong><br><br>Tetap melihat data hasil pencarian?",
							type: 'info',
							padding: 50,
							width: 600,
							showCancelButton: true,
							confirmButtonColor: '#0376B8',
							cancelButtonColor: '#e12330',
							confirmButtonText: 'Ya',
							cancelButtonText: 'Tidak',
							allowOutsideClick: false
						}).then(function () {
							App.blockUI();
							window.location.href = candidate.attr('href');
						});

                        return;
                    }
				}
			});
		 });
	});
});
</script>
@endpush
@endif