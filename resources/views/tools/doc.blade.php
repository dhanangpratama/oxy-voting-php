@extends('layouts.main')
@section('content')

<iframe style="width: 100%;height: 640px;" src="{{ config('url.serverAddress') }}:55580/doc/"></iframe>
                
@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        let pageContent = $('.page-content').attr('style');

        if (pageContent !== undefined)
        {
            pageContent = pageContent.match(/\d+/);
            var iframeHeight = pageContent[0] - 37;
            $('iframe').height(iframeHeight);
        }
    });
});
</script>
@endpush