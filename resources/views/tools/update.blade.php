@extends('layouts.main')
@section('content')

@if (session()->has('update_status'))
<?php $updateStatus = session('update_status'); ?>
<div class="alert alert-{{ $updateStatus['class'] }}">{{ $updateStatus['message'] }}</div>
@endif

<div class="text-center update-page-wrapper">
	@if ( $updateAvailable )
	<h1><strong>Versi {{ !empty($serverVersion['ver']) ? $serverVersion['ver'] : 'Tidak diketahui' }}</strong></h1>
	<p>Pembaruan tersedia</p>
	<p><a href="{{ url('tool/update/software?do=yes') }}" class="btn btn-md blue overlay">Perbarui Sekarang</a></p>
	<br>
	<h4><strong>Catatan Perubahan</strong></h4>
	<div class="change-log"><textarea class="form-control" rows="10" readonly>{!! $serverChangesLog !!}</textarea></div>
	@else
	<h1><strong>Versi {{ !empty($localVersion['ver']) ? $localVersion['ver'] : 'Tidak diketahui' }}</strong></h1>
	<p>Pembaruan tidak tersedia</p>
	<br>
	<h4><strong>Catatan Perubahan</strong></h4>
	<div class="change-log"><textarea class="form-control" rows="10" readonly>{!! $localChangesLog !!}</textarea></div>
	@endif
</div>

@endsection