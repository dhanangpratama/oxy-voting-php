@extends('layouts.main')
@section('content')

<!-- BEGIN PAGE TITLE-->
<h3 class="page-title"> Halaman Pengaturan</h3>
<!-- END PAGE TITLE-->
<!-- END PAGE HEADER-->
<div class="note note-info">
    <p> A black page template with a minimal dependency assets to use as a base for any custom page you create </p>
</div>

<div class="portlet box dark">
    <div class="portlet-body">
        <!-- BEGIN REGISTRATION FORM -->
        <form method="POST" action="http://inafis.ak23/user/create" accept-charset="UTF-8" class="register-form"><input name="_token" type="hidden" value="7qmHzyOFtXleUSMXlUG3P9qBsoxGHs57OqURtlKa">
        
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input ">
                        <input class="form-control placeholder-no-fix" id="name" name="name" type="text">
                        <label for="name">Tingkat Terang</label>
                        
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input ">
                        <input class="form-control placeholder-no-fix" id="email" name="email" type="text">
                        <label for="mail">Tingkat Kecerahan</label>
                        
                    </div>
                </div>
                                <div class="col-sm-12">
                    <div class="form-group form-md-line-input">
                        <select class="form-control" id="role" name="role"><option>-- Pilih Lokasi --</option>
                        </select>
                        <label for="role">Lokasi</label>
                    </div>
                </div>
                                <div class="col-sm-6">
                    <div class="form-group form-md-line-input ">
                        <input class="form-control placeholder-no-fix" id="password" autocomplete="off" name="password" type="password" value="">
                        <label for="password">Password</label>
                        
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input ">
                        <input class="form-control placeholder-no-fix" id="rpassword" autocomplete="off" name="rpassword" type="password" value="">
                        <label for="rpassword">Ulangi Password</label>
                        
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-actions">
                        <button type="submit" id="register-submit-btn" class="btn red btn-block uppercase">Simpan</button>
                    </div>
                </div>
            </div>
        </form>
        <!-- END REGISTRATION FORM -->
    </div>
</div>
                
@endsection