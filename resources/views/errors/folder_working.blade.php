@extends('layouts.main')
@section('content')

<div class="note note-info">
    <h4 class="block">Data yang sedang anda kerjakan sudah tidak ditemukan.</h4>
    <p>Kemungkinan besar telah selesai dikerjakan oleh pengguna lain. Silahkan anda <a class="btn blue btn-xs" href="{{ url('/') }}">Kembali ke Dashboard</a></p>
</div>

@endsection