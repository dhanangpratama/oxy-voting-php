@extends('layouts.main')
@section('content')

<div class="note note-info">
    <h4 class="block">{{ $data['title'] }}</h4>
    {!! (! empty($data['description']) ) ? '<p>' . $data['description'] . '</p>' : '' !!}
</div>

@endsection