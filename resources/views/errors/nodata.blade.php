@extends('layouts.main')
@section('content')

    <div class="nodata-text">
        <p>Data Tidak ditemukan</p>

        <p><a href="{{ URL::previous() }}" class="btn red uiblock-on">Kembali</a></p>
    </div>

@endsection