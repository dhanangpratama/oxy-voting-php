@extends('layouts.error')
@section('content')

<?php $message = $exception->getMessage(); ?>

    <div class="text-center wrapper">
        <div class="holder relative">
            <h1 class="white">Ma'af, terjadi kesalahan</h1>
            <p class="white">{!! preg_replace('/[[\s\S]+?]/', '', $message);  !!}</p>

            @if ( strpos($message, '[btn_back]') !== false )

            <p class="text-center"><a href="{{ URL::previous() }}" class="btn blue btn-md uppercase uiblock-on">Kembali</a></p>

            @elseif ( strpos($message, '[btn_restart]') !== false )

            <p class="text-center"><a href="javascript:Biover.ApplicationRestart();" class="btn blue btn-md uppercase uiblock-on">Restart</a></p>

            @elseif ( strpos($message, '[btn_close_window]') !== false )

            <p class="text-center"><a href="javascript:window.close();" class="btn blue btn-md uppercase uiblock-on">Tutup</a></p>

            @elseif ( strpos($message, '[btn_shutdown]') !== false )

            <p class="text-center"><a href="javascript:Biover.ApplicationShutdown();" class="btn blue btn-md uppercase  uiblock-on">Shut Down</a></p>

            @else

            @if ( strpos($message, '[btn_remove]') === false )
            <p class="text-center"><a href="{{ route('dashboard') }}" class="btn blue btn-md uppercase uiblock-on">Kembali ke Dashboard</a></p>
            @endif
            
            @endif

            <p class="white" style="position: absolute; bottom: -50px; font-size: 11px; left: 0; width: 100%;">Tanggal Log: {{ Carbon\Carbon::now()->format('d-m-Y H:i:s') }}</p>
        </div>
    </div>

@endsection