@extends('layouts.error')
@section('content')

    <div class="text-center wrapper">
        <div class="holder">
            <h1 class="white">Halaman tidak ditemukan</h1>
            <p class="text-center"><a href="{{ URL::previous() }}" class="btn blue btn-sm">Kembali</a></p>
        </div>
    </div>

@endsection