@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'post', 'class' => 'edit-form']) !!}
<div class="portlet box dark">
    <div class="portlet-title">
        <div class="caption">
            Profile
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group form-md-line-input {{ ! empty($errors->edit->first('name')) ? 'has-error' : '' }}">
                    {!! Form::text('name', $user->name, ['class' => 'form-control placeholder-no-fix', 'id' => 'name']) !!}
                    {{ Form::label('name', 'Nama') }}
                    {!! ! empty($errors->edit->first('name')) ? '<span id="name" class="help-block">'.$errors->edit->first('name').'</span>' : '' !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group form-md-line-input {{ ! empty($errors->edit->first('email')) ? 'has-error' : '' }}">
                    {!! Form::text('email', $user->email, ['class' => 'form-control placeholder-no-fix', 'id' => 'email']) !!}
                    {{ Form::label('mail', 'Email') }}
                    {!! ! empty($errors->edit->first('email')) ? '<span id="email" class="help-block">'.$errors->edit->first('email').'</span>' : '' !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group form-md-line-input {{ ! empty($errors->edit->first('phone_mobile')) ? 'has-error' : '' }}">
                    {!! Form::text('phone_mobile', $user->phone_mobile, ['class' => 'form-control placeholder-no-fix', 'id' => 'phone_number']) !!}
                    {!! Form::label('phone_mobile', 'No. Handphone') !!}
                    {!! ! empty($errors->edit->first('phone_mobile')) ? '<span id="email" class="help-block">'.$errors->edit->first('phone_mobile').'</span>' : '' !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group form-md-line-input {{ ! empty($errors->edit->first('old_password')) ? 'has-error' : '' }}">
                    {!! Form::password('old_password', ['class' => 'form-control placeholder-no-fix', 'id' => 'old_password', 'autocomplete' => 'off']) !!}
                    {{ Form::label('old_password', 'Masukkan Password Lama') }}
                    {!! ! empty($errors->edit->first('old_password')) ? '<span id="old_password" class="help-block">'.$errors->edit->first('old_password').'</span>' : '' !!}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group form-md-line-input {{ ! empty($errors->edit->first('password')) ? 'has-error' : '' }}">
                    {!! Form::password('password', ['class' => 'form-control placeholder-no-fix', 'id' => 'password', 'autocomplete' => 'off', 'data-indicator' => 'pwindicator']) !!}
                    {{ Form::label('password', 'Password Baru') }}
                    <div class="pwindicator-wrapper top">
                        <div id="pwindicator">
                            <div class="bar"></div>
                            <div class="label"></div>
                            <div class="text">
                                Gunakan password yang baik agar tidak mudah ditebak oleh orang lain
                            </div>
                        </div>
                    </div>
                    {!! ! empty($errors->edit->first('password')) ? '<span id="password" class="help-block">'.$errors->edit->first('password').'</span>' : '' !!}
                    
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group form-md-line-input {{ ! empty($errors->edit->first('rpassword')) ? 'has-error' : '' }}">
                    {!! Form::password('rpassword', ['class' => 'form-control placeholder-no-fix', 'id' => 'rpassword', 'autocomplete' => 'off']) !!}
                    {{ Form::label('rpassword', 'Ulangi Password Baru') }}
                    {!! ! empty($errors->edit->first('rpassword')) ? '<span id="rpassword" class="help-block">'.$errors->edit->first('rpassword').'</span>' : '' !!}
                </div>
            </div>

            @role('administrator')
            <div class="col-sm-12">
                <div class="form-group">
                    <div class="mt-checkbox-list">
                        <label class="mt-checkbox mt-checkbox-outline"> Aktifkan Pengguna
                            {!! Form::checkbox('activated', 1, $user->activated == 1 ? true : false); !!}
                            <span></span>
                        </label>
                    </div>
                </div>
            </div>
            @endrole
        </div> 
    </div>
</div>
<div class="form-actions text-center">
    <button type="submit" id="register-submit-btn" class="btn red btn-lg b uppercase">Simpan Perubahan</button>
</div>
{!! Form::close() !!}
            
@endsection

@push('js')
<script>
var Form = function() {

    var handleEdit = function() {

        jQuery('.edit-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                // simple rule, converted to {required:true}
                
                name: {
                    required: true,
                    maxlength: 255
                },
                phone_mobile: {
                    required: true,
                    maxlength: 255,
                    digits: true
                }
                
                // compound rule
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                }
            },
            messages: {
                password: {
                    required: "Tidak boleh kosong"
                },
                rpassword: {
                    required: "Tidak boleh kosong",
                    equalTo: "Tidak sama dengan password"
                },
                name: {
                    required: "Tidak boleh kosong"
                },
                phone_mobile: {
                    required: "Tidak boleh kosong",
                    digits: "Hanya boleh angka"
                },
                email: {
                    required: "Tidak boleh kosong",
                    email: "Format email salah"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            submitHandler: function(form) {
                alertify.okBtn("Ya").cancelBtn("Tidak").confirm("Yakin perubahan sudah benar?").then(function (resolvedValue) {
                    resolvedValue.event.preventDefault();
                    // alertify.alert("You clicked the " + resolvedValue.buttonClicked + " button!");
                    if (resolvedValue.buttonClicked == 'ok')
                    {
                        $('.uiblock').show();
                        form.submit();
                    }
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handleEdit();

        }

    };

}();

head.ready(function() 
{
    jQuery(document).ready(function($) {

        $('.equal').matchHeight();

        $('#name').focus();

        var $pwIndicator = $('#pwindicator');

        $('#password').focusin(function() {
            $(this).closest('.form-group').addClass('show-pwstrength');

            if ( $pwIndicator.hasClass('pw') === false )
            {
                $pwIndicator.addClass('pw pw-very-weak');
                $pwIndicator.children('.label').html('Sangat lemah');
            }
        });

        $('#password').focusout(function() {
            $(this).closest('.form-group').removeClass('show-pwstrength');
        });

        $('#password').pwstrength({
            classes: ['pw pw-very-weak', 'pw pw-weak', 'pw pw-mediocre', 'pw pw-strong', 'pw pw-very-strong'],
            texts: ['Sangat lemah', 'Lemah', 'Menengah', 'Kuat', 'Sangat kuat']
        }); 

        $('button[type=submit]').click(function(e) {
            e.preventDefault();

            swal({
                title: 'Konfirmasi',
                html: 'Yakin perubahan sudah benar?',
                type: 'warning',
                padding: 50,
                width: 600,
                showCancelButton: true,
                confirmButtonColor: '#0376B8',
                cancelButtonColor: '#e12330',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                allowOutsideClick: false
            }).then(function () {
                App.blockUI();    
                $('form').submit();
            });
        });

        $('#tempat_lahir').focus();
    });
});
</script>
@endpush