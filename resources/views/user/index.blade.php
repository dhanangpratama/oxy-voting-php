@extends('layouts.main')
@section('content')

    {!! Form::open(['method' => 'get']) !!}
    <div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">
                Data Pengguna Aplikasi
            </div>
        </div>
        <div class="portlet-body">
            <div class="row m-b-30">
                <div class="col-md-9">
                    <div class="filter">
                        <div class="select2-location-wrapper">
                            {!! Form::select('location_id', $dataService->locations(), null, ['class' => 'form-control select2', 'id' => 'location_id', 'placeholder' => '-- Pilih Lokasi --']) !!}
                        </div>
                        <button type="submit" class="btn blue btn-md uppercase uiblock-on">Filter</button>

                        @if ( ! empty($_SERVER['QUERY_STRING']) )
                        <a href="{{ route('users') }}" class="btn red uppercase uiblock-on">Lihat Semua</a>
                        @endif
                    </div>
                </div>

                <div class="col-md-3">
                    <a href="{{ route('user_create') }}" class="btn blue pull-right uppercase uiblock-on">Buat Baru</a>
                </div>
            </div>
            
            <table class="table grey">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>No.Handphone</th>
                        <th>Level</th>
                        <th>Aktif</th>
                        <th>Tanggal Buat</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if ($users->count())
                    @foreach ($users as $user)
                    <tr>
                        <td><a class="white" href="{{ route('user_profile', [encrypt($user->user_id)]) }}">{{ check_empty($user->name) }}</a></td>
                        <td>{{ check_empty($user->email) }}</td>
                        <td>{{ check_empty(phone_format($user->phone_mobile)) }}</td>
                        <td>{{ displayRoles($user->roles) }}</td>
                        <td>{{ yesNo($user->activated) }}</td>
                        <td>{{ date('d-m-Y', strtotime($user->created_at)) }}</td>
                        <td width="100">
                            <a href="{{ route('user_profile', [encrypt($user->user_id)]) }}" class="btn blue btn-xs uiblock-on">Ubah</a>

                            @if ( ! $user->activated )
                            <button type="button" data-url="{{ route('user_remove', [encrypt($user->user_id)]) }}" class="remove btn red btn-xs {{ $user->demographics->count() > 0 ? 'disabled' : '' }}"><i class="fa fa-times"></i></button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="7" align="center">Belum ada data</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
    {!! Form::close() !!}

    {{ $users->links() }}
                
@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        if ("function" !== typeof Promise) {
            alertify.alert("Your browser doesn't support promises");
            return;
        }

        $('.remove').click(function(event) {
            event.preventDefault();
            if ( ! $(this).hasClass('disabled') )
            {
                var $url = $(this).attr('data-url');
                alertify.okBtn("Ya").cancelBtn("Tidak").confirm("Yakin data akan dihapus?").then(function (resolvedValue) {
                    resolvedValue.event.preventDefault();
                    // alertify.alert("You clicked the " + resolvedValue.buttonClicked + " button!");
                    if (resolvedValue.buttonClicked == 'ok')
                    {
                        $('.uiblock').show();
                        go($url);
                    }
                });
            }
        });
    });
});
</script>
@endpush