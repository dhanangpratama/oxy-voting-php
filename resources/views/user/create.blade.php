@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'post', 'class' => 'register-form', 'url' => route('user_create_submit')]) !!}
<div class="portlet box dark">
    <div class="portlet-title">
        <div class="caption">
            Buat Pengguna Baru
        </div>
    </div>
    <div class="portlet-body">
        <!-- BEGIN REGISTRATION FORM -->
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input {{ ! empty($errors->register->first('name')) ? 'has-error' : '' }}">
                        {!! Form::text('name', null, ['class' => 'form-control placeholder-no-fix', 'id' => 'name']) !!}
                        {{ Form::label('name', 'Nama') }}
                        {!! ! empty($errors->register->first('name')) ? '<span id="name" class="help-block">'.$errors->register->first('name').'</span>' : '' !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input {{ ! empty($errors->register->first('email')) ? 'has-error' : '' }}">
                        {!! Form::text('email', null, ['class' => 'form-control placeholder-no-fix', 'id' => 'email']) !!}
                        {{ Form::label('mail', 'Email') }}
                        {!! ! empty($errors->register->first('email')) ? '<span id="email" class="help-block">'.$errors->register->first('email').'</span>' : '' !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input {{ ! empty($errors->register->first('phone_mobile')) ? 'has-error' : '' }}">
                        {!! Form::text('phone_mobile', null, ['class' => 'form-control placeholder-no-fix', 'id' => 'phone_number']) !!}
                        {!! Form::label('phone_mobile', 'No. Handphone') !!}
                        {!! ! empty($errors->register->first('phone_mobile')) ? '<span id="email" class="help-block">'.$errors->register->first('phone_mobile').'</span>' : '' !!}
                    </div>
                </div>
                
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input {{ ! empty($errors->register->first('password')) ? 'has-error' : '' }}">
                        {!! Form::password('password', ['class' => 'form-control placeholder-no-fix', 'id' => 'password', 'autocomplete' => 'off', 'data-indicator' => 'pwindicator']) !!}
                        {{ Form::label('password', 'Password') }}
                        <div class="pwindicator-wrapper top">
                            <div id="pwindicator">
                                <div class="bar"></div>
                                <div class="label"></div>
                                <div class="text">
                                    Gunakan password yang baik agar tidak mudah ditebak oleh orang lain
                                </div>
                            </div>
                        </div>
                        {!! ! empty($errors->register->first('password')) ? '<span id="password" class="help-block">'.$errors->register->first('password').'</span>' : '' !!}
                        
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input {{ ! empty($errors->register->first('rpassword')) ? 'has-error' : '' }}">
                        {!! Form::password('rpassword', ['class' => 'form-control placeholder-no-fix', 'id' => 'rpassword', 'autocomplete' => 'off']) !!}
                        {{ Form::label('rpassword', 'Ulangi Password') }}
                        {!! ! empty($errors->register->first('rpassword')) ? '<span id="rpassword" class="help-block">'.$errors->register->first('rpassword').'</span>' : '' !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <div class="mt-checkbox-list">
                            <label class="mt-checkbox mt-checkbox-outline"> Aktifkan Pengguna
                                {!! Form::checkbox('activated', 1); !!}
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        
        <!-- END REGISTRATION FORM -->
    </div>
</div>
<div class="text-center">
    <button type="submit" id="register-submit-btn" class="btn blue uppercase btn-lg">Buat</button>
</div>
{!! Form::close() !!}    
@endsection

@push('js')
<script>
var Login = function() {

    var handleRegister = function() {

        jQuery('.register-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                // simple rule, converted to {required:true}
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 255
                },
                name: {
                    required: true,
                    maxlength: 255
                },
                phone_mobile: {
                    required: true,
                    maxlength: 255,
                    digits: true
                },
                location_id: {
                    required: true,
                    maxlength: 255
                },
                role: {
                    required: true,
                    maxlength: 255
                },
                rpassword: {
                    required: true,
                    equalTo: "#password",
                    maxlength: 255
                },
                // compound rule
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                }
            },
            messages: {
                password: {
                    required: "Tidak boleh kosong"
                },
                rpassword: {
                    required: "Tidak boleh kosong",
                    equalTo: "Tidak sama dengan password"
                },
                name: {
                    required: "Tidak boleh kosong"
                },
                phone_mobile: {
                    required: "Tidak boleh kosong",
                    digits: "Hanya boleh angka"
                },
                location_id: {
                    required: "Tidak boleh kosong"
                },
                role: {
                    required: "Tidak boleh kosong"
                },
                email: {
                    required: "Tidak boleh kosong",
                    email: "Format email salah"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            submitHandler: function(form) { 
                App.blockUI();
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.register-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.register-form').validate().form()) {
                    App.blockUI();
                    $('.register-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handleRegister();

        }

    };

}();

head.ready(function() 
{
    Login.init();

    $('#name').focus();

    var $pwIndicator = $('#pwindicator');

    $('#password').focusin(function() {
        $(this).closest('.form-group').addClass('show-pwstrength');

        if ( $pwIndicator.hasClass('pw') === false )
        {
            $pwIndicator.addClass('pw pw-very-weak');
            $pwIndicator.children('.label').html('Sangat lemah');
        }
    });

    $('#password').focusout(function() {
        $(this).closest('.form-group').removeClass('show-pwstrength');
    });

    jQuery(document).ready(function($) {
        $('#password').pwstrength({
            classes: ['pw pw-very-weak', 'pw pw-weak', 'pw pw-mediocre', 'pw pw-strong', 'pw pw-very-strong'],
            texts: ['Sangat lemah', 'Lemah', 'Menengah', 'Kuat', 'Sangat kuat']
        }); 
    });
});
</script>
@endpush