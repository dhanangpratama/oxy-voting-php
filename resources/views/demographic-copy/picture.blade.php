@extends('layouts.main')
@section('content')

@if ( $scanType == 'card' )
@include('layouts.card_indicator')
@else
@include('layouts.live_scan_indicator')
@endif

{!! Form::open(['method'=>'POST', 'files' => true])  !!}
	<div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">Foto</div>
        </div>
        <div class="portlet-body form">
        	<div class="form-body">
	        	<div class="row">
	        		@if (count($errors) > 0)
                    <div class="col-sm-12">
                        <div class="alert alert-danger">
                            <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                            </ul>
                        </div>
                    </div>
                    @endif

	        		<div class="col-sm-4">
						<div class="panel panel-default">
                            <div class="panel-body alt text-center">
								@if ( file_exists($file['basePath'].DIRECTORY_SEPARATOR.$faceFront.".{$ext}") )
								<a class="image-popup" title="Wajah Tampak Depan" href="{{ $file['baseUrl'] . "/{$faceFront}.{$ext}" }}?{{ time() }}"><img src="{{ $file['baseUrl'] . "/{$faceFront}.{$ext}" }}?{{ time() }}"  class="img-people img-responsive" alt="Wajah muka"></a>
								<div class="btn-group">
									<a href="{{ route('demographic_picture_crop', [$faceFront]) }}" class="btn  uiblock-on green btn-lg uppercase">Potong</a>
									{{-- <div class="fileinput fileinput-new" data-provides="fileinput">
										<span class="btn   blue btn-file">
											<span class="fileinput-new"> Ganti Gambar </span>
											<span class="fileinput-exists"> Ganti </span>
											<input type="file" name="picture_front_side"> </span>
										<span class="fileinput-filename"> </span> &nbsp;
										<a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"></a>
									</div> --}}
									<a href="{{ url("picture/remove/{$faceFront}") }}" class="btn  red remove-btn btn-lg uppercase">Hapus</a>
								</div>
								@else
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new" style="margin-bottom: 30px">
										<img src="{{ asset('assets/images/front.png') }}?{{ time() }}" alt="Front side" class="img-responsive" /> </div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
									<div>
										{{-- <span class="btn   blue btn-file">
											<span class="fileinput-new"> Pilih Gambar </span>
											<span class="fileinput-exists"> Ganti </span>
											<input type="file" name="picture_front_side"> 
										</span>
										<a href="javascript:;" class="btn   red fileinput-exists" data-dismiss="fileinput"> Hapus </a> --}}
										<a href="{{ route('demographic_picture_crop', [$faceFront]) }}" class="btn btn-lg uiblock-on green">Ambil Gambar</a>
									</div>
									
								</div>
								@endif
							</div>
							<div class="panel-footer uppercase aligncenter relative">
								Depan
							</div>
	                    </div>
	        		</div>

	        		<div class="col-sm-4">
	        			<div class="panel panel-default">
                            <div class="panel-body alt text-center">
								@if ( file_exists($file['basePath'].DIRECTORY_SEPARATOR.$faceLeft.".{$ext}") )
								<a class="image-popup" title="Wajah Tampak Samping Kiri" href="{{ $file['baseUrl'] . "/{$faceLeft}.{$ext}" }}?{{ time() }}"><img src="{{ $file['baseUrl'] . "/{$faceLeft}.{$ext}" }}?{{ time() }}"  class="img-people img-responsive" alt="Face left"></a>
								<div class="btn-group">
									<a href="{{ route('demographic_picture_crop', [$faceLeft]) }}" class="btn  uiblock-on btn-lg green uppercase">Potong</a>
									{{-- <div class="fileinput fileinput-new" data-provides="fileinput">
										<span class="btn   blue btn-file">
											<span class="fileinput-new"> Ganti Gambar </span>
											<span class="fileinput-exists"> Ganti </span>
											<input type="file" name="picture_left_side"> </span>
										<span class="fileinput-filename"> </span> &nbsp;
										<a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
									</div> --}}
									<a href="{{ url("picture/remove/{$faceLeft}") }}" class="btn  red remove-btn btn-lg uppercase">Hapus</a>
								</div>
								@else
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new" style="margin-bottom: 30px">
										<img src="{{ asset('assets/images/left-side.png') }}?{{ time() }}" alt="left side" class="img-responsive" /> </div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
									<div>
										{{-- <span class="btn   blue btn-file">
											<span class="fileinput-new"> Pilih Gambar </span>
											<span class="fileinput-exists"> Ganti </span>
											<input type="file" name="picture_left_side"> 
										</span>
										<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a> --}}
										<a href="{{ route('demographic_picture_crop', [$faceLeft]) }}" class="btn btn-lg uiblock-on green">Ambil Gambar</a>
									</div>
								</div>
								@endif
							</div>
							<div class="panel-footer uppercase aligncenter relative">
								Samping Kiri
							</div>
	                    </div>
	        		</div>

					<div class="col-sm-4">
	        			<div class="panel panel-default">
                            <div class="panel-body alt text-center">
								@if ( file_exists($file['basePath'].DIRECTORY_SEPARATOR.$faceRight.".{$ext}") )
								<a class="image-popup" title="Wajah Tampak Samping Kanan" href="{{ $file['baseUrl'] . "/{$faceRight}.{$ext}" }}?{{ time() }}"><img src="{{ $file['baseUrl'] . "/{$faceRight}.{$ext}" }}?{{ time() }}" class="img-people img-responsive" alt="Face right"></a>
								<div class="btn-group">
									<a href="{{ route('demographic_picture_crop', [$faceRight]) }}" class="btn  uiblock-on green btn-lg uppercase">Potong</a>
									{{-- <div class="fileinput fileinput-new" data-provides="fileinput">
										<span class="btn   blue btn-file">
											<span class="fileinput-new"> Ganti Gambar </span>
											<span class="fileinput-exists"> Ganti </span>
											<input type="file" name="picture_right_side"> </span>
										<span class="fileinput-filename"> </span> &nbsp;
										<a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
									</div> --}}
									<a href="{{ url("picture/remove/{$faceRight}") }}" class="btn  red remove-btn btn-lg uppercase">Hapus</a>
								</div>
								@else
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new" style="margin-bottom: 30px">
										<img src="{{ asset('assets/images/right-side.png') }}?{{ time() }}" class="img-responsive" alt="" /> </div>
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
									<div>
										{{-- <span class="btn   blue btn-file">
											<span class="fileinput-new"> Pilih Gambar </span>
											<span class="fileinput-exists"> Ganti </span>
											<input type="file" name="picture_right_side"> 
										</span>
										<a href="javascript:;" class="btn   red fileinput-exists" data-dismiss="fileinput"> Hapus </a> --}}

										<a href="{{ route('demographic_picture_crop', [$faceRight]) }}" class="btn btn-lg uiblock-on green">Ambil Gambar</a>
									</div>
								</div>
								@endif
								</div>
								<div class="panel-footer uppercase aligncenter relative">
									Samping Kanan
								</div>
							</div>
	                    </div>
	        		</div>
	        	</div>
        	</div>
        </div>
    </div>

{!! Form::close() !!}

	<div class="text-center m-b-30">
		<div class="btn-group">
        	<a class="btn red btn-prev uiblock-on btn-lg uppercase" tabindex="37" href="{{ route('demographic_formula') }}"><i class="fa fa-angle-left m-r-15"></i> Kembali</a>
			<a class="btn blue btn-next uiblock-on btn-lg uppercase" tabindex="36" href="{{ route('demographic_picture_submit_next') }}">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></a>
		</div>
    </div>

@endsection

@push('js')
<script>
head.ready(function() {
	$(document).ready(function() {
		$(':input').on('change', function (e) 
		{
		    $(this).addClass('changed-input');
		});

		$('.btn-next, .btn-prev').on('click', function(e) 
		{
			e.preventDefault();

			var btn = $(this);

			if ($('.changed-input').length) 
		    {
		    	alertify.okBtn("Ya").cancelBtn("Tidak").confirm("Anda melakukan perubahan pada Foto, namun anda belum menekan tombol upload.<br><br><strong>Yakin akan meninggalkan halaman ini tanpa mengupload foto?</strong>").then(function (resolvedValue) {
			        resolvedValue.event.preventDefault();
			        if (resolvedValue.buttonClicked == 'ok')
			        {
			            window.open(btn.attr('href'), "_self");
			        }
			    });
		    }
		    else
		    {
		    	window.open(btn.attr('href'), "_self");
		    }
		});

		$('.remove-btn').click(function(event) {
			event.preventDefault();
			var $link = $(this);
			swal({
				title: 'Konfirmasi',
				html: 'Yakin akan menghapus foto?',
				type: 'info',
				padding: 50,
				width: 600,
				showCancelButton: true,
				confirmButtonColor: '#0376B8',
				cancelButtonColor: '#e12330',
				confirmButtonText: 'Ya',
				cancelButtonText: 'Tidak',
				allowOutsideClick: false
			}).then(function () {
				App.blockUI();
				window.open($link.attr('href'), '_self');
			});
		});
	});
});
</script>
@endpush