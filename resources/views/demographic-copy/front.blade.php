@extends('layouts.main')
@section('content')
<?php $bg = ($scanType != 'card') ? '' : 'style="background-image: url('.$file['url_front'].');"'; ?>

@if ($scanType == 'card')
<a class="image-link" data-toggle="tooltip" title="Lihat kartu" data-placement="right" href="{{ $file['url_front'] }}"><i class="fa fa-file-image-o"></i></a>
@include('layouts.card_indicator')
@else
@include('layouts.live_scan_indicator')
@endif

<div id="img_scroll" class="front">
    <div class="container">
        <img src="{{ $file['url_front'] }}" class="img-responsive">
    </div>
</div>

{!! Form::open(['method'=>'POST'])  !!}
<div id="form_scroll" class="front">
    <div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">Tampilan Depan</div>
        </div>
        <div class="portlet-body form">
            <div class="form-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="row data">
                            @if (count($errors) > 0)
                            <div class="col-sm-12">
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <div class="col-sm-12">
                                <div class="form-group form-md-line-input">
                                    <select name="status_id" class="form-control pophover infoable" id="status" tabindex="1">
                                        <option>-- Pilih salah satu --</option>
                                        @foreach ($status as $key => $value)
                                        <option value="{{ $key }}" {{ $key == $demographicData['status_id'] || old('status_id') == $key ? 'selected="selected"' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>

                                    @if ($scanType == 'card')
                                    <div class="popinfo status">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="status">Status</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    {!! Form::text('fullname', old('fullname', array_get($demographicData, 'fullname')), ['id' => 'fullname', 'tabindex' => 2,'class' => 'form-control infoable', 'autocomplete' => 'off']) !!}

                                    @if ($scanType == 'card')
                                    <div class="popinfo fullname">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="fullname">Nama</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    <input type="text" class="form-control infoable" autocomplete="off" id="nickname" name="nickname" value="{{ old('nickname', array_get($demographicData, 'nickname')) }}" tabindex="3">

                                    @if ($scanType == 'card')
                                    <div class="popinfo nickname">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="nickname">Nama Kecil / Alias</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    {{ Form::text('job', old('job', array_get($demographicData, 'job')), ['id' => 'job', 'tabindex' => 4,'class' => 'form-control infoable', 'autocomplete' => 'off']) }} 

                                    @if ($scanType == 'card')
                                    <div class="popinfo job">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="job">Pekerjaan</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label class="square-label">Jenis Kelamin</label>
                                    <div class="pophover">
                                        <label class="radio-square-label">
                                            {!! Form::radio('sex', 'm', old('sex', isset($demographicData['sex']) && $demographicData['sex'] == 'm' ? 'checked' : ''), ['class' => 'icheck', 'tabindex' => 5, 'id' => 'male']) !!} Laki-laki
                                            <span></span>
                                        </label>
                                        <label class="radio-square-label">
                                            {!! Form::radio('sex', 'f', old('sex', isset($demographicData['sex']) && $demographicData['sex'] == 'f' ? 'checked' : ''), ['class' => 'icheck', 'tabindex' => 6, 'id' => 'female']) !!} Perempuan
                                            <span></span>
                                        </label>
                                    </div>

                                    @if ($scanType == 'card')
                                    <div class="popinfo sex">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    <input type="text" class="form-control infoable" autocomplete="off" id="location" name="location" value="{{ old('location', array_get($demographicData, 'location')) }}" tabindex="7">

                                    @if ($scanType == 'card')
                                    <div class="popinfo location">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="location">Lokasi Pengambilan</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    <div class="input-group">
                                        <input type="text" class="form-control infoable" autocomplete="off" data-id="datein" id="mask_date2" name="taken_date" value="{{ ! empty($demographicData['taken_date']) ? old('taken_date', $demographicData['taken_date']) : $takenDate }}" tabindex="8"> 
                                        <label for="mask_date2">Tanggal Pengambilan</label>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>

                                    @if ($scanType == 'card')
                                    <div id="datein" class="popinfo datepick">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    <input type="text" class="form-control infoable" id="takenby" autocomplete="off" name="taken_by" value="{{ ! empty($demographicData['taken_by']) ? old('taken_by', $demographicData['taken_by']) : $takenBy }}" tabindex="9">

                                    @if ($scanType == 'card')
                                    <div class="popinfo taken-by">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="takenby">Diambil Oleh</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input  ">
                                    <input type="text" class="form-control infoable" id="saksi" autocomplete="off" name="viewed_by" value="{{ old('viewed_by', array_get($demographicData, 'viewed_by')) }}" tabindex="10">

                                    @if ($scanType == 'card')
                                    <div class="popinfo saksi">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif

                                    <label for="saksi">Disaksikan Oleh</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-md-line-input  ">
                                    <textarea class="form-control infoable" rows="3" id="note" name="note" tabindex="11">{{ old('note', array_get($demographicData, 'note')) }}</textarea>

                                    @if ($scanType == 'card')
                                    <div class="popinfo note">
                                        <div class="image" <?php echo $bg; ?>></div>
                                    </div>
                                    @endif
                                    
                                    <label for="note">Catatan</label>

                                    <span class="help-block m-t-10">Bila ada jari cacat, buntung, dsb</span>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>  
            </div>
        </div>
    </div>
    </div>
</div>

<div class="text-center">
    <div class="btn-group m-b-30">
        <button class="btn red uiblock-on btn-lg uppercase" type="submit" name="back" tabindex="13"><i class="fa fa-angle-left m-r-15"></i> Kembali</button>  
        <button type="submit" id="nextButton" class="btn blue uiblock-on btn-lg uppercase" tabindex="12">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></button> 
    </div>
</div>
{!! Form::close() !!}

@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        var eKtp = $('#searchEKtp');
        var name = $('#fullname');
        var job = $('#job');

        BioverKtpel.Init({
            OnDataReceived: function(ktp){
                /**
                 * Available KTP Field Name:
                 * Nik, Name, Address, Neighbourhood, CommunityAssociation, District, Village,
                 * Gender, BloodType, Religion, MarriageStatus, Occupation, PlaceOfBirth,
                 * DateOfBirth, Province, City, DateOfExpiry, Nationality;
                 */

                if ( ktp.Nik != null )
                {
                    name.val(ktp.Name);
                    job.val(ktp.Occupation);

                    if( ktp.Gender == 'PEREMPUAN' ) 
                    {
                        $('#female').iCheck('check');
                    }
                    else
                    {
                        $('#male').iCheck('check');
                    }

                    $.ajax({
                        type: 'POST',
                        url: "{{ route('ajax_ektp_create_demographic') }}",
                        cache: false,
                        timeout: 300000,
                        data: ktp,
                        beforeSend: function() {
                            // App.blockUI();
                        },
                        error: function(xmlhttprequest, textstatus, message) {
                            // App.unblockUI();
                            if( textstatus === "timeout" ) 
                            {
                                alertify.alert("Ma'af, permintaan timeout");
                            } 
                            else 
                            {
                                alertify.alert(message);
                            }
                        },
                        success: function(data){
                            console.log(data);
                        }
                    });
                }
            },
            OnStartWait: function(){ App.blockUI(); },
            OnStopWait: function() { App.unblockUI(); }
        });

        eKtp.click(function(e) {
            BioverKtpel.ReadKtpEl();
        });

        //Autoread on load, in case there is ktp on device
        BioverKtpel.ReadKtpEl();
    });
});
</script>
@endpush