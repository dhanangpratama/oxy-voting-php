@extends('layouts.main')
@section('content')

@include('layouts.card_indicator')

{!! Form::open(['method'=>'POST', 'url'=>route('demographic_rolled_crop_do', [$finger])]) !!}

	<div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">{{ $fingerData[$finger]['title'] }}</div>
        </div>
        <div class="portlet-body form">
        	<div class="form-body">
                {{-- Hide rotate button, still error in cropping result
                <div class="m-b-20 text-center">
                    <div class="btn-group">
                        <button type="button" id="rotate_left" class="btn blue" title="Rotate Left">
                            <span class="fa fa-rotate-left"></span>
                        </button>
                        <button type="button" id="rotate_right" class="btn blue" title="Rotate Right">
                            <span class="fa fa-rotate-right"></span>
                        </button>
                    </div>
                </div> --}}
	        	<div class="row">
	        		<div class="col-sm-12" style="height: 640px;">
	        			<!-- This is the image we're attaching Jcrop to -->
                         <img src="{{ $file['baseUrl']."/front.{$ext}" }}?{{ time() }}" class="" id="cropbox" />

                         <input type="hidden" id="x" name="x" value="{{ array_has($dataFinger['fingers'], "{$finger}.point_crop.x") ? $dataFinger['fingers'][$finger]['point_crop']['x'] : $selectedPoint[0] }}" />
                         <input type="hidden" id="y" name="y" value="{{ array_has($dataFinger['fingers'], "{$finger}.point_crop.y") ? $dataFinger['fingers'][$finger]['point_crop']['y'] : $selectedPoint[1] }}" />
                         <input type="hidden" id="w" name="w" value="{{ array_has($dataFinger['fingers'], "{$finger}.point_crop.w") ? $dataFinger['fingers'][$finger]['point_crop']['w'] : $selectedPoint[2] }}" />
                         <input type="hidden" id="h" name="h" value="{{ array_has($dataFinger['fingers'], "{$finger}.point_crop.h") ? $dataFinger['fingers'][$finger]['point_crop']['h'] : $selectedPoint[3] }}" />
                         <input type="hidden" id="r" name="r" value="0" />
	        		</div>
	        	</div>
        	</div>
        </div>
    </div>

    <div class="text-center m-b-30">
        <div class="btn-group">
            <a class="btn red uiblock-on btn-lg uppercase" tabindex="37" href="{{ route('demographic_formula') }}"><i class="fa fa-times m-r-15"></i> Batal</a>
            <button class="btn blue uiblock-on btn-lg uppercase" tabindex="1" type="submit">Potong <i class="fa fa-check m-l-15"></i></button>
        </div>
    </div>

{!! Form::close() !!}
@endsection

@push('js_top')  
<script>  
head.ready(function() {
    jQuery(document).ready(function($) {
        $('#cropbox').cropper({
          aspectRatio: 1 / 1,
          background: false,
          zoomable: false,
          viewMode: 3,
          dragMode: 'move',
          data: {
            x: {{ array_has($dataFinger['fingers'], "{$finger}.point_crop.x") ? $dataFinger['fingers'][$finger]['point_crop']['x'] : $selectedPoint[0] }},
            y: {{ array_has($dataFinger['fingers'], "{$finger}.point_crop.y") ? $dataFinger['fingers'][$finger]['point_crop']['y'] : $selectedPoint[1] }},
            width: {{ array_has($dataFinger['fingers'], "{$finger}.point_crop.w") ? $dataFinger['fingers'][$finger]['point_crop']['w'] : $selectedPoint[2] }},
            height: {{ array_has($dataFinger['fingers'], "{$finger}.point_crop.h") ? $dataFinger['fingers'][$finger]['point_crop']['h'] : $selectedPoint[3] }}
          },
          crop: function(e) {
            // Output the result data for cropping image.
            $('#x').val(e.x);
            $('#y').val(e.y);
            $('#w').val(e.width);
            $('#h').val(e.height);
            $('#r').val(e.rotate);
          },
          built: function () {
            $('#rotate_right').click( function() {
                $('#cropbox').cropper('rotate', 3);
            });
            $('#rotate_left').click( function() {
                $('#cropbox').cropper('rotate', -3);
            });
          }
        });
    });
});
</script>
@endpush