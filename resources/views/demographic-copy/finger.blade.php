@extends('layouts.main')
@section('content')
<?php $bg = 'style="background-image: url('.$file['url_front'].');"'; ?>

@if ($scanType == 'card')
<a class="image-link" data-toggle="tooltip" title="Lihat kartu" data-placement="right" href="{{ $file['url_front'] }}"><i class="fa fa-file-image-o"></i></a>
@include('layouts.card_indicator')
@else
@include('layouts.live_scan_indicator')
@endif

<div class="ak-tab m-b-30">
    @if ($type == 'live' || $routeName == 'demographic_formula')
    <!-- Nav tabs -->
    <ul class="nav nav-tabs m-b-30" role="tablist">
        <li role="presentation" class="active"><a href="#roll" aria-controls="roll" role="tab" data-toggle="tab">Rolled</a></li>
        <li role="presentation"><a href="#flat" aria-controls="flat" role="tab" data-toggle="tab">Flat</a></li>
    </ul>
    @endif

    <!-- Tab panes -->
    
    {!! Form::open(['method'=>'POST'])  !!}

    <div class="tab-content {{ $scanType }}">
        <div role="tabpanel" class="tab-pane fade in active" id="roll">
            <div class="row">
                <?php $no = 1; ?>
                @foreach (config('ak23.finger_data') as $finger => $detail)
                <div class="col-xs-15 {{ array_has($dataFinger['fingers'], "{$finger}.pattern_classification") ? 'done' : '' }}">
                    <div class="panel panel-default">
                      <div class="panel-body rolled-equal-height">
                          
                            <a href="{{ route('demographic_finger_roll', [$finger]) }}" class="btn blue uppercase btn-block title btn-md m-t-0 m-b-15 uiblock-on">
                            {{ array_has($dataFinger, "fingers.{$finger}.pattern_classification") ? str_replace('_', ' ', $dataFinger['fingers'][$finger]['pattern_classification']) : '-' }}
                            </a>
                          
                        <div class="finger-wrapper">
                          {{-- <div class="text-center m-b-10">{{ $detail['num'] }}</div> --}}
                          @if ( file_exists($file['basePath'] . DIRECTORY_SEPARATOR . $finger.".{$ext}") )
                          <div class="relative">
                            <img class="img-responsive zoom" title="Scroll untuk memperbesar" src="{{ $file['baseUrl'].'/'.$finger.".{$ext}" }}?{{ time() }}" alt="{{ $finger }}">

                            @if (session('process.type') != 'live')
                            <a data-toggle="tooltip" data-placement="left" title="Potong" href="{{ route('demographic_rolled_crop', [$finger]) }}" class="btn btn-xs red uiblock-on btn-crop rd-0"><i class="fa fa-scissors"></i></a>
                            @endif

                            @if ( session('process.type') == 'card' && array_has($dataFinger['fingers'], "{$finger}.image_quality") )
                            <div class="finger-quality {{ image_quality_class($dataFinger['fingers'][$finger]['image_quality']) }}">{!! $dataFinger['fingers'][$finger]['image_quality'] !!}</div>
                            @endif
                          </div>
                          
                          @else
                          <img class="img-responsive" src="{{ asset('assets/images/blank-finger.png') }}" alt="{{ $finger }}">
                          @endif
                          
                        </div>
                      </div>
                      <div class="panel-footer uppercase aligncenter relative">
                        {{ $detail['title'] }}
                        @if ($routeName != 'demographic_formula')
                        <div class="mark-done"><i class="fa fa-check-circle"></i></div>
                        @endif
                      </div>
                    </div>
                </div>
                <?php $no++; ?>
                @endforeach
            </div>
        </div>

        @if ($scanType == 'live' || $routeName == 'demographic_formula')
        <div role="tabpanel" class="tab-pane fade in" id="flat">
          <div class="row">
            @foreach (config('ak23.finger_data') as $finger => $detail)
            <div class="col-sm-15">
              <div class="panel panel-default">
                  <div class="panel-body flat-equal-height">
                    <div class="btn blue uppercase btn-block title btn-md m-t-0 m-b-15">
                        {{ array_has($dataFinger, "fingers.{$finger}.pattern_classification") ? str_replace('_', ' ', $dataFinger['fingers'][$finger]['pattern_classification']) : '-' }}
                    </div>
                    <div class="finger-wrapper flat">
                    @if ( file_exists($file['basePath'] . DIRECTORY_SEPARATOR . $detail['flat_name']) )
                    <div class="relative">
                        <img class="img-responsive" src="{{ $file['baseUrl']."/{$detail['flat_name']}" }}" alt="{{ $detail['title'] }}">

                        @if ( session('process.type') == 'card' && array_has($dataFinger['fingers'], "flat_{$finger}.image_quality") )
                        <div class="finger-quality {{ image_quality_class($dataFinger['fingers']['flat_'.$finger]['image_quality']) }}">{{ $dataFinger['fingers']['flat_'.$finger]['image_quality'] }}</div>
                        @endif
                    </div>
                    @else
                    <img class="img-responsive" src="{{ asset('assets/images/blank-finger.png') }}" alt="{{ $finger }}">
                    @endif
                    </div>
                  </div>
                  <div class="panel-footer uppercase aligncenter relative">
                    {{ $detail['title'] }}
                  </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        @endif
      </div>
</div>

        @if ($routeName == 'demographic_formula')
        <div class="rumus m-b-30 clearfix text-center">
            <div class="rumus-box">
                <div class="rumus-wrapper pophover infoable">
                    <div class="rumus-label white">Rumus</div>
                    <?php $formula = !empty($demographicData['formula']) ? explode('|', $demographicData['formula']) : []; ?>
                    @for ($i = 0; $i <= 5; $i++)
                    <input type="text" class="" maxlength="4" autocomplete="off" id="formula_{{ $i+1 }}" name="formula[]" value="{{ old('formula[]', array_key_exists($i, $formula) ? trim($formula[$i]) : '') }}" tabindex="{{ 16+$i }}">
                    @endfor
                </div>
                @if ($scanType == 'card')
                <div class="popinfo rumus">
                    <div class="image" <?php echo $bg; ?>></div>
                </div>
                @endif
                <div class="lrumus-wrapper pophover infoable"> 
                    <div class="rumus-label white">Lihat Rumus</div>
                    <?php $formula_view = !empty($demographicData['formula_view']) ? explode('|', $demographicData['formula_view']) : []; ?>
                    @for ($i = 0; $i <= 5; $i++)
                    <input type="text" class="" maxlength="4" autocomplete="off" id="formula_view_{{ $i+1 }}" name="formula_view[]" value="{{ old('formula_view[]', array_key_exists($i, $formula_view) ? trim($formula_view[$i]) : '') }}" tabindex="{{ 23+$i }}">
                    @endfor
                </div>
                @if ($scanType == 'card')
                <div class="popinfo lrumus">
                    <div class="image" <?php echo $bg; ?>></div>
                </div>
                @endif
            </div>
        </div>
        @endif

        <div class="clearfix m-b-30 text-center">
            <div class="btn-group">
                <button class="btn red uiblock-on btn-lg uppercase" name="back" type="submit"><i class="fa fa-angle-left m-r-15"></i> Kembali</button>
                <button class="btn blue uiblock-on btn-lg uppercase" type="submit">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></button>
            </div>
        </div>
    {!! Form::close() !!}

@endsection

@push('js')

<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        $('.flat-equal-height, .rolled-equal-height').matchHeight();
    });
});
</script>
@if ( empty($error_status) && ! session()->has('finger_data') )
<script type="text/javascript">
head.ready("{{ asset('assets/js/henry.js') }}", function() {
 var jari = {!! $finger_formula !!}
  
  key = Key(jari);
  
  // console.log(key);
  // console.log(jari);

  maj=Major(jari);
  maj0 = maj[0] || " ";
  maj1 = maj[1] || " ";

  pri=Primary(jari);
  pri0= pri[0] || " ";
  pri1 = pri[1] || " ";

  sec=Secondary(jari);
  sec0 = sec[0] || " ";
  sec1 = sec[1] || " ";

  sub=Subsecondary(jari);
  sub0 = sub[0] || " ";
  sub1 = sub[1] || " ";

  fin=Final(jari);
  fin0 = fin[0] || " ";
  fin1 = fin[1] || " ";

  var rumus = maj0 + "|" + pri0 + "|" + sec0 + "|" + sub0 + "|" + fin0;
  var lrumus = maj1 + "|" + pri1 + "|" + sec1 + "|" + sub1 + "|" + fin1;

  var rumusArr = rumus.split("|");
  var lrumusArr = lrumus.split("|");

  console.log(rumusArr.length, lrumusArr);

  jQuery(document).ready(function($) {
    var fc = 1;
    for (i = 0; i < rumusArr.length; i++) 
    { 
      $("#formula_" + fc).val(rumusArr[i]);
      fc++;
    }

    var fvc = 1;
    for (i = 0; i < lrumusArr.length; i++) 
    { 
      $("#formula_view_" + fvc).val(lrumusArr[i]);
      fvc++;
    }

    $.ajax({
        url: "{{ url('demographic/formula_session') }}",
        method: 'GET',
        data: {
            'formula': rumus,
            'formula_view': lrumus
        },
        statusCode: {
          404: function() 
          {
            alertify.alert( "URL untuk menyimpan formula session tidak ditemukan." );
          }
        },
        success: function(data) {
            console.log(data); // do with data e.g success message
        },
        error: function(xhr, textStatus, errorThrown) {
            console.log(errorThrown);
        }
    });
  });
});
</script>
@endif
@endpush