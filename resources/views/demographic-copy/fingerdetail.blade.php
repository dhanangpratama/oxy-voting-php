@extends('layouts.main')
@section('content')
<?php $bg = 'style="background-image: url('.$file['url_front'].');"'; ?>

@if ( $scanType == 'card' )
@include('layouts.card_indicator')
@else
@include('layouts.live_scan_indicator')
@endif

  {!! Form::open(['method'=>'POST']) !!}
      <div class="panel panel-default">
          <div class="panel-heading">
              <h3 class="panel-title">
                  <strong>{{ $finger[$type]['title'] }}</strong>
              </h3>
          </div>
          <div class="panel-body">
            <div class="row">
              @if (count($errors) > 0)
              <div class="col-sm-12">
                  <div class="alert alert-danger">
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              </div>
              @endif
              <div class="col-sm-5">
                @if ( file_exists($file['basePath'] . "/{$type}.{$ext}") )
                <img class="zoom img-responsive img-bordered" title="Scroll untuk memperbesar" class="img-responsive img-thumbnail aligncenter" src="{{ $file['baseUrl'].'/'.$type.'.'.$ext }}?{{ time() }}" alt="{{ $finger[$type]['title'] }}">
                @else
                <img style="margin-top: 80px;" class="img-responsive img-thumbnail aligncenter" src="{{ asset('assets/images/blank-finger.png') }}" alt="{{ $finger[$type]['title'] }}">
                @endif
              </div>
              <div class="col-sm-7">
                <div class="row">
                  @foreach(config('ak23.henry') as $key => $code)
                  <div class="col-sm-4 equal-height">
                    <div class="chkbox {{ henry_checked($type, $key, $dataFinger['fingers']) }}" data-henry="{{ $key }}">
                      {{--<img src="{{ asset("assets/images/{$key}.jpg") }}" alt="1" class="img-responsive aligncenter">--}}
                      <?php
                      $label = str_replace('_', ' ', $key);
                      if ( $label == 'AMPUTATION' )
                        $label = 'AMPUTASI';

                      if ( $label == 'SCAR' )
                        $label = 'LUKA';

                      if ( $label == 'UNKNOWN' )
                        $label = 'TIDAK DIKETAHUI';
                      ?>
                      <div class="lable">{{ $label }}</div>
                    </div>
                  </div>
                  @endforeach
                  
                  <div class="col-sm-4 equal-height" style="position:relative;">
                  </div>
                  <?php /* <div class="col-sm-4">
                    <div class="chkbox {{ henry_checked($type, 'RIGHT_SLANT_LOOP') }}" data-henry="RIGHT_SLANT_LOOP">
                      <img src="{{ asset('assets/images/r-l.jpg') }}" alt="2" class="img-responsive aligncenter">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="chkbox {{ henry_checked($type, 'PLAIN_ARCH') }}" data-henry="PLAIN_ARCH">
                      <img src="{{ asset('assets/images/p-a.jpg') }}" alt="3" class="img-responsive aligncenter">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="chkbox {{ henry_checked($type, 'WHORL') }}" data-henry="WHORL">
                      <img src="{{ asset('assets/images/w.jpg') }}" alt="4" class="img-responsive aligncenter">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="chkbox {{ henry_checked($type, 'TENTED_ARCH') }}" data-henry="TENTED_ARCH">
                      <img src="{{ asset('assets/images/t-a.jpg') }}" alt="5" class="img-responsive aligncenter">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="chkbox {{ henry_checked($type, 'AMPUTATION') }}" data-henry="AMPUTATION">
                      <img src="{{ asset('assets/images/t-a.jpg') }}" alt="5" class="img-responsive aligncenter">
                    </div>
                  </div> */ ?>
                </div>
              </div>
            </div>
          </div>
      </div>

      <div class="text-center">
        <div class="btn-group">
          <a class="btn red uiblock-on btn-lg uppercase" href="{{ route('demographic_formula') }}"><i class="fa fa-times m-r-15"></i> Batal</a>
          <button type="submit" class="btn blue uiblock-on btn-lg uppercase">Setuju <i class="fa fa-check m-l-15"></i></button>
        </div>
      </div>
      <input type="hidden" name="henry" id="henry" value="{{ array_has($dataFinger['fingers'], "{$type}.patternClassification") ? $dataFinger['fingers'][$type]['pattern_classification'] : '' }}">
  {!! Form::close() !!}

@endsection

@push('js')
<script>
head.ready(function() {
  jQuery(document).ready(function($) {
      $('.equal-height').matchHeight();
  });
});
</script>
@endpush