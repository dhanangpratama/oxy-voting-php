@extends('layouts.main')
@section('content')

@if ( $scanType == 'card' )
@include('layouts.card_indicator')
@else
@include('layouts.live_scan_indicator')
@endif

{!! Form::open(['method'=>'POST', 'url' => route('demographic_picture_crop_do', [$position])]) !!}

	<div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">{{ facePositionName($position) }}</div>
        </div>
        <div class="portlet-body form">
        	<div class="form-body">
                {{-- Hide rotate button, still error in cropping result
                <div class="m-b-20 text-center">
                    <div class="btn-group">
                        <button type="button" id="rotate_left" class="btn blue" title="Rotate Left">
                            <span class="fa fa-rotate-left"></span>
                        </button>
                        <button type="button" id="rotate_right" class="btn blue" title="Rotate Right">
                            <span class="fa fa-rotate-right"></span>
                        </button>
                    </div>
                </div> --}}
	        	<div class="row">
	        		<div class="col-sm-12" style="height: 600px;">
	        			<!-- This is the image we're attaching Jcrop to -->
                         <img src="{{ $imageUrl }}?{{ time() }}" class="" id="cropbox" />

                         <input type="hidden" id="x" name="x" value="159" />
                         <input type="hidden" id="y" name="y" value="3094" />
                         <input type="hidden" id="w" name="w" value="1113" />
                         <input type="hidden" id="h" name="h" value="1536" />
                         <input type="hidden" id="r" name="r" value="0" />
	        		</div>
	        	</div>
        	</div>
        </div>
    </div>

    <div class="text-center m-b-30">
        <div class="btn-group">
            <a class="btn red  uiblock-on btn-lg uppercase" tabindex="37" href="{{ $cancelUrl }}"><i class="fa fa-times m-r-15"></i> Batal</a>
            <button class="btn blue uiblock-on btn-lg uppercase" tabindex="1" type="submit">Potong <i class="fa fa-check m-l-15"></i></button>
        </div>
    </div>

{!! Form::close() !!}

@endsection

@push('js_top')
<script type="text/javascript">
head.ready(function() {
    jQuery(document).ready(function($) {
        $('#cropbox').cropper({
          aspectRatio: 4 / 6,
          background: false,
          zoomable: false,
          viewMode: 0,
          dragMode: 'move',
          data: {
            x: 159,
            y: 3094,
            width: 1113,
            height: 1536
          },
          crop: function(e) {
            // Output the result data for cropping image.
            $('#x').val(e.x);
            $('#y').val(e.y);
            $('#w').val(e.width);
            $('#h').val(e.height);
            $('#r').val(e.rotate);
          },
          built: function () {
            $('#rotate_right').click( function() {
                $('#cropbox').cropper('rotate', 3);
            });
            $('#rotate_left').click( function() {
                $('#cropbox').cropper('rotate', -3);
            });
          }
        });
    });
});
</script>
@endpush