@extends('layouts.main')
@section('content')

@include('layouts.card_indicator')

    {!! Form::open() !!}

     <div id="sectionContainer" class="bg-main-color p-20" style="margin-bottom: 30px; min-height: 730px;">
          <div class="row">
               <div class="col-sm-6">
                    <div id="front" class="card-box">
                        <?php
                            $imageFront = "";
                            if(file_exists(session('process.basePath') . DIRECTORY_SEPARATOR . 'front.jpg')) $imageFront = data_url('card/front.jpg');
                        ?>
                        <img src="{{ $imageFront }}" class="img-responsive" id="image-front">
                    </div>
               </div>
               <div class="col-sm-6" style="">
                    <div id="back" class="card-box">
                        <?php
                            $imageBack = "";
                            if(file_exists(session('process.basePath') . DIRECTORY_SEPARATOR . 'back.jpg')) $imageBack = data_url('card/back.jpg');
                        ?>
                        <img src="{{ $imageBack }}" class="img-responsive" id="image-back">
                    </div>
               </div>
          </div>
     </div>

     <div class="text-center">
        <div class="btn-group">
            <a href="{{ route('demographic_card_scan_record', ['action' => 'new']) }}" id="button-repeat-card" name="repeat" value="1" class="btn red btn-lg uppercase uiblock-on"><i class="fa fa-repeat m-r-15"></i> Ulangi</a>
            <button type="submit" id="button-next" class="btn blue btn-lg uppercase uiblock-on" disabled>Selanjutnya <i class="fa fa-chevron-right m-l-15"></i></button>
        </div>
     </div>

     {!! Form::close() !!}

@endsection

@push('js')
    <script type="application/javascript">
        head.ready(function() {
            jQuery(document).ready(function() {

                function checkFileComplete()
                {
                    $.post("{{ route('ajax_card_check_complete') }}", function(data, status){
                        if (data == 'OK')
                        {
                            $('#button-next').prop("disabled", false);
                        }
                    });
                }

                $(window).load(function() 
                {
                    checkFileComplete();
                });

                BioverCardScan.Init({
                    OnStatusMessage: function(message){ console.log(message); },
                    OnImageReady: OnImageReady,
                    OnNextImagePosition: OnNextImagePosition
                });

                var position = null;

                function OnImageReady(imagePosition) {
                    switch(imagePosition) {
                        case "FRONT":
                            $("#image-front").attr("src", "{{ data_url('card/front.jpg') }}?" + Math.random());
                            position = 'front';
                            break;

                        case "BACK":
                            $("#image-back").attr("src", "{{ data_url('card/back.jpg') }}?" + Math.random());
                            position = 'back';
                            break;
                    }

                    checkFileComplete();
                }

                function OnNextImagePosition(imagePosition) {

                    $(".card-box").removeClass("card-box-blink");
                    switch(imagePosition) {
                        case "FRONT":
                            $("#front").addClass("card-box-blink");
                            break;

                        case "BACK":
                            $("#back").addClass("card-box-blink");
                            break;
                    }

                }

                BioverCardScan.StartScan();
                BioverCardScan.IsDeviceAvailable().then(function(isDeviceAvailable) {
                    if(isDeviceAvailable){
                        BioverCardScan.GetNextImagePosition().then(function(imagePosition) {
                            OnNextImagePosition(imagePosition);
                        });

                    } else {
                        swal({
                            html: 'Tolong hubungkan <em>Card Scanner</em> dan tekan tombol <strong>Restart</strong> di bawah ini.',
                            type: 'error',
                            title: 'Perangkat Tidak Terhubung',
                            padding: 30,
                            width: 650,
                            showCancelButton: true,
                            confirmButtonColor: '#0376B8',
                            cancelButtonColor: '#e12330',
                            confirmButtonText: 'Restart',
                            cancelButtonText: 'Tidak',
                            allowOutsideClick: false
                        }).then(function () {
                            swal({
                                html: 'Apakah anda yakin mau restart?',
                                type: 'warning',
                                padding: 30,
                                width: 600,
                                showCancelButton: true,
                                confirmButtonColor: '#0376B8',
                                cancelButtonColor: '#e12330',
                                confirmButtonText: 'Ya',
                                cancelButtonText: 'Tidak',
                                allowOutsideClick: false
                            }).then(function () {
                                Biover.ApplicationRestart();
                            }, function() {
                                App.blockUI();
                                go(home_url + '/dashboard');
                            });
                        }, function() {
                            App.blockUI();
                            go(home_url + '/dashboard');
                        });
                    }
                });

                $("#front").on("click", function(){ BioverCardScan.SetImagePosition("FRONT");});
                $("#back") .on("click", function(){ BioverCardScan.SetImagePosition("BACK"); });
            });
        });
    </script>
@endpush