@extends('layouts.main')
@section('content')

<div id="live_scan_wrapper">
     <div class="slaps bg-main-color p-20 m-b-20">
          <table class="table table-main table-slaps mb-0">
               <tr>
                    <td class="title">Empat Jari Kiri</td>
                    <td class="title">Dua Jempol</td>
                    <td class="title">Empat Jari Kanan</td>
               </tr>
               <tr>
                    <td>
                         <div id="flat_four_left_fingers" class="slaps-fingers"></div>
                    </td>
                    <td>
                         <div id="flat_thumbs" class="slaps-fingers"></div>
                    </td>
                    <td>
                         <div id="flat_four_right_fingers" class="slaps-fingers"></div>
                    </td>
               </tr>
          </table>
     </div>

     <div class="detail-fingers m-b-20">
          <div class="tabbable-custom ">
               <ul class="nav nav-tabs ">
                    <li class="active">
                         <a href="#flat" data-toggle="tab" aria-expanded="true"> Flat Fingers </a>
                    </li>
                    <li class="">
                         <a href="#rolled" data-toggle="tab" aria-expanded="false"> Rolled Fingers </a>
                    </li>
               </ul>
               <div class="tab-content bg-main-color">
                    <div class="tab-pane active" id="flat">
                         <table class="table table-main table-fingers mb-0">
                              <tr>
                                   <td class="title">Jempol Kanan</td>
                                   <td class="title">Jari Telunjuk Kanan</td>
                                   <td class="title">Jari Tengah Kanan</td>
                                   <td class="title">Jari Manis Kanan</td>
                                   <td class="title">Jari Kelingking Kanan</td>
                              </tr>
                              <tr>
                                   <td>
                                        <div id="flat_right_thumb" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="flat_right_index" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="flat_right_middle" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="flat_right_ring" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="flat_right_little" class="single-finger"></div>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="title">Jempol Kiri</td>
                                   <td class="title">Jari Telunjuk Kiri</td>
                                   <td class="title">Jari Tengah Kiri</td>
                                   <td class="title">Jari Manis Kiri</td>
                                   <td class="title">Jari Kelingking Kiri</td>
                              </tr>
                              <tr>
                                   <td>
                                        <div id="flat_left_thumb" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="flat_left_index" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="flat_left_middle" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="flat_left_ring" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="flat_left_little" class="single-finger"></div>
                                   </td>
                              </tr>
                         </table>
                    </div>
                    <div class="tab-pane" id="rolled">
                         <table class="table table-main table-fingers mb-0">
                              <tr>
                                   <td class="title">Jempol Kanan</td>
                                   <td class="title">Jari Telunjuk Kanan</td>
                                   <td class="title">Jari Tengah Kanan</td>
                                   <td class="title">Jari Manis Kanan</td>
                                   <td class="title">Jari Kelingking Kanan</td>
                              </tr>
                              <tr>
                                   <td>
                                        <div id="rolled_right_thumb" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="rolled_right_index" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="rolled_right_middle" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="rolled_right_ring" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="rolled_right_little" class="single-finger"></div>
                                   </td>
                              </tr>
                              <tr>
                                   <td class="title">Jempol Kiri</td>
                                   <td class="title">Jari Telunjuk Kiri</td>
                                   <td class="title">Jari Tengah Kiri</td>
                                   <td class="title">Jari Manis Kiri</td>
                                   <td class="title">Jari Kelingking Kiri</td>
                              </tr>
                              <tr>
                                   <td>
                                        <div id="rolled_left_thumb" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="rolled_left_index" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="rolled_left_middle" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="rolled_left_ring" class="single-finger"></div>
                                   </td>
                                   <td>
                                        <div id="rolled_left_little" class="single-finger"></div>
                                   </td>
                              </tr>
                         </table>
                    </div>
               </div>
          </div>
     </div>

     <div class="button-wrapper">
          <a class="btn btn-circle btn-lg blue btn-block" href="#">Simpan</a>
     </div>
</div>

@endsection