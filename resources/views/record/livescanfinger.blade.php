@extends('layouts.main')
@section('content')

     @include('layouts.live_scan_indicator')

    {!! Form::open() !!}
     <div id="sectionContainer" class="bg-main-color p-20" style="margin-bottom: 30px;">
          <div class="row">
               <div class="col-sm-6 ">
                    <div class="finger-preview"></div>
               </div>
               <div class="col-sm-6  text-center" style="overflow-y: scroll">
                    <div class="relative">
                        <div id="finger-quality" class=" white"></div>
                    </div>
                    <div id="device-message" class="alert alert-info"></div>
                    <h1 id="finger-seq-name" class="finger-title"></h1>
                    <div class="finger-image">
                         <img id="finger-seq-image" src="{{ asset('assets/images/palm.png') }}" class="img-responsive" alt="hand">
                    </div>
                    
                    <div class="button-wrapper">
                         <p>
                              <button type="button" id="button-start"  class="btn red hide" disabled>Mulai</button>
                         </p>
                    </div>

               </div>
          </div>
     </div>

     @foreach(config('ak23.finger_index') as $finger)
     <?php
     $flat = 'flat_'.$finger;
     ?>
     {!! Form::textarea($finger, null, ['id' => strtoupper($finger), 'class' => 'hidden']) !!}
     {!! Form::textarea($flat, null, ['id' => strtoupper($flat), 'class' => 'hidden']) !!}
     @endforeach

     {!! Form::textarea('flat_thumbs', null, ['id' => 'FLAT_THUMBS', 'class' => 'hidden']) !!}
     {!! Form::textarea('flat_right_four_fingers', null, ['id' => 'FLAT_RIGHT_FOUR_FINGERS', 'class' => 'hidden']) !!}
     {!! Form::textarea('flat_left_four_fingers', null, ['id' => 'FLAT_LEFT_FOUR_FINGERS', 'class' => 'hidden']) !!}

     <div class="text-center">
        <div class="btn-group">
            <a href="{{ route('demographic_finger_scan_record', ['action' => 'new']) }}" class="btn red btn-lg uppercase uiblock-on" diabled><i class="fa fa-repeat m-r-15"></i> Ulangi</a>
            <button type="submit" id="button-next"   class="btn blue btn-lg uppercase uiblock-on" disabled>Selanjutnya <i class="fa fa-chevron-right m-l-15"></i></button>
            <button type="button" id="button-skip" class="btn red btn-lg uppercase">Lewati <i class="fa fa-chevron-right m-l-15"></i></button>
        </div>
     </div>

     {!! Form::close() !!}


@endsection

@push('js')
<script type="application/javascript">
    head.load("{{ asset('js/livescanfinger.js') }}?{{ time() }}", function(){

        var bodyHeight = $('body').height();
        $('.smhe').height(bodyHeight - 450);

        $.fn.livescanfinger({
            elementFingerScreenPreview: $('.finger-preview'),
            elementDeviceMessage: $("#device-message"),
            elementButtonStart: $("#button-start"),
            elementButtonNext: $("#button-next"),
            elementButtonRepeat: $("#button-repeat"),
            elementButtonSkip: $("#button-skip"),
            elementFingerSeqName: $("#finger-seq-name"),
            elementFingerSeqImage: $("#finger-seq-image"),
            elementFingerQuality: $("#finger-quality"),
            urlBaseImage: "{{ asset('assets/images/') }}/",
            urlBaseImageFingerSequence: "{{ asset('assets/images/fingersequence') }}/"

        });

    });
</script>
@endpush