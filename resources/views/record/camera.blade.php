@extends('layouts.main')
@section('content')

@include('layouts.live_scan_indicator')

{!! Form::open(['class' => 'sinyalemen-form']) !!}
<div class="camera-wrapper bg-main-color p-20 m-b-20">
     <div class="row">
          <div class="col-sm-8">
               <select class="video-source"></select>
               <div class="camera-preview" style="overflow: hidden">
                    <video muted autoplay class="video-webcam"></video>
                   <div class="face-holder front" ></div>
               </div>
          </div>
          <div class="col-sm-4">
               <div class="camera-button-wrapper">
                    <button type="button" data-toggle="tooltip" title="Tampak Depan" data-placement="bottom" id="front" class="face-position">
                        <img id="front-image" src="{{ file_exists($file['basePath'] . DIRECTORY_SEPARATOR . 'face-front.jpg') ? $file['baseUrl'] . '/face-front.jpg?' . time() : asset('assets/images/ico-front.png?' . time()) }}" alt="">
                        
                        {!! Form::hidden("face_front",  "", ['class' => '', 'id' => 'front-image-text']) !!}

                        <a href="#" title="Unggah" data-photo="face-front.jpg" data-update="#front-image" class="upload-image"><i class="fa fa-upload"></i></a>
                        <a href="#" title="Hapus" data-remove="#front-image-text" data-photo="face-front.jpg" class="remove-image"><i class="fa fa-trash"></i></a>
                    </button>
                    <button type="button" id="left" data-toggle="tooltip" title="Samping Kiri" data-placement="bottom" id="left" class="face-position">
                        <img id="left-image" src="{{ file_exists($file['basePath'] . DIRECTORY_SEPARATOR . 'face-left.jpg') ? $file['baseUrl'] . '/face-left.jpg?' . time() : asset('assets/images/ico-left-side.png?' . time()) }}" alt="">
                        
                        {!! Form::hidden("face_left",  "", ['class' => '', 'id' => 'left-image-text']) !!}

                        <a href="#" title="Unggah" data-photo="face-left.jpg" data-update="#left-image" class="upload-image"><i class="fa fa-upload"></i></a>
                        <a href="#" title="Hapus" data-remove="#left-image-text" data-photo="face-left.jpg" class="remove-image"><i class="fa fa-trash"></i></a>
                    </button>
                    <button type="button" id="right" data-toggle="tooltip" title="Samping Kanan" data-placement="bottom" id="front" class="face-position">
                        <img id="right-image" src="{{ file_exists($file['basePath'] . DIRECTORY_SEPARATOR . 'face-right.jpg') ? $file['baseUrl'] . '/face-right.jpg?' . time() : asset('assets/images/ico-right-side.png?' . time()) }}" alt="">

                        {!! Form::hidden("face_right",  "", ['class' => '', 'id' => 'right-image-text']) !!}
                        
                        <a href="#" title="Unggah" data-photo="face-right.jpg" data-update="#right-image" class="upload-image"><i class="fa fa-upload"></i></a>
                        <a href="#" title="Hapus" data-remove="#right-image-text" data-photo="face-right.jpg" class="remove-image"><i class="fa fa-trash"></i></a>
                    </button>
               </div>
          </div>
     </div>
</div>

<div class="text-center">
    <div class="btn-group">
        <button type="submit" class="btn red uiblock-on btn-lg uppercase" name="back"><i class="fa fa-angle-left m-r-15"></i> Kembali</button>
        <button type="submit" class="btn green uiblock-on btn-lg uppercase">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></button>
    </div>
</div>
{!! Form::close() !!}

<!-- Modal -->
<div class="modal fade" id="chooseFile" tabindex="-1" role="dialog" aria-labelledby="chooseFileLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="chooseModalLabel">Cari File</h4>
      </div>
      <div class="modal-body">
        <div id="preview" class="pull-right" style="max-width: 100px;"></div>
        <?php 
	
            if($drive != '')
            {	
                $arrayList = driveRead($drive, $arrayList = array());
                
                if(count($arrayList) > 0) 
                {
                    asort($arrayList);
                
                    echo '<ul>';
                    
                    foreach($arrayList as $thisArray) 
                    {
                        foreach($thisArray as $label => $value)
                            $$label = $value;
                        
                        echo '<li><a href="#" class="path-file" data-full="'.$full.'" data-ext="'.$ext.'">'.$name.'</a></li>';
                    }
                    
                    echo '</ul>';	
                }
                else
                    echo 'Tidak menemukan file yang didukung';		
            }
            else
                echo '<p>Mohon untuk menghubungkan USB Drive.</p> <p>Klik tombol dibawah jika sudah menghubungkan USB Drive. Kemudian lakukan proses filih file lagi.</p><p><a href="javascript:reload()" class="btn btn-sm blue">Cek USB Drive</a></p>';	
        ?>

        <div class="clearfix"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn red uppercase btn-lg" data-dismiss="modal">Batal</button>
        <button type="button" class="btn blue uppercase btn-lg"  id="btnSelect" disabled>Pilih</button>
      </div>
    </div>
  </div>
</div>


@endsection
@push('js')


<script type="application/javascript">

    head.load("{{ asset('js/jquery.webcam.js') }}?{{ time() }}", function(){
        var camera = $('video');

        camera.webcam({
            videoDeviceName: "HD Pro Webcam C920",
            videoSelect: document.querySelector('select.video-source')
        });

        $('#front').on('click', onClickCapture);
        $('#left').on('click', onClickCapture);
        $('#right').on('click', onClickCapture);

        function onClickCapture() {
            var btn = this;
            camera.webcam('captureImage', function(image)
            {
                {{--  $("textarea", btn).html(image);
                $("img", btn).attr("src", image);  --}}

                $.ajax({
                    url: home_url + '/ajax/process-photo',
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function() {
                        App.blockUI();
                    },
                    data: {
                        imageData: image 
                    },
                    cache: false,
                    error: function(xhr, status, errorThrown) {
                         alertify.alert("Gagal merekam gambar");
                    },
                    success: function(response, status, xhr) {
                        App.unblockUI();

                        if ( response.status == 'OK' )
                        {
                            $("img", btn).attr("src",response.image);
                            $("input[type=hidden]", btn).val(response.image);

                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                theme: 'metroui',
                                text: 'Foto berhasil direkam',
                                timeout: 3000,
                                progressBar: true,
                                closeWith: ['button'],
                                animation: {
                                    open: 'noty_effects_open',
                                    close: 'noty_effects_close'
                                }
                            }).show();
                        }
                        else
                        {
                            alertify.alert("Gagal merekam gambar");
                        }   
                    }
                });
            });
        }

        $('.upload-image').click(function(e) {
            e.stopPropagation();

            $('#chooseFile').modal('show');
            $('#btnSelect').attr({"data-position": $(this).data('photo'), "data-update":$(this).data('update')});
        });

        $('#chooseFile').on('hide.bs.modal', function (e) {
            // do something...
            $('#btnSelect').removeAttr('data-position');
            $('#btnSelect').prop("disabled", true);
            $('.path-file').removeClass('selected');
        });

        $('.path-file').click(function(e)
        {
            e.preventDefault();
            var $this = $(this);

            $('.path-file').removeClass('selected');
            $this.addClass('selected');

            $.ajax({ 
				url: "{{ route('ajax_laten_preview') }}",
				data: {'filename' : $this.data('full'), 'ext' : $this.data('ext')},
				type: 'post',
				beforeSend: function(){
					/*	SHOW PLEASE WAIT MESSAGE OR A LOADING STATE	*/
					
					var thisHTML = '<p>Please wait...</p>';
					$('#preview').html(thisHTML).show();
			   },
				success: function(data)
				{
                    // console.log(data);
					
					var thisHTML = '<img class="img-responsive" src="' + data + '" />';
					$('#preview').html(thisHTML).show();

                    $('#btnSelect').prop("disabled", false);
				},
				error: function ()
				{
                    $('#chooseFile').modal('hide');

                    swal({
                        html: "Ma'af, File sudah tidak ditemukan.",
                        type: 'error',
                        padding: 30,
                        width: 600,
                        showCancelButton: false,
                        confirmButtonColor: '#0376B8',
                        cancelButtonColor: '#e12330',
                        confirmButtonText: 'OK',
                        cancelButtonText: 'Tidak',
                        allowOutsideClick: false
                    }).then(function () {
                        App.blockUI();
                        window.location.reload(false); 
                    });

					$('#preview').hide();
				}
            });
        });

        $(document).on('click', '#btnSelect', function(e) {
            e.preventDefault();

            $('#fingerPreview').html('');

            var $this = $('.path-file.selected');
            var $t = '{{ time() }}';

            $.ajax({
                type: 'POST',
                url: "{{ route('ajax_upload_live_photo') }}",
                cache: false,
                timeout: 300000,
                data: {
                    path: $this.attr('data-full'),
                    ext: $this.attr('data-ext'),
                    position: $(this).attr('data-position')
                },
                beforeSend: function() {
                    App.blockUI();
                },
                error: function(xmlhttprequest, textstatus, message) {
                    App.unblockUI();
                    if( textstatus === "timeout" ) 
                    {
                        alertify.alert("Ma'af, permintaan timeout");
                    } 
                    else 
                    {
                        alertify.alert(message);
                    }
                },
                success: function(data)
                {
                    $('#chooseFile').modal('hide');

                    App.unblockUI();

                    $($('#btnSelect').attr('data-update')).attr('src', data.url + '?t=' + Math.random());

                    new Noty({
                        type: 'success',
                        layout: 'topRight',
                        theme: 'metroui',
                        text: 'Foto berhasil diunggah',
                        timeout: 8000,
                        progressBar: true,
                        closeWith: ['button'],
                        animation: {
                            open: 'noty_effects_open',
                            close: 'noty_effects_close'
                        }
                    }).show();
                }
            });

            {{--  $('#fingerFile').val($this.data('full'));
            $('#fingerFileType').val($this.data('ext'));
            $('#chooseFinger').modal('hide');
            
            
            var thisHTML = '<img class="img-responsive" src="'+home_url+'/files/tmp/file-preview.jpg?t='+Math.random()+'" />';
			$('#fingerPreview').html(thisHTML);  --}}
        });

        $('.remove-image').click(function(e) {
            e.preventDefault();
            e.stopPropagation();

            var thisButton = $(this);
            var textRemove = thisButton.attr('data-remove');

            swal({
                html: 'Yakin akan menghapus foto ini?',
                type: 'warning',
                padding: 50,
                width: 600,
                showCancelButton: true,
                confirmButtonColor: '#0376B8',
                cancelButtonColor: '#e12330',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                allowOutsideClick: false
            }).then(function () {
                $.ajax({
                    url: home_url + '/ajax/remove-photo',
                    type: 'post',
                    dataType: 'json',
                    beforeSend: function() {
                        App.blockUI();
                    },
                    data: {
                        image: thisButton.attr('data-photo')
                    },
                    cache: false,
                    error: function(xhr, status, errorThrown) {
                            alertify.alert("Gagal menghapus foto");
                    },
                    success: function(response, status, xhr) {
                        App.unblockUI();

                        if (response.status == 'OK')
                        {
                            thisButton.closest('button').children('img').attr('src', response.noImageUrl);
                            $(textRemove).val('');

                            new Noty({
                                type: 'success',
                                layout: 'topRight',
                                theme: 'metroui',
                                text: 'Foto berhasil dihapus',
                                timeout: 8000,
                                progressBar: true,
                                closeWith: ['button'],
                                animation: {
                                    open: 'noty_effects_open',
                                    close: 'noty_effects_close'
                                }
                            }).show();
                        }
                        else
                        {
                            alertify.alert(response.message);
                        }
                    }
                });
            });
        });
    });

</script>
@endpush