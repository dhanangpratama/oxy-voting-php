<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!--><html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@if ( ! empty($title) ){{ $title }} -@endif {{ config('const.AppName') }}</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}?{{ time() }}" type="image/x-icon" />
<link rel="icon" href="{{ asset('assets/images/favicon.png') }}?{{ time() }}" type="image/x-icon" />
<!--[if lt IE 9]>
<script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
<script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script> 
<script src="{{ asset('js/html5shiv.min.js') }}'"></script>
<![endif]-->
<link href="{{ asset('css/vendor.min.css') }}?{{ time() }}" rel="stylesheet" type="text/css" />
@stack('css')
<script src="{{ asset('js/head.load.js') }}"></script>
<script>
var home_url = "{{ url('') }}";
var asset_url = "{{ asset('assets') }}";
var apiServer = "{{ config('url.javaApiServer') }}";
var appAccess = "{{ config('ak23.access') }}";
var unknownImage = asset_url+'/images/unknown-big.png';
var winWidth = 1935;
var winHeight = 1080;


    head.load("{{ asset('js/jquery.min.js') }}?{{ time() }}", "{{ asset('js/vendor.js') }}?{{ time() }}", "{{ asset('js/jquery.pwstrength.min.js') }}?{{ time() }}", "{{ asset('assets/js/scripts.js') }}?{{ time() }}");



head.ready(function() {
    $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
});
</script>
@stack('js_top')
<!-- END HEAD -->
</head>