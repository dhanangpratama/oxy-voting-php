<div class="step-indicator card clearfix {{ session('process.mode') == 'edit' ? 'edit' : '' }} m-b-20">
  @foreach(config('ak23.card_breadcrumbs') as $key => $data)
  <?php
  if (session('process.mode') == 'edit' && ($data['id'] == 'identification' || $data['id'] == 'scan_card') ) continue;
  $args = [];
  if ($data['route'] == 'demographic_finger_flat') $args = ['right_thumb']; ?>
  <a class="step uiblock-on {{ $key < session('current_indicator') ? 'completed' : '' }} {{ $key == session('current_indicator') ? 'current' : '' }} }}" href="{{ route($data['route'], $args) }}">{{ $data['title'] }}</a>
  @endforeach
</div>