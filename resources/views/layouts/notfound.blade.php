<!DOCTYPE html>
<html>
<head>
<title>404 Not Found</title>
<link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/dist/css/bootstrap.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/bootstrap/dist/css/bootstrap-theme.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<script src="{{ asset('vendor/jquery/dist/jquery.min.js') }}"></script>
</head>
<body>
@include('layouts.nav')
@yield('content')
@include('layouts.footer')