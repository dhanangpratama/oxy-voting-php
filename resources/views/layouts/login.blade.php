@include('layouts.header')

<body {!! config('ak23.access') == 'web' ? 'oncontextmenu="return false;"' : '' !!} class=" login">
    <div class="wrapper">
        <!-- BEGIN LOGO -->
        <div class="logo-ak23">
            <a href="{{ url('/') }}">
                <img src="{{ asset('assets/images/logo.jpg') }}?{{ time() }}" class="img-logo-login"  alt="" />
            </a>
        </div>
        <!-- END LOGO -->
        
        @yield('content')
    
    </div>

@include('layouts.footer.login')