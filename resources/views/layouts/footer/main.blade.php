<div class="page-footer">
    <div class="page-footer-inner"> &copy; {{ Carbon\Carbon::now()->format('Y') }} Partai Amanat Nasional. All rights reserved.</a>
    </div>
    @if ( config('ak23.access') == 'desktop' )
    <div id="clock" class="pull-right">
        <span class="unit" id="hours"></span>:<span class="unit" id="minutes"></span>:<span class="unit" id="seconds"></span> <span class="unit" id="ampm"></span>
    </div>

    @if ( !empty($option->get('last_sync_time')) )
    <div class="pull-right" style="color:#98a6ba">
        Sinkronisasi Terakhir: <span id="last_sync_time">{{ date('d M Y H:i', $option->get('last_sync_time')) }}</span>
    </div>
    @endif
    @endif
    <div class="scroll-to-top" style="display: none;">
        <i class="icon-arrow-up"></i>
    </div>
</div>

{{--  @foreach ($messageBroadcast as $dataMessage)
<div id="message_{{ $dataMessage->message_id }}" class="white-popup mfp-hide broadcast-{{ $dataMessage->type }}">
    <h3 class="mt-0">Informasi</h3>
  <div class="alert alert-{{ $dataMessage->type }}">{!! $dataMessage->message !!}</div>
</div>
@endforeach  --}}

<div id="mac_address" class="white-popup mfp-hide">
</div>

<script>
head.ready(function() {
    $(window).load(function() {   
        $.sessionTimeout({
            warnAfter: 600000, // 10 minutes
            redirAfter: 900000, // 15 minutes
            appendTime: true,
            logoutUrl: home_url+'/logout',
            redirUrl: home_url+'/logout',
            stayConnectedBtn: 'Tetap Terhubung',
            logoutBtn: 'Keluar',
            message: 'Aplikasi akan keluar otomatis jika tidak ada aktifitas',
            title: 'Sesi akan habis'
        });
    });
});
</script>
{{-- <script src="{{ asset('assets/js/vue.min.js') }}"></script> --}}
@if ( session()->has('status') )
<script>
head.ready(function() {
  $(window).load(function() {
    setTimeout(function() {
      new Noty({
          type: '{{ session('status')['type'] }}',
          layout: 'topRight',
          theme: 'metroui',
          text: '{!! session('status')['message'] !!}',
          timeout: 8000,
          progressBar: true,
          closeWith: ['button'],
          animation: {
              open: 'noty_effects_open',
              close: 'noty_effects_close'
          }
      }).show();
    }, 500);
});
});
</script>
@endif
@stack('js')
</body>
</html>