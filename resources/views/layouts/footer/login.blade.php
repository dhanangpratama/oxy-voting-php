
<script>
head.ready(function() {
    $(window).load(function() {
        $('body').animate({
            opacity: 1
        }, 300, function() {
            App.unblockUI();
            $('body').addClass('visible');
        });
    });
});
</script>

@if ( session()->has('status') )
<script>
head.ready(function() {
    $(window).load(function() {
    setTimeout(function() {
        new Noty({
            type: '{{ session('status')['type'] }}',
            layout: 'topRight',
            theme: 'metroui',
            text: '{!! session('status')['message'] !!}',
            timeout: 8000,
            progressBar: true,
            closeWith: ['button'],
            animation: {
                open: 'noty_effects_open',
                close: 'noty_effects_close'
            }
        }).show();
    }, 500);
});
});
</script>
@endif
@stack('js')
</body>
</html>