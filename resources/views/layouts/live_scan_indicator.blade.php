<div class="step-indicator {{ session('process.mode') == 'edit' ? 'edit' : '' }} clearfix m-b-20">
  @foreach(config('ak23.live_breadcrumbs') as $key => $data)
  <?php if (session('process.mode') == 'edit' && $data['id'] == 'identification') continue; ?>
  <a class="step uiblock-on {{ $key < session('current_indicator') ? 'completed' : '' }} {{ $key == session('current_indicator') ? 'current' : '' }} }}" href="{{ route($data['route']) }}">{{ $data['title'] }}</a>
  @endforeach
</div>