@if ($breadcrumbs)
    <ul class="page-breadcrumb breadcrumb pull-right">
        @foreach ($breadcrumbs as $breadcrumb)
            @if (!$breadcrumb->last)
                <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
                <li class="active"><span>{{ $breadcrumb->title }}</span></li>
            @endif
        @endforeach
    </ul>
@endif