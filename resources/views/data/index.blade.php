@extends('layouts.main')
@section('content')

    <div class="portlet box dark">
        <div class="portlet-title">
            
            <div class="caption">
                Data {{ $dataRelation[$type]['label'] }}
            </div>
            
        </div>
        <div class="portlet-body">
            <a href="{{ route('data_create', [$type]) }}" class="btn blue pull-right uppercase uiblock-on">Buat Baru</a>
            <table class="table grey">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th>Label</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if ( $data->count() )
                    @foreach ($data as $value)
                    <tr>
                        <td width="80" class="text-center">{{ $value->$primaryKey }}</td>
                        <td>{{ $value->label }}</td>
                        <td width="100">
                            <a href="{{ route('data_edit', [$type, encrypt($value->$primaryKey)]) }}" class="btn blue btn-xs uiblock-on">Ubah</a>
                            <button type="button" data-url="{{ route('data_remove', [$type, encrypt($value->$primaryKey)]) }}" class="remove btn red btn-xs"><i class="fa fa-times"></i></button>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="3" align="center">Belum ada data</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

    {{ $data->links() }}
                
@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        $('.remove').click(function(event) {
            event.preventDefault();
            if ( ! $(this).hasClass('disabled') )
            {
                var $url = $(this).attr('data-url');
                swal({
                    title: 'Konfirmasi',
                    html: 'Yakin data akan dihapus?',
                    type: 'warning',
                    padding: 50,
                    width: 600,
                    showCancelButton: true,
                    confirmButtonColor: '#0376B8',
                    cancelButtonColor: '#e12330',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    allowOutsideClick: false
                }).then(function () {
                    App.blockUI();
                    go($url);
                });
            }
        });
    });
});
</script>
@endpush