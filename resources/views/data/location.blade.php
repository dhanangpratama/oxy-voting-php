@extends('layouts.main')
@section('content')

    <div class="portlet box dark">
        <div class="portlet-title">
            
            <div class="caption">
                Data Lokasi
            </div>
            
        </div>
        <div class="portlet-body">
            <a href="{{ route('data_location_create') }}" class="btn blue pull-right uppercase uiblock-on">Buat Baru</a>
            <table class="table grey">
                <thead>
                    <tr>
                        <th>Nama Lokasi</th>
                        <th>Aktif</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if ( $locations->count() )
                    @foreach ($locations as $value)
                    <tr>
                        <td>{{ $value->name }}</td>
                        <td width="80">{{ yesNo($value->activated) }}</td>
                        <td width="150">
                            <a href="{{ route('data_location_edit', [encrypt($value->location_id)]) }}" class="btn blue btn-xs uiblock-on">Ubah</a>
                            <button type="button" data-url="{{ route('data_remove', ['location', encrypt($value->location_id)]) }}" class="remove btn red btn-xs">Hapus</button>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="4" align="center">Belum ada data</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
                
@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        $('.remove').click(function(event) {
            event.preventDefault();
            if ( ! $(this).hasClass('disabled') )
            {
                var $url = $(this).attr('data-url');
                swal({
                    title: 'Konfirmasi',
                    html: 'Yakin data akan dihapus?',
                    type: 'warning',
                    padding: 50,
                    width: 600,
                    showCancelButton: true,
                    confirmButtonColor: '#0376B8',
                    cancelButtonColor: '#e12330',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    allowOutsideClick: false
                }).then(function () {
                    App.blockUI();    
                    go($url);
                });
            }
        });
    });
});
</script>
@endpush