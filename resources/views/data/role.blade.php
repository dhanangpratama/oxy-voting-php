@extends('layouts.main')
@section('content')

    <div class="portlet box dark">
        <div class="portlet-title">
            
            <div class="caption">
                Data Level
            </div>
            
        </div>
        <div class="portlet-body">
            <a href="{{ route('data_role_create') }}" class="btn blue pull-right uppercase uiblock-on">Buat Baru</a>
            <table class="table grey">
                <thead>
                    <tr>
                        <th class="text-center">ID</th>
                        <th>Nama</th>
                        <th>Nama Tampilan</th>
                        <th>Deskripsi</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if ( $roles->count() )
                    @foreach ($roles as $value)
                    <tr>
                        <td width="80" class="text-center">{{ $value->id }}</td>
                        <td width="200">{{ $value->name }}</td>
                        <td width="300">{{ $value->display_name }}</td>
                        <td>{{ check_empty($value->description) }}</td>
                        <td width="100">
                            <a href="{{ route('data_role_edit', [encrypt($value->id)]) }}" class="btn blue btn-xs uiblock-on">Ubah</a>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td colspan="5" align="center">Belum ada data</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>

    {{ $roles->links() }}
                
@endsection