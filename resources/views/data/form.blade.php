@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'post', 'class' => 'sinyalemen-form']) !!}
<div class="portlet box dark">
    <div class="portlet-title">
        <div class="caption">
            {{ $page == 'edit-data' ? 'Ubah' : 'Buat' }} Data {{ $dataRelation[$type]['label'] }}
        </div>
    </div>
    <div class="portlet-body">
        <!-- BEGIN REGISTRATION FORM -->
        
            <div class="row">
                @if ($page == 'edit-data')
                <div class="col-sm-12">
                    <div class="form-group form-md-line-input">
                        {!! Form::text(null,  $data->$primaryKey, ['class' => 'form-control placeholder-no-fix', 'id' => 'id', 'disabled' => 'disabled']) !!}
                        {{ Form::label('id', 'ID') }}
                    </div>
                </div>
                @endif
                <div class="col-sm-12">
                    <div class="form-group form-md-line-input {{ ! empty($errors->data->first('label')) ? 'has-error' : '' }}">
                        {!! Form::text('label', $page == 'edit-data' ? $data->label : null, ['class' => 'form-control placeholder-no-fix', 'id' => 'label']) !!}
                        {{ Form::label('label', 'Label') }}
                        {!! ! empty($errors->data->first('label')) ? '<span id="name" class="help-block">'.$errors->data->first('label').'</span>' : '' !!}
                    </div>
                </div>
            </div>
        
        <!-- END REGISTRATION FORM -->
    </div>
</div>
<div class="form-actions text-center">
    <div class="btn-group">
        <a href="{{ route('data', [$type]) }}" class="btn red uppercase btn-lg uiblock-on"><i class="fa fa-times m-r-10"></i> Batal</a>
        <button type="submit" id="register-submit-btn" class="btn blue uppercase btn-lg">Simpan <i class="fa fa-check m-l-10"></i></button>
    </div>
</div>
{!! Form::close() !!}
            
@endsection

@push('js')
<script>
var Form = function() {

    var handleCreate = function() {

        jQuery('.sinyalemen-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                // simple rule, converted to {required:true}
                label: {
                    required: true
                }
            },
            messages: {
                label: {
                    required: "Tidak boleh kosong"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                swal({
                    html: 'Yakin pengisian data sudah benar?',
                    type: 'warning',
                    padding: 50,
                    width: 600,
                    showCancelButton: true,
                    confirmButtonColor: '#0376B8',
                    cancelButtonColor: '#e12330',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    allowOutsideClick: false
                }).then(function () {
                    App.blockUI();
                    form.submit();
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handleCreate();

        }

    };

}();

head.ready(function() 
{
    Form.init();

    jQuery(document).ready(function($) {
        $('#start_date').datetimepicker({
            format: 'DD-MM-YYYY',
            debug: false
        });
        $('#end_date').datetimepicker({
            useCurrent: false, 
            format: 'DD-MM-YYYY'
        });
        $("#start_date").on("dp.change", function (e) {
            $('#end_date').data("DateTimePicker").minDate(e.date);
        });
        $("#end_date").on("dp.change", function (e) {
            $('#start_date').data("DateTimePicker").maxDate(e.date);
        });
    });
});
</script>
@endpush