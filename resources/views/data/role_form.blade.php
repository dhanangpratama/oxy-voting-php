@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'post', 'class' => 'role-form']) !!}
<div class="portlet box dark">
    <div class="portlet-title">
        <div class="caption">
            {{ $page == 'edit-role' ? 'Ubah' : 'Buat' }} Data Level Pengguna
        </div>
    </div>
    <div class="portlet-body">
        <!-- BEGIN REGISTRATION FORM -->
            <div class="row">
                @if ($page == 'edit-role')
                <div class="col-sm-12">
                    <div class="form-group form-md-line-input">
                        {!! Form::text(null,  $role->id, ['class' => 'form-control placeholder-no-fix', 'id' => 'id', 'disabled' => 'disabled']) !!}
                        {{ Form::label('id', 'ID') }}
                    </div>
                </div>
                @endif
                <div class="col-sm-12">
                    <div class="form-group form-md-line-input {{ ! empty($errors->role->first('name')) ? 'has-error' : '' }}">
                        {!! Form::text('name', $page == 'edit-role' ? $role->name : null, ['class' => 'form-control placeholder-no-fix', 'id' => 'name']) !!}
                        {{ Form::label('name', 'Nama') }}
                        {!! ! empty($errors->role->first('name')) ? '<span id="name" class="help-block">'.$errors->role->first('name').'</span>' : '' !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-md-line-input {{ ! empty($errors->role->first('display_name')) ? 'has-error' : '' }}">
                        {!! Form::text('display_name', $page == 'edit-role' ? $role->display_name : null, ['class' => 'form-control placeholder-no-fix', 'id' => 'display_name']) !!}
                        {{ Form::label('display_name', 'Nama Tampilan') }}
                        {!! ! empty($errors->role->first('display_name')) ? '<span id="name" class="help-block">'.$errors->role->first('display_name').'</span>' : '' !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-md-line-input {{ ! empty($errors->role->first('description')) ? 'has-error' : '' }}">
                        {!! Form::textarea('description', $page == 'edit-role' ? $role->description : null, ['class' => 'form-control placeholder-no-fix', 'id' => 'description', 'rows' => 5]) !!}
                        {{ Form::label('description', 'Deskripsi') }}
                        {!! ! empty($errors->role->first('description')) ? '<span id="name" class="help-block">'.$errors->role->first('description').'</span>' : '' !!}
                    </div>
                </div>
            </div>
        
        <!-- END REGISTRATION FORM -->
    </div>
</div>
<div class="text-center">
    <div class="btn-group">
        <a href="{{ route('data_role') }}" class="btn red uppercase btn-lg uiblock-on"><i class="fa fa-times m-r-10"></i> Batal</a>
        <button type="submit" id="register-submit-btn" class="btn blue uppercase btn-lg">Simpan <i class="fa fa-check m-l-10"></i></button>
    </div>
</div>
{!! Form::close() !!}
            
@endsection

@push('js')
<script>
var Form = function() {

    var handleCreate = function() {

        jQuery('.role-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                // simple rule, converted to {required:true}
                name: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Tidak boleh kosong"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                swal({
                    title: 'Konfirmasi',
                    html: 'Yakin pengisian data sudah benar?',
                    type: 'warning',
                    padding: 50,
                    width: 600,
                    showCancelButton: true,
                    confirmButtonColor: '#0376B8',
                    cancelButtonColor: '#e12330',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    allowOutsideClick: false
                }).then(function () {
                    App.blockUI();    
                    form.submit();
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handleCreate();

        }

    };

}();

head.ready(function() 
{
    Form.init();

    jQuery(document).ready(function($) {
        $('#start_date').datetimepicker({
            format: 'DD-MM-YYYY',
            debug: false
        });
        $('#end_date').datetimepicker({
            useCurrent: false, 
            format: 'DD-MM-YYYY'
        });
        $("#start_date").on("dp.change", function (e) {
            $('#end_date').data("DateTimePicker").minDate(e.date);
        });
        $("#end_date").on("dp.change", function (e) {
            $('#start_date').data("DateTimePicker").maxDate(e.date);
        });
    });
});
</script>
@endpush