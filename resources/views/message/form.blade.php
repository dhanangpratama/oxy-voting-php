@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'post', 'class' => 'message-form']) !!}

<div class="portlet box dark">
    <div class="portlet-title">
        <div class="caption">
            Buat Pesan Baru
        </div>
    </div>
    <div class="portlet-body">
        <!-- BEGIN REGISTRATION FORM -->
        <form class="register-form" action="index.html" method="post">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input {{ ! empty($errors->message->first('start_date')) ? 'has-error' : '' }}">
                        {!! Form::text('start_date', $page == 'edit-message' ? format_date($message->start_date) : null, ['class' => 'form-control placeholder-no-fix', 'id' => 'start_date']) !!}
                        {{ Form::label('start_date', 'Tanggal Mulai') }}
                        {!! ! empty($errors->message->first('start_date')) ? '<span id="name" class="help-block">'.$errors->message->first('start_date').'</span>' : '' !!}
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group form-md-line-input {{ ! empty($errors->message->first('end_date')) ? 'has-error' : '' }}">
                        {!! Form::text('end_date', $page == 'edit-message' ? format_date($message->end_date) : null, ['class' => 'form-control placeholder-no-fix', 'id' => 'end_date']) !!}
                        {{ Form::label('end_date', 'Tanggal Selesai') }}
                        {!! ! empty($errors->message->first('end_date')) ? '<span id="end_date" class="help-block">'.$errors->message->first('end_date').'</span>' : '' !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-md-line-input {{ ! empty($errors->message->first('type')) ? 'has-error' : '' }}">
                        {!! Form::select('type', config('ak23.messageType'), $page == 'edit-message' ? $message->type : null, ['class' => 'form-control', 'id' => 'role', 'placeholder' => '-- Pilih Tipe --']) !!}
                        {{ Form::label('type', 'Tipe') }}
                        {!! ! empty($errors->message->first('type')) ? '<span id="email" class="help-block">'.$errors->message->first('type').'</span>' : '' !!}
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-md-line-input {{ ! empty($errors->message->first('message')) ? 'has-error' : '' }}">
                        {!! Form::textarea('message', $page == 'edit-message' ? $message->message : null, ['class' => 'form-control placeholder-no-fix', 'id' => 'message', 'rows' => '5']) !!}
                        {{ Form::label('message', 'Pesan') }}
                        {!! ! empty($errors->message->first('message')) ? '<span id="email" class="help-block">'.$errors->message->first('message').'</span>' : '' !!}
                    </div>
                </div>
            </div>
        <!-- END REGISTRATION FORM -->
    </div>
</div>


<div class="form-actions text-center">
    <button type="submit" id="register-submit-btn" class="btn blue btn-lg uppercase">Buat</button>
</div>

{!! Form::close() !!}
            
@endsection

@push('js')
<script>
var Form = function() {

    var handleCreate = function() {

        jQuery('.message-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                // simple rule, converted to {required:true}
                start_date: {
                    required: true
                },
                end_date: {
                    required: true
                },
                type: {
                    required: true
                },
                message: {
                    required: true
                }
            },
            messages: {
                start_date: {
                    required: "Tidak boleh kosong"
                },
                end_date: {
                    required: "Tidak boleh kosong"
                },
                type: {
                    required: "Tidak boleh kosong"
                },
                message: {
                    required: "Tidak boleh kosong"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            submitHandler: function(form) {
                swal({
                    html: 'Yakin pengisian sudah benar?',
                    type: 'info',
                    padding: 50,
                    width: 600,
                    showCancelButton: true,
                    confirmButtonColor: '#0376B8',
                    cancelButtonColor: '#e12330',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak',
                    allowOutsideClick: false
                }).then(function () {
                    App.blockUI();
                    form.submit();
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handleCreate();

        }

    };

}();

head.ready(function() 
{
    Form.init();

    jQuery(document).ready(function($) {
        $('#start_date').datetimepicker({
            format: 'DD-MM-YYYY',
            debug: false
        });
        $('#end_date').datetimepicker({
            useCurrent: false, 
            format: 'DD-MM-YYYY'
        });
        $("#start_date").on("dp.change", function (e) {
            $('#end_date').data("DateTimePicker").minDate(e.date);
        });
        $("#end_date").on("dp.change", function (e) {
            $('#start_date').data("DateTimePicker").maxDate(e.date);
        });
    });
});
</script>
@endpush