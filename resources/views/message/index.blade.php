@extends('layouts.main')
@section('content')

    <div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">
                Data Pesan 
            </div>
        </div>
        <div class="portlet-body">
            <a href="{{ route('message_create') }}" class="btn blue pull-right uiblock-on">Buat Baru</a>
            <table class="table grey">
                <thead>
                    <tr>
                        <th>Tanggal Mulai</th>
                        <th>Tanggal Akhir</th>
                        <th>Tipe</th>
                        <th>Pesan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @if ( $messages->count() )
                    @foreach ($messages as $message)
                    <tr>
                        <td width="200">{{ Date::formatIndonesia($message->start_date) }}</td>
                        <td width="200">{{ Date::formatIndonesia($message->end_date) }}</td>
                        <td width="100">{{ $message->type }}</td>
                        <td>{{ str_limit($message->message, 150) }}</td>
                        <td width="80">
                            <a href="{{ route('message_edit', [encrypt($message->message_id)]) }}" class="btn blue btn-xs uiblock-on"><i class="fa fa-pencil"></i></a>
                            <a href="{{ route('message_remove', [encrypt($message->message_id)]) }}" class="remove btn red btn-xs"><i class="fa fa-trash"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5" align="center">Belum ada data</td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>

    {{ $messages->links() }}
                
@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        $('.remove').click(function(event) {
            event.preventDefault();
            var $url = $(this).attr('href');
            swal({
                title: 'Konfirmasi',
                html: 'Yakin data akan dihapus?',
                type: 'warning',
                padding: 50,
                width: 600,
                showCancelButton: true,
                confirmButtonColor: '#0376B8',
                cancelButtonColor: '#e12330',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                allowOutsideClick: false
            }).then(function () {
                App.blockUI();
                go($url);
            });
        });
    });
});
</script>
@endpush