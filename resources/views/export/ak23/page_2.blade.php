<table width="100%" cellpadding="0" cellspacing="0">
<tr>
    <td width="370"></td>
    <td>
        <table width="100%" cellpadding="5" cellspacing="0">
            <tr>
                <td width="30"></td>
                <td>SINYALEMEN</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td width="370">
        <table width="100%" cellpadding="3" cellspacing="0">
            <tr>
                <td width="30"></td>
                <td width="23">1.</td>
                <td width="125">Tanggal lahir</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people->dob) ? date('d-m-Y', strtotime($people->dob)) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23"></td>
                <td width="125">Tempat lahir</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people->pob) ? $people->pob : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">2.</td>
                <td width="125">Kebangsaan/Suku</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people->nationality) ? $people->nationality : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">3.</td>
                <td width="125">Agama</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($religion) ? $religion : '-' }} </td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">4.</td>
                <td width="125">Alamat Terakhir</td>
                <td width="15">:</td>
                <td width="200">{!! ! empty($address) ? trim($address) : '-' !!}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">5.</td>
                <td width="125">KTP. No.</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people->nik) ? $people->nik : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">6.</td>
                <td width="125">Pendidikan Terakhir</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($education) ? $education : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">7.</td>
                <td width="125">Nama Ayah/Alamat</td>
                <td width="15">:</td>
                <td width="165">{!! ! empty($people->father_name.$father_address) ? $people->father_name.$father_address : '-' !!}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">8.</td>
                <td width="125">Nama Ibu/Alamat</td>
                <td width="15">:</td>
                <td width="165">{!! ! empty($people->mother_name.$mother_address) ? $people->mother_name.$mother_address : '-' !!}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">9.</td>
                <td width="125">Nama Istri/Suami</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people->mate_name) ? $people->mate_name : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23"></td>
                <td width="125">Alamat</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people->mate_address) ? $people->mate_address : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">10.</td>
                <td width="125">Nama Anak-anak</td>
                <td width="15">:</td>
                <td></td>
            </tr>
            <tr>
                <td width="30"></td>
                <td></td>
                <td colspan="3">
                    <table width="100%" cellpadding="5" cellspacing="0">
                        <tr>
                            <td width="50%"> {{ ! empty($child_1) ? $child_1 : '-' }}</td>
                            <td width="50%">{{ $child_2 }}</td>
                        </tr>
                        <tr>
                            <td width="50%">{{ $child_3 }}</td>
                            <td width="50%">{{ $child_4 }}</td>
                        </tr>
                        <tr>
                            <td width="50%">{{ $child_5 }}</td>
                            <td width="50%">{{ $child_6 }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">11.</td>
                <td width="125">Tulisan/Tanda Tangan</td>
                <td width="15">:</td>
                <td></td>
            </tr>
        </table>
    </td>
    <td>
        <table width="100%" cellpadding="3" cellspacing="0">
            <tr>
                <td width="30"></td>
                <td width="115">Golongan Darah</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people->blood_type) ? strtoupper($people->blood_type) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Tinggi badan</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people->height) ? $people->height : '-' }} CM</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Berat timbangan</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people->weight) ? $people->weight : '-' }} KG</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Warna kulit</td>
                <td width="15">:</td>
                <td width="200">{{ $skin }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Bentuk tubuh</td>
                <td width="15">:</td>
                <td>{{ $body }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Bentuk kepala</td>
                <td width="15">:</td>
                <td width="200">{{ $head }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Warna rambut</td>
                <td width="15">:</td>
                <td width="200">{{ $hair_color }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Jenis rambut</td>
                <td width="15">:</td>
                <td width="200">{{ $hair }} </td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Bentuk muka</td>
                <td width="15">:</td>
                <td width="200">{{ $face }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Dahi</td>
                <td width="15">:</td>
                <td width="200">{{ $forehead }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Warna mata</td>
                <td width="15">:</td>
                <td width="200">{{ $eye_color }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Kelainan pada mata</td>
                <td width="15">:</td>
                <td width="200">{{ $eye_irregularity }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Hidung</td>
                <td width="15">:</td>
                <td width="200">{{ $nose }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Bibir</td>
                <td width="15">:</td>
                <td width="200">{{ $lip }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Gigi</td>
                <td width="15">:</td>
                <td width="200">{{ $tooth }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Dagu</td>
                <td width="15">:</td>
                <td width="200">{{ $chin }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Telinga</td>
                <td width="15">:</td>
                <td width="200">{{ $ear }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Tatoo</td>
                <td width="15">:</td>
                <td width="140">{{ ! empty($people->tattoo) ? $people->tattoo : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Dipotong &amp; Cacat</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people->scars_and_handicap) ? $people->scars_and_handicap : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Kode Pasal</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people->provision_code) ? $people->provision_code : '-' }}</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="3" height="10"></td>
</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td width="100%">
        <table width="100%" cellpadding="5" cellspacing="0" border="1">
            <tr>
                <td align="center" rowspan="2" height="180"> {!! $face_front !!} </td>
                <td align="center" rowspan="2" height="180"> {!! $face_left !!} </td>
                <td align="center" rowspan="2" height="180"> {!! $face_right !!} </td>
                <td align="center" height="22">Keterangan</td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
    </td>
</tr>
</table>