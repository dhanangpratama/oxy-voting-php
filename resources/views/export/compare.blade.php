<?php $hal = 1; ?>
<table border="1" cellpadding="3" cellspacing="0">
	<tr>
		<td width="20%">Nama kandidat:</td>
		<td width="30%">{{ ! empty($candidate_data->fullname) ? title_case($candidate_data->fullname) : "-" }}</td>
		<td width="20%">NIK:</td>
		<td width="30%">{{ ! empty($candidate_data->nik) ? $candidate_data->nik : '-' }}</td>
	</tr>
	<tr>
		<td width="20%">Tanggal Cetak:</td>
		<td width="30%">{{ date('d/m/Y H:i:s') }}</td>
		<td width="20%">Halaman</td>
		<td width="30%">{{ $hal++ }}</td>
	</tr>
</table>
<div class="test" height="20"></div>
<table border="1" cellpadding="5" cellspacing="0">
	<tr>
		<td width="100%">
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="100%" align="center"><h2>Demographic Data Verification</h2></td>
				</tr>
				<tr>
					<td width="100%" align="center" height="30"><p>{{ date('H:i:s / d/m/Y') }}</p></td>
				</tr>
			</table>

			<table border="0" cellpadding="2" cellspacing="0">
				<tr>
					<td width="50%">
						<table border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td align="center" colspan="4" height="30"><strong>Permintaan</strong></td>
							</tr>
							<tr>
								<td width="30%">ID</td>
								<td width="3%">:</td>
								<td width="37%">-</td>
								<td width="30%" rowspan="6">
									<img src="{{ $request_image }}" alt="{{ title_case($request_data['fullname']) }}">
								</td>
							</tr>
							<tr>
								<td width="30%">Jenis Kelamin</td>
								<td width="3%">:</td>
								<td width="37%">{{ $request_data['sex'] != 'm' ? 'Perempuan' : 'Laki-laki' }}</td>
							</tr>
							<tr>
								<td width="30%">Tgl Lahir</td>
								<td width="3%">:</td>
								<td width="37%">{{ ! empty($request_data['dob']) ? format_date($request_data['dob']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Tempat Lahir</td>
								<td width="3%">:</td>
								<td width="37%">{{ ! empty($request_data['pob']) ? title_case($request_data['pob']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Tempat Pengambilan</td>
								<td width="3%">:</td>
								<td width="37%">{{ ! empty($request_data['location']) ? title_case($request_data['location']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Tanggal Pengambilan</td>
								<td width="3%">:</td>
								<td width="37%">{{ ! empty($request_data['taken_date']) ? format_date($request_data['taken_date']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Rumus</td>
								<td width="3%">:</td>
								<td width="270">
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="center"> {{ $formula['requested'][0] }} </td>
				                            <td align="center"> {{ $formula['requested'][1] }} </td>
				                            <td align="center"> {{ $formula['requested'][2] }} </td>
				                            <td align="center"> {{ $formula['requested'][3] }} </td>
				                            <td align="center"> {{ $formula['requested'][4] }} </td>
				                            <td align="center"> {{ $formula['requested'][5] }} </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="30%">Lihat Rumus</td>
								<td width="3%">:</td>
								<td width="270">
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="center"> {{ $formula_view['requested'][0] }} </td>
				                            <td align="center"> {{ $formula_view['requested'][1] }} </td>
				                            <td align="center"> {{ $formula_view['requested'][2] }} </td>
				                            <td align="center"> {{ $formula_view['requested'][3] }} </td>
				                            <td align="center"> {{ $formula_view['requested'][4] }} </td>
				                            <td align="center"> {{ $formula_view['requested'][5] }} </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="30%">Nama</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($request_data['fullname']) ? title_case($request_data['fullname']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Alamat</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($request_data['address']) ? $request_data['address'] : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Pekerjaan</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($request_data['job']) ? title_case($request_data['job']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Nama Ayah</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($request_data['father_name']) ? title_case($request_data['father_name']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Nama Ibu</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($request_data['mother_name']) ? title_case($request_data['mother_name']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Nama Istri / Suami</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($request_data['mate_name']) ? title_case($request_data['mate_name']) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Tinggi Badan (cm)</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($request_data['height']) ? $request_data['height'] : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Berat Badan (kg)</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($request_data['weight']) ? $request_data['weight'] : '-' }}</td>
							</tr>
						</table>
					</td>
					<td width="50%">
						<table border="0" cellpadding="3" cellspacing="0">
							<tr>
								<td align="center" colspan="4" height="30"><strong>Kandidat</strong></td>
							</tr>
							<tr>
								<td width="30%" rowspan="6">
									<img src="{{ $candidate_image }}" alt="{{ title_case($candidate_data->fullname) }}">
								</td>
								<td width="30%">ID</td>
								<td width="3%">:</td>
								<td width="37%">{{ $candidate_data->subject_id }}</td>
							</tr>
							<tr>
								<td width="30%">Jenis Kelamin</td>
								<td width="3%">:</td>
								<td width="37%">{{ $candidate_data->sex != 'm' ? 'Perempuan' : 'Laki-laki' }}</td>
							</tr>
							<tr>
								<td width="30%">Tgl. Lahir</td>
								<td width="3%">:</td>
								<td width="37%">{{ ! empty($candidate_data->dob) ? format_date($candidate_data->dob) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Tempat Lahir</td>
								<td width="3%">:</td>
								<td width="37%">{{ ! empty($candidate_data->pob) ? title_case($candidate_data->pob) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Tempat Pengambilan</td>
								<td width="3%">:</td>
								<td width="37%">{{ ! empty($candidate_data->location) ? title_case($candidate_data->location) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Tanggal Pengambilan</td>
								<td width="3%">:</td>
								<td width="37%">{{ ! empty($candidate_data->taken_date) ? format_date($candidate_data->taken_date) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Rumus</td>
								<td width="3%">:</td>
								<td width="270">
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="center"> {{ $formula['candidate'][0] }} </td>
				                            <td align="center"> {{ $formula['candidate'][1] }} </td>
				                            <td align="center"> {{ $formula['candidate'][2] }} </td>
				                            <td align="center"> {{ $formula['candidate'][3] }} </td>
				                            <td align="center"> {{ $formula['candidate'][4] }} </td>
				                            <td align="center"> {{ $formula['candidate'][5] }} </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="30%">Lihat Rumus</td>
								<td width="3%">:</td>
								<td width="270">
									<table border="0" cellpadding="0" cellspacing="0">
										<tr>
											<td align="center"> {{ $formula_view['candidate'][0] }} </td>
				                            <td align="center"> {{ $formula_view['candidate'][1] }} </td>
				                            <td align="center"> {{ $formula_view['candidate'][2] }} </td>
				                            <td align="center"> {{ $formula_view['candidate'][3] }} </td>
				                            <td align="center"> {{ $formula_view['candidate'][4] }} </td>
				                            <td align="center"> {{ $formula_view['candidate'][5] }} </td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td width="30%">Nama</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($candidate_data->fullname) ? title_case($candidate_data->fullname) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Alamat</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($candidate_data->address) ? title_case($candidate_data->address) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Pekerjaan</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($candidate_data->job) ? title_case($candidate_data->job) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Nama Ayah</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($candidate_data->father_name) ? title_case($candidate_data->father_name) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Nama Ibu</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($candidate_data->mother_name) ? title_case($candidate_data->mother_name) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Nama Istri / Suami</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($candidate_data->mate_name) ? title_case($candidate_data->mate_name) : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Tinggi Badan (cm)</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($candidate_data->height) ? $candidate_data->height : '-' }}</td>
							</tr>
							<tr>
								<td width="30%">Berat Badan (kg)</td>
								<td width="3%">:</td>
								<td width="67%">{{ ! empty($candidate_data->weight) ? $candidate_data->weight : '-' }}</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<br pagebreak="true" />


@foreach (array_reverse(config('ak23.finger_data')) as $key => $data)
	<table border="1" cellpadding="3" cellspacing="0">
		<tr>
			<td width="20%">Nama kandidat:</td>
			<td width="30%">{{ ! empty($candidate_data->fullname) ? title_case($candidate_data->fullname) : "-" }}</td>
			<td width="20%">NIK:</td>
			<td width="30%">{{ ! empty($candidate_data->nik) ? $candidate_data->nik : '-' }}</td>
		</tr>
		<tr>
			<td width="20%">Tanggal Cetak:</td>
			<td width="30%">{{ date('d/m/Y H:i:s') }}</td>
			<td width="20%">Halaman</td>
			<td width="30%">{{ $hal++ }}</td>
		</tr>
	</table>
	<div height="30"></div>
	<table border="1" cellpadding="3" cellspacing="0">
		<tr>
			<td width="100%" align="center"><strong>{{ title_case($data['title']) }}</strong></td>
		</tr>
	</table>
	<div height="30"></div>
	<table border="0" cellspacing="3" cellpadding="0">
		<tr>
			<td width="50%" align="center">
				@if ( File::exists(config('path.data.tmp_minutiae') . DIRECTORY_SEPARATOR . "{$key}.jpg") )
					<img class="aligncenter" src="{{ config('path.data.tmp_minutiae') . DIRECTORY_SEPARATOR . "{$key}.jpg" }}" alt="{{ $data['title'] }}" />
				@else
					Data sidik jari tidak tersedia
				@endif
			</td>
			<td width="50%" align="center">
				@if ( File::exists(array_get($candidate_path_data, 'path') . DIRECTORY_SEPARATOR . "_minutiae_{$key}.jpg") )
					<img class="aligncenter" src="{{ array_get($candidate_path_data, 'path') . DIRECTORY_SEPARATOR . "_minutiae_{$key}.jpg" }}" alt="{{ $data['title'] }}" />
				@else
					Data sidik jari tidak tersedia
				@endif
			</td>
		</tr>
	</table>
	
	@if ( $key != 'right_thumb' )
	<br pagebreak="true" />
	@endif
@endforeach