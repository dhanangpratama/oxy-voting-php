<table width="100%" cellpadding="0" cellspacing="0">
<tr>
    <td colspan="3" align="right">Bentuk : AK-23</td>
</tr>
<tr>
    <td align="center" width="215">
        &nbsp;<br>
        BADAN RESERSE KRIMINAL POLRI<br>PUSINAFIS<br>
        <table cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td width="7"></td>
                <td>
                    <hr width="195" height="2">
                </td>
            </tr>
        </table>
    </td>
    <td width="10"></td>
    <td align="left">
        <table width="100%" cellpadding="3" border="1" cellspacing="0">
            <tr>
                <td {!! ($people[0]['personstatus'] == 'PENDUDUK') ? 'bgcolor="#000000" style="color: #FFFFFF"' : '' !!}>PENDUDUK</td>
                <td {!! ($people[0]['personstatus'] == 'SKLD') ? 'bgcolor="#000000" style="color: #FFFFFF"' : '' !!}>SKLD</td>
            </tr>
            <tr>
                <td {!! ($people[0]['personstatus'] == 'PEGAWAI') ? 'bgcolor="#000000" style="color: #FFFFFF"' : '' !!}>PEGAWAI</td>
                <td {!! ($people[0]['personstatus'] == 'TERSANGKA') ? 'bgcolor="#000000" style="color: #FFFFFF"' : '' !!}>TERSANGKA</td>
            </tr>
            <tr>
                <td {!! ($people[0]['personstatus'] == 'SKCK') ? 'bgcolor="#000000" style="color: #FFFFFF"' : '' !!}>SKCK</td>
                <td {!! ($people[0]['personstatus'] == 'LAIN-LAIN') ? 'bgcolor="#000000" style="color: #FFFFFF"' : '' !!}>LAIN-LAIN</td>
            </tr>
            <tr>
                <td {!! ($people[0]['personstatus'] == 'SIM') ? 'bgcolor="#000000" style="color: #FFFFFF"' : '' !!}>S.I.M.</td>
                <td></td>
            </tr>
        </table>
    </td>
    <td width="5"></td>
    <td>
        <table width="100%" cellpadding="0"  cellspacing="0">
            <tr>
                @for($i = 1; $i <= 12; $i++)
                <td height="20" border="1">
                    &nbsp;
                </td>
                @endfor
            </tr>
            <tr>
                @for($i = 1; $i <= 6; $i++)
                <td height="20" border="1">
                    &nbsp;
                </td>
                @endfor
            </tr>
            <tr>
                <td colspan="12" height="20"></td>
            </tr>
        </table>

        <table width="100%" cellpadding="3" cellspacing="0">
            <tr>
                <td width="18" border="1">
                    &nbsp;
                </td>
                <td width="10"></td>
                <td border="1" width="175">
                    {{ ! empty($people[0]['sexid']) ? title_case(drmSex($people[0]['sexid'])) : '-' }}
                </td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="3" height="10"></td>
</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
    <td width="336">

        <table width="100%" cellpadding="5" cellspacing="0">
            <tr>
                <td width="110">Nama</td>
                <td width="15">:</td>
                <td width="250">{{ ! empty($people[0]['fullname']) ? title_case($people[0]['fullname']) : '-' }}</td>
            </tr>
            <tr>
                <td width="110">Nama kecil/Alias</td>
                <td width="15">:</td>
                <td>{{ ! empty($people[0]['nickname']) ? title_case($people[0]['nickname']) : '-' }}</td>
            </tr>
            <tr>
                <td width="110">Pekerjaan</td>
                <td width="15">:</td>
                <td>{{ ! empty($people[0]['titleoccupation']) ? title_case($people[0]['titleoccupation']) : '-' }}</td>
            </tr>
        </table>
    </td>
    <td>
        <table width="100%" cellpadding="4" cellspacing="0">
            <tr>
                <td width="80">Rumus</td>
                <td width="15">:</td>
                <td>
                    {{ ! empty($people[0]['henry']) ? $people[0]['henry'] : '-' }}
                </td>
            </tr>
            {{--<tr>
                <td width="80">Lihat Rumus</td>
                <td width="15">:</td>
                <td>
                    @if ( isset($formula_view[0]) && isset($formula_view[1]) && isset($formula_view[2]) && isset($formula_view[3]) && isset($formula_view[4]) && isset($formula_view[5]) )
                    <table width="220">
                        <tr>
                            <td align="center">{{ $formula_view[0] }}</td>
                            <td align="center">{{ $formula_view[1] }}</td>
                            <td align="center">{{ $formula_view[2] }}</td>
                            <td align="center">{{ $formula_view[3] }}</td>
                            <td align="center">{{ $formula_view[4] }}</td>
                            <td align="center">{{ $formula_view[5] }}</td>
                        </tr>
                    </table>
                    @endif
                </td>
            </tr>--}}
        </table>
    </td>
</tr>
</table>

<table width="100%" cellpadding="5" cellspacing="0" border="0">
<tr>
    <td>
        <table width="100%" cellpadding="5" cellspacing="0" border="1" style="margin-bottom: 20px;">
            <tr>
                <td width="20%" align="center">1. Jempol kanan</td>
                <td width="20%" align="center">2. Telunjuk kanan</td>
                <td width="20%" align="center">3. Jari tengah kanan</td>
                <td width="20%" align="center">4. Jari manis kanan</td>
                <td width="20%" align="center">5. Kelingking kanan</td>
            </tr>
            <tr>
                <td width="20%" height="100">{!! ! empty($right_thumb) ? $right_thumb : '' !!}</td>
                <td width="20%" height="100">{!! ! empty($right_index) ? $right_index : '' !!}</td>
                <td width="20%" height="100">{!! ! empty($right_middle) ? $right_middle : '' !!}</td>
                <td width="20%" height="100">{!! ! empty($right_ring) ? $right_ring : '' !!}</td>
                <td width="20%" height="100">{!! ! empty($right_little) ? $right_little : '' !!}</td>
            </tr>
            <tr>
                <td width="20%" align="center">6. Jempol kiri</td>
                <td width="20%" align="center">7. Telunjuk kiri</td>
                <td width="20%" align="center">8. Jari tengah kiri</td>
                <td width="20%" align="center">9. Jari manis kiri</td>
                <td width="20%" align="center">10. Kelingking kiri</td>
            </tr>
            <tr>
                <td width="20%" height="100">{!! ! empty($left_thumb) ? $left_thumb : '' !!}</td>
                <td width="20%" height="100">{!! ! empty($left_index) ? $left_index : '' !!}</td>
                <td width="20%" height="100">{!! ! empty($left_middle) ? $left_middle : '' !!}</td>
                <td width="20%" height="100">{!! ! empty($left_ring) ? $left_ring : '' !!}</td>
                <td width="20%" height="100">{!! ! empty($left_little) ? $left_little : '' !!}</td>
            </tr>
            <tr>
                <td colspan="2">
                    <table width="100%" cellpadding="3" cellspacing="0" border="0">
                        <tr>
                            <td colspan="3" width="107" align="center"> {{ ! empty($people[0]['pot']) ? title_case($people[0]['pot']) : '-' }} </td>
                            <td width="100">Tgl.  {{ ! empty($people[0]['dot']) ? date('d-m-Y', strtotime($people[0]['dot'])) : '-' }} </td>
                        </tr>
                        <tr>
                            <td width="20">I.</td>
                            <td width="85">Diambil oleh</td>
                            <td width="10">:</td>
                            <td width="200">{{ ! empty($people[0]['takenby']) ? title_case($people[0]['takenby']) : '-' }}</td>
                        </tr>
                        <tr>
                            <td width="20">II.</td>
                            <td width="85">Disaksikan oleh</td>
                            <td width="10">:</td>
                            <td width="200">{{ ! empty($people[0]['witnessedby']) ? title_case($people[0]['witnessedby']) : '-' }}</td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table width="100%" cellpadding="3" cellspacing="0" border="0">
                        <tr>
                            <td colspan="2" align="center">Tanda Tangan</td>
                        </tr>
                        <tr>
                            <td width="20">I.</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td width="20">II.</td>
                            <td></td>
                        </tr>
                    </table>
                </td>
                <td colspan="2" valign="top">
                    <table width="100%" cellpadding="3" cellspacing="0" border="0">
                        <tr>
                            <td>Catatan :  <br> </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>

        <table width="100%" cellpadding="5" cellspacing="0" border="1" style="margin-bottom: 20px;">
            <tr>
                <td width="10%" align="center">Kelingking kiri</td>
                <td width="10%" align="center">Manis kiri</td>
                <td width="10%" align="center">Tengah kiri</td>
                <td width="10%" align="center">Telunjuk kiri</td>
                <td width="10%" align="center">Jempol kiri</td>
                <td width="10%" align="center">Jempol kanan</td>
                <td width="10%" align="center">Telunjuk kanan</td>
                <td width="10%" align="center">Tengah kanan</td>
                <td width="10%" align="center">Manis kanan</td>
                <td width="10%" align="center">Kelingking kanan</td>   
            </tr>
            <tr>
                <td height="80"> {!! ! empty($flat_left_little_finger) ? $flat_left_little_finger : '' !!} </td>
                <td height="80"> {!! ! empty($flat_left_ring_finger) ? $flat_left_ring_finger : '' !!} </td>
                <td height="80"> {!! ! empty($flat_left_middle_finger) ? $flat_left_middle_finger : '' !!} </td>
                <td height="80"> {!! ! empty($flat_left_index_finger) ? $flat_left_index_finger : '' !!} </td>
                <td height="80"> {!! ! empty($flat_left_thumb) ? $flat_left_thumb : '' !!} </td>
                <td height="80"> {!! ! empty($flat_right_thumb) ? $flat_right_thumb : '' !!} </td>
                <td height="80"> {!! ! empty($flat_right_index_finger) ? $flat_right_index_finger : '' !!} </td>
                <td height="80"> {!! ! empty($flat_right_middle_finger) ? $flat_right_middle_finger : '' !!} </td>
                <td height="80"> {!! ! empty($flat_right_ring_finger) ? $flat_right_ring_finger : '' !!} </td>
                <td height="80"> {!! ! empty($flat_right_little_finger) ? $flat_right_little_finger : '' !!} </td>
            </tr>
        </table>
    </td>
</tr>
</table>