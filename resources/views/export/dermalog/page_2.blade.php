<table width="100%" cellpadding="0" cellspacing="0">
<tr>
    <td width="370"></td>
    <td>
        <table width="100%" cellpadding="5" cellspacing="0">
            <tr>
                <td width="30"></td>
                <td>SINYALEMEN</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td width="370">
        <table width="100%" cellpadding="3" cellspacing="0">
            <tr>
                <td width="30"></td>
                <td width="23">1.</td>
                <td width="125">Tanggal lahir</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people[0]['dob']) ? date('d-m-Y', strtotime($people[0]['dob'])) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23"></td>
                <td width="125">Tempat lahir</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people[0]['pobregion']) ? title_case($people[0]['pobregion']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">2.</td>
                <td width="125">Kebangsaan/Suku</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people[0]['nationality']) ? title_case($people[0]['nationality']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">3.</td>
                <td width="125">Agama</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people[0]['religion']) ? title_case($people[0]['religion']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">4.</td>
                <td width="125">Alamat Terakhir</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['lastaddress']) ? title_case($people[0]['lastaddress']) : '-' }} {{ ! empty($people[0]['lastaddressregion']) ? title_case($people[0]['lastaddressregion']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">5.</td>
                <td width="125">KTP. No.</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people[0]['idcardno']) ? $people[0]['idcardno'] : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">6.</td>
                <td width="125">Pendidikan Terakhir</td>
                <td width="15">:</td>
                <td width="165">{{ ! empty($people[0]['education']) ? title_case($people[0]['education']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">7.</td>
                <td width="125">Nama Ayah/Alamat</td>
                <td width="15">:</td>
                <td width="165">-</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">8.</td>
                <td width="125">Nama Ibu/Alamat</td>
                <td width="15">:</td>
                <td width="165">-</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">9.</td>
                <td width="125">Nama Istri/Suami</td>
                <td width="15">:</td>
                <td width="165">-</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23"></td>
                <td width="125">Alamat</td>
                <td width="15">:</td>
                <td width="165">-</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">10.</td>
                <td width="125">Nama Anak-anak</td>
                <td width="15">:</td>
                <td></td>
            </tr>
            <tr>
                <td width="30"></td>
                <td></td>
                <td colspan="3">
                    <table width="100%" cellpadding="5" cellspacing="0">
                        <tr>
                            <td width="50%">{{ ! empty($child_1) ? $child_1 : '-' }}</td>
                            <td width="50%">{{ ! empty($child_2) ? $child_2 : '-' }}</td>
                        </tr>
                        <tr>
                            <td width="50%">{{ ! empty($child_3) ? $child_3 : '-' }}</td>
                            <td width="50%">{{ ! empty($child_4) ? $child_4 : '-' }}</td>
                        </tr>
                        <tr>
                            <td width="50%">{{ ! empty($child_5) ? $child_5 : '-' }}</td>
                            <td width="50%">{{ ! empty($child_6) ? $child_6 : '-' }}</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="23">11.</td>
                <td width="125">Tulisan/Tanda Tangan</td>
                <td width="15">:</td>
                <td></td>
            </tr>
        </table>
    </td>
    <td>
        <table width="100%" cellpadding="3" cellspacing="0">
            <tr>
                <td width="30"></td>
                <td width="115">Tinggi badan</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['heightcm']) ? $people[0]['heightcm'] : '-' }} CM</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Berat timbangan</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['weightkg']) ? $people[0]['weightkg'] : '-' }} KG</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Warna kulit</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['skincolor']) ? title_case($people[0]['skincolor']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Bentuk tubuh</td>
                <td width="15">:</td>
                <td>{{ ! empty($people[0]['bodyshape']) ? title_case($people[0]['bodyshape']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Bentuk kepala</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['shapeofhead']) ? title_case($people[0]['shapeofhead']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Warna rambut</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['haircolor']) ? title_case($people[0]['haircolor']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Jenis rambut</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['typeofhair']) ? title_case($people[0]['typeofhair']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Bentuk muka</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['typeofface']) ? title_case($people[0]['typeofface']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Dahi</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['forehead']) ? title_case($people[0]['forehead']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Warna mata</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['eyescolor']) ? title_case($people[0]['eyescolor']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Kelainan pada mata</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['irregularitesoneyes']) ? title_case($people[0]['irregularitesoneyes']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Hidung</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['nose']) ? title_case($people[0]['nose']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Bibir</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['lips']) ? title_case($people[0]['lips']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Gigi</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['tooth']) ? title_case($people[0]['tooth']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Dagu</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['chin']) ? title_case($people[0]['chin']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Telinga</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['ear']) ? title_case($people[0]['ear']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Tatoo</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['tattoo']) ? title_case($people[0]['tattoo']) : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Dipotong &amp; Cacat</td>
                <td width="15">:</td>
                <td width="200">{{ ! empty($people[0]['scarsandhandicap']) ? $people[0]['scarsandhandicap'] : '-' }}</td>
            </tr>
            <tr>
                <td width="30"></td>
                <td width="115">Kode Pasal</td>
                <td width="15">:</td>
                <td width="200">-</td>
            </tr>
        </table>
    </td>
</tr>
<tr>
    <td colspan="3" height="10"></td>
</tr>
</table>

<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
    <td width="100%">
        <table width="100%" cellpadding="5" cellspacing="0" border="1">
            <tr>
                <td align="center" rowspan="2" height="220"> {!! ! empty($face_right) ? $face_right : '' !!} </td>
                <td align="center" rowspan="2" height="220"> {!! ! empty($face_front) ? $face_front : '' !!} </td>
                <td align="center" rowspan="2" height="220"> {!! ! empty($face_left) ? $face_left : '' !!} </td>
                <td align="center" height="22">Keterangan</td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
    </td>
</tr>
</table>