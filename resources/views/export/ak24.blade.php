<td width="361">
    <table border="0" cellspacing="0" cellpadding="2">
        <tr>
            <td colspan="5" align="right">Bentuk AK-24</td>
        </tr>
        <tr>
            <td colspan="3" align="center">{{ $status }}</td>
            <td colspan="2" align="right">{{ $sex }}</td>
        </tr>
        <tr>
            <td colspan="5" align="center">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="3" width="230"></td>
            <td width="53">Bangsa</td>
            <td width="60">: {{ $people->nationality }}</td>
        </tr>
        <tr>
            <td colspan="3">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nama : {{ ucwords($people->fullname) }}</td>
            <td>Suku</td>
            <td>: {{ $people->race }}</td>
        </tr>
        <tr>
            <td colspan="3"></td>
            <td>Tahun Lahir</td>
            <td>: {{ !empty($birth_date) ? date('Y', $birth_date) : '-' }}</td>
        </tr>

        <tr>
            <td></td>
            <td colspan="4" height="30" valign="middle">
                <table cellspacing="0" cellpadding="0">
                  <tr>
                    <td valign="middle" width="40">Rumus : </td>
                    <td width="150">
                        <table cellspacing="0" cellpadding="0">
                            @if ( isset($formula[0]) && isset($formula[1]) && isset($formula[2]) && isset($formula[3]) && isset($formula[4]) && isset($formula[5]) )
                            <tr>
                                <td>{{ $formula[0] }}</td>
                                <td>{{ $formula[1] }}</td>
                                <td>{{ $formula[2] }}</td>
                                <td>{{ $formula[3] }}</td>
                                <td>{{ $formula[4] }}</td>
                                <td>{{ $formula[5] }}</td>
                            </tr>
                            @endif
                            <tr>
                                <td colspan="6">------------------------------------------------</td>
                            </tr>
                            @if ( isset($formula_view[0]) && isset($formula_view[1]) && isset($formula_view[2]) && isset($formula_view[3]) && isset($formula_view[4]) && isset($formula_view[5]) )
                            <tr>
                                <td height="2">{{ $formula_view[0] }}</td>
                                <td height="2">{{ $formula_view[1] }}</td>
                                <td height="2">{{ $formula_view[2] }}</td>
                                <td height="2">{{ $formula_view[3] }}</td>
                                <td height="2">{{ $formula_view[4] }}</td>
                                <td height="2">{{ $formula_view[5] }}</td>
                            </tr>
                            @endif
                        </table>
                    </td>
                  </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">Catatan2 yang diperlukan : </td>
            <td></td>
            <td colspan="2" align="right" width="125">Nama / Tanda Tangan Petugas : </td>
        </tr>
        <tr>
            <td colspan="5" height="23"></td>
        </tr>
        <tr>
            <td colspan="2">................................................</td>
            <td></td>
            <td colspan="2" align="right">.....................................................</td>
        </tr>
        <tr><td colspan="5" height="10"></td></tr>
    </table>
</td>