@extends('layouts.login')
@section('content')

<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    {!! Form::open(['method' => 'post', 'class' => 'newpassword-form']) !!}
        <div class="form-title">
            <span class="form-title">Masukkan password baru</span>
        </div>
        
        <div style="position:relative" class="form-group {{ ! empty($errors->newpassword->first('password')) ? 'has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            {!! Form::password('password', ['class' => 'form-control form-control-solid placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => 'Password', 'id' => 'password', 'data-indicator' => 'pwindicator']) !!}
            <div class="pwindicator-wrapper">
                <div id="pwindicator">
                    <div class="bar"></div>
                    <div class="label"></div>
                    <div class="text">
                        Gunakan password yang baik agar tidak mudah ditebak oleh orang lain
                    </div>
                </div>
            </div>
            {!! ! empty($errors->newpassword->first('password')) ? '<span id="password" class="help-block">'.$errors->newpassword->first('password').'</span>' : '' !!}
        </div>
        <div class="form-group {{ ! empty($errors->newpassword->first('rpassword')) ? 'has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Ulangi Password</label>
            {!! Form::password('rpassword', ['class' => 'form-control form-control-solid placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => 'Ulangi Password']) !!}
            {!! ! empty($errors->newpassword->first('rpassword')) ? '<span id="password" class="help-block">'.$errors->newpassword->first('rpassword').'</span>' : '' !!}
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue btn-block uppercase">Ganti</button>
        </div>
    {!! Form::close() !!}
    <!-- END LOGIN FORM -->
</div>
<!-- END LOGIN --> 

@endsection

@push('js')
<script>
var Login = function() {

    var handleNewPassword = function() {

        jQuery('.newpassword-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                // simple rule, converted to {required:true}
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 255
                },
                rpassword: {
                    required: true,
                    equalTo: "#password",
                    maxlength: 255
                }
            },
            messages: {
                password: {
                    required: "Tidak boleh kosong"
                },
                rpassword: {
                    required: "Tidak boleh kosong",
                    equalTo: "Tidak sama dengan password"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            submitHandler: function(form) {
                App.blockUI();
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.newpassword-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.newpassword-form').validate().form()) {
                     App.blockUI();
                    $('.newpassword-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handleNewPassword();

        }

    };

}();

head.ready(function() {
    jQuery(document).ready(function($) 
    {
        Login.init();

        var $pwIndicator = $('#pwindicator');

        $('#password').focusin(function() {
            $(this).closest('.form-group').addClass('show-pwstrength');

            if ( $pwIndicator.hasClass('pw') === false )
            {
                $pwIndicator.addClass('pw pw-very-weak');
                $pwIndicator.children('.label').html('Sangat lemah');
            }
        });

        $('#password').focusout(function() {
            $(this).closest('.form-group').removeClass('show-pwstrength');
        });

        $('#password').pwstrength({
            classes: ['pw pw-very-weak', 'pw pw-weak', 'pw pw-mediocre', 'pw pw-strong', 'pw pw-very-strong'],
            texts: ['Sangat lemah', 'Lemah', 'Menengah', 'Kuat', 'Sangat kuat']
        }); 
    });
});
</script>
@endpush