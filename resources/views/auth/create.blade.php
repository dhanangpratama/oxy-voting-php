@extends('layouts.login')
@section('content')

<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    {!! Form::open(['method' => 'post', 'class' => 'defoperator-form']) !!}
        <div class="form-title">
            <span class="form-title">Pendaftaran Operator Baru</span>
        </div>
        <div class="form-group {{ ! empty($errors->defoperator->first('name')) ? 'has-error' : '' }}">
            {!! Form::text('name', null, ['class' => 'form-control form-control-solid placeholder-no-fix', 'id' => 'name', 'placeholder' => 'Nama',]) !!}
            {!! ! empty($errors->defoperator->first('name')) ? '<span id="name" class="help-block">'.$errors->defoperator->first('name').'</span>' : '' !!}
        </div>
        <div class="form-group {{ ! empty($errors->defoperator->first('email')) ? 'has-error' : '' }}">
            {!! Form::text('email', null, ['class' => 'form-control form-control-solid placeholder-no-fix', 'id' => 'email', 'placeholder' => 'Email',]) !!}
            {!! ! empty($errors->defoperator->first('email')) ? '<span id="email" class="help-block">'.$errors->defoperator->first('email').'</span>' : '' !!}
        </div>
        <div class="form-group {{ ! empty($errors->defoperator->first('phone_mobile')) ? 'has-error' : '' }}">
            {!! Form::text('phone_mobile', null, ['class' => 'form-control form-control-solid placeholder-no-fix', 'id' => 'phone_number', 'placeholder' => 'No. Handphone',]) !!}
            {!! ! empty($errors->defoperator->first('phone_mobile')) ? '<span id="email" class="help-block">'.$errors->defoperator->first('phone_mobile').'</span>' : '' !!}
        </div>
        <div style="position:relative" class="form-group {{ ! empty($errors->defoperator->first('password')) ? 'has-error' : '' }}">
            {!! Form::password('password', ['class' => 'form-control form-control-solid placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => 'Password', 'id' => 'password', 'data-indicator' => 'pwindicator']) !!}
            <div class="pwindicator-wrapper">
                <div id="pwindicator">
                    <div class="bar"></div>
                    <div class="label"></div>
                    <div class="text">
                        Gunakan password yang baik agar tidak mudah ditebak oleh orang lain
                    </div>
                </div>
            </div>
            {!! ! empty($errors->defoperator->first('password')) ? '<span id="password" class="help-block">'.$errors->defoperator->first('password').'</span>' : '' !!}
        </div>
        <div class="form-group {{ ! empty($errors->defoperator->first('rpassword')) ? 'has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Ulangi Password</label>
            {!! Form::password('rpassword', ['class' => 'form-control form-control-solid placeholder-no-fix', 'autocomplete' => 'off', 'placeholder' => 'Ulangi Password']) !!}
            {!! ! empty($errors->defoperator->first('rpassword')) ? '<span id="password" class="help-block">'.$errors->defoperator->first('rpassword').'</span>' : '' !!}
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-sm-6">
                    <a href="{{ route('default_operator_logout') }}" class="btn red uiblock-on btn-block uppercase">Keluar</a>
                </div>
                <div class="col-sm-6">
                    <button type="submit" class="btn blue btn-block uppercase">Daftar</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    <!-- END LOGIN FORM -->
</div>
<!-- END LOGIN --> 
@endsection

@push('js')
<script>
var Login = function() {

    var handleRegister = function() {

        jQuery('.defoperator-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                // simple rule, converted to {required:true}
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 255
                },
                name: {
                    required: true,
                    maxlength: 255
                },
                phone_mobile: {
                    required: true,
                    maxlength: 255,
                    digits: true
                },
                location_id: {
                    required: true,
                    maxlength: 255
                },
                role: {
                    required: true,
                    maxlength: 255
                },
                rpassword: {
                    required: true,
                    equalTo: "#password",
                    maxlength: 255
                },
                // compound rule
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                }
            },
            messages: {
                password: {
                    required: "Tidak boleh kosong"
                },
                rpassword: {
                    required: "Tidak boleh kosong",
                    equalTo: "Tidak sama dengan password"
                },
                name: {
                    required: "Tidak boleh kosong"
                },
                phone_mobile: {
                    required: "Tidak boleh kosong",
                    digits: "Hanya boleh angka"
                },
                location_id: {
                    required: "Tidak boleh kosong"
                },
                role: {
                    required: "Tidak boleh kosong"
                },
                email: {
                    required: "Tidak boleh kosong",
                    email: "Format email salah"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            errorPlacement: function(error, element) 
            {
                error.appendTo( element.parents('.form-group') );
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            submitHandler: function(form) {
                App.blockUI();
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.defoperator-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.defoperator-form').validate().form()) {
                    App.blockUI();
                    $('.defoperator-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }

    return {
        //main function to initiate the module
        init: function() {

            handleRegister();

        }

    };

}();

head.ready(function() {
    jQuery(document).ready(function($) 
    {
        Login.init();

        $('select').select2();

        var $pwIndicator = $('#pwindicator');

        $('#password').focusin(function() {
            $(this).closest('.form-group').addClass('show-pwstrength');

            if ( $pwIndicator.hasClass('pw') === false )
            {
                $pwIndicator.addClass('pw pw-very-weak');
                $pwIndicator.children('.label').html('Sangat lemah');
            }
        });

        $('#password').focusout(function() {
            $(this).closest('.form-group').removeClass('show-pwstrength');
        });

        $('#password').pwstrength({
            classes: ['pw pw-very-weak', 'pw pw-weak', 'pw pw-mediocre', 'pw pw-strong', 'pw pw-very-strong'],
            texts: ['Sangat lemah', 'Lemah', 'Menengah', 'Kuat', 'Sangat kuat']
        }); 
    });
});
</script>
@endpush