@extends('layouts.login')
@section('content')

<!-- BEGIN LOGIN -->
<div class="content">
    <!-- BEGIN LOGIN FORM -->
    {!! Form::open(['method' => 'post', 'class' => 'login-form', 'url' => url()->full() . '/']) !!}
        <div class="form-title">
            <span class="form-title">Selamat Datang.</span>
            <span class="form-subtitle">Silahkan log in.</span>
        </div>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Masukkan E-Mail dan Password </span>
        </div>
        <div class="form-group {{ ! empty($errors->login->first('email')) ? 'has-error' : '' }}">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Email</label>
            {!! Form::text('email', null, ['class' => 'form-control form-control-solid placeholder-no-fix', 'type' => 'text', 'autocomplete' => 'off', 'placeholder' => 'Email', 'id' => 'loginEmail']) !!}
            {!! ! empty($errors->login->first('email')) ? '<span id="email" class="help-block">'.$errors->login->first('email').'</span>' : '' !!}
        </div>
        <div class="form-group {{ ! empty($errors->login->first('password')) ? 'has-error' : '' }}">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            {!! Form::password('password', ['class' => 'form-control form-control-solid placeholder-no-fix', 'type' => 'password', 'autocomplete' => 'off', 'placeholder' => 'Password']) !!}
            {!! ! empty($errors->login->first('password')) ? '<span id="password" class="help-block">'.$errors->login->first('password').'</span>' : '' !!}
        </div>
        <div class="form-actions">
            <button type="submit" class="btn blue btn-block uppercase">Log in</button>
            {{--  @if ( config('ak23.access') == 'desktop' )
            <button type="button" class="btn red btn-block uppercase" onclick="Biover.ShutdownWindows()">Shut down</button>
            @endif  --}}
        </div>
        <div class="form-actions">
            {{-- <div class="pull-left">
                <label class="rememberme mt-checkbox mt-checkbox-outline">
                    <input type="checkbox" name="remember" value="1" /> Remember me
                    <span></span>
                </label>
            </div> --}}
            <div class="text-center forget-password-block">
                {{--  @if ( config('ak23.access') == 'desktop' )
                <a href="javascript:;" class="forget-password" onclick="Biover.ApplicationRestart()">Restart</a>
                <span class="white">&middot;</span>
                @endif  --}}
                <a href="javascript:;" id="forget-password" class="forget-password">Lupa Password?</a>
            </div>
        </div>
    {!! Form::close() !!}
    <!-- END LOGIN FORM -->
    <!-- BEGIN FORGOT PASSWORD FORM -->
    {!! Form::open(['method' => 'post', 'class' => 'forget-form', 'url' => route('user_forget_submit')]) !!}
        <div class="form-title">
            <span class="form-title">Lupa Password ?</span>
            <span class="form-subtitle">Masukkan informasi anda</span>
        </div>
        <div class="form-group {{ ! empty($errors->forget->first('name')) ? 'has-error' : '' }}">
            {!! Form::text('name', null, ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Nama', 'autocomplete' => 'off', 'id' => 'forgetName']) !!}
            {!! ! empty($errors->forget->first('name')) ? '<span id="name" class="help-block">'.$errors->forget->first('name').'</span>' : '' !!}
        </div>
        <div class="form-group {{ ! empty($errors->forget->first('email')) ? 'has-error' : '' }}">
            {!! Form::text('email', null, ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'Email', 'autocomplete' => 'off', 'id' => 'forgetEmail']) !!}
            {!! ! empty($errors->forget->first('email')) ? '<span id="email" class="help-block">'.$errors->forget->first('email').'</span>' : '' !!}
        </div>
        <div class="form-group {{ ! empty($errors->forget->first('phone_mobile')) ? 'has-error' : '' }}">
            {!! Form::text('phone_mobile', null, ['class' => 'form-control placeholder-no-fix', 'placeholder' => 'No. Handphone', 'autocomplete' => 'off', 'id' => 'forgetPhone']) !!}
            {!! ! empty($errors->forget->first('phone_mobile')) ? '<span id="phone_mobile" class="help-block">'.$errors->forget->first('phone_mobile').'</span>' : '' !!}
        </div>
        <div class="form-actions">
            <div class="row">
                <div class="col-sm-6">
                    <button type="button" id="back-btn" class="btn red btn-block uppercase">Kembali</button>
                </div>
                <div class="col-sm-6">
                    <button type="submit" class="btn blue pull-right btn-block uppercase">Ganti</button>
                </div>
            </div>
        </div>
    {!! Form::close() !!}
<!-- END FORGOT PASSWORD FORM -->

</div>

<!-- END LOGIN --> 

@endsection

@push('js')
<script>
var Login = function() {

    var handleLogin = function() {
        jQuery('.login-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                // simple rule, converted to {required:true}
                password: "required",
                // compound rule
                email: {
                    required: true,
                    email: false
                }
            },
            messages: {
                password: {
                    required: "Tidak boleh kosong"
                },
                email: {
                    required: "Tidak boleh kosong",
                    email: "Format email salah"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            submitHandler: function(form) 
            {
                App.blockUI();
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.login-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.login-form').validate().form()) {
                    App.blockUI();
                    $('.login-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });
    }

    var handleForgetPassword = function() {

        jQuery('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true,
                    maxlength: 255
                },
                phone_mobile: {
                    required: true,
                    digits: true,
                    maxlength: 255
                },
                location_id: {
                    required: true
                }
            },
            messages: {
                name: {
                    required: "Tidak boleh kosong"
                },
                email: {
                    required: "Tidak boleh kosong",
                    email: "Format email salah"
                },
                phone_mobile: {
                    required: "Tidak boleh kosong",
                    digits: "Hanya boleh angka"
                },
                location_id: {
                    required: "Harus dipilih"
                }
            },
            highlight: function(element) { // hightlight error inputs
                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            success: function(label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },

            submitHandler: function(form) {
                App.blockUI();
                form.submit(); // form validation success, call ajax form submit
            }
        });

        $('.forget-form input').keypress(function(e) {
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    App.blockUI();
                    $('.forget-form').submit(); //form validation success, call ajax form submit
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function() {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function() {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

    }

    return {
        //main function to initiate the module
        init: function() {

            handleForgetPassword();
            handleLogin();

        }

    };

}();

head.ready(function() {
    jQuery(document).ready(function($) 
    {
        Login.init();

        $('select').select2();

        @if( empty(Request::get('page')) )
        $('#loginEmail').focus();
        @endif

        @if( Request::get('page') == 'register' )
        $('.login-form').hide();
        $('.register-form').show();
        @endif

        @if( Request::get('page') == 'forget' )
        $('.login-form').hide();
        $('.forget-form').show();
        $('#forgetEmail').focus();
        @endif

        $('#clickmewow').click(function()
        {
            $('#radio1003').attr('checked', 'checked');
        });
    });
});
</script>
@endpush

@push('css')
<style>
.select2-container--bootstrap .select2-selection--single {
    font-size: 14px !important;
}
</style>
@endpush