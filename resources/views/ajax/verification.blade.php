@if ( !empty($data_results) )
<div class="row">
	<div class="col-sm-6">
		<div class="row">
			<div class="col-sm-3">
				@if ( file_exists($source . "/{$file['folder']}/face-front.jpg") )
	        	<img src="{{ asset("files/{$source_folder}/{$file['folder']}/face-front.jpg") }}?{{ time() }}" style="margin: 0 auto 30px;" class="img-responsive img-thumbnail" alt="Wajah muka">
	        	@else
	        	<img src="{{ asset('assets/images/no-photo.png') }}" class="img-responsive img-thumbnail" alt="no photo" />
	        	@endif
			</div>
			<div class="col-sm-9">
				<div class="current-info">
					<div>
						<span>Nama</span> {{ ! empty($front_data['fullname']) ? $front_data['fullname'] : '-' }}
					</div>
					<div>
						<span>TL</span> {{ ! empty($back_data['birth_date']) ? $back_data['birth_date'] : '-' }}
					</div>
					<div>
						<span>NIK</span> {{ ! empty($back_data['id_card']) ? $back_data['id_card'] : '-' }}
					</div>
				</div>
			</div>
		</div>
		
		<ul id="verification_results">
			@foreach ($data_results as $result)
			<li data-id="{{ $result['id'] }}">
				<table>
					<tr>
						<td width="80"><strong>Nama:</strong></td>
						<td>{{ $result['fullname'] }}</td>
					</tr>
					<tr>
						<td width="80"><strong>TL:</strong></td>
						<td>{{ $result['birthdate'] }}</td>
					</tr>
					<tr>
						<td width="80"><strong>NIK:</strong></td>
						<td>{{ ! empty($result['nik']) ? $result['nik'] : '-' }}</td>
					</tr>
					<tr>
						<td width="80"><strong>Skor:</strong></td>
						<td>{{ ! empty($result['score']) ? $result['score'] : '-' }}</td>
					</tr>
				</table>
			</li>
			@endforeach
		</ul>
		
	</div>
	<div class="col-sm-6">
		<div id="resultDetail"></div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
	$('#verification_results').on('click', 'li', function() {
		var loading = '<?php echo asset('assets/images/loading.gif'); ?>';

		$('#verification_results li').removeClass('selected');
        $(this).addClass('selected');
        $('#resultDetail').html('<div style="text-align: center; padding: 30px 0;"><img style="margin: 0 auto 20px; display: block;" src="'+loading+'" alt="loading"> Sedang menampilkan data...</div>');
        var dataID = $(this).data('id');
        // $('#resultDetail').html(dataID);
        $('#resultDetail').load("{{ url('ajax/verification') }}" + "/" + dataID);
    });
});
</script>

@else

<p>Data tidak ditemukan</p>

@endif