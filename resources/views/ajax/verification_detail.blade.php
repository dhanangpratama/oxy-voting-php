{!! Form::open(['method'=>'POST', 'url' => url("demographic/update")]) !!}
	<div class="m-b-20"><strong>{{ ucwords(strtolower($people->fullname)) }}</strong></div>
	<div class="row">
		<div class="col-sm-6">
			@if ( file_exists(config('path.data.complete') . "/{$people->id}/face-front.{$ext}") )
			<img src="{{ asset("files/".config('filename.complete_folder')."/{$people->id}/face-front.{$ext}") }}?{{ time() }}" class="img-responsive img-thumbnail">
			@else
			<img src="{{ asset("assets/images/no-photo.png") }}" class="img-responsive img-thumbnail" alt="no photo">
			@endif
		</div>
		<div class="col-sm-6">
			@if ( ! empty($compare) )
			<a href="#fingerCompare" class="open-popup-link"><img src="{{ $compare['founded_url'] }}?{{ time() }}" class="img-responsive img-thumbnail"></a>
			@else
			<p class="text-center">Jenis jari permintaan dan kandidat tidak ada yang sama.</p>
			@endif
		</div>
	</div>
	
	{{--<div class="item-label"><span>Nama</span> {{ $people->id }}</div>
	<div class="item-label"><span>TL</span> {{ date('d-m-Y', strtotime($people->birth_date)) }}</div>
	<div class="item-label"><span>KTP</span> {{ !empty($people->id_card) ? $people->id_card : '-' }}</div>--}}

	<p>
		<input type="hidden" name="SubjectId" value="{{ $people->SubjectId }}">
		@if ( ! empty($compare) )
		<a href="#fingerCompare" class="open-popup-link btn btn-block red">Bandingkan Sidik Jari</a>
		@endif
		<a href="{{ url("export/compare/{$compare_id}") }}" target="_blank" class="btn btn-block blue">Cetak Perbandingan</a>
		<button type="submit" class="btn btn-block green overlay">Pilih</button>
	</p>

{!! Form::close() !!}

<div id="fingerCompare" class="white-popup mfp-hide">
	@if ( ! empty($compare) )
	<div class="alert alert-info m-t-20 text-center">Perbandingan berdasarkan sidik jari {{ $compare['title'] }}</div>
	  <div class="row m-b-20">
	  	<div class="col-sm-6">
	  		<p><strong>{{ ucwords(strtolower($front_data['fullname'])) }}</strong> (Baru)</p>
	  		@if ( File::exists(config('path.data.tmp') . DIRECTORY_SEPARATOR . 'single-compare' . DIRECTORY_SEPARATOR . 'requested.jpg') )
	  		<img src="{{ asset(config('url.tmp') . "/single-compare/requested.jpg") }}?{{ time() }}" class="img-fullwidth img-thumbnail">
	  		@else
			<img src="{{ $compare['requested_url'] }}" class="img-fullwidth img-thumbnail">
			@endif
	  	</div>
	  	<div class="col-sm-6">
	  		<p><strong>{{ ucwords(strtolower($people->fullname)) }}</strong> (Lama)</p>
	  		@if ( File::exists(config('path.data.tmp') . DIRECTORY_SEPARATOR . 'single-compare' . DIRECTORY_SEPARATOR . 'founded.jpg') )
	  		<img src="{{ asset(config('url.tmp') . "/single-compare/founded.jpg") }}?{{ time() }}" class="img-fullwidth img-thumbnail">
	  		@else
			<img src="{{ $compare['founded_url'] }}" class="img-fullwidth img-thumbnail">
			@endif
	  	</div>
	  </div>
	@else
	<p>Data jari tidak ditemukan.</p>
	@endif
</div>

<script>
$(document).ready(function() {
	$('.open-popup-link').magnificPopup({
	  type:'inline',
	  midClick: true // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
	});

	$('form').submit(function() {
		App.blockUI();
	});
});
</script>