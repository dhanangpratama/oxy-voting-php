@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'GET', 'class' => 'overlay']) !!}
<div class="portlet box dark">
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group form-md-line-input form-md-floating-label m-b-0">
                    {!! Form::text('nik', array_get($data, 'nik'), ['class' => 'form-control', 'id' => 'nik','tabindex' => 1]) !!}
                    <label for="nik">NIK</label>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group form-md-line-input form-md-floating-label m-b-0">
                    {!! Form::text('name', array_get($data, 'name'), ['class' => 'form-control', 'id' => 'name','tabindex' => 2]) !!}
                    <label for="name">Nama</label>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group form-md-line-input form-md-floating-label m-b-0">
                    {!! Form::text('pob', array_get($data, 'pob'), ['class' => 'form-control', 'id' => 'pob','tabindex' => 3]) !!}
                    <label for="pob">Tempat Lahir</label>
                </div>
            </div>

            <div class="col-md-2">
                <div class="form-group form-md-line-input form-md-floating-label m-b-0">
                    {!! Form::text('dob', array_get($data, 'dob'), ['class' => 'form-control mask_date2', 'id' => 'dob','tabindex' => 3]) !!}
                    <label for="dob">Tanggal Lahir</label>
                </div>
            </div>

            <div class="col-md-2">
                <button type="submit" class="btn blue btn-lg" style="margin-top: 25px">Cari</button>
                @if ( !empty(array_filter($data)) )
                <a href="{{ route('members') }}" class="btn red btn-lg" style="margin-top: 25px">Semua</a>
                @endif
            </div>
        </div>
    </div>
</div>
 
{!! Form::close() !!}

<div class="portlet box dark">
    <div class="portlet-body">
        <div class="table">
            <table class="table">
                <thead class="uppercase">
                    <tr>
                        <th>Nama Lengkap</th>
                        <th width="250">NIK</th>
                        <th width="220">Tempat Lahir</th>
                        <th width="200">Tanggal Lahir</th>
                        <th width="100"></th>
                    </tr>
                </thead>
                <tbody>
                @if ($members->count() > 0)
                @foreach($members as $member)
                    <tr>
                        <td class="uppercase">{{ check_empty($member->name) }}</td>
                        <td class="uppercase">{{ check_empty($member->nik) }}</td>
                        <td class="uppercase">{{ check_empty($member->pob) }}</td>
                        <td class="uppercase">{{ format_date($member->dob) }}</td>
                        <td>
                            <a href="#" onclick="go('{{ url('member/view/' .  encrypt($member->demographic_id)) }}?source=local')" class="btn green btn-sm uiblock-on" title="Lihat member">Lihat</a>
                        </td>
                    </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="5" align="center">Data tidak ditemukan.</td>
                </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="pagination"> {{ $members->appends(array_filter($data))->links() }} </div>

@endsection