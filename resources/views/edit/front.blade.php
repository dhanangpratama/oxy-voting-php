@extends('layouts.main')
@section('content')

@include('edit.nav')

{!! Form::open(['method'=>'POST', 'url' => $request->fullUrl()])  !!}
    <div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">Tampilan Depan</div>
        </div>
        <div class="portlet-body form">
            <div class="form-body">
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="row data">
                            @if (count($errors) > 0)
                            <div class="col-sm-12">
                                <div class="alert alert-danger">
                                    <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                    </ul>
                                </div>
                            </div>
                            @endif
                            <div class="col-sm-12">
                                <div class="form-group form-md-line-input ">
                                    <select name="status_id" class="form-control pophover infoable" id="status" tabindex="0">
                                        <option>-- Pilih salah satu --</option>
                                        @foreach ($status as $key => $value)
                                        <option value="{{ $key }}" {{ $key == $front_data['status_id'] ? 'selected="selected"' : '' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>

                                    <label for="status">Status</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input form-md-floating-label ">
                                    {{ Form::text('fullname', old('fullname', $front_data['fullname']), ['id' => 'fullname', 'tabindex' => 1,'class' => 'form-control infoable']) }} 

                                    <label for="fullname">Nama</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input form-md-floating-label ">
                                    <input type="text" class="form-control infoable" id="nickname" name="nickname" value="{{ old('nickname', $front_data['nickname']) }}" tabindex="2">

                                    <label for="nickname">Nama Kecil / Alias</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input form-md-floating-label ">
                                    <input type="text" class="form-control infoable" id="job" name="job" value="{{ old('job', $front_data['job']) }}" tabindex="3">

                                    <label for="job">Pekerjaan</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-radios">
                                    <label>Jenis Kelamin</label>
                                    <div class="md-radio-inline pophover">
                                        <div class="md-radio">
                                            <input type="radio" id="male" name="sex" value="m" {{ $front_data['sex'] == 'm' ? 'checked' : '' }} class="md-radiobtn" tabindex="4">
                                            <label for="male">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Laki-laki 
                                            </label>
                                        </div>
                                        <div class="md-radio">
                                            <input type="radio" id="female" name="sex" value="f" {{ $front_data['sex'] == 'f' ? 'checked' : '' }} class="md-radiobtn" tabindex="5">
                                            <label for="female">
                                                <span class="inc"></span>
                                                <span class="check"></span>
                                                <span class="box"></span> Perempuan 
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input form-md-floating-label ">
                                    <input type="text" class="form-control infoable" id="location" name="location" value="{{ old('location', $front_data['location']) }}" tabindex="8">

                                    <label for="location">Lokasi</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input ">
                                    <div class="input-group date">
                                        <input type="text" class="form-control infoable" data-id="datein" id="mask_date2" name="taken_date" value="{{ old('taken_date', ! empty($front_data['taken_date']) ? date('d-m-Y', strtotime($front_data['taken_date'])) : '-') }}" tabindex="9"> 
                                        <label for="mask_date2">Tanggal Pengambilan</label>
                                        <span class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input form-md-floating-label ">
                                    <input type="text" class="form-control infoable" id="takenby" name="taken_by" value="{{ old('taken_by', $front_data['taken_by']) }}" tabindex="10">

                                    <label for="takenby">Diambil Oleh</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input form-md-floating-label ">
                                    <input type="text" class="form-control infoable" id="saksi" name="view_by" value="{{ old('view_by', $front_data['view_by']) }}" tabindex="11">

                                    <label for="saksi">Disaksikan Oleh</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-md-line-input form-md-floating-label ">
                                    <textarea class="form-control infoable" rows="3" id="note" name="note" tabindex="12">{{ old('note', $front_data['note']) }}</textarea>
                                    
                                    <label for="note">Catatan</label>
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>  
            </div>
        </div>
    </div>

    <div class="alignright m-b-30">
        <div class="btn-group">
            <a class="btn red btn-lg" tabindex="14" href="{{ url('people') }}"><i class="fa fa-angle-left m-r-15"></i> Batal</a>
            <button class="btn blue btn-lg overlay" type="submit" tabindex="13">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></button>
        </div>
    </div>
{!! Form::close() !!}

@include('edit.verification_modal')

@endsection