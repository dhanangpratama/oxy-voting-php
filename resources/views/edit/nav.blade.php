<div class="clearfix">
	<a data-toggle="modal" href="#verificationModal" class="btn blue m-b-20 pull-right" style="margin-left: 10px;">Verifikasi</a>
	@if ($people->sync_status == 'SYNCED')
	<span class="btn green cur-default m-b-20 pull-right">Terkirim</span>
	@else
	<span class="btn red cur-default m-b-20 pull-right">Belum Terkirim</span>
	@endif
</div>