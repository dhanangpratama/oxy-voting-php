@extends('layouts.main')
@section('content')

<div class="ak-tab m-b-30">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs m-b-30" role="tablist">
        <li role="presentation" class="active"><a href="#roll" aria-controls="roll" role="tab" data-toggle="tab">Rolled</a></li>
        <li role="presentation"><a href="#flat" aria-controls="flat" role="tab" data-toggle="tab">Flat</a></li>
    </ul>

    <!-- Tab panes -->
    
    {!! Form::open(['url' => 'demographic/edit/save', 'method'=>'POST', 'id' => 'saveEdit'])  !!}

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane fade in active" id="roll">
            <div class="row">
                <?php $no = 1; ?>
                @foreach (config('ak23.finger_data') as $finger => $detail)
                <div class="col-xs-15 {{ finger_done($finger) }}">
                    <div class="panel panel-default">
                      <div class="panel-body rolled-equal-height">
                          <div class="henry-label">
                            {{ (!empty(session($finger))) ? str_replace('_', ' ', session($finger)) : '-' }}
                          </div>
                        <div class="finger-wrapper">
                          <div class="text-center m-b-10">{{ $detail['num'] }}</div>
                          
                          @if ( file_exists("{$complete_path}/{$people->demographic_id}/{$detail['roll_name']}") )
                          <img class="img-responsive zoom" title="Scroll untuk memperbesar" src="{{ asset("files/{$source_folder}/".$people->demographic_id.'/'.$detail['roll_name']) }}?{{ time() }}" alt="{{ $finger }}">
                          @else
                          <img class="img-responsive" src="{{ asset('assets/images/blank-finger.png') }}" alt="{{ $finger }}">
                          @endif
                        </div>
                      </div>
                      <div class="panel-footer aligncenter relative">
                        <a class="uiblock-on" href="{{ url("demographic/edit/{$crypted_id}/finger/{$finger}") }}"><strong>{{ $no }}. {{ $detail['title'] }}</strong></a>
                        <div class="mark-done"><i class="fa fa-check-circle"></i></div>
                      </div>
                    </div>
                </div>
                <?php $no++; ?>
                @endforeach
            </div>
        </div>

        <div role="tabpanel" class="tab-pane fade in" id="flat">
          <div class="row">
            <?php $no = 1; ?>
            @foreach (config('ak23.finger_data') as $finger => $detail)
            <div class="col-sm-15">
              <div class="panel panel-default">
                  <div class="panel-body flat-equal-height">
                    @if ( file_exists(public_path() . "/files/{$source_folder}/{$people->demographic_id}/{$detail['flat_name']}") )
                    <img class="img-responsive" src="{{ asset("files/{$source_folder}/{$people->demographic_id}/{$detail['flat_name']}") }}?{{ time() }}" alt="{{ $detail['title'] }}">
                    @else
                    <img class="img-responsive" src="{{ asset('assets/images/blank-finger.png') }}" alt="{{ $finger }}">
                    @endif
                  </div>
                  <div class="panel-footer aligncenter relative">
                    <strong>{{ $no }}. {{ $detail['title'] }}</strong>
                  </div>
              </div>
            </div>
            <?php $no++; ?>
            @endforeach
          </div>
        </div>
      </div>
</div>

        <div class="rumus m-b-30 clearfix text-center">
            <div class="rumus-box">
                <div class="rumus-wrapper pophover infoable">
                    <div class="rumus-label white">Rumus</div>
                    @for ($i = 0; $i <= 5; $i++)
                    <input type="text" class="" maxlength="3" autocomplete="off" id="formula_{{ $i+1 }}" name="formula[]" value="{{ old('formula[]', array_key_exists($i, $formula) ? trim($formula[$i]) : '') }}" tabindex="{{ 16+$i }}">
                    @endfor
                </div>
 
                <div class="lrumus-wrapper pophover infoable"> 
                    <div class="rumus-label white">Lihat Rumus</div>
                    @for ($i = 0; $i <= 5; $i++)
                    <input type="text" class="" maxlength="3" autocomplete="off" id="formula_view_{{ $i+1 }}" name="formula_view[]" value="{{ old('formula_view[]', array_key_exists($i, $formula_view) ? trim($formula_view[$i]) : '') }}" tabindex="{{ 23+$i }}">
                    @endfor
                </div>
            </div>
        </div>

        <div class="aligncenter m-b-30">
            <div class="btn-group">
                <a class="btn yellow btn-lg uiblock-on" href="{{ url("demographic/edit/{$crypted_id}/picture") }}"><i class="fa fa-angle-left m-r-15"></i> Kembali</a>
                <a class="btn red btn-lg" href="{{ url("people") }}">Batal</a>
                <button class="btn blue btn-lg" type="button" id="saveEditBtn">Simpan</button>
            </div>
        </div>
    {!! Form::close() !!}

@endsection

@push('js')
<script src="{{ asset('assets/js/jquery.matchHeight-min.js') }}"></script>
<script type="text/javascript">
$(document).ready(function() {
  if ("function" !== typeof Promise) {
      alertify.alert("Your browser doesn't support promises");
      return;
  }

  $('#saveEditBtn').click(function(event) {
    event.preventDefault();
    alertify.okBtn("Ya").cancelBtn("Tidak").confirm("Anda yakin data yang diubah sudah benar?").then(function (resolvedValue) {
        resolvedValue.event.preventDefault();
        // alertify.alert("You clicked the " + resolvedValue.buttonClicked + " button!");
        if (resolvedValue.buttonClicked == 'ok')
        {
            App.blockUI();
            $('#saveEdit').submit();
        }
    });
  });

  $('.flat-equal-height, .rolled-equal-height').matchHeight();
});
</script>
@endpush