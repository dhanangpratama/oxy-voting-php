<div class="modal fade" id="verificationModal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Verifikasi</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <a href="#" type="button" id="btn_verification" class="btn green btn-outline" style="visibility: hidden;" data-dismiss="modal">Verifikasi</a>
                    <button type="button" class="btn dark btn-outline" data-dismiss="modal">Batal</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <script type="text/javascript">
        $(document).ready(function() 
        {
            var body = $('#verificationModal .modal-body');
            var loading = '<?php echo asset('assets/images/loading.gif'); ?>';

            // event if modal shown
            $('#verificationModal').on('shown.bs.modal', function (e) {
                // console.log('run modal');
                body.html('<div style="text-align: center; padding: 30px 0;"><img style="margin: 0 auto 20px; display: block;" src="'+loading+'" alt="loading"> Mencari data...</div>');
                body.load( "{{ url('ajax/edit/verification/' . $crypted_id) }}", function() {
                    // do the callback here
                    $('#btn_verification').css('visibility', 'visible');
                });
            });

            // event if modal hidden
            $('#verificationModal').on('hidden.bs.modal', function(e) {
                body.html('');
                $('#btn_verification').css('visibility', 'hidden');
            });

            $('#btn_verification').on('click', function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ url('ajax/verification/update/'.$crypted_id) }}", 
                    type: "GET",
                    dataType: "json",
                    error: function(xhr,status,error)
                    {
                        console.log(error);
                    },
                    success: function(result)
                    {
                        console.log(result);
                        App.unblockUI()
                        // alertify.alert("Data tidak ditemukan.");
                        $.bootstrapGrowl(result.message, {
                          ele: 'body', // which element to append to
                          type: 'info', // (null, 'info', 'danger', 'success')
                          offset: {from: 'bottom', amount: 120}, // 'top', or 'bottom'
                          align: 'center', // ('left', 'right', or 'center')
                          width: 500, // (integer, or 'auto')
                          delay: 4000, // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                          allow_dismiss: true, // If true then will display a cross to close the popup.
                          stackup_spacing: 10 // spacing between consecutively stacked growls.
                        });
                    }
                });
            });
        });
    </script>