@extends('layouts.main')
@section('content')

@include('edit.nav')

{!! Form::open(['method'=>'POST', 'files' => true])  !!}
	<div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">Foto</div>
        </div>
        <div class="portlet-body form">
        	<div class="form-body">
				@if (count($errors) > 0)
				<div class="col-sm-12">
					<div class="alert alert-danger">
						<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
						</ul>
					</div>
				</div>
				@endif

	        	<div class="row">
	        		<div class="col-sm-4 m-b-30">
	        			<div class="p-b-30 text-center">
	        				@if ( file_exists($source . "/{$data['demographic_id']}/face-right.jpg") )
	        				<img src="{{ asset("files/{$source_folder}/{$data['demographic_id']}/face-right.jpg") }}?{{ time() }}" style="margin: 0 auto 30px;" class="img-responsive" alt="Face right">
							<a href="{{ route('demographic_picture_crop', [$faceRight]) }}" class="btn btn-outline  green m-r-15">Potong</a>
	        				<div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn green btn-file">
                                    <span class="fileinput-new"> Ganti Gambar </span>
                                    <span class="fileinput-exists"> Ganti </span>
                                    <input type="file" name="picture_right_side"> </span>
                                <span class="fileinput-filename"> </span> &nbsp;
                                <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                            </div>
							<button type="button" onclick="go('{{ url("demographic/edit/picture/remove/face-right") }}')" class="btn red overlay">Hapus</button>
	                        @else
	                        <div class="fileinput fileinput-new" data-provides="fileinput">
	                            <div class="fileinput-new thumbnail" style="width: 225px;">
	                                <img src="{{ asset('assets/images/right-side.jpg') }}" alt="" /> </div>
	                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
	                            <div>
	                                <span class="btn green btn-file">
	                                    <span class="fileinput-new"> Pilih Gambar </span>
	                                    <span class="fileinput-exists"> Ganti </span>
	                                    <input type="file" name="picture_right_side"> 
	                                </span>
	                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
	                            </div>
	                        </div>
	                        @endif
	                    </div>
	        		</div>

	        		<div class="col-sm-4 m-b-30">
	        			<div class="p-b-30 text-center">
	        				@if ( file_exists($source . "/{$data['demographic_id']}/face-front.jpg") )
	        				<img src="{{ asset("files/{$source_folder}/{$data['demographic_id']}/face-front.jpg") }}?{{ time() }}" style="margin: 0 auto 30px;" class="img-responsive" alt="Wajah muka">
							<a href="{{ route('demographic_picture_crop', [$faceFront]) }}" class="btn btn-outline  green m-r-15">Potong</a>
	        				<div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn green btn-file">
                                    <span class="fileinput-new"> Ganti Gambar </span>
                                    <span class="fileinput-exists"> Ganti </span>
                                    <input type="file" name="picture_front_side"> </span>
                                <span class="fileinput-filename"> </span> &nbsp;
                                <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                            </div>
							<button type="button" onclick="go('{{ url("demographic/edit/picture/remove/face-front") }}')" class="btn red overlay">Hapus</button>
	                        @else
	                        <div class="fileinput fileinput-new" data-provides="fileinput">
	                            <div class="fileinput-new thumbnail" style="width: 225px;">
	                                <img src="{{ asset('assets/images/front-side.jpg') }}" alt="Front side" /> </div>
	                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
	                            <div>
	                                <span class="btn green btn-file">
	                                    <span class="fileinput-new"> Pilih Gambar </span>
	                                    <span class="fileinput-exists"> Ganti </span>
	                                    <input type="file" name="picture_front_side"> 
	                                </span>
	                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
	                            </div>
	                        </div>
	                        @endif
	                    </div>
	        		</div>
	        		
	        		<div class="col-sm-4 m-b-30">
	        			<div class="p-b-30 text-center">
	        				@if ( file_exists($source . "/{$data['demographic_id']}/face-left.jpg") )
	        				<img src="{{ asset("files/{$source_folder}/{$data['demographic_id']}/face-left.jpg") }}?{{ time() }}" style="margin: 0 auto 30px;" class="img-responsive" alt="Face left">
							<a href="{{ route('demographic_picture_crop', [$faceLeft]) }}" class="btn btn-outline  green m-r-15">Potong</a>
	        				<div class="fileinput fileinput-new" data-provides="fileinput">
                                <span class="btn green btn-file">
                                    <span class="fileinput-new"> Ganti Gambar </span>
                                    <span class="fileinput-exists"> Ganti </span>
                                    <input type="file" name="picture_left_side"> </span>
                                <span class="fileinput-filename"> </span> &nbsp;
                                <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                            </div>
							<button type="button" onclick="go('{{ url("demographic/edit/picture/remove/face-left") }}')" class="btn red overlay">Hapus</button>
	        				@else
	                        <div class="fileinput fileinput-new" data-provides="fileinput">
	                            <div class="fileinput-new thumbnail" style="width: 225px;">
	                                <img src="{{ asset('assets/images/left-side.jpg') }}" alt="left side" /> </div>
	                            <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
	                            <div>
	                                <span class="btn green btn-file">
	                                    <span class="fileinput-new"> Pilih Gambar </span>
	                                    <span class="fileinput-exists"> Ganti </span>
	                                    <input type="file" name="picture_left_side"> 
	                                </span>
	                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Hapus </a>
	                            </div>
	                        </div>
	                        @endif
	                    </div>
	        		</div>

					<div class="col-sm-12">
						<button type="submit" class="btn blue btn-block btn-lg uppercase overlay"><i class="fa fa-upload m-r-15"></i> Upload</button>
					</div>
	        	</div>
        	</div>
        </div>
    </div>
    
{!! Form::close() !!}

{!! Form::open(['url' => 'demographic/edit/save', 'method'=>'POST'])  !!}
	<div class="aligncenter m-b-30">
        <div class="btn-group">
            <a class="btn red btn-lg btn-prev" tabindex="37" href="{{ url("demographic/edit/{$crypted_id}/back") }}"><i class="fa fa-angle-left m-r-15"></i> Kembali</a>
            <a class="btn blue btn-lg btn-next" tabindex="36" href="{{ url("demographic/edit/{$crypted_id}/finger") }}">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></a>
        </div>
    </div>
{!! Form::close() !!}

@include('edit.verification_modal')

@endsection

@push('js')
<script>
	$(document).ready(function() {

		$(':input').on('change', function (e) 
		{
		    $(this).addClass('changed-input');
		});

		$('.btn-next, .btn-prev').on('click', function(e) 
		{
			e.preventDefault();
			var btn = $(this);

			if ($('.changed-input').length) 
		    {
		    	alertify.okBtn("Ya").cancelBtn("Tidak").confirm("Anda melakukan perubahan pada Foto, namun anda belum menekan tombol upload.<br><br><strong>Yakin akan meninggalkan halaman ini tanpa mengupload foto?</strong>").then(function (resolvedValue) {
			        resolvedValue.event.preventDefault();
			        // alertify.alert("You clicked the " + resolvedValue.buttonClicked + " button!");
			        if (resolvedValue.buttonClicked == 'ok')
			        {
			            App.blockUI();
			            window.open(btn.attr('href'), "_self");
			        }
			    });
		    }
		    else
		    {
		    	App.blockUI();
		    	window.open(btn.attr('href'), "_self");
		    }
		});
	});
</script>
@endpush