@extends('layouts.main')
@section('content')

@include('edit.nav')

{!! Form::open(['method'=>'POST'])  !!}
    <div class="portlet box dark">
        <div class="portlet-title">
            <div class="caption">Tampilan Belakang</div>
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#page1" aria-controls="page-1" role="tab" data-toggle="tab">Demographic</a></li>
                <li role="presentation"><a href="#page2" aria-controls="page -2" role="tab" data-toggle="tab">Sinyalemen</a></li>
            </ul>
        </div>
        <div class="portlet-body form">
          <div class="form-body">
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="page1">
                    <div class="row data">
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="tempat_lahir" name="pob" value="{{ old('pob', $back_data['pob']) }}" tabindex="1">

                                <label for="tempat_lahir">Tempat Lahir</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <div id="birthday" class="input-group date">
                                    <input type="text" class="form-control infoable" data-id="birthdate" id="mask_date2" name="dob" value="{{ old('dob', ! empty($back_data['dob']) ? date('d-m-Y', strtotime($back_data['dob'])) : '') }}" tabindex="2"> 
                                    <label for="mask_date2">Tanggal Lahir</label>

                                    <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="nationality" name="nationality" value="{{ old('nationality', $back_data['nationality']) }}" tabindex="3">

                                <label for="nationality">Kebangsaan / Suku</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="religion_id" id="religion" tabindex="4">
                                    <option value="0">-- Pilih Agama --</option>
                                    @foreach ($religion as $value)
                                    <option value="{{ $value->religion_id }}" {{ $value->religion_id == $back_data['religion_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="religion">Agama</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="lastaddress" name="last_address" value="{{ old('last_address', $back_data['last_address']) }}" tabindex="5">

                                <label for="lastaddress">Alamat Terakhir</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="idcard" name="card_id" value="{{ old('card_id', $back_data['card_id']) }}" tabindex="6">

                                <label for="idcard">No. KTP</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <select class="form-control pophover infoable" name="education_id" id="education" tabindex="7">
                                    <option value="0">-- Pilih Pendidikan --</option>
                                    @foreach ($educations as $value)
                                    <option {{ $value->education_id == $back_data['education_id'] ? 'selected' : '' }} value="{{ $value->education_id }}">{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="education">Pendidikan Terakhir</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="father_name" name="father_name" value="{{ old('father_name', $back_data['father_name']) }}" tabindex="8">

                                <label for="father_name">Nama Ayah</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="father_address" name="father_address" value="{{ old('father_address', $back_data['father_address']) }}" tabindex="9">

                                <label for="father_address">Alamat Ayah</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="mother_name" name="mother_name" value="{{ old('mother_name', $back_data['mother_name']) }}" tabindex="10">

                                <label for="mother_name">Nama Ibu</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="mother_address" name="mother_address" value="{{ old('mother_address', $back_data['mother_address']) }}" tabindex="11">

                                <label for="mother_address">Alamat Ibu</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="wh_name" name="mate_name" value="{{ old('mate_name', $back_data['mate_name']) }}" tabindex="12">

                                <label for="wh_name">Nama Istri/Suami</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="wh_address" name="mate_address" value="{{ old('mate_address', $back_data['mate_address']) }}" tabindex="13">

                                <label for="wh_address">Alamat Istri/Suami</label>
                            </div>
                        </div>

                            
                        <?php $child = !empty($back_data['children']) ? explode(',', $back_data['children']) : []; ?>
                        @for ($i = 0; $i <= 5; $i++)
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="child_{{$i+1}}" name="children[]" value="{{ old('children[]', array_key_exists($i, $child) ? trim($child[$i]) : '') }}" tabindex="{{ 16+$i }}">

                                <label for="child_{{$i+1}}">Anak {{$i+1}}</label>
                            </div>
                        </div>
                        @endfor
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="page2">
                    <div class="row data">
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <div class="input-group">
                                    <input type="text" class="form-control infoable" id="body_height" name="height" value="{{ old('height', $back_data['height']) }}" tabindex="22"> 

                                    <label for="body_height">Tinggi Badan</label>

                                    <span class="input-group-addon">
                                        CM
                                    </span>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <div class="input-group">
                                    <input type="text" class="form-control infoable" id="body_weight" name="weight" value="{{ old('weight', $back_data['weight']) }}" tabindex="23"> 

                                    <label for="body_weight">Berat Timbangan</label>

                                    <span class="input-group-addon">
                                        KG
                                    </span>
                                </div>
                                
                            </div>
                        </div>

                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="skin_id" id="skin_color" tabindex="24">
                                <option value="0">-- Pilih Warna Kulit --</option>
                                    @foreach ($sinyalemen['skin'] as $value)
                                    <option {{ $value->skin_id == $back_data['skin_id'] ? 'selected' : '' }} value="{{ $value->skin_id }}">{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="skin_color">Warna Kulit</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="body_id" id="body_shape" tabindex="25">
                                    <option value="0">-- Pilih Bentuk Tubuh --</option>
                                    @foreach ($sinyalemen['body'] as $value)
                                    <option value="{{ $value->body_id }}" {{ $value->body_id == $back_data['body_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="body_shape">Bentuk Tubuh</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="head_id" id="shape_of_head" tabindex="26">
                                    <option value="0">-- Pilih Bentuk Kepala --</option>
                                    @foreach ($sinyalemen['head'] as $value)
                                    <option value="{{ $value->head_id }}" {{ $value->head_id == $back_data['head_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach 
                                </select>

                                <label for="shape_of_head">Bentuk Kepala</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="hair_color_id" id="hair_color" tabindex="27">
                                    <option value="0">-- Pilih Warna Rambut --</option>
                                    @foreach ($sinyalemen['hair_color'] as $value)
                                    <option value="{{ $value->hair_color_id }}" {{ $value->hair_color_id == $back_data['hair_color_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach 
                                </select>

                                <label for="hair_color">Warna Rambut</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="hair_id" id="type_of_hair" tabindex="28">
                                    <option value="0">-- Pilih Jenis Rambut --</option>
                                    @foreach ($sinyalemen['hair'] as $value)
                                    <option value="{{ $value->hair_id }}" {{ $value->hair_id == $back_data['hair_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="type_of_hair">Jenis Rambut</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="face_id" id="type_of_face" tabindex="29">
                                    <option value="0">-- Pilih Bentuk Muka --</option>
                                    @foreach ($sinyalemen['face'] as $value)
                                    <option value="{{ $value->face_id }}" {{ $value->face_id == $back_data['face_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="type_of_face">Bentuk Muka</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="forehead_id" id="forehead" tabindex="30">
                                    <option value="0">-- Pilih Dahi --</option>
                                    @foreach ($sinyalemen['forehead'] as $value)
                                    <option value="{{ $value->forehead_id }}" {{ $value->forehead_id == $back_data['forehead_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="forehead">Dahi</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="eye_color_id" id="eyes_color" tabindex="31">
                                    <option value="0">-- Pilih Warna Mata --</option>
                                    @foreach ($sinyalemen['eye_color'] as $value)
                                    <option value="{{ $value->eye_color_id }}" {{ $value->eye_color_id == $back_data['eye_color_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="eyes_color">Warna Mata</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="eye_irregularity_id" id="irregularitesoneyes" tabindex="32">
                                    <option value="0">-- Pilih Kelainan pada Mata --</option>
                                    @foreach ($sinyalemen['eye_irregularity'] as $value)
                                    <option value="{{ $value->eye_irregularity_id }}" {{ $value->eye_irregularity_id == $back_data['eye_irregularity_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="irregularitesoneyes">Kelainan pada mata</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="nose_id" id="nose" tabindex="33">
                                    <option value="0">-- Pilih Hidung --</option>
                                    @foreach ($sinyalemen['nose'] as $value)
                                    <option value="{{ $value->nose_id }}" {{ $value->nose_id == $back_data['nose_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="nose">Hidung</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="lip_id" id="lips" tabindex="34">
                                    <option value="0">-- Pilih Bentuk Bibir --</option>
                                    @foreach ($sinyalemen['lip'] as $value)
                                    <option value="{{ $value->lip_id }}" {{ $value->lip_id == $back_data['lip_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="lips">Bibir</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="tooth_id" id="tooth" tabindex="35">
                                    <option value="0">-- Pilih Bentuk Gigi --</option>
                                    @foreach ($sinyalemen['tooth'] as $value)
                                    <option value="{{ $value->tooth_id }}" {{ $value->tooth_id == $back_data['tooth_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="tooth">Gigi</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="chin_id" id="chin" tabindex="36">
                                    <option value="0">-- Pilih Bentuk Dagu --</option>
                                    @foreach ($sinyalemen['chin'] as $value)
                                    <option value="{{ $value->chin_id }}" {{ $value->chin_id == $back_data['chin_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="chin">Dagu</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input ">
                                <select class="form-control pophover infoable" name="ear_id" id="ear" tabindex="37">
                                    <option value="0">-- Pilih Bentuk Telinga --</option>
                                    @foreach ($sinyalemen['ear'] as $value)
                                    <option value="{{ $value->ear_id }}" {{ $value->ear_id == $back_data['ear_id'] ? 'selected' : '' }}>{{ $value->label }}</option>
                                    @endforeach
                                </select>

                                <label for="ear">Telinga</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="tattoo" name="tattoo" value="{{ old('tattoo', $back_data['tattoo']) }}" tabindex="38">

                                <label for="tattoo">Tatoo</label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="scars_and_handicap" name="scars_and_handicap" value="{{ old('scars_and_handicap', $back_data['scars_and_handicap']) }}" tabindex="39">

                                <label for="scars_and_handicap">Dipotong dan Cacat</label>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group form-md-line-input form-md-floating-label ">
                                <input type="text" class="form-control infoable" id="provision_code" name="provision_code" value="{{ old('provision_code', $back_data['provision_code']) }}" tabindex="40">
                                
                                <label for="provision_code">Kode Pasal</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
          </div>  
        </div>
    </div>

    <div class="alignright m-b-30">
        <div class="btn-group">
            <a class="btn red btn-lg" tabindex="42" href="{{ url("demographic/edit/{$crypted_id}/front") }}"><i class="fa fa-angle-left m-r-15"></i> Kembali</a>
            <button class="btn blue btn-lg overlay" tabindex="41" type="submit">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></button>
        </div>
    </div>
{!! Form::close() !!}

@include('edit.verification_modal')
    
@endsection