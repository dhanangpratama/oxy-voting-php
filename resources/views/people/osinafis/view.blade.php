@extends('layouts.main')
@section('content')

@if ( ! empty($people) )

@if ( config('ak23.access') == 'desktop' && array_get($people, 'source') != 'DERM' )
<a href="{{ route('create_finger_scan_process', ['get_data' => 1]) }}" class="btn red btn-lg uppercase m-b-20 uiblock-on pull-right"><i class="fa fa-plus m-r-15"></i> Tambah ke AK-23</a>
@endif
<a href="{{ url()->previous() }}" class="btn blue btn-lg uppercase m-b-20 uiblock-on"><i class="fa fa-angle-left m-r-15"></i> Kembali</a>

<div class="portlet light portlet-fit">
	<div class="portlet-title">
		<div class="caption">
            <span class="caption-subject sbold uppercase"><i class="icon-user"></i> &nbsp; {{ check_empty(array_get($people, 'full_name')) }}</span>
        </div>
	</div>
	<div class="portlet-body">
		<div class="row">
		    <div class="col-md-12">
				<div class="content">
		            <div class="row">
		                <div class="col-md-9">
		                	<table class="table laten-data uppercase">
								<tr>
		                			<th width="250" class="">Database</th> 
									<td width="20">:</td>
		                			<td>{{ db_source(array_get($people, 'source')) }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">N.I.K.</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'ktp_number')) }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">JENIS KELAMIN</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'sex')) }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">STATUS KAWIN</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'marital_status')) }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">AGAMA</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'religion')) }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">SUKU</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'race')) }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">TTL</th> 
									<td width="20">:</td>
		                			<td>{{ !empty(array_get($people, 'birth_place')) ? array_get($people, 'birth_place') . ', ' : '' }}{{ check_empty(array_get($people, 'date_of_birth')) }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">PEKERJAAN</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'occupation')) }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">ALAMAT</th> 
									<td width="20">:</td>
		                			<td>
		                				{{ check_empty(array_get($people, 'address')) }}
		                			</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">RT</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'rt_number')) }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">RW</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'rw_number')) }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">DESA</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'village')) }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">KECAMATAN</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'district_number')) }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">KABUPATEN</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'regent_number')) }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">PROVINSI</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'province_number')) }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">N.I.K. Ayah</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'father_nik_number')) }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">N.I.K. Ibu</th> 
									<td width="20">:</td>
		                			<td>{{ check_empty(array_get($people, 'mother_nik_number')) }}</td>
		                		</tr>
		                	</table>
		                </div>
						<div class="col-md-3">
							@if ( !empty(array_get($images, 'images.FACE')) )
							<div class="panel panel-default">
								<div class="panel-body alt">
									<img src="data:image/jpg;base64, {{ array_get($images, 'images.FACE') }}" class="img-fullwidth">
								</div>
								<div class="panel-footer uppercase aligncenter relative">
									Foto
								</div>
							</div>
							@else
							<img src="{{ asset("assets/images/no-photo.png") }}?{{ time() }}" class="img-responsive">
							@endif
	                	</div>
		            </div>
		        </div>
		    </div> 
		</div>
	</div>
</div>


<div class="holder-inner live">
	<div class="row gutter-10" style="margin-bottom: 10px;">
		@foreach(config('ak23.os_inafis_finger_key') as $keyFinger => $finger)
			<div class="col-sm-15 equal-height">
				<div class="panel panel-default">
					<div class="panel-body flat-equal-height">
						<div class="finger-wrapper flat">
							@if ( ! empty(array_get($images, 'images.'.strtoupper($keyFinger))) )
							<a class="image-popup" title="{{ title_case(str_replace('_', ' ', $finger['alt'])) }}" href="data:image/jpg;base64, {{ array_get($images, 'images.'.strtoupper($keyFinger)) }}">
								<img src="data:image/jpg;base64, {{ array_get($images, 'images.'.strtoupper($keyFinger)) }}" class="aligncenter img-responsive" alt="">
							</a>
							@else
							<a href="javascript:;">
								<img src="{{ asset("assets/images/disabled.png") }}" class="aligncenter img-responsive" alt="">
							</a>
							@endif
						</div>
					</div>
					<div class="panel-footer uppercase aligncenter relative">
						{{ str_replace('_', ' ', $finger['alt']) }}
					</div>
				</div>
				
			</div>
		@endforeach
	</div>
</div>

@endif

@endsection