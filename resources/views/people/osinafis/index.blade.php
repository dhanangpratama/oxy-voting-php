@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'GET']) !!}
<div class="clearfix m-b-15">
    <h4 class="white uppercase"><strong>Data Server {{ db_source($source) }}</strong></h4>
</div>

<div class="portlet box dark">
    <div class="portlet-body">
        <div class="table">
            <table class="table">
                <thead>
                    <tr>
                        <th class="uppercase">Nama Lengkap</th>
                        <th class="uppercase">Tanggal Lahir</th>
                        <th class="uppercase">Jenis Kelamin</th>
                        <th class="uppercase">Pekerjaan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if ( ! empty($peoples) )
                    @foreach($peoples as $people)
                    {{-- */$x++;/* --}}
                    <tr>
                        <td>{{ strtoupper(check_empty(array_get($people, 'NAMA_LGKP'))) }}</td>
                        <td>{{ ! empty(array_get($people, 'TGL_LHR')) ? format_date(array_get($people, 'TGL_LHR')) : '-' }}</td>
                        <td>{{ strtoupper(check_empty(array_get($people, 'JENIS_KLMIN'))) }}</td>
                        <td>{{ strtoupper(stripslashes(check_empty(array_get($people, 'PEKERJAAN')))) }}</td>
                        <td>
                            <a href="javascript:;" onclick="go('{{ route('people_osinafis_detail', [encrypt(array_get($people, 'EXTERNAL_ID'))]) }}')" class="btn blue btn-sm uiblock-on" title="View Person">Lihat</a>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="text-center" colspan="6">Data tidak ditemukan</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

{!! Form::close() !!}

<div class="pagination-wrapper text-right m-b-30 uppercase">
    <div class="btn-group">
        @if ( $prev )
        <a href="{{ $prevUrl }}" class="btn red btn-md uiblock-on"><i class="fa fa-angle-left m-r-15"></i> Sebelumnya</a>
        @endif
        @if ( $next )
        <a href="{{ $nextUrl }}" class="btn blue btn-md uiblock-on">Selanjutnya <i class="fa fa-angle-right m-l-15"></i></a>
        @endif
    </div>
</div>

@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) 
    {
        /*$('#print_ak24').on('click', function(event) {
            setTimeout(function() {
                location.reload();
            }, 1250);
            
        });*/

        $("input[name='print']").change(function() 
        {
            var action = (this.checked) ? 'add' : 'remove';

            $.ajax({
                url: "{{ url('ajax/print') }}",
                type: "POST",
                dataType: 'json',
                data: {
                    peopleID: $(this).val(),
                    action: action
                },
                beforeSend: function()
                {
                    App.blockUI();
                },
                error: function(xhr,status,error)
                {
                    console.log(error);
                },
                success: function(data) 
                {
                    //console.log(data);
                    if ( data.status == 'success')
                    {
                        /**
                         * Update count print AK-24
                         */
                         $('#print_count').html(data.count);

                         if ( data.count > 0 )
                         {
                             $('#ak24_clear').removeClass('hide');
                         }
                         else
                         {
                             $('#ak24_clear').addClass('hide');
                         }
                    }

                    if ( data.status == 'error')
                    {
                        /**
                         * Remove checked if error
                         */
                         $("input[name='print'][value="+data.ID+"]").removeAttr('checked');

                         alertify.alert("Hanya bisa memilih maksimal 10 data.");
                    }

                    App.unblockUI();
                }
            });
        }); 
    });
});
</script>
@endpush