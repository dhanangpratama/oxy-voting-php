@extends('layouts.main')
@section('content')

@include('people.top_toolbar')

@include('people.notification')

<div class="portlet box dark">
        @include('people.nav')
        <div class="portlet-body form">
            <div class="tab-content">
                <div class="tab-pane active" id="data">
                    <div class="form-body">                     
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row data pop-disable">
                                    <div class="col-sm-12">
                                            <div class="form-group form-md-line-input">
                                                <select class="form-control pophover infoable" disabled="disabled" id="status" tabindex="0">
                                                    <option>-</option>
                                                    @foreach ($status as $value)
                                                    <option value="{{ $value->status_id }}" {{ $value->status_id == $people->status_id ? 'selected="selected"' : '' }}>{{ $value->label }}</option>
                                                    @endforeach
                                                </select>

                                                <label for="status">Status</label>
                                            </div>
                                        </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="fullname" name="fullname" value="{{ ! empty($people->fullname) ? $people->fullname : '-' }}" tabindex="1" readonly="readonly">

                                            <label for="fullname">Nama</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="nickname" name="nickname" readonly="readonly" value="{{ ! empty($people->nickname) ? $people->nickname : '-' }}" tabindex="2">

                                            <label for="nickname">Nama Kecil / Alias</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="job" name="job" value="{{ ! empty($people->job) ? $people->job : '-' }}" readonly="readonly" tabindex="3">

                                            <label for="job">Pekerjaan</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="sex" value="{{ ($people->sex == 'm') ? 'Laki-laki' : 'Perempuan' }}" readonly="readonly" tabindex="3">
                                            <label for="sex">Jenis Kelamin</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="location" name="location" readonly="readonly" value="{{ ! empty($people->location) ? $people->location : '-' }}" tabindex="8">

                                            <label for="location">Lokasi</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <div id="datepicker--" class="input-group date">
                                                <input type="text" class="form-control infoable" data-id="datein" id="mask_date2--" readonly="readonly"  value="{{ (! empty($people->taken_date)) ? format_date($people->taken_date) : '-' }}" tabindex="9"> 
                                                <label for="mask_date2">Tanggal Pengambilan</label>
                                                <span class="input-group-addon disabled">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="takenby" name="taken_by" readonly="readonly" value="{{ ! empty($people->taken_by) ? $people->taken_by : '-' }}" tabindex="10">

                                            <label for="takenby">Diambil Oleh</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="saksi" name="view_by" readonly="readonly" value="{{ ! empty($people->viewed_by) ? $people->viewed_by : '-' }}" tabindex="11">

                                            <label for="saksi">Disaksikan Oleh</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group form-md-line-input">
                                            <textarea class="form-control infoable" rows="3" id="note" name="note" readonly="readonly" tabindex="12">{{ ! empty($people->note) ? $people->note : '-' }}</textarea>

                                            <label for="note">Catatan</label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="tempat_lahir" value="{{ $people->pob }}" readonly="readonly" tabindex="1">

                                    <label for="tempat_lahir">Tempat Lahir</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <div id="datepicker--" class="input-group date">
                                        <input type="text" class="form-control infoable" data-id="birthdate" id="mask_date2--" value="{{ (! empty($people->dob) && $people->dob != '0000-00-00 00:00:00' ) ? format_date($people->dob) : '-' }}" readonly="readonly" tabindex="2"> 
                                        <label for="mask_date2--">Tanggal Lahir</label>

                                        <span class="input-group-addon disabled">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="nationality" name="nationality" readonly="readonly" value="{{ $people->nationality }}" tabindex="3">

                                    <label for="nationality">Kebangsaan</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="race" readonly="readonly" value="{{ $people->race }}" tabindex="3">

                                    <label for="race">Suku</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-md-line-input">
                                    <?php
                                    if ( $is_datacenter )
                                    {
                                        $religion = ! empty($people->religion_id) ? $people->religion_id : '-';
                                    }
                                    else
                                    {
                                        $religion = ! empty($people->religion_id) ? $people->religion_data->label : '-';
                                    }
                                    ?>
                                    <input type="text" class="form-control infoable" value="{{ $religion }}" disabled="disabled">
                                    
                                    <label for="religion">Agama</label>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="address" name="address" value="{{ $people->address }}" readonly="readonly" tabindex="5">

                                    <label for="address">Alamat</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="rt" name="rt" value="{{ $people->rt }}" readonly="readonly" tabindex="5">

                                    <label for="rt">RT</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="rw" name="rw" value="{{ $people->rw }}" readonly="readonly" tabindex="5">

                                    <label for="rw">RW</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="village" name="village" value="{{ $people->village }}" readonly="readonly" tabindex="5">

                                    <label for="village">Desa</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="regent" name="regent" value="{{ $people->regent }}" readonly="readonly" tabindex="5">

                                    <label for="regent">Kota / Kabupaten</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="province" name="province" value="{{ $people->province }}" readonly="readonly" tabindex="5">

                                    <label for="province">Provinsi</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="district" name="district" value="{{ $people->district }}" readonly="readonly" tabindex="5">

                                    <label for="district">Wilayah</label>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="country" name="country" value="{{ $people->country }}" readonly="readonly" tabindex="5">

                                    <label for="country">Negara</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="postal_code" name="postal_code" value="{{ $people->postal_code }}" readonly="readonly" tabindex="5">

                                    <label for="postal_code">Kodepos</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="idcard" value="{{ $people->nik }}" readonly="readonly" tabindex="6">

                                    <label for="idcard">No. KTP</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="passport_number" value="{{ $people->passport_number }}" readonly="readonly" tabindex="6">

                                    <label for="passport_number">No. Paspor</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <?php
                                    if ( $is_datacenter )
                                    {
                                        $education = ! empty($people->education_id) ? $people->education_id : '-';
                                    }
                                    else
                                    {
                                        $education = ! empty($people->education_id) ? $people->education_data->label : '-';
                                    }
                                    ?>
                                    <input type="text" class="form-control infoable" value="{{ $education }}" disabled="disabled">

                                    <label for="education">Pendidikan Terakhir</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="father_name" name="father_name" name="last_address" value="{{ $people->father_name }}" readonly="readonly" tabindex="8">

                                    <label for="father_name">Nama Ayah</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="father_address" name="father_address" name="last_address" value="{{ $people->father_address }}" readonly="readonly" tabindex="9">

                                    <label for="father_address">Alamat Ayah</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="mother_name" name="last_address" value="{{ $people->mother_name }}" readonly="readonly" tabindex="10">

                                    <label for="mother_name">Nama Ibu</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="mother_address" name="last_address" value="{{ $people->mother_address }}" readonly="readonly" tabindex="11">

                                    <label for="mother_address">Alamat Ibu</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="wh_name" name="last_address" value="{{ $people->mate_name }}" readonly="readonly" tabindex="12">

                                    <label for="wh_name">Nama Istri/Suami</label>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="wife_husband_address" value="{{ $people->mate_address }}" readonly="readonly" tabindex="13">

                                    <label for="wh_address">Alamat Istri/Suami</label>
                                </div>
                            </div>
                            
                            @if ( ! empty($people->children) )
                            <?php 
                            $childs = explode(',', $people->children);
                            $no = 1; ?>
                            @foreach ($childs as $key => $child)
                            <div class="col-sm-6">
                                <div class="form-group form-md-line-input">
                                    <input type="text" class="form-control infoable" id="child_{{ $key+1 }}" name="childs[]" value="{{ $child }}" readonly="readonly" tabindex="16">

                                    <label for="child_{{ $key }}">Anak {{ $key+1 }}</label>
                                </div>
                            </div>
                            @endforeach
                            @endif
                        </div>  
                    </div>
                </div>
                <div class="tab-pane" id="sinyalemen">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="padding-top: 5px;">
                                    <div class="row data pop-disable">
                                        <div class="col-sm-4">
                                            <div class="form-group form-md-line-input">
                                                <div class="input-group pophover">
                                                    <input type="text" class="form-control infoable" id="body_height" name="height" name="height" value="{{ $people->height }}" readonly="readonly" tabindex="14"> 
                                                    <label for="body_height">Tinggi Badan</label>

                                                    <span class="input-group-addon disabled">
                                                        CM
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-md-line-input">
                                                <div class="input-group pophover">
                                                    <input type="text" class="form-control infoable" id="body_weight" name="weight" value="{{ $people->weight }}" readonly="readonly" tabindex="15"> 
                                                    <label for="body_weight">Berat Timbangan</label>

                                                    <span class="input-group-addon disabled">
                                                        KG
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ ! empty($people->blood_type) ? $people->blood_type : '-' }}" disabled="disabled">

                                                <label for="blood_type">Golongan Darah</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ ! empty($people->skin_id) ? $people->skin_data->label : '-' }}" disabled="disabled">

                                                <label for="skin_color">Warna Kulit</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ ! empty($people->body_id) ? $people->body_data->label : '-' }}" disabled="disabled">

                                                <label for="body_shape">Bentuk Tubuh</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ ! empty($people->head_id) ? $people->head_data->label : '-' }}" disabled="disabled">

                                                <label for="shape_of_head">Bentuk Kepala</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ ! empty($people->hair_color_id) ? $people->hairColor->label : '-' }}" disabled="disabled">

                                                <label for="hair_color">Warna Rambut</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ ! empty($people->hair_id) ? $people->hair_data->label : '-' }}" disabled="disabled">

                                                <label for="type_of_hair">Jenis Rambut</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ ! empty($people->face_id) ? $people->face_data->label : '-' }}" disabled="disabled">

                                                <label for="type_of_face">Bentuk Muka</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ ! empty($people->forehead_id) ? $people->forehead_data->label : '-' }}" disabled="disabled">

                                                <label for="forehead">Dahi</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ ! empty($people->eye_color_id) ? $people->eyeColor->label : '-' }}" disabled="disabled">

                                                <label for="eyes_color">Warna Mata</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ ! empty($people->eye_irregularity_id) ? $people->eyeIrregularity->label : '-' }}" disabled="disabled">

                                                <label for="irregularitesoneyes">Kelainan pada mata</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ ! empty($people->nose_id) ? $people->nose_data->label : '-' }}" disabled="disabled">

                                                <label for="nose">Hidung</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ ! empty($people->lip_id) ? $people->lip_data->label : '-' }}" disabled="disabled">

                                                <label for="lips">Bibir</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ ! empty($people->tooth_id) ? $people->tooth_data->label : '-' }}" disabled="disabled">

                                                <label for="tooth">Gigi</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ ! empty($people->chin_id) ? $people->chin_data->label : '-' }}" disabled="disabled">

                                                <label for="chin">Dagu</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ ! empty($people->ear_id) ? $people->ear_data->label : '-' }}" disabled="disabled">

                                                <label for="ear">Telinga</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" id="tattoo" name="tattoo" value="{{ $people->tattoo }}" readonly="readonly" tabindex="35">

                                                <label for="tattoo">Tatoo</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" id="scars_and_handicap" name="scars_and_handicap" value="{{ $people->scars_and_handicap }}" readonly="readonly" tabindex="35">

                                                <label for="scars_and_handicap">Dipotong dan Cacat</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" id="provision_code" name="provision_code" value="{{ $people->provision_code }}" readonly="readonly" tabindex="35">

                                                <label for="provision_code">Kode Pasal</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-md-line-input">
                                                <textarea class="form-control infoable" rows="3" readonly="readonly" tabindex="12">{{ ! empty($people->information) ? $people->information : '-' }}</textarea>

                                                <label for="note">Keterangan</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="tab-pane" id="photo">
                    <div class="portlet-body form">
                        <div class="form-body">
                            <div class="row m-t-20">
                                <div class="col-sm-4 picture-box">
                                    <div class="panel panel-default">
                                        <div class="panel-body alt">
                                            @if ( file_exists($path['data']['complete'] . "/{$file['folder']}/face-front.jpg") )
                                            <img src="{{ config('ak23.access') == 'desktop' ? data_url("complete/{$file['folder']}/face-front.jpg") : config('url.demographic') . "{$file['folder']}/face-front.jpg" }}?{{ time() }}" style="margin: 0 auto;" class="img-fullwidth" alt="Wajah depan">
                                            @else
                                            <img src="{{ asset("assets/images/unknown-big.png") }}" class="img-fullwidth" alt="Tidak ada foto">
                                            @endif
                                        </div>
                                        <div class="panel-footer uppercase aligncenter relative">
                                            Tampak Depan
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4 picture-box">
                                    <div class="panel panel-default">
                                        <div class="panel-body alt">
                                            @if ( file_exists($path['data']['complete'] . "/{$file['folder']}/face-left.jpg") )
                                            <img src="{{ config('ak23.access') == 'desktop' ? data_url("complete/{$file['folder']}/face-left.jpg") : config('url.demographic') . "{$file['folder']}/face-left.jpg" }}?{{ time() }}" style="margin: 0 auto;" class="img-fullwidth" alt="Wajah kiri">
                                            @else
                                            <img src="{{ asset("assets/images/unknown-big.png") }}" class="img-fullwidth" alt="Tidak ada foto">
                                            @endif
                                        </div>
                                        <div class="panel-footer uppercase aligncenter relative">
                                            Tampak Kiri
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-4 picture-box">
                                    <div class="panel panel-default">
                                        <div class="panel-body alt">
                                            @if ( file_exists($path['data']['complete'] . "/{$file['folder']}/face-right.jpg") )
                                            <img src="{{ config('ak23.access') == 'desktop' ? data_url("complete/{$file['folder']}/face-right.jpg") : config('url.demographic') . "{$file['folder']}/face-right.jpg" }}?{{ time() }}" style="margin: 0 auto;" class="img-fullwidth" alt="Wajah kanan">
                                            @else
                                            <img src="{{ asset("assets/images/unknown-big.png") }}" class="img-fullwidth" alt="Tidak ada foto">
                                            @endif
                                        </div>
                                        <div class="panel-footer uppercase aligncenter relative">
                                            Tampak Kanan
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="finger">
                    <div class="form-body">
                        <div class="ak-tab m-b-30 m-t-20">
                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#roll" aria-controls="roll" role="tab" data-toggle="tab">Rolled</a></li>
                                <li role="presentation"><a href="#flat" aria-controls="flat" role="tab" data-toggle="tab">Flat</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content {{ $people->enrollment_type }}">
                                <div role="tabpanel" class="tab-pane active" id="roll">
                                    <div class="row">
                                        <?php $no = 1; ?>
                                        @foreach (config('ak23.finger_data') as $finger => $detail)
                                        <div class="col-sm-15">
                                            <div class="panel panel-default">
                                                <div class="panel-body alt rolled-equal-height">
                                                    <div class="henry-label">
                                                        {{ ! empty($finger_json['fingers'][$finger]['pattern_classification']) ? str_replace('_', ' ', $finger_json['fingers'][$finger]['pattern_classification']) : '-' }}
                                                    </div>
                                                    <div class="finger-wrapper">
                                                        {{-- <div class="text-center m-b-10">{{ $detail['num'] }}</div> --}}
                                                        @if ( file_exists($path['data']['complete'] . "/{$file['folder']}/{$finger}.{$ext}") )
                                                        <img class="img-fullwidth" src="{{ config('ak23.access') == 'desktop' ? data_url('complete/'.$file['folder'].'/'.$finger.".{$ext}") : config('url.demographic') . "{$file['folder']}/{$finger}.{$ext}" }}?{{ time() }}" alt="<?php echo $finger; ?>">
                                                        @else
                                                        <img class="img-fullwidth" src="{{ asset('assets/images/unknown-big.png') }}?{{ time() }}" alt="{{ $finger }}">
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="panel-footer aligncenter uppercase relative">
                                                    {{ $detail['title'] }}
                                                </div>
                                            </div>
                                        </div>
                                        <?php $no++ ?>
                                        @endforeach
                                    </div>  
                                </div>
                                <div role="tabpanel" class="tab-pane" id="flat">
                                    <div class="row">
                                        @foreach (config('ak23.finger_data') as $finger => $detail)
                                        <div class="col-sm-15">
                                            <div class="panel panel-default">
                                                <div class="panel-body alt flat-equal-height">
                                                <div class="henry-label">
                                                    {{ ! empty($finger_json['fingers'][$finger]['pattern_classification']) ? str_replace('_', ' ', $finger_json['fingers'][$finger]['pattern_classification']) : '-' }}
                                                </div>
                                                <div class="finger-wrapper flat">
                                                @if ( file_exists($path['data']['complete'] . "/{$file['folder']}/{$detail['flat_name']}") )
                                                <img class="img-fullwidth" src="{{ config('ak23.access') == 'desktop' ? data_url("complete/".$file['folder']."/{$detail['flat_name']}") : config('url.demographic') . "{$file['folder']}/{$detail['flat_name']}" }}?{{ time() }}" alt="{{ $detail['title'] }}">
                                                @else
                                                <img class="img-fullwidth" src="{{ asset('assets/images/unknown-big.png') }}?{{ time() }}" alt="{{ $finger }}">
                                                @endif
                                                </div>
                                                </div>
                                                <div class="panel-footer uppercase aligncenter relative">
                                                {{ $detail['title'] }}
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rumus m-b-30 clearfix text-center">
                            <div class="rumus-box">
                                <div class="rumus-wrapper pophover infoable">
                                    <div class="rumus-label white">Rumus</div>
                                    <?php $formula = !empty($people->formula) ? explode('|', $people->formula) : []; ?>
                                    @for ($i = 0; $i <= 5; $i++)
                                    <input type="text" class="" maxlength="3" readonly="readonly" autocomplete="off" id="formula_{{ $i+1 }}" name="formula[]" value="{{ old('formula[]', array_key_exists($i, $formula) ? trim($formula[$i]) : '') }}" tabindex="{{ 16+$i }}">
                                    @endfor
                                </div>
                                <div class="lrumus-wrapper pophover infoable"> 
                                    <div class="rumus-label white">Lihat Rumus</div>
                                    <?php $formula_view = !empty($people->formula_view) ? explode('|', $people->formula_view) : []; ?>
                                    @for ($i = 0; $i <= 5; $i++)
                                    <input type="text" class="" maxlength="3" readonly="readonly" autocomplete="off" id="formula_view_{{ $i+1 }}" name="formula_view[]" value="{{ old('formula_view[]', array_key_exists($i, $formula_view) ? trim($formula_view[$i]) : '') }}" tabindex="{{ 23+$i }}">
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        $('.flat-equal-height, .rolled-equal-height').matchHeight();
    });
});
</script>
@endpush