<div class="portlet-title relative mt-element-ribbon">
           
    {{--  <div class="flag-ribbon {{ isset($people->is_verified_local) && ! $people->is_verified_local ? '' : 'ok' }}"></div>  --}}
    <div class="ribbon ribbon-vertical-left ribbon-border-dash-vert ribbon-shadow ribbon-color-{{ isset($people->is_verified_local) && ! $people->is_verified_local || isset($people->is_verified_server) && ! $people->is_verified_server ? 'danger' : 'primary' }} uppercase">
        <div class="ribbon-sub ribbon-bookmark"></div>
        <i class="fa fa-{{ isset($people->is_verified_local) && ! $people->is_verified_local || isset($people->is_verified_server) && ! $people->is_verified_server ? 'times' : 'check' }}"></i>
    </div>

    <div class="caption">{{ ! empty($people->fullname) ? $people->fullname : '-' }}</div>

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#data" class="uppercase" data-toggle="tab" aria-expanded="true"> Data </a></li>
        <li role="presentation" class=""><a href="#sinyalemen" class="uppercase" data-toggle="tab" aria-expanded="true"> Sinyalemen </a></li>
        <li role="presentation" class=""><a href="#photo" class="uppercase" data-toggle="tab" aria-expanded="true"> Foto </a></li>
        <li role="presentation" class=""><a href="#finger" class="uppercase" data-toggle="tab" aria-expanded="true"> Jari </a></li>
    </ul>
</div>