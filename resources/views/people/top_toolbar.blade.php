<?php $id = encrypt($people->demographic_id); ?>

<div id="top_toolbar" class="clearfix" style="margin-bottom: 20px;">
	@if ( ! Request::get('noback') )
	<a href="{{ url()->previous() }}" class="btn red btn-lg uppercase uiblock-on"><i class="fa fa-chevron-left"></i> Kembali</a>
	@endif

	@if ( config('ak23.access') == 'desktop' )
		<button onclick="go('{{ url('demographic/edit/'.$people->subject_id.'/front') }}')" class="btn green btn-lg uppercase pull-right uiblock-on" style="margin-left: 10px;"><i class="fa fa-pencil"></i> Ubah data</button>

		<button type="button" onclick="PopupWindowCenter('{{ url('export/ak23/' . $id) }}', '_blank', winWidth, winHeight)" class="btn blue btn-lg uppercase pull-right" title="Print AK-23"><i class="fa fa-print" aria-hidden="true"/></i> Cetak AK-23</button>

		@if ($people->sync_status == 'SYNCED' )
		<span class="btn btn-lg cur-default pull-right" style="margin-right: 10px; color: #FFF">Terkirim ke server {{ date('d-m-Y H:i', strtotime($people->date_uploaded)) }}</span>
		@else
		<span class="btn btn-lg uppercase red cur-default pull-right" style="margin-right: 10px;">Belum Terkirim</span>
		@endif
	@endif
</div>