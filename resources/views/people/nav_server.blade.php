<div class="portlet-title relative mt-element-ribbon">
    {{--  <div class="flag-ribbon {{ isset($people->is_verified_local) && ! $people->is_verified_local ? '' : 'ok' }}"></div>  --}}
    <div class="ribbon ribbon-vertical-left ribbon-border-dash-vert ribbon-shadow ribbon-color-{{ array_get($people, 'isVerifiedLocal') || array_get($people, 'isVerifiedServer') ? 'primary' : 'danger' }} uppercase">
        <div class="ribbon-sub ribbon-bookmark"></div>
        <i class="fa fa-{{ array_get($people, 'isVerifiedLocal') || array_get($people, 'isVerifiedServer') ? 'check' : 'times' }}"></i>
    </div>

    <div class="caption">{{ check_empty(array_get($people, 'fullname')) }}</div>

    <!-- Nav tabs -->
    <ul id="mainNavTab" class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#data" class="uppercase" data-toggle="tab" aria-expanded="true"> Data </a></li>
        <li role="presentation" class=""><a href="#sinyalemen" class="uppercase" data-toggle="tab" aria-expanded="true"> Sinyalemen </a></li>
        <li role="presentation" class=""><a href="#photo" class="uppercase" data-toggle="tab" aria-expanded="true"> Foto </a></li>
        <li role="presentation" class=""><a href="#finger" class="uppercase" data-toggle="tab" aria-expanded="true"> Jari </a></li>
    </ul>
</div>