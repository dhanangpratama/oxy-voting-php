@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'GET']) !!}
<div class="clearfix m-b-15">
    <button type="button" id="print_ak24" class="btn blue uppercase btn-lg pull-right" onclick="PopupWindowCenter('{{ $server ? url('export/ak24-server') : url('export/ak24') }}', '_blank', 1300, 800)">Cetak AK-24 (<span id="print_count">{{ count($print_data) }}</span>)</button>
    <a  id="ak24_clear" href="{{ route('ak24_clear') }}" class="btn red pull-right {{ empty(session('print_data')) ? 'hide' : '' }} uppercase btn-lg uiblock-on">Bersihkan AK24 Terpilih</a>
    @if ( config('ak23.access') == 'desktop' )
    <h4 class="white uppercase"><strong>{{ $server ? 'Data Server' : 'Data Lokal' }} AK-23</strong></h4>
    @endif
</div>
<div class="portlet box dark">
    <div class="portlet-body">
        <div class="table">
            <table class="table">
                <thead>
                    <tr>
                        <th class="uppercase">Nama Lengkap</th>
                        <th width="180" class="uppercase">Tanggal Lahir</th>
                        <th class="uppercase">NIK</th>
                        @if ( $server )
                        <th class="uppercase">Lokasi</th>
                        @endif
                        <th class="uppercase">Status</th>
                        <th class="uppercase">Verifikasi</th>
                        @if ( ! $server && config('ak23.access') == 'desktop' )
                        <th class="uppercase">Terkirim</th>
                        @endif
                        <th width="160" class="uppercase">Cetak AK-24{{--<input type="checkbox" id="checkAll"/>--}}</th>
                        <th width="160"></th>
                    </tr>
                </thead>
                <tbody>
                @if ( $server && array_get($people, 'results.page.totalElements') > 0 )

                @foreach(array_get($people, 'results.content') as $item)
                    {{-- */$x++;/* --}}
                    <tr>
                        <td class="uppercase">{{ ! empty($item['fullname']) ? title_case($item['fullname']) : '-' }}</td>
                        <td class="uppercase">{{ ! empty($item['dob']) ? format_date($item['dob']) : '-' }}</td>
                        <td class="uppercase">{{ ! empty($item['nik']) ? $item['nik'] : '-' }}</td>
                        <td class="uppercase">{{ array_has($item, 'workstation.location.name') ? array_get($item, 'workstation.location.name') : '-' }}</td>
                        <td class="uppercase">{{ ! empty($item['status']) ? $item['status']['label'] : '-' }}</td>
                        <td align="center">{!! ! empty($item['isVerifiedLocal']) && $item['isVerifiedLocal'] || ! empty($item['isVerifiedServer']) && $item['isVerifiedServer'] ? '<i class="fa fa-check green"></i>' : '<i class="fa fa-times red"></i>' !!}</td>
                        <td><input type="checkbox" name="print" value="{{ $item['id'] }}" {{ (in_array($item['id'], $print_data)) ? 'checked' : '' }} /> ({{ ! empty($item['ak24Counter']) ? $item['ak24Counter'] : 0 }}x)</td>
                        <td>
                            <a href="#" onclick="go('{{ url('people/view/' .  encrypt($item['id'])) }}?source=server')" class="btn blue btn-sm uiblock-on" title="View Person">Lihat</a>
                            <a href="#" onclick="PopupWindowCenter('{{ url('export/ak23/' .  encrypt($item['id'])) }}?dc=1', '_blank', winWidth, winHeight)" class="btn blue btn-sm" title="Print AK-23">Cetak</a>
                        </td>
                    </tr>
                @endforeach

                @elseif ( ! $server && $people->total() > 0 )
                {{-- */$x=0;/* --}}
                @foreach($people as $item)
                    {{-- */$x++;/* --}}
                    <tr>
                        <td class="uppercase">{{ ! empty($item->fullname) ? title_case($item->fullname) : '-' }}</td>
                        <td class="uppercase">{{ (! empty($item->dob) && $item->dob != '0000-00-00 00:00:00' ) ? date ('d-m-Y', strtotime($item->dob)) : '-' }}</td>
                        <td class="uppercase">{{ ! empty($item->nik) ? $item->nik : '-' }}</td>
                        <td class="uppercase">{{ ! empty($item->status_id) && ! empty($item->status_data->label) ? $item->status_data->label : '-' }}</td>
                        <td align="center uppercase">{!! ! empty($item->is_verified_local) && $item->is_verified_local || ! empty($item->is_verified_server) && $item->is_verified_server ? '<i class="fa fa-check green"></i>' : '<i class="fa fa-times red"></i>' !!}</td>
                        @if ( config('ak23.access') == 'desktop' )
                        <td align="center">
                            @if ($item->sync_status == 'SYNCED')
                            <i class="fa fa-check green"></i>
                            @else
                            <i class="fa fa-times red"></i>
                            @endif
                        </td>
                        @endif
                        <td><input type="checkbox" name="print" value="{{ $item->subject_id }}" {{ (in_array($item->subject_id, $print_data)) ? 'checked' : '' }} /> ({{ $ak24->where('subject_id', $item->subject_id)->count() }}x)</td>
                        <td>
                            <a href="#" onclick="go('{{ url('people/view/' .  Crypt::encrypt($item->demographic_id)) }}?source=local')" class="btn green btn-sm uiblock-on" title="View Person">Lihat</a>
                            {{--<button type="button" onclick="go('{{ url('export/ak24/' .  Crypt::encrypt($item->demographic_id)) }}', '_blank')" class="btn blue btn-xs" title="Print AK-24"><span class="fa fa-print" aria-hidden="true"/></button>--}}
                             @if ( config('ak23.access') == 'desktop' )
                            <a href="#" onclick="PopupWindowCenter('{{ url('export/ak23/' .  Crypt::encrypt($item->demographic_id)) }}', '_blank', winWidth, winHeight)" class="btn blue btn-sm" title="Print AK-23">Cetak</a>
                            @endif
                        </td>
                    </tr>
                @endforeach
                @else
                <tr>
                    <td colspan="8" align="center">Data tidak ditemukan.</td>
                </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="pagination"> {!! $server ? $paginator->render() : $people->render() !!} </div>

{!! Form::close() !!}
@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) 
    {
        /*$('#print_ak24').on('click', function(event) {
            setTimeout(function() {
                location.reload();
            }, 1250);
            
        });*/

        $("input[name='print']").change(function() 
        {
            var action = (this.checked) ? 'add' : 'remove';

            $.ajax({
                url: "{{ url('ajax/print') }}",
                type: "POST",
                dataType: 'json',
                data: {
                    peopleID: $(this).val(),
                    action: action
                },
                beforeSend: function()
                {
                    App.blockUI();
                },
                error: function(xhr,status,error)
                {
                    console.log(error);
                },
                success: function(data) 
                {
                    //console.log(data);
                    if ( data.status == 'success')
                    {
                        /**
                         * Update count print AK-24
                         */
                         $('#print_count').html(data.count);

                         if ( data.count > 0 )
                         {
                             $('#ak24_clear').removeClass('hide');
                         }
                         else
                         {
                             $('#ak24_clear').addClass('hide');
                         }
                    }

                    if ( data.status == 'error')
                    {
                        /**
                         * Remove checked if error
                         */
                         $("input[name='print'][value="+data.ID+"]").removeAttr('checked');

                         alertify.alert("Hanya bisa memilih maksimal 10 data.");
                    }

                    App.unblockUI();
                }
            });
        }); 
    });
});
</script>
@endpush