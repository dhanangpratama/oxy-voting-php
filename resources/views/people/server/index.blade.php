@extends('layouts.main')

@section('content')
{!! Form::open(['method' => 'GET']) !!}
<div class="clearfix m-b-15">
    <button type="button" id="print_ak24" class="btn blue pull-right" onclick="go('{{ url('export/ak24-server') }}', '_blank')">Cetak AK-24 (<span id="print_count">{{ count($print_data) }}</span>)</button>
    <h4 class="white"><strong>Data Server</strong></h4>
</div>
<div class="table">
    <table class="table">
        <thead>
            <tr>
                <th>Nama Lengkap</th>
                <th>Tanggal Lahir</th>
                <th>NIK</th>
                <th>Status</th>
                <th>Verifikasi</th>
                <th>Cetak AK-24</th>
                <th width="150">Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if ( $people['results']['page']['totalElements'] > 0 )
        {{-- */$x=0;/* --}}
        @foreach($people['results']['content'] as $item)
            {{-- */$x++;/* --}}
            <tr>
                <td>{{ ! empty($item['fullname']) ? title_case($item['fullname']) : '-' }}</td>
                <td>{{ ! empty($item['birthDate']) ? format_date($item['birthDate']) : '-' }}</td>
                <td>{{ ! empty($item['idCard']) ? $item['idCard'] : '-' }}</td>
                <td>{{ ! empty($item['status']) ? $status_data[$item['status']] : '-' }}</td>
                <td>{!! ! empty($item['isVerifiedLocal']) && $item['isVerifiedLocal'] ? '<button type="button" class="btn btn-xs green btn-block">Ya</button>' : '<i class="fa fa-times red"></i>' !!}</td>
                <td><input type="checkbox" name="print" value="{{ $item['subjectId'] }}" {{ (in_array($item['subjectId'], $print_data)) ? 'checked' : '' }} /> ({{ ! empty($item['ak24Counter']) ? $item['ak24Counter'] : 0 }}x)</td>
                <td>
                    <button type="button" onclick="go('{{ url('demographic/edit/'. Crypt::encrypt($item['id']).'/front') }}')" class="btn red btn-xs overlay" title="Ubah data"><span class="fa fa-pencil" aria-hidden="true"/></button>
                    <button type="button" onclick="go('{{ url('people/view/' .  Crypt::encrypt($item['id'])) }}')" class="btn green btn-xs overlay" title="View Person"><span class="fa fa-eye" aria-hidden="true"/></button>
                    {{--<button type="button" onclick="go('{{ url('export/ak24/' .  Crypt::encrypt($item['id'])) }}', '_blank')" class="btn blue btn-xs" title="Print AK-24"><span class="fa fa-print" aria-hidden="true"/></button>--}}
                    <button type="button" onclick="go('{{ url('export/ak23/' .  Crypt::encrypt($item['id'])) }}?dc=1', '_blank')" class="btn yellow btn-xs" title="Print AK-23"><span class="fa fa-print" aria-hidden="true"/></button>
                </td>
            </tr>
        @endforeach
        @else
        <tr>
            <td colspan="6" align="center">Data tidak ditemukan.</td>
        </tr>
        @endif
        </tbody>
    </table>
    <div class="pagination"> {!! $paginator->render() !!} </div>
</div>
{!! Form::close() !!}

<script>
    jQuery(document).ready(function($) 
    {
        $("input[name='print']").change(function() 
        {
            var action = (this.checked) ? 'add' : 'remove';

            $.ajax({
                url: "{{ url('ajax/print') }}",
                type: "POST",
                dataType: 'json',
                data: {
                    peopleID: $(this).val(),
                    action: action
                },
                beforeSend: function()
                {
                    App.blockUI();
                },
                error: function(xhr,status,error)
                {
                    console.log(error);
                },
                success: function(data) 
                {
                    //console.log(data);
                    if ( data.status == 'success')
                    {
                        /**
                         * Update count print AK-24
                         */
                         $('#print_count').html(data.count);
                    }

                    if ( data.status == 'error')
                    {
                        /**
                         * Remove checked if error
                         */
                         $("input[name='print'][value="+data.ID+"]").removeAttr('checked');

                         alertify.alert("Hanya bisa memilih maksimal 10 data.");
                    }

                    App.unblockUI();
                }
            });
        }); 
    });
</script>
@endsection
