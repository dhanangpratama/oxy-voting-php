@extends('layouts.main')
@section('content')

@include('people.server.top_toolbar')

@include('people.notification')

<div class="portlet box dark">
        @include('people.nav_server')
        <div class="portlet-body form">
            <div class="tab-content">
                <div class="tab-pane active" id="data">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row data pop-disable">
                                    <div class="col-sm-12">
                                        <div class="form-group form-md-line-input">
                                            <select name="status" class="form-control pophover infoable" disabled="disabled" id="status" tabindex="0">
                                                <option>-</option>
                                                @foreach ($status as $value)
                                                <option value="{{ $value->status_id }}" {{ ! empty(array_get($people, 'status.id')) && $value->status_id == array_get($people, 'status.id') ? 'selected="selected"' : '' }}>{{ $value->label }}</option>
                                                @endforeach
                                            </select>

                                            <label for="status">Status</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="fullname" name="fullname" value="{{ check_empty(array_get($people, 'fullname')) }}" tabindex="1" readonly="readonly">

                                            <label for="fullname">Nama</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="nickname" name="nickname" readonly="readonly" value="{{ check_empty(array_get($people, 'nickname')) }}" tabindex="2">

                                            <label for="nickname">Nama Kecil / Alias</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="job" name="job" value="{{ check_empty(array_get($people, 'job')) }}" readonly="readonly" tabindex="3">

                                            <label for="job">Pekerjaan</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control" id="sex" value="{{ sex_label(array_get($people, 'sex')) }}" readonly="readonly" tabindex="3">
                                            <label for="sex">Jenis Kelamin</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="location" name="location" readonly="readonly" value="{{ check_empty(array_get($people, 'location')) }}" tabindex="8">

                                            <label for="location">Lokasi</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <div id="datepicker--" class="input-group date">
                                                <input type="text" class="form-control infoable" data-id="datein" id="mask_date2--" readonly="readonly" name="date" value="{{ (! empty(array_get($people, 'takenDate'))) ? format_date(array_get($people, 'takenDate')) : '-' }}" tabindex="9"> 
                                                <label for="mask_date2">Tanggal Pengambilan</label>
                                                <span class="input-group-addon disabled">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="takenby" name="taken_by" readonly="readonly" value="{{ check_empty(array_get($people, 'takenBy')) }}" tabindex="10">

                                            <label for="takenby">Diambil Oleh</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="saksi" name="view_by" readonly="readonly" value="{{ check_empty(array_get($people, 'viewedBy')) }}" tabindex="11">

                                            <label for="saksi">Disaksikan Oleh</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group form-md-line-input">
                                            <textarea class="form-control infoable" rows="3" id="note" name="note" readonly="readonly" tabindex="12">{{ check_empty(array_get($people, 'note')) }}</textarea>

                                            <label for="note">Catatan</label>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="tempat_lahir" name="birth_place" value="{{ check_empty(array_get($people, 'pob')) }}" readonly="readonly" tabindex="1">

                                            <label for="tempat_lahir">Tempat Lahir</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <div id="datepicker--" class="input-group date">
                                                <input type="text" class="form-control infoable" data-id="birthdate" id="mask_date2--" name="birth_date" value="{{ (! empty(array_get($people, 'dob')) && array_get($people, 'dob') != '0000-00-00 00:00:00' ) ? format_date(array_get($people, 'dob')) : '-' }}" readonly="readonly" tabindex="2"> 
                                                <label for="mask_date2--">Tanggal Lahir</label>

                                                <span class="input-group-addon disabled">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="nationality" name="nationality" readonly="readonly" value="{{ check_empty(array_get($people, 'nationality')) }}" tabindex="3">

                                            <label for="nationality">Kebangsaan</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="race" name="race" readonly="readonly" value="{{ check_empty(array_get($people, 'race')) }}" tabindex="3">

                                            <label for="race">Suku</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'religion.label')) }}" disabled="disabled">
                                            
                                            <label for="religion">Agama</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="lastaddress" name="last_address" value="{{ check_empty(array_get($people, 'address')) }}" readonly="readonly" tabindex="5">

                                            <label for="lastaddress">Alamat</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="rt" name="rt" value="{{ check_empty(array_get($people, 'rt')) }}" readonly="readonly" tabindex="5">

                                            <label for="rt">RT</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="rw" name="rw" value="{{ check_empty(array_get($people, 'rw')) }}" readonly="readonly" tabindex="5">

                                            <label for="rw">RW</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="village" name="village" value="{{ check_empty(array_get($people, 'village')) }}" readonly="readonly" tabindex="5">

                                            <label for="village">Desa</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="regent" name="regent" value="{{ check_empty(array_get($people, 'regent')) }}" readonly="readonly" tabindex="5">

                                            <label for="regent">Kota / Kabupaten</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="province" name="province" value="{{ check_empty(array_get($people, 'province')) }}" readonly="readonly" tabindex="5">

                                            <label for="province">Propinsi</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="district" name="district" value="{{ check_empty(array_get($people, 'district')) }}" readonly="readonly" tabindex="5">

                                            <label for="district">Wilayah</label>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-4">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="country" name="country" value="{{ check_empty(array_get($people, 'country')) }}" readonly="readonly" tabindex="5">

                                            <label for="country">Negara</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="postalCode" name="postalCode" value="{{ check_empty(array_get($people, 'postalCode')) }}" readonly="readonly" tabindex="5">

                                            <label for="postalCode">Kodepos</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="idcard" name="id_card_no" name="last_address" value="{{ check_empty(array_get($people, 'nik')) }}" readonly="readonly" tabindex="6">

                                            <label for="idcard">No. KTP</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="passport" name="passport" name="last_address" value="{{ check_empty(array_get($people, 'passportNumber')) }}" readonly="readonly" tabindex="6">

                                            <label for="idcard">No. Paspor</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'education.label')) }}" disabled="disabled">

                                            <label for="education">Pendidikan Terakhir</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="father_name" name="father_name" name="last_address" value="{{ check_empty(array_get($people, 'fatherName')) }}" readonly="readonly" tabindex="8">

                                            <label for="father_name">Nama Ayah</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="father_address" name="father_address" name="last_address" value="{{ check_empty(array_get($people, 'fatherAddress')) }}" readonly="readonly" tabindex="9">

                                            <label for="father_address">Alamat Ayah</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="mother_name" name="mother_name" name="last_address" value="{{ check_empty(array_get($people, 'motherName')) }}" readonly="readonly" tabindex="10">

                                            <label for="mother_name">Nama Ibu</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="mother_address" name="mother_address" name="last_address" value="{{ check_empty(array_get($people, 'motherAddress')) }}" readonly="readonly" tabindex="11">

                                            <label for="mother_address">Alamat Ibu</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="wh_name" name="wife_husband_name" name="last_address" value="{{ check_empty(array_get($people, 'mateName')) }}" readonly="readonly" tabindex="12">

                                            <label for="wh_name">Nama Istri/Suami</label>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="wife_husband_address" name="wh_address" name="last_address" value="{{ check_empty(array_get($people, 'mateAddress')) }}" readonly="readonly" tabindex="13">

                                            <label for="wh_address">Alamat Istri/Suami</label>
                                        </div>
                                    </div>
                                    
                                    @if ( ! empty(array_get($people, 'children')) )
                                    <?php 
                                    $childs = explode(',', array_get($people, 'children'));
                                    $no = 1; ?>
                                    @foreach ($childs as $key => $child)
                                    <div class="col-sm-6">
                                        <div class="form-group form-md-line-input">
                                            <input type="text" class="form-control infoable" id="child_{{ $key+1 }}" name="childs[]" value="{{ $child }}" readonly="readonly" tabindex="16">

                                            <label for="child_{{ $key }}">Anak {{ $key+1 }}</label>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="tab-pane" id="sinyalemen">
                    <div class="form-body">
                        <div class="row">
                            <div class="col-sm-12">
                                <div style="padding-top: 5px;">
                                    <!-- Nav tabs -->
                                    <div class="row data pop-disable">
                                        <div class="col-sm-4">
                                            <div class="form-group form-md-line-input">
                                                <div class="input-group pophover">
                                                    <input type="text" class="form-control infoable" id="body_height" name="height" name="height" value="{{ check_empty(array_get($people, 'height')) }}" readonly="readonly" tabindex="14"> 
                                                    <label for="body_height">Tinggi Badan</label>

                                                    <span class="input-group-addon disabled">
                                                        CM
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-md-line-input">
                                                <div class="input-group pophover">
                                                    <input type="text" class="form-control infoable" id="body_weight" name="weight" value="{{ check_empty(array_get($people, 'weight')) }}" readonly="readonly" tabindex="15"> 
                                                    <label for="body_weight">Berat Timbangan</label>

                                                    <span class="input-group-addon disabled">
                                                        KG
                                                    </span>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'bloodType')) }}" disabled="disabled">

                                                <label for="bloodType">Golongan Darah</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'skin.label')) }}" disabled="disabled">

                                                <label for="skin_color">Warna Kulit</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'body.label')) }}" disabled="disabled">

                                                <label for="body_shape">Bentuk Tubuh</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'head.label')) }}" disabled="disabled">

                                                <label for="shape_of_head">Bentuk Kepala</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'hairColor.label')) }}" disabled="disabled">

                                                <label for="hair_color">Warna Rambut</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'hair.label')) }}" disabled="disabled">

                                                <label for="type_of_hair">Jenis Rambut</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'face.label')) }}" disabled="disabled">

                                                <label for="type_of_face">Bentuk Muka</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'forehead.label')) }}" disabled="disabled">

                                                <label for="forehead">Dahi</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'eyeColor.label')) }}" disabled="disabled">

                                                <label for="eyes_color">Warna Mata</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                    <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'eyeIrregularity.label')) }}" disabled="disabled">

                                                <label for="irregularitesoneyes">Kelainan pada mata</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'nose.label')) }}" disabled="disabled">

                                                <label for="nose">Hidung</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'lip.label')) }}" disabled="disabled">

                                                <label for="lips">Bibir</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'tooth.label')) }}" disabled="disabled">

                                                <label for="tooth">Gigi</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'chin.label')) }}" disabled="disabled">

                                                <label for="chin">Dagu</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" value="{{ check_empty(array_get($people, 'ear.label')) }}" disabled="disabled">

                                                <label for="ear">Telinga</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" id="tattoo" name="tattoo" value="{{ check_empty(array_get($people, 'tattoo')) }}" readonly="readonly" tabindex="35">

                                                <label for="tattoo">Tatoo</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" id="scars_and_handicap" name="scars_and_handicap" value="{{ check_empty(array_get($people, 'scarsAndHandicap')) }}" readonly="readonly" tabindex="35">

                                                <label for="scars_and_handicap">Dipotong dan Cacat</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-md-line-input">
                                                <input type="text" class="form-control infoable" id="provision_code" name="provision_code" value="{{ check_empty(array_get($people, 'provisionCode')) }}" readonly="readonly" tabindex="35">

                                                <label for="provision_code">Kode Pasal</label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group form-md-line-input">
                                                <textarea class="form-control infoable" rows="3" readonly="readonly" tabindex="12">{{check_empty(array_get($people, 'information')) }}</textarea>

                                                <label for="note">Keterangan</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="tab-pane" id="photo">
                    <div class="form-body">
                        <div class="row m-t-20">
                            <div class="col-sm-4 picture-box">
                                <div class="panel panel-default">
                                    <div class="panel-body alt">
                                        
                                        <img data-src="{{ array_get($people, 'baseUrlAssets').$fileNaming['face_front'] }}?{{ time() }}" style="margin: 0 auto;" class="" alt="Wajah depan">
                                        
                                    </div>
                                    <div class="panel-footer uppercase aligncenter relative">
                                        Tampak Depan
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-sm-4 picture-box">
                                <div class="panel panel-default">
                                    <div class="panel-body alt">
                                        
                                        <img data-src="{{ array_get($people, 'baseUrlAssets').$fileNaming['face_left'] }}?{{ time() }}" style="margin: 0 auto;" class="" alt="Wajah kiri">
                                        
                                    </div>
                                    <div class="panel-footer uppercase aligncenter relative">
                                        Tampak Kiri
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4 picture-box">
                                <div class="panel panel-default">
                                    <div class="panel-body alt">
                                       
                                        <img data-src="{{ array_get($people, 'baseUrlAssets').$fileNaming['face_right'] }}?{{ time() }}" style="margin: 0 auto;" class="" alt="Wajah kanan">
                                        
                                    </div>
                                    <div class="panel-footer uppercase aligncenter relative">
                                        Tampak Kanan
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="finger">
                    <div class="form-body">
                        <div class="ak-tab m-b-30 m-t-20">
                            <ul id="fingerTab" class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#roll" aria-controls="roll" role="tab" data-toggle="tab">Rolled</a></li>
                                <li role="presentation"><a href="#flat" aria-controls="flat" role="tab" data-toggle="tab">Flat</a></li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content {{ array_get($people, 'enrollmentType') }}">
                                <div role="tabpanel" class="tab-pane active" id="roll">
                                    <div class="row">
                                        <?php $no = 1; ?>
                                        @foreach (config('ak23.finger_data') as $finger => $detail)
                                        <div class="col-sm-15 <?php echo finger_search_done($finger, $finger_json['fingers']); ?>">
                                            <div class="panel panel-default">
                                                <div class="panel-body alt rolled-equal-height">
                                                    <div class="henry-label">
                                                        {{ (!empty($finger_json['fingers'][$finger]['pattern_classification'])) ? str_replace('_', ' ', $finger_json['fingers'][$finger]['pattern_classification']) : '-' }}
                                                    </div>
                                                    <div class="finger-wrapper">
                                                        {{--  <div class="text-center m-b-10">{{ $detail['num'] }}</div>  --}}
                                                       
                                                        <img class=" alt-zoom" data-src="{{ array_get($people, 'baseUrlAssets').$detail['roll_name'] }}?{{ time() }}" alt="{{ $finger }}">
                                                       
                                                    </div>
                                                </div>
                                                <div class="panel-footer uppercase aligncenter relative">
                                                    {{ $detail['title'] }}
                                                </div>
                                            </div>
                                        </div>
                                        <?php $no++ ?>
                                        @endforeach
                                    </div>  
                                </div>
                                <div role="tabpanel" class="tab-pane" id="flat">
                                    <div class="row">
                                        @foreach (config('ak23.finger_data') as $finger => $detail)
                                        <div class="col-sm-15">
                                            <div class="panel panel-default">
                                                <div class="panel-body alt flat-equal-height">
                                                <div class="finger-wrapper flat">
                                                
                                                <img class="" data-src="{{ array_get($people, 'baseUrlAssets').$detail['flat_name'] }}?{{ time() }}" alt="{{ $detail['title'] }}">
                                                
                                                </div>
                                                </div>
                                                <div class="panel-footer uppercase aligncenter relative">
                                                {{ $detail['title'] }}
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="rumus m-b-30 clearfix text-center">
                            <div class="rumus-box">
                                <div class="rumus-wrapper pophover infoable">
                                    <div class="rumus-label white">Rumus</div>
                                    <?php $formula = !empty(array_get($people, 'formula')) ? explode('|', array_get($people, 'formula')) : []; ?>
                                    @for ($i = 0; $i <= 5; $i++)
                                    <input type="text" class="" maxlength="3" readonly="readonly" autocomplete="off" id="formula_{{ $i+1 }}" name="formula[]" value="{{ old('formula[]', array_key_exists($i, $formula) ? trim($formula[$i]) : '') }}" tabindex="{{ 16+$i }}">
                                    @endfor
                                </div>
                                <div class="lrumus-wrapper pophover infoable"> 
                                    <div class="rumus-label white">Lihat Rumus</div>
                                    <?php $formula_view = !empty(array_get($people, 'formulaView')) ? explode('|', array_get($people, 'formulaView')) : []; ?>
                                    @for ($i = 0; $i <= 5; $i++)
                                    <input type="text" class="" maxlength="3" readonly="readonly" autocomplete="off" id="formula_view_{{ $i+1 }}" name="formula_view[]" value="{{ old('formula_view[]', array_key_exists($i, $formula_view) ? trim($formula_view[$i]) : '') }}" tabindex="{{ 23+$i }}">
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        $('.flat-equal-height, .rolled-equal-height').matchHeight();

        $("#mainNavTab a").click(function(e){
            e.preventDefault();
        });
    });
});
</script>
@endpush