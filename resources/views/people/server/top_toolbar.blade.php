<?php $id = Crypt::encrypt(array_get($people, 'id')); ?>

<div id="top_toolbar" class="clearfix" style="margin-bottom: 20px;">
	@if ( ! Request::get('noback') )
	<a href="{{ url()->previous() }}" class="btn red btn-lg uppercase uiblock-on"><i class="fa fa-chevron-left"></i> Kembali</a>
	@endif

	<button onclick="go('{{ url('demographic/edit/'.array_get($people, 'subjectId').'/front') }}')" class="btn green btn-lg uppercase pull-right uiblock-on" style="margin-left: 10px;"><i class="fa fa-pencil"></i> Ubah data</button>

	<button type="button" onclick="PopupWindowCenter('{{ url('export/ak23/' .  $id) }}?dc=1', '_blank', winWidth, winWidth)" class="btn blue btn-lg uppercase pull-right" title="Print AK-23"><i class="fa fa-print" aria-hidden="true"/></i> Cetak AK-23</button>
</div>