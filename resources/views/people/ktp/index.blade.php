@extends('layouts.main')
@section('content')

{!! Form::open(['method' => 'GET']) !!}
<div class="clearfix m-b-15">
    <h4 class="white uppercase"><strong>Data Server KTP</strong></h4>
</div>

<div class="portlet box dark">
    <div class="portlet-body">
        <div class="table">
            <table class="table">
                <thead>
                    <tr>
                        <th class="uppercase">NIK</th>
                        <th class="uppercase">Nama Lengkap</th>
                        <th class="uppercase">Tanggal Lahir</th>
                        <th class="uppercase">Pekerjaan</th>
                        <th class="uppercase">Kabupaten</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @if ( ! empty($peoples['result']) )
                    @if ( $sourceIndex == 'detail' )
                        @foreach($peoples['result'] as $people)
                        {{-- */$x++;/* --}}
                        <tr>
                            <td>{{ ! empty($people['NIK']) ? title_case($people['NIK']) : '-' }}</td>
                            <td>{{ ! empty($people['NAMA_LGKP']) ? title_case($people['NAMA_LGKP']) : '-' }}</td>
                            <td>{{ ! empty($people['TGL_LHR']) ? format_date($people['TGL_LHR']) : '-' }}</td>
                            <td>{{ ! empty($people['PEKERJAAN']) ? title_case(stripslashes($people['PEKERJAAN'])) : '-' }}</td>
                            <td>{{ ! empty($people['NAMA_KAB']) ? title_case($people['NAMA_KAB']) : '-' }}</td>
                            <td>
                                <a href="javascript:;" onclick="go('{{ route('people_ktp_detail', [encrypt($people['NIK'])]) }}')" class="btn blue btn-sm uiblock-on" title="View Person">Lihat</a>
                            </td>
                        </tr>
                        @endforeach
                    @else
                        <tr>
                            <td>{{ ! empty($peoples['result']['NIK']) ? title_case($peoples['result']['NIK']) : '-' }}</td>
                            <td>{{ ! empty($peoples['result']['Name']) ? title_case($peoples['result']['Name']) : '-' }}</td>
                            <td>{{ ! empty($peoples['result']['DOB']) ? format_date($peoples['result']['DOB']) : '-' }}</td>
                            <td>{{ ! empty($peoples['result']['Job']) ? title_case(stripslashes($peoples['result']['Job'])) : '-' }}</td>
                            <td>{{ ! empty($peoples['result']['Kabupaten']) ? title_case($peoples['result']['Kabupaten']) : '-' }}</td>
                            <td>
                                <a href="javascript:;" onclick="go('{{ route('people_ktp_detail', [encrypt($peoples['result']['NIK'])]) }}')" class="btn blue btn-sm uiblock-on" title="View Person">Lihat</a>
                            </td>
                        </tr>
                    @endif
                @else
                    <tr>
                        <td class="text-center" colspan="6">Data tidak ditemukan</td>
                    </tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>

{!! Form::close() !!}
@endsection

@push('js')
<script>
head.ready(function() {
    jQuery(document).ready(function($) 
    {
        /*$('#print_ak24').on('click', function(event) {
            setTimeout(function() {
                location.reload();
            }, 1250);
            
        });*/

        $("input[name='print']").change(function() 
        {
            var action = (this.checked) ? 'add' : 'remove';

            $.ajax({
                url: "{{ url('ajax/print') }}",
                type: "POST",
                dataType: 'json',
                data: {
                    peopleID: $(this).val(),
                    action: action
                },
                beforeSend: function()
                {
                    App.blockUI();
                },
                error: function(xhr,status,error)
                {
                    console.log(error);
                },
                success: function(data) 
                {
                    //console.log(data);
                    if ( data.status == 'success')
                    {
                        /**
                         * Update count print AK-24
                         */
                         $('#print_count').html(data.count);

                         if ( data.count > 0 )
                         {
                             $('#ak24_clear').removeClass('hide');
                         }
                         else
                         {
                             $('#ak24_clear').addClass('hide');
                         }
                    }

                    if ( data.status == 'error')
                    {
                        /**
                         * Remove checked if error
                         */
                         $("input[name='print'][value="+data.ID+"]").removeAttr('checked');

                         alertify.alert("Hanya bisa memilih maksimal 10 data.");
                    }

                    App.unblockUI();
                }
            });
        }); 
    });
});
</script>
@endpush