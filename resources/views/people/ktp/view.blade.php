@extends('layouts.main')
@section('content')

@if ( ! empty($people['result']) )

@if ( config('ak23.access') == 'desktop' )
<a href="{{ route('create_finger_scan_process', ['get_data' => 1]) }}" class="btn red btn-lg uppercase m-b-20 uiblock-on pull-right"><i class="fa fa-plus m-r-15"></i> Tambah ke AK-23</a>
@endif
<a href="{{ url()->previous() }}" class="btn blue btn-lg uppercase m-b-20 uiblock-on"><i class="fa fa-angle-left m-r-15"></i> Kembali</a>

<div class="portlet light portlet-fit">
	<div class="portlet-title">
		<div class="caption">
            <span class="caption-subject sbold uppercase"><i class="icon-user"></i> &nbsp; {{ !empty($people['result']['Name']) ? $people['result']['Name'] : $candidatesData[$key]['full_name'] }}</span>
        </div>
	</div>
	<div class="portlet-body">
		<div class="row">
		    <div class="col-md-12">
				<div class="content">
		            <div class="row">
		                <div class="col-md-9">
		                	<table class="table laten-data">
		                		<tr>
		                			<th width="250" class="">N.I.K.</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['NIK']) ? $people['result']['NIK'] : '-' }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">KARTU KK</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['Kartukk']) ? $people['result']['Kartukk'] : '-' }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">JENIS KELAMIN</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['Sex']) ? $people['result']['Sex'] : '-' }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">GOLONGAN DARAH</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['BloodType']) ? $people['result']['BloodType'] : '-' }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">STATUS KAWIN</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['Marital']) ? $people['result']['Marital'] : '-' }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">AGAMA</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['Religion']) ? $people['result']['Religion'] : '-' }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">TTL</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['POB']) ? $people['result']['POB'] . ', ' : '' }}{{ !empty($people['result']['DOB']) ? title_case($people['result']['DOB']) : '-' }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">PEKERJAAN</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['Job']) ? $people['result']['Job'] : '-' }}</td>
		                		</tr>
		                		<tr>
		                			<th width="250" class="">ALAMAT</th> 
									<td width="20">:</td>
		                			<td>
		                				{{ ! empty($alamat) ? trim($alamat) : '-' }}
		                			</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">NAMA IBU</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['Ibu']) ? $people['result']['Ibu'] : '-' }}</td>
		                		</tr>
								<tr>
		                			<th width="250" class="">NAMA AYAH</th> 
									<td width="20">:</td>
		                			<td>{{ !empty($people['result']['Ayah']) ? $people['result']['Ayah'] : '-' }}</td>
		                		</tr>
		                	</table>
		                </div>
						<div class="col-md-3">
							@if (! empty($people['result']['Photo']))
							<div class="panel panel-default">
								<div class="panel-body alt">
									<img src="data:image/jpg;base64, {{ $people['result']['Photo'] }}" class="img-responsive">
								</div>
								<div class="panel-footer uppercase aligncenter relative">
									Foto
								</div>
							</div>
							@else
							<img src="{{ asset("assets/images/no-photo.png") }}?{{ time() }}" class="img-responsive">
							@endif

							@if ( ! empty($people['result']['signature']) )
	                		<img src="data:image/png;base64, {{ $people['result']['signature'] }}" class="img-responsive img-signature" style="margin-top:10px;">
	                		@endif
	                	</div>
		            </div>
		        </div>
		    </div> 
		</div>
	</div>
</div>


<div class="holder-inner live">
	<div class="row gutter-10" style="margin-bottom: 10px;">
		@foreach(config('ak23.os_inafis_finger_key') as $finger)
			<div class="col-sm-15 equal-height">
				<div class="panel panel-default">
					<div class="panel-body flat-equal-height">
						<div class="finger-wrapper flat">
							@if ( ! empty($people['result'][$finger['alt']]) )
							<a class="image-popup" title="{{ title_case(str_replace('_', ' ', $finger['alt'])) }}" href="data:image/jpg;base64, {{ $people['result'][$finger['alt']] }}">
								<img src="data:image/jpg;base64, {{ $people['result'][$finger['alt']] }}" class="aligncenter img-responsive" alt="">
							</a>
							@else
							<a href="javascript:;">
								<img src="{{ asset("assets/images/disabled.png") }}" class="aligncenter img-responsive" alt="">
							</a>
							@endif
						</div>
					</div>
					<div class="panel-footer uppercase aligncenter relative">
						{{ str_replace('_', ' ', $finger['alt']) }}
					</div>
				</div>
				
			</div>
		@endforeach
	</div>
</div>

@endif

@endsection