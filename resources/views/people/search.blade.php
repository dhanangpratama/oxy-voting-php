@extends('layouts.main')

@section('content')

{!! Form::open(['method'=>'GET', 'url' => 'people', 'role'=>'search']) !!}

@if ( config('ak23.access') == 'desktop' )
@include('layouts.server_alert')

@include('layouts.osinafis_alert')
@endif

<div class="portlet box dark">
    <div class="portlet-title">
        <div class="caption">
            Pencarian
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-sm-6">
                <div class="form-group form-md-line-input ">
                    <select name="source" class="form-control pophover infoable" id="source" tabindex="4">
                        <option id="op_ak23" value="AK23">AK-23</option>
                        <option id="op_ktp" value="EKTP">KTP-el</option>
                        <option id="op_drm" value="DERM">Dermalog</option>
                    </select>
                    {{ Form::label('source', 'Pilih Database') }}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group form-md-line-input ">
                    {{ Form::text('FullName', '', ['id' => 'FullName', 'tabindex' => 1, 'class' => 'form-control']) }}
                    {{ Form::label('FullName', 'Nama') }}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group form-md-line-input ">
                    {{ Form::text('pob', '', ['id' => 'pob', 'tabindex' => 2,'class' => 'form-control']) }} 
                    {{ Form::label('pob', 'Tempat Lahir') }}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group form-md-line-input ">
                    <div class="input-group">
                        {{ Form::text('DateOfBirth', '', ['id' => 'DateOfBirth', 'tabindex' => 2,'class' => 'form-control date-mask', 'tabindex' => 2]) }}
                        {{ Form::label('DateOfBirth', 'Tanggal Lahir') }}
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group form-md-line-input ">
                    {{ Form::text('IDCard', '', ['id' => 'IDCard', 'tabindex' => 3,'class' => 'form-control']) }} 
                    {{ Form::label('IDCard', 'NIK') }}
                </div>
            </div>

            <div class="col-sm-6">
                <div class="form-group form-md-line-input ">
                    <select name="status" class="form-control pophover infoable" id="status" tabindex="4">
                        <option value="">-- Pilih status --</option>
                        @foreach ($status as $key => $value)
                        <option value="{{ $key }}">{{ $value }}</option>
                        @endforeach
                    </select>
                    {{ Form::label('status', 'Status') }}
                </div>
            </div>

            {{--  <div class="col-sm-6">
                <a id="viewAll" href="{{ url('people') }}" class="btn green btn-block uiblock-on" tabindex="7">Lihat semua data</a>
            </div>  --}}
        </div>
    </div>
</div>

<div class="text-center m-b-30">
    <button type="submit" id="btnSearch" class="btn blue btn-lg uppercase" tabindex="6"><i class="fa fa-search"></i> Cari</button>
</div>

{!! Form::close() !!}

@endsection

@push('js')
@if ( config('ak23.access') == 'desktop' )
<script>
head.ready(function() {
    jQuery(document).ready(function($) {
        var eKtp = $('#searchEKtp');
        var name = $('#FullName');
        var dob = $('#DateOfBirth');
        var id_card = $('#IDCard');
        var pob = $('#pob');

        BioverKtpel.Init({
            OnDataReceived: function(ktp){
                /**
                 * Available KTP Field Name:
                 * Nik, Name, Address, Neighbourhood, CommunityAssociation, District, Village,
                 * Gender, BloodType, Religion, MarriageStatus, Occupation, PlaceOfBirth,
                 * DateOfBirth, Province, City, DateOfExpiry, Nationality;
                 */

                name.val(ktp.Name);
                dob.val(ktp.DateOfBirth);
                id_card.val(ktp.Nik);
                pob.val(ktp.PlaceOfBirth);
            },
            OnStartWait: function(){ App.blockUI(); },
            OnStopWait: function() { App.unblockUI(); }
        });


        eKtp.click(function(e) {
            BioverKtpel.ReadKtpEl();
        });

        //Autoread on load, in case there is ktp on device
        BioverKtpel.ReadKtpEl();
    });
});
</script>
@endif

<script>
function disabledInput()
{
    var val = $('#source').val();
    $('#status, #IDCard').prop('disabled', false);

    if ( val == 'AK23')
    {
        $('#viewAll').attr('href', home_url + '/people');
    }
    else if ( val == 'EKTP' )
    {
        $('#status').val('');
        $('#status').prop('disabled', true);
    }
    else
    {
        $('#IDCard, #status').val('');
        $('#status, #IDCard').prop('disabled', true);
        $('#viewAll').attr('href', home_url + '/people?source=dermalog');
    }
}

head.ready(function() {
    jQuery(document).ready(function($) {
        $('#source').change(function() {
            disabledInput();
        });

        disabledInput();

        $('#btnSearch').click(function(e) {
            e.preventDefault();

            var isValid = false;

            $("form input[type=text], form select#status").each(function(){
                var input = $(this); // This is the jquery object of the input, do what you will

                if (input.val().length != 0)
                {
                    isValid = true;
                }
            });

            if ( isValid )
            {
                App.blockUI();
                $('form').submit();
            }
            else
            {
                new Noty({
                    type: 'error',
                    layout: 'topRight',
                    theme: 'metroui',
                    text: 'Mohon untuk mengisi salah satu kriteria pencarian',
                    timeout: 8000,
                    progressBar: true,
                    closeWith: ['button'],
                    animation: {
                        open: 'noty_effects_open',
                        close: 'noty_effects_close'
                    }
                }).show();
            }
        });
    });
});
</script>
@endpush
