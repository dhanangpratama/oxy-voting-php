@extends('layouts.main')
@section('content')

<div class="dashboard">
    @if ($activityLogs->count() > 0)
    <div class="log-ticker">
        <div class="panel panel-default">
            <div class="panel-body transparent">
                <ul class="logticker">
                    @foreach($activityLogs as $log)
                    <li>
                        <div class="desc">
                        {{ date('d-m-Y H:i', strtotime($log->created_at)) }} &#8212; {{ $log->user->name }}<br>
                        {{ $log->description }}</div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="panel-footer uppercase aligncenter relative">
                Aktifitas Kegiatan {!! config('ak23.access') == 'desktop' ? '"' . $option->get('com.oxycreative.inafis.ak23.core.models.WorkstationEntity.name') . '"' : '' !!}
            </div>
        </div>
    </div>
    @endif
    
    <div class="clearfix">
        <div id="chart_2" class="chart"></div>
    </div>
    <div class="tiles">          
        <a href="{{ route('report') }}" class="tile bg-green uiblock-on">
            <div class="tile-body">
                <i class="fa fa-bar-chart-o"></i>
            </div>
            <div class="tile-object">
                <div class="name"> Laporan </div>
                <div class="number"> </div>
            </div>
        </a>
        <a href="{{ route('people.search') }}" class="tile bg-blue uiblock-on">
            <div class="tile-body">
                <i class="fa fa-search"></i>
            </div>
            <div class="tile-object">
                <div class="name"> Pencarian </div>
                <div class="number"> </div>
            </div>
        </a>

        @if ( config('ak23.access') == 'desktop' )
        <a href="{{ route('create_finger_scan_process') }}" class="tile bg-red uiblock-on">
            <div class="tile-body">
                <i class="fa fa-user"></i>
            </div>
            <div class="tile-object">
                <div class="name"> Live Scan </div>
                <div class="number"> </div>
            </div>
        </a>
        <a href="{{ route('create_scan_card_process') }}" class="tile bg-yellow uiblock-on">
            <div class="tile-body">
                <i class="icon-docs"></i>
            </div>
            <div class="tile-object">
                <div class="name"> Card Scan </div>
                <div class="number"> </div>
            </div>
        </a>
        @else
        <a href="{{ route('users') }}" class="tile bg-red uiblock-on">
            <div class="tile-body">
                <i class="fa fa-user"></i>
            </div>
            <div class="tile-object">
                <div class="name"> Pengguna </div>
                <div class="number"> </div>
            </div>
        </a>
        <a href="{{ route('message') }}" class="tile bg-yellow uiblock-on">
            <div class="tile-body">
                <i class="fa fa-comments-o"></i>
            </div>
            <div class="tile-object">
                <div class="name"> Pesan Broadcast </div>
                <div class="number"> </div>
            </div>
        </a>
        @endif
    </div>
</div>

@endsection

@push('js')
<script>
head.ready(function() {
    var ChartsFlotcharts = function() {

    return {
        //main function to initiate the module

        initCharts: function() {

            if (!jQuery.plot) {
                return;
            }
                
            //Interactive Chart

            function chart2() {
                if ($('#chart_2').size() != 1) {
                    return;
                }

                function randValue() {
                    return (Math.floor(Math.random() * (1 + 40 - 20))) + 20;
                }
                var livescan = {!! $dataChart['live'] !!};

                var cardscan = {!! $dataChart['card'] !!};

                var plot = $.plot($("#chart_2"), [{
                    data: livescan,
                    label: "Live Scan",
                    lines: {
                        lineWidth: 2,
                    },
                    shadowSize: 0

                }, {
                    data: cardscan,
                    label: "Card Scan",
                    lines: {
                        lineWidth: 2,
                    },
                    shadowSize: 0
                }], {
                    series: {
                        lines: {
                            show: true,
                            lineWidth: 2,
                            fill: true,
                            fillColor: {
                                colors: [{
                                    opacity: 0.05
                                }, {
                                    opacity: 0.01
                                }]
                            }
                        },
                        points: {
                            show: true,
                            radius: 3,
                            lineWidth: 1
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#444D58",
                        borderColor: "#444D58",
                        borderWidth: 1,
                        margin: {
                            top: 0,
                            left: 0,
                            bottom: 20,
                            right: 30
                        }
                    },
                    colors: ["#e73d4a", "#0376B8"],
                    xaxis: {
                        ticks: {!! $dataChart['label'] !!},
                        tickDecimals: 0,
                        tickColor: "#444D58",
                    },
                    yaxis: {
                        ticks: 11,
                        tickDecimals: 0,
                        tickColor: "#444D58",
                    }
                });


                function showTooltip(x, y, contents) {
                    $('<div id="tooltip">' + contents + '</div>').css({
                        position: 'absolute',
                        display: 'none',
                        top: y + 5,
                        left: x + 15,
                        border: '1px solid #333',
                        padding: '4px',
                        color: '#fff',
                        'border-radius': '3px',
                        'background-color': '#333',
                        'z-index': 999,
                        'font-size': '12px'
                    }).appendTo("body").fadeIn(200);
                }

                var previousPoint = null;
                $("#chart_2").bind("plothover", function(event, pos, item) {
                    $("#x").text(pos.x.toFixed(2));
                    $("#y").text(pos.y.toFixed(2));

                    if (item) {
                        if (previousPoint != item.dataIndex) {
                            previousPoint = item.dataIndex;

                            $("#tooltip").remove();
                            var x = item.datapoint[0].toFixed(0),
                                y = item.datapoint[1].toFixed(0);

                            showTooltip(item.pageX, item.pageY, " tanggal " + item.series.xaxis.ticks[x].label + " ada " + y + ' data');
                        }
                    } else {
                        $("#tooltip").remove();
                        previousPoint = null;
                    }
                });
            }

            chart2();

        }
    };
}();

jQuery(document).ready(function() {    
    ChartsFlotcharts.initCharts();

    $('.logticker').newsTicker({
        row_height: 80,
        max_rows: 5
    });
});
});
</script>
@endpush